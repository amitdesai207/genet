// JavaScript Document



	var _node_Schema=new Array();
	var _edge_Schema=new Array();
	//String[] node_Schema, String[] edge_Schema, ArrayList<ArrayList<String>> node_Data, ArrayList<ArrayList<String>> edge_Data
	function toCytoScape(node_Schema,edge_Schema,node_Data,edge_Data)
	{
		var cyto="";
		cyto="{"+dataSchema(nodeSchema(node_Schema),edgeSchema(edge_Schema))+","+graphData(nodeData(node_Data),edgeData(edge_Data))+"}";
		return cyto;
	}
	//String node_data, String edge_data
	function graphData(node_data, edge_data)
	{
		var data="\"data\":{}";
		if(node_data!="" && edge_data!="")
		{
			data="\"data\":{"+node_data+","+edge_data+"}";
		}
		else
		{
			if(node_data!="")
			{
				data="\"data\":{"+node_data+"}";
			}
			else
			{
				if(edge_data!="")
				{
					data="\"data\":{"+edge_data+"}";
				}
			}
		}
		return data;	
	}
	//ArrayList<ArrayList<String>> data
	function nodeData(data)
	{
		//data should be in this format:  data[i][0]=id  , data[i][j]=associated value to _nodeSchema[j]
		var nodes="";
		if(data.length>0)
		{
			
			nodes="\"nodes\":[";
			//alert("nodes!!:"+data);
			for(var i=0; i<data.length;i++)
			{
				nodes+="{\"id\":\""+data[i][0]+"\"";
				for(var j=0; j<_node_Schema.length;j++)
				{	
					nodes+=",\""+_node_Schema[j]+"\":\""+data[i][j+1]+"\"";
				}
				nodes+="}";

				if((i+1)<data.length)
				{
					nodes+=",";
				}
			}
			nodes+="]";
		}
		return nodes;
	}
	//ArrayList<ArrayList<String>> data
	function edgeData(data)
	{
		//data: {nodes: [ { id: "1" }, { id: "2" } ],edges: [ { id: "2to1", target: "1", source: "2" } ]}
		//data should be in this format:  data[i][0]=id  , data[i][j]=associated value to _nodeSchema[j]
		var edges="";
		if(data.length>0)
		{
			edges="\"edges\":[";
			for(var i=0; i<data.length;i++)
			{
				edges+="{\"id\":\""+data[i][0]+"\","+"\"target\":\""+data[i][1]+"\","+"\"source\":\""+data[i][2]+"\"";
				for(var j=0; j<_edge_Schema.length;j++)
				{
					
					edges+=",\""+_edge_Schema[j]+"\":\""+data[i][j+3]+"\"";
				}
				edges+="}";

				if((i+1)<data.length)
				{
					edges+=",";
				}
			}
			edges+="]";
		}
		return edges;
	}
		

	
	//this function create an string like this one: "dataSchema":{"nodes":[{"name":"label","type":"string"}],"edges":[{"name":"weight","type":"string"}]}
	//String node_Schema, String edge_Schema
	function dataSchema(node_Schema, edge_Schema)
	{
		var schema="";
		if(node_Schema!="" && edge_Schema!="")
		{
			schema="\"dataSchema\":{"+node_Schema+","+edge_Schema+"}";
		}
		else
		{
			if(node_Schema!="")
			{
				schema="\"dataSchema\":{"+node_Schema+"}";
			}
			else
			{
				if(edge_Schema!="")
				{
					schema="\"dataSchema\":{"+edge_Schema+"}";
				}
			}
		}
		return schema;	
	}
	// data is an array of data's names ()
	//this function create a string like this:>>"nodes": [ { "name": "label", "type": "string" }]<<
	//String[] data
	function nodeSchema(data)
	{
		var nodes="";
		if(data.length>0)
		{
			_node_Schema=data;
			nodes="\"nodes\":[";
			for(var i=0; i<data.length;i++)
			{
				nodes+="{\"name\":\""+data[i]+"\",\"type\":\"string\"}";
				if((i+1)<data.length)
				{
					nodes+=",";
				}
			}
			nodes+="]";
		}
		return nodes;
	}
	// data is an array of data's names ()
	//this function create a string like this:>>"edges": [ { "name": "weight", "type": "string" }]<<
	//String[] data
	function edgeSchema(data)
	{
		var edges="";
		if(data.length>0)
		{
			_edge_Schema=data;
			edges="\"edges\":[";
			for(var i=0; i<data.length;i++)
			{
				edges+="{\"name\":\""+data[i]+"\",\"type\":\"string\"}";
				if((i+1)<data.length)
				{
					edges+=",";
				}
			}
			edges+="]";
		}
		return edges;
	}
//========================================================visual_style=================================================================	

	//String[] node_Schema, String[] edge_Schema, ArrayList<ArrayList<String>> node_Data, ArrayList<ArrayList<String>> edge_Data,ArrayList<ArrayList<String>> genesColor
	function networkOption(node_Schema,edge_Schema, node_Data, edge_Data,genesColor)
	{
		var returnStr="";
		returnStr+="{\"network\":\""+toCytoScape(node_Schema, edge_Schema, node_Data, edge_Data)+"\",\"visualStyle\":\""+nodeStyle(genesColor)+"\"}";
		return returnStr;
	}
	//ArrayList<ArrayList<String>> geneColor
	function networkStyle(geneColor)
	{
		var retunStr="";
		retunStr+="{"+nodeStyle(geneColor)+","+edgeStyle()+"}";
		return retunStr;
		
	}
	
	//ArrayList<ArrayList<String>> genesColor
	function nodeStyle(genesColor)
	{
		var returnStr="\"nodes\":{";
		//add  border width, border color
		returnStr+="\"borderWidth\":"+"\"1\",";
		returnStr+="\"borderColor\":"+"\"#e5e5e5\",";
		//add nodeSize
		returnStr+="\"size\":"+"\"25\",";
		returnStr+="\"labelYOffset\":"+"\"-25\",";
		returnStr+="\"opacity\":"+"\"0.99\",";
		 
		//add color based on gene module
		returnStr+="\"color\":{\"discreteMapper\":{\"attrName\":\"id\",\"entries\":[";
		for(var i=0;i<genesColor.length;i++)
		{
			returnStr+="{\"attrValue\":\""+genesColor[i][0]+"\",\"value\":\""+genesColor[i][1]+"\"}";
			if(i<genesColor.length-1)
			{
				returnStr+=",";
			}
		}
		returnStr+="]}}";
		//close the node style
		returnStr+="}";
		
		return returnStr;
	}
	function edgeStyle()
	{
		/*{
                            defaultValue: 25,
                            continuousMapper: { attrName: "weight", minValue: 25, maxValue: 75 }
                        }*/
		var returnStr="\"edges\":{";
		//add   width, color
		returnStr+="\"width\":"+"{\"defaultValue\":2,\"continuousMapper\":{\"attrName\":\"weight\",\"minValue\":\"2\",\"maxValue\":\"40\",\"minAttrValue\":\"0.08\",\"maxAttrValue\":\"1.0\"}},";
		returnStr+="\"color\":"+"\"#c1c1c1\"";
		//close edge style
		returnStr+="}";
		return returnStr;
	}
	
//=================================================================================================================
function isExist(key,arr)
{
	for(var i=0; i<arr.length;i++)
	{
		if(arr[i][0]==key)
		{
			
			return i;
		}
	}
	return -1;
}
//input:String[][] data -- output:ArrayList<String[][]>
function inputNetworkConstructorForModule(nodeData,edgeData)
	{
		//nodes only have id and label
		var nodes=new Array();
		//edges are assumed to have id, source, target, weight, correlation
		var edges=new Array();
		var counter=0;
		for(var i=0;i<nodeData.length;i++)
		{
				var temp=new Array();
				//node id
				temp[0]=nodeData[i];
				//node label
				temp[1]=nodeData[i];
				
				nodes[i]=temp;
		}
		for(var i=0; i<edgeData.length;i++)
		{
			// the value of id and label are set to be equal
			//alert("key1:"+data[i][0]+"--key2:"+data[i][1]+"--arr:"+nodes);
			
			var temp=new Array();
			//edge id
			//temp.add(data[i][0]+"to"+data[i][1]);
			temp[0]=edgeData[i][0]+"to"+edgeData[i][1];
			//edge source
			temp[1]=edgeData[i][0]
			//edge target
			temp[2]=edgeData[i][1];
			//edge weight
			temp[3]=edgeData[i][2];
			//edge correlation
			temp[4]=edgeData[i][3];
			edges[i]=temp;
		}
		//alert("!!!:"+nodes);
		var returnResult=new Array();
		returnResult[0]=nodes;
		returnResult[1]=edges;
		return returnResult;
	}

function inputNetworkConstructor(data)
	{
		//nodes only have id and label
		var nodes=new Array();
		//edges are assumed to have id, source, target, weight, correlation
		var edges=new Array();
		var counter=0;
		for(var i=0; i<data.length;i++)
		{
			// the value of id and label are set to be equal
			//alert("key1:"+data[i][0]+"--key2:"+data[i][1]+"--arr:"+nodes);
			if(isExist(data[i][0],nodes)<0)
			{
				var temp=new Array();
				//node id
				temp[0]=data[i][0];
				//node label
				temp[1]=data[i][0];
				
				nodes[counter]=temp;
				
				counter++;
			}
			if(isExist(data[i][1],nodes)<0)
			{
				var temp=new Array();
				//node id
				temp[0]=data[i][1];
				//node label
				temp[1]=data[i][1];
				//nodes.add(temp);
				nodes[counter]=temp;
				counter++;
			}
			var temp=new Array();
			//edge id
			//temp.add(data[i][0]+"to"+data[i][1]);
			temp[0]=data[i][0]+"to"+data[i][1];
			//edge source
			//temp.add(data[i][0]);
			temp[1]=data[i][0]
			//edge target
			//temp.add(data[i][1]);
			temp[2]=data[i][1];
			//edge weight
			//temp.add(data[i][2]);
			temp[3]=data[i][2];
			//edge correlation
			//temp.add(data[i][3]);
			temp[4]=data[i][3];
			edges[i]=temp;
		}
		
		var returnResult=new Array();
		returnResult[0]=nodes;
		returnResult[1]=edges;
		return returnResult;
	}
	/*String[][] ArrayListToArray(ArrayList<ArrayList<String>> data)
	{
		String[][] returnData=new String[data.size()][data.get(0).size()];
		for(int i=0; i<data.size();i++)
		{
			for(int j=0; j<data.get(0).size(); j++)
			{
				returnData[i][j]=data.get(i).get(j);
			}
		}
		return returnData;
	}*/

