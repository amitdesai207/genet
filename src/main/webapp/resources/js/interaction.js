function initializeDocument() {

	$(function() {
		$('.no-content').remove();
		$('#page-contents').show();
		$( document ).tooltip();
		
		if ($("#genesTable")[0] != undefined) {
			$("#genesTable").bootgrid({
				rowCount : 10,
				columnSelection : false,
				caseSensitive : false
			}).on(
					"click.rs.jquery.bootgrid",
					function(e, columns, row) {
						var symbol = row["symbol"];
						var geneName = document.getElementById("valueHidden");
						geneName.value = symbol;
						geneName.innerHTML = symbol;
						// this.form.target='_blank';return true;
						var geneSubmitForm = document
								.getElementById("geneSubmitHidden");
						geneSubmitForm.target = '_blank';
						geneSubmitForm.submit();
						$.blockUI({
							css : {
								border : 'none',
								padding : '15px',
								backgroundColor : '#000',
								'-webkit-border-radius' : '10px',
								'-moz-border-radius' : '10px',
								opacity : .5,
								color : '#fff',
								'z-index': 100
							}
						});
					});
		}

		$("#dialog-message").dialog({
			autoOpen : false,
			modal : true,
			buttons : {
				Ok : function() {
					$(this).dialog("close");
				}
			}
		});

		$("#dialog-message1").dialog({
			autoOpen : false,
			modal : true,
			buttons : {
				Ok : function() {
					$(this).dialog("close");
				}
			}
		});
		
		$("#dialog-message2").dialog({
			autoOpen : false,
			modal : true,
			buttons : {
				Ok : function() {
					$(this).dialog("close");
				}
			}
		});

		$("#dialog-message3").dialog({
			autoOpen : false,
			modal : true			
		});
		
		if ($("#slider")) {
			$("#slider").slider({
				value : $("#noOfGenes").val(),
				min : 20,
				max : 100,
				step : 10,
				stop : function(event, ui) {
					$("#noOfGenes").val(ui.value);
					$.blockUI({
						css : {
							border : 'none',
							padding : '15px',
							backgroundColor : '#000',
							'-webkit-border-radius' : '10px',
							'-moz-border-radius' : '10px',
							opacity : .5,
							color : '#fff',
							'z-index': 100
						}
					});
					loadMoreGenes(ui.value);
				}
			});
		}

		if ($("#geneSlider")) {
			if ($("#noOfGenes").val() > 20) {
				window.scrollTo(0, 1000);
			}
			$("#geneSlider").slider({
				value : $("#noOfGenes").val(),
				min : 20,
				max : 100,
				step : 10,
				stop : function(event, ui) {
					$("#noOfGenes").val(ui.value);
					$.blockUI({
						css : {
							border : 'none',
							padding : '15px',
							backgroundColor : '#000',
							'-webkit-border-radius' : '10px',
							'-moz-border-radius' : '10px',
							opacity : .5,
							color : '#fff',
							'z-index': 100
						}
					});
					$('<input>').attr({
						type : 'hidden',
						id : 'noOfGenes',
						name : 'noOfGenes'
					}).val(ui.value).appendTo('#geneSubmit');
					$('#geneValue').val($('#symbolValue').html());
					$('#geneSubmit').submit();
				}
			});
		}

		$("text[id^='lineText']").tooltip();

		var positivenumbercombo = $(".positivenumbercombo");
		for (var i = 1; i <= 10; i++) {
			positivenumbercombo.append($('<option></option>').val(i).html(i))
		}
		$('.positivenumbercombo option[value="4"]').attr("selected",true);

	});

	$(".positivenumbercombo").change(function() {
		setGraphLimits($(".positivenumbercombo").val());
		update();
	});

	$("#moduleCategory").change(function() {
		var val = $("#moduleCategory option:selected").val();
		$('#downloadCategory option').filter(function () { return $(this).html() == val; }).attr('selected', 'selected');
	});
	
	moduleBTNClicked();
	$('.goBTN').css('cursor', 'pointer');
	$("#moduleGoBTN").click(function(event) {
		if ($("#moduleCategory")[0].selectedIndex == 0) {
			$("#dialog-message").dialog("open");
			return false;
		}
		document.getElementById("moduleSubmit").submit();
		$.blockUI({
			css : {
				border : 'none',
				padding : '15px',
				backgroundColor : '#000',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#fff',
				'z-index': 100
			}
		});
	});

	$("#downloadGoBTN").click(function(event) {
		if ($("#downloadCategory")[0].selectedIndex == 0) {
			$("#dialog-message").dialog("open");
			return false;
		}
		document.getElementById("downloadSubmit").submit();
		$.blockUI({
			css : {
				border : 'none',
				padding : '15px',
				backgroundColor : '#000',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#fff',
				'z-index': 100
			}
		});
	});
	
	$("#geneGoBTN").click(function(event) {
		if ($("#geneCategory")[0].selectedIndex == 0) {
			$("#dialog-message").dialog("open");
			return false;
		}
		if (!$.trim($("#geneValue").val()).length) {
			$("#dialog-message1").dialog("open");
			return false;
		}
		var geneName = $('#geneValue').val();
		geneName = geneName.split(" |")[0];
		var proceed = false;
		$('#genedatalist option').each(function() {
			if($(this).val().split(" |")[0]===geneName)
				proceed = true;
		});
		if(!proceed){			
			event.preventDefault();
			event.stopPropagation();
			$("#dialog-message2").dialog("open");
		} else {
			$('#geneValue').val(geneName);
			document.getElementById("geneSubmit").submit();
			$.blockUI({
				css : {
					border : 'none',
					padding : '15px',
					backgroundColor : '#000',
					'-webkit-border-radius' : '10px',
					'-moz-border-radius' : '10px',
					opacity : .5,
					color : '#fff',
					'z-index': 100
				}
			});
		}
	});

	$("#pathwayGoBTN").click(function(event) {
		if ($("#pathwayCategory")[0].selectedIndex == 0) {
			$("#dialog-message").dialog("open");
			return false;
		}
		if (!$.trim($("#pathwayValue").val()).length) {
			$("#dialog-message1").dialog("open");
			return false;
		}
		document.getElementById("pathwaySubmit").submit();
		$.blockUI({
			css : {
				border : 'none',
				padding : '15px',
				backgroundColor : '#000',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#fff',
				'z-index': 100
			}
		});
	});

	loadOrganisms();
	$('.goBTN').css('cursor', 'pointer');

};

function loadOrganisms() {
	$.ajax({
		type : "get",
		url : "/genet/organismList",
		cache : false,
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		mimeType : 'application/json',
		success : function(response) {
			$.each(response, function(index) {
				var option = new Option(response[index].name);
				$(option).html(response[index].name);
				if(response[index].selected == true)
					$(option).prop('selected', true);
				$('[name="category"]').append(option);
			});
			$('[name="category"]').val($('#category').val());
		},
		error : function(response) {
			alert('Error while fetching organisms');

		}
	});
}

function loadMoreGenes(noOfGenes) {
	var urlOfGenes = window.location.href;
	var urlArrayOfGenes = urlOfGenes.split("/genet");
	if (urlArrayOfGenes.length == 2)
		urlOfGenes = urlArrayOfGenes[1];
	else
		return;
	if (urlOfGenes.indexOf("geneValue") == -1)
		urlOfGenes = urlOfGenes + "&geneValue=" + noOfGenes;
	else {
		urlOfGenes = urlOfGenes.substr(0, urlOfGenes.indexOf("geneValue"))
				+ "&geneValue=" + noOfGenes;
	}
	$("body").load("/genet" + urlOfGenes, '', function(response) {
		$.unblockUI();
		initializeDocument();
		initModuleAccordian();
	});
}

function initModuleAccordian() {
	btnDIVWidth = 120;
	var difference = (110 - btnDIVWidth) / 2;
	btnDIVWidth = 110;
	var destination = btnDIVWidth / 2 + 8;
	var prevLeft = parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left", (prevLeft + difference) + "px");
	$("#topArrow").animate({
		"left" : destination + "px"
	}, "slow");
	$("#geneSearchForm").css("display", "none");
	$("#pathwaySearchForm").css("display", "none");
	// =========================================================
	// store the initial height of the div containers in the assoiciated
	// variables. Note that this height must be set directly in the html not in
	// the css file.
	graphContainer_height = document.getElementById('graphContent').style.height;

	expreContainer_height = document.getElementById('expreContent').style.height;
	annotationContainer_height = document.getElementById('annotationContent').style.height;

	// -----------------------------
	// add a click handler for each div container
	document.getElementById('graphHeader').onclick = function() {
		min_max_DIV('graphContent', graphContainer_height, 'graphBTN_line');
	};
	document.getElementById('annotationHeader').onclick = function() {
		min_max_DIV('annotationContent', annotationContainer_height,
				'annoBTN_line');
	};
	document.getElementById('expreHeader').onclick = function() {
		min_max_DIV('expreContent', expreContainer_height, 'expreBTN_line');
	};
	// --------------
	// add click handler for the min/max buttons located in the head
	document.getElementById('expreBTN').onclick = function() {
		min_max_DIV('expreContent', expreContainer_height, 'expreBTN_line');
	};
	document.getElementById('graphBTN').onclick = function() {
		min_max_DIV('graphContent', graphContainer_height, 'graphBTN_line');
	};
	document.getElementById('annoBTN').onclick = function() {
		min_max_DIV('annotationContent', annotationContainer_height,
				'annoBTN_line');
	};
}