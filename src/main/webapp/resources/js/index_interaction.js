// JavaScript Document
//var geneBTN;
$(function() {
	
	$("#dialog-message").dialog({
		autoOpen : false,
		modal : true,
		buttons : {
			Ok : function() {
				$(this).dialog("close");
			}
		}
	});

	$("#dialog-message1").dialog({
		autoOpen : false,
		modal : true,
		buttons : {
			Ok : function() {
				$(this).dialog("close");
			}
		}
	});
	
	$("#dialog-message2").dialog({
		autoOpen : false,
		modal : true,
		buttons : {
			Ok : function() {
				$(this).dialog("close");
			}
		}
	});
	
	var geneBTN = document.getElementById("geneBTN");
	geneBTN.addEventListener("mouseover", mouseover, false);
	geneBTN.addEventListener("mouseout", mouseout, false);
	// ---hover for moduleBTN
	var moduleBTN = document.getElementById("moduleBTN");
	moduleBTN.addEventListener("mouseover", mouseover, false);
	moduleBTN.addEventListener("mouseout", mouseout, false);
	// ---hover for moduleBTN
	var pathwayBTN = document.getElementById("pathwayBTN");
	pathwayBTN.addEventListener("mouseover", mouseover, false);
	pathwayBTN.addEventListener("mouseout", mouseout, false);
	
	$('#geneGoBTN').css('cursor', 'pointer');
	$("#category").change(function(event) {
		categoryChanged();
	});

	$("#moduleBTN").click(function(event) {
		event.preventDefault();
		if ($("#category")[0].selectedIndex == 0) {
			$("#dialog-message").dialog("open");
			return false;
		}
		document.getElementById("moduleSubmit").submit();
		$.blockUI({
			css : {
				border : 'none',
				padding : '15px',
				backgroundColor : '#000',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#fff',
				'z-index': 100
			}
		});
	});
	
	$("#downloadBTN").click(function(event) {
		event.preventDefault();
		if ($("#category")[0].selectedIndex == 0) {
			$("#dialog-message").dialog("open");
			return false;
		}
		document.getElementById("downloadSubmit").submit();
		$.blockUI({
			css : {
				border : 'none',
				padding : '15px',
				backgroundColor : '#000',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#fff',
				'z-index': 100
			}
		});
	});
	
	$("#geneGoBTN").click(function(event) {
		event.preventDefault();
		if ($("#category")[0].selectedIndex == 0) {
			$("#dialog-message").dialog("open");
			return false;
		}
		if (!$.trim($("#geneValue").val()).length) {
			$("#dialog-message1").dialog("open");
			return false;
		}
		var geneName = $('#geneValue').val();
		geneName = geneName.split(" |")[0];
		var proceed = false;
		$('#genedatalist option').each(function() {
			if($(this).val().split(" |")[0]===geneName)
				proceed = true;
		});
		if(!proceed){			
			event.preventDefault();
			event.stopPropagation();
			$("#dialog-message2").dialog("open");
		} else {
			$('#geneValue').val(geneName);
			document.getElementById("geneSubmit").submit();
			$.blockUI({
				css : {
					border : 'none',
					padding : '15px',
					backgroundColor : '#000',
					'-webkit-border-radius' : '10px',
					'-moz-border-radius' : '10px',
					opacity : .5,
					color : '#fff',
					'z-index': 100
				}
			});
		}
	});

	$("#pathwayGoBTN").click(function(event) {
		event.preventDefault();
		if ($("#category")[0].selectedIndex == 0) {
			$("#dialog-message").dialog("open");
			return false;
		}
		if (!$.trim($("#valuePatway").val()).length) {
			$("#dialog-message1").dialog("open");
			return false;
		}
		document.getElementById("pathwaySubmit").submit();
		$.blockUI({
			css : {
				border : 'none',
				padding : '15px',
				backgroundColor : '#000',
				'-webkit-border-radius' : '10px',
				'-moz-border-radius' : '10px',
				opacity : .5,
				color : '#fff',
				'z-index': 100
			}
		});
	});

});

function loadOrganisms() {
	$.ajax({
		type : "get",
		url : "/genet/organismList",
		cache : false,
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		mimeType : 'application/json',
		success : function(response) {
			$.each(response, function(index) {
				var option = new Option(response[index].name);
				$(option).html(response[index].name);
				if(response[index].selected == true) {
					$(option).prop('selected', true);
				}					
				$('#category').append(option);
			});
			categoryChanged();
		},
		error : function(response) {
			alert('Error while fetching organisms');

		}
	});
}

function categoryChanged() {
	if ($("#category")[0].selectedIndex == 0) {
		return;
	} else {
		$("#geneCategory").val($("#category option:selected").text());
		$("#moduleCategory").val($("#category option:selected").text());
		$("#pathwayCategory").val($("#category option:selected").text());
		$("#downloadCategory").val($("#category option:selected").text());
	}

}

function moduleBTNClicked(event) {
	event.preventDefault();
	if ($("#category")[0].selectedIndex == 0) {
		$("#dialog-message").dialog("open");
		return false;
	}
	document.getElementById("moduleSubmit").submit();
}

function downloadBTNClicked(event) {
	event.preventDefault();
	if ($("#category")[0].selectedIndex == 0) {
		$("#dialog-message").dialog("open");
		return false;
	}
	document.getElementById("downloadSubmit").submit();
}

// geneBTN.addEventListener("mouseout", mouseOuted, false);
function mouseover() {

	this.style.borderColor = "#1B1B1B";

}
function mouseout() {

	this.style.borderColor = "#cccccc";

}
