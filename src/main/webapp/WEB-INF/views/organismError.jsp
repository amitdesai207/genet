<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page isErrorPage="true" import="java.io.*,org.slf4j.*,org.springframework.stereotype.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Organism Error</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />
</head>

<body style="background-color: #e5e4e9">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block; width: 100%">
				<div style="width: 50%; float: left;">
					<a href="home"><img
						style="padding-left: 30px; height: 100%; width: 30%;"
						src="resources/images/Genet.png" /></a>
				</div>
				<div style="position: inherit; margin-top: -37px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="help">Help</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer">
			<table style="width: 100%; min-width: 1000px">
				<tr>
					<td align="center">
						<div id="bottunContainer">
							<table class="containerTable">
								<center>
									<h2>
										<spring:message code="page.error"
											text="Please select an organism first to retrive data. Organism can be selected on home page" />
									</h2>
									<%
										Logger logger = LoggerFactory.getLogger(Controller.class);
										logger.warn("Error in controller: ", exception);
									%>
								</center>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center">
						<div style="position: relative; top: -20px">
							<table style="border-collapse: collapse">
								<tr>
									<td>
										<p style="font-size: 12px">&copy;</p>
									</td>
									<td><a style="font-size: 12px" href="http://www.mun.ca" target="_blank">Memorial
											University of Newfoundland</a></td>
									<td>
										<p style="font-size: 12px; padding-left: 2px">2016</p>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div></div>
	</div>
</body>
</html>
