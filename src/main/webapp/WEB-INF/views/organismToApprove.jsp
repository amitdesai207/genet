<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Approve Organism</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/jquery.bootgrid.js"></script>
<script type="text/javascript" src="resources/js/moderniz.2.8.1.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<link href="resources/css/jquery.bootgrid.css" rel="stylesheet" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
	$(function() {
		$("#grid-basic").bootgrid({
			formatters : {
				"link" : function(column, row) {
					var id = row["id"];
					return "<button class=\"submit\" onclick=\"approveOrganism(" + id
							+ ")\"><span>Approve</span></button><button class=\"submit\" onclick=\"rejectOrganism("
							+ id + ")\"><span>Reject</span></button>";
				}
			}
		});
	});

	function approveOrganism(id) {
		var tdElement = $(event.srcElement.parentElement.parentElement);
		$.ajax({
					type : "POST",
					url : "approveOrganism",
					data : "id=" + id,
					success : function(msg) {
						if (msg == true) {
							tdElement.html("Approved");
						} else
							tdElement.html("<font color='red'>!Error. Try again!</font><br>" + tdElement.html());
					}
				});
	}

	function rejectOrganism(id) {
		var tdElement = $(event.srcElement.parentElement.parentElement);
		var result = confirm("Rejecting an organism, will delete all uploaded data related to the organism. Do you want to continue?");
		if (result == true) {
			$.ajax({
						type : "POST",
						url : "approveOrganism?reject=true",
						data : "id=" + id,
						success : function(msg) {
							if (msg == true) {
								tdElement.html("Rejected");
							} else
								tdElement.html("<font color='red'>!Error. Try again!</font><br>" + tdElement.html());
						}
					});
		}
	}
</script>
</head>
<body style="background-color: #e5e4e9; height: 100%;">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block; width: 100%">
				<div style="width: 50%; float: left;">
					<a href="home"><img
						style="padding-left: 30px; height: 100%; width: 30%;"
						src="resources/images/Genet.png" /></a>
				</div>
				<div style="position: inherit; margin-top: -21px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="help">Help</a></li>
						<li><a href="logout">Sign Out</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer" style="height: 100%;">
			<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="jobHomePage"><span>Contribute Data</span></a></li>
						<c:if test="${(sessionScope.ROLE eq  'Web_Tool_Admin')  }">
							<li class="active"><a href="organismToApprove"><span>Approve
										Organism</span></a></li>
						</c:if>
						<li><a href="alljobs"><span>All Import Jobs</span></a></li>
						<li><a href="jobsearch"><span>Search Import Jobs</span></a></li>
						<c:if test="${(sessionScope.ROLE eq  'Web_Tool_Admin')  }">
							<li><a href="downloadLogs"><span>Email Logs</span></a></li>
							<li><a href="serverStatisticsPage"><span>Server
										statistics</span></a></li>
						</c:if>
						<li><a href="reset"><span>Reset Password</span></a></li>
					</ul>
				</div>
			</div>
			</nav>
			<table style="width: 100%; min-width: 1000px">
				<tr style="height: 410px;">
					<td style="vertical-align: top;">
						<div class="container">
							<div style="text-align: center; width: 700px;">
								<span><b>Organisms for approval</b></span>
							</div>
							<div style="overflow: auto; height: 98%; text-align: left;">
								<table id="grid-basic"
									class="table table-condensed table-hover table-striped">
									<thead>
										<tr>
											<th data-column-id="name">Organism Name</th>
											<th data-column-id="id" data-formatter="link">Approve / Reject</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="organism" items="${organisms}">
											<tr>
												<c:forEach var="data" items="${organism}">
													<td>${data}</td>
												</c:forEach>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="row">
			<div class="col-sm-12 centerText">
				&copy; <a href="http://www.mun.ca" target="_blank">Memorial University
					of Newfoundland</a> 2016
			</div>
		</div>
	<div></div>
	</div>
</body>
</html>
