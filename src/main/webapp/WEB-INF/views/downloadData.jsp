<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="org.mun.genet.model.DownloadDataModel"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Download Data</title>
<script type="text/javascript"
	src="resources/js/d3.v2_diagramDrawer-V2.js"></script>
<script type="text/javascript" src="resources/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.11.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/bootstrapValidator.js"></script>
<style type="text/css">
.ui-widget-overlay {
	position: fixed !important;
}
</style>
<script type="text/javascript" src="resources/js/interaction.js"></script>
<%
	String category = (String) request.getAttribute("category");
	DownloadDataModel dataModel = (DownloadDataModel) request.getAttribute("dataModel");
%>
<script>

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(function() {
	$('#downloadBtn').click(function() {
		$("#dialog-message3").dialog("open");
		$.ajax({
			type : "post",
			url : "/genet/downloadData",
			cache : false,
			data: JSON.stringify($("#uploadForm").serializeObject()),
			mimeType : 'application/json',
			contentType : "application/json; charset=utf-8",
			success : function(response) {
			},
			error : function(response) {
				$("#dialog-message3").dialog("close");
				if(response.responseText.length == 0)
					window.location.href = "/genet";
				else
					window.location.href = "/genet/downloadFile?fileName=" + response.responseText;
			}
		});
	});
	
	if(<%=!dataModel.isCondition_information()%>){
		var input = $('input[name="condition_information"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isGene_expression()%>){
		var input = $('input[name="gene_expression"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isGene_info()%>){
		var input = $('input[name="gene_info"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isModule_significance()%>){
		var input = $('input[name="module_significance"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isGene_kegg()%>){
		var input = $('input[name="gene_kegg"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isGene_pfam_desc()%>){
		var input = $('input[name="gene_pfam_desc"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isGene_pfam()%>){
		var input = $('input[name="gene_pfam"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isGene_kegg_desc()%>){
		var input = $('input[name="gene_kegg_desc"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isGene_metacyc()%>){
		var input = $('input[name="gene_metacyc"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isGene_protein()%>){
		var input = $('input[name="gene_protein"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isGene_tu()%>){
		var input = $('input[name="gene_tu"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isModule_enrichment()%>){
		var input = $('input[name="module_enrichment"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isCorrelation_matrix()%>){
		var input = $('input[name="correlation_matrix"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isCorrelation_matrix_cyto()%>){
		var input = $('input[name="correlation_matrix_cyto"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isWeighted_matrix()%>){
		var input = $('input[name="weighted_matrix"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isWeighted_matrix_cyto()%>){
		var input = $('input[name="weighted_matrix_cyto"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	if(<%=!dataModel.isProtein_sequence()%>){
		var input = $('input[name="protein_sequence"]');
		input.parent().addClass('control-disabled');
		input.prop("disabled", true);
	}
	
	$('#selectAllBtn').click(function(event) {
		event.preventDefault();
		$('input[name="all"]').prop("checked", true);
		if(!$('input[name="condition_information"]').prop("disabled"))
			$('input[name="condition_information"]').prop("checked", true);
		if(!$('input[name="gene_expression"]').prop("disabled"))
			$('input[name="gene_expression"]').prop("checked", true);
		if(!$('input[name="gene_info"]').prop("disabled"))
			$('input[name="gene_info"]').prop("checked", true);
		if(!$('input[name="module_significance"]').prop("disabled"))
			$('input[name="module_significance"]').prop("checked", true);
		if(!$('input[name="gene_kegg"]').prop("disabled"))
			$('input[name="gene_kegg"]').prop("checked", true);
		if(!$('input[name="gene_pfam_desc"]').prop("disabled"))
			$('input[name="gene_pfam_desc"]').prop("checked", true);
		if(!$('input[name="gene_pfam"]').prop("disabled"))
			$('input[name="gene_pfam"]').prop("checked", true);
		if(!$('input[name="gene_kegg_desc"]').prop("disabled"))
			$('input[name="gene_kegg_desc"]').prop("checked", true);
		if(!$('input[name="gene_metacyc"]').prop("disabled"))
			$('input[name="gene_metacyc"]').prop("checked", true);
		if(!$('input[name="gene_protein"]').prop("disabled"))
			$('input[name="gene_protein"]').prop("checked", true);
		if(!$('input[name="gene_tu"]').prop("disabled"))
			$('input[name="gene_tu"]').prop("checked", true);
		if(!$('input[name="module_enrichment"]').prop("disabled"))
			$('input[name="module_enrichment"]').prop("checked", true);
		if(!$('input[name="correlation_matrix"]').prop("disabled"))
			$('input[name="correlation_matrix"]').prop("checked", true);
		if(!$('input[name="correlation_matrix_cyto"]').prop("disabled"))
			$('input[name="correlation_matrix_cyto"]').prop("checked", true);
		if(!$('input[name="weighted_matrix"]').prop("disabled"))
			$('input[name="weighted_matrix"]').prop("checked", true);
		if(!$('input[name="weighted_matrix_cyto"]').prop("disabled"))
			$('input[name="weighted_matrix_cyto"]').prop("checked", true);
		if(!$('input[name="protein_sequence"]').prop("disabled"))
			$('input[name="protein_sequence"]').prop("checked", true);
	});
});

var moduleButtonClicked = false;
var btnDIVWidth=650;

function geneBTNClicked()
{
	var difference=(640-btnDIVWidth)/2;
	btnDIVWidth=640;
	var destination=btnDIVWidth/2-70;
	var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left",(prevLeft+difference)+"px");
	$("#topArrow").animate({"left":destination+"px"}, "slow");
	$("#pathwaySearchForm").css("display","none");
	$("#moduleSearchForm").css("display","none");
	$("#downloadForm").css("display","none");
	$("#geneSearchForm").css("display","table");
	moduleButtonClicked = false;
}
function moduleBTNClicked()
{
	if(moduleButtonClicked) {
		$("#moduleGoBTN").trigger( "click" );
	} else {
		moduleButtonClicked=!moduleButtonClicked;
		var difference=(280-btnDIVWidth)/2;
		btnDIVWidth=280;
		var destination=btnDIVWidth/2+35;
		var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
		$("#topArrow").css("left",(prevLeft+difference)+"px");
		$("#topArrow").animate({"left":destination+"px"}, "slow");
		$("#geneSearchForm").css("display","none");
		$("#pathwaySearchForm").css("display","none");
		$("#downloadForm").css("display","none");
		$("#moduleSearchForm").css("display","table");
		moduleButtonClicked = true;
	}
}

function downloadBTNClicked() {	
	if(moduleButtonClicked) {
		$("#downloadGoBTN").trigger( "click" );
	} else {
		var difference=(640-btnDIVWidth)/2;
		btnDIVWidth=640;
		var destination=btnDIVWidth/2-50;
		var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
		$("#topArrow").css("left",(prevLeft+difference)+"px");
		$("#topArrow").animate({"left":destination+"px"}, "slow");
		$("#pathwaySearchForm").css("display","none");
		$("#moduleSearchForm").css("display","none");
		$("#geneSearchForm").css("display","none");
		$("#downloadForm").css("display","table");		
		moduleButtonClicked = true;
	}
}

function pathwayBTNClicked()
{
	var difference=(625-btnDIVWidth)/2;
	btnDIVWidth=625;
	var destination=btnDIVWidth/2+0;
	var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left",(prevLeft+difference)+"px");
	$("#topArrow").animate({"left":destination+"px"}, "slow");
	$("#geneSearchForm").css("display","none");
	$("#moduleSearchForm").css("display","none");
	$("#downloadForm").css("display","none");
	$("#pathwaySearchForm").css("display","table");
	moduleButtonClicked = false;
}
</script>
<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link rel="stylesheet" href="resources/css/bootstrap.min.css" />
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/table.css" rel="stylesheet" type="text/css"
	media="screen" />
</head>
<body onload="initializeDocument()">
	<div>
		<div id="main" style="padding-bottom: 0px;">
			<div id="logo">
				<a href="home"> <img src="resources/images/Genet.png" /></a>
			</div>
			<div id="headContainer" align="center">
				<div class="header-menu">
					<table id="header_tb" class="noSpace">
						<tr>
							<td align="center" style="vertical-align: top;">
								<div id="headerBTNS">
									<div id="headBTNContainer" align="center">
										<table
											style="border-collapse: collapse; padding: 0px; margin: 0px;"
											cellpadding="0px" cellspacing="0px">
											<tr>
												<td align="center">
													<div id="btnCont" style="padding-top: 3px">
														<table id="btns_tbl"
															style="border-collapse: collapse; padding: 0px; margin: 0px; margin-top: 10px;"
															cellpadding="0px" cellspacing="0px">
															<tr>
																<td>
																	<div id="geneDIV" onclick="geneBTNClicked()"
																		style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px">
																		<img src="resources/images/gene_h.png"
																			title="Search Gene" />
																	</div>
																</td>
																<td>
																	<div id="moduleDIV" onclick="moduleBTNClicked()"
																		style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px; padding-left: 8px">
																		<img src="resources/images/module_h.png"
																			title="Browse Module" />
																		<!-- <form id="moduleSubmit" method="post" action="Module_of_Interest"> -->
																		<!-- </form> -->
																	</div>
																</td>
																<td>
																	<div id="pathwayDIV" onclick="pathwayBTNClicked()"
																		style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px; padding-left: 8px">
																		<img src="resources/images/pathway_h.png"
																			title="Search Pathway" />
																	</div>
																</td>
																<td>
																	<div id="downloadDIV" onclick="downloadBTNClicked()"
																		style="padding-bottom: 3px; padding-right: 8px; padding-left: 8px">
																		<img src="resources/images/download_h.png"
																			title="Download Data" />
																	</div>
																</td>
															</tr>
														</table>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div id="bottomLineBTN"
														style="border-bottom: 2px solid #FFFFFF;">
														<div align="left">
															<img id="topArrow"
																style="vertical-align: bottom; text-align: center; position: relative; bottom: -2px; left: 0px"
																src="resources/images/arrowTop.png" />
														</div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div id="geneSearchForm"
														style="display: none; text-align: center">
														<form id="geneSubmit" method="post"
															action="Gene_of_Interest">
															<table id="formContainer_tb" style="width: 650px"
																class="noSpace">
																<tr>
																	<td align="left">
																		<p style="width: 94px">Find Genes by</p>
																	</td>

																	<td width="100px"><select id="geneId" name="id">
																			<option>Symbol</option>
																			<option>Systematic ID</option>
																			<option>Entrez ID</option>
																	</select></td>
																	<td width="100px"><input autocomplete="off"
																		id="geneValue" name="value" list="genedatalist"
																		type="text" /> <datalist id="genedatalist"></datalist>
																	</td>
																	<td align="left">
																		<p style="width: 76px">in organism</p>
																	</td>
																	<td width="90px"><select class="btn"
																		id="geneCategory" name="category"
																		style="width: 150px;">
																			<option>------Please Select------</option>
																	</select></td>

																	<td width="20px">
																		<button id="geneGoBTN" type="submit" class="btn goBTN">
																			<p class="noSpace">
																				<strong>Go</strong>
																			</p>
																		</button>
																	</td>
																	<td width="10px" align="center"></td>
																</tr>
															</table>
														</form>
													</div>
													<div id="moduleSearchForm" style="display: none">
														<form id="moduleSubmit" method="post"
															action="Module_of_Interest">
															<table id="moduleContainer_tb" class="noSpace">
																<tr>
																	<td align="left">
																		<p style="width: 110px">Select organism</p>
																	</td>
																	<td><select class="btn" id="moduleCategory"
																		name="category" style="width: 250px;">
																			<option>------Please Select------</option>
																	</select></td>
																	<td width="20px">
																		<button id="moduleGoBTN" type="submit"
																			class="btn goBTN moduleBTN">
																			<p class="noSpace">
																				<strong>Go</strong>
																			</p>
																		</button>

																	</td>
																</tr>
															</table>
														</form>
													</div>
													<div id="pathwaySearchForm" style="display: none">
														<form id="pathwaySubmit" method="post"
															action="Pathway_Representation">
															<table id="pathwayContainer_tb" class="noSpace">
																<tr>
																	<td align="left">
																		<p style="width: 94px">Find pathways</p>
																	</td>
																	<td width="210px"><input style="width: 210px"
																		id="pathwayValue" name="value" type="text" /></td>
																	<td align="center" width="80px">
																		<p style="width: 80px">in organism</p>
																	</td>

																	<td width="90px"><select class="btn"
																		id="pathwayCategory" name="category"
																		style="width: 150px;">
																			<option>------Please Select------</option>
																	</select></td>
																	<td width="20px">
																		<button id="pathwayGoBTN" type="submit"
																			class="btn goBTN">
																			<p style="visibility: inherit" class="noSpace">
																				<strong>Go</strong>
																			</p>
																		</button>
																	</td>
																</tr>
															</table>
														</form>
													</div>
													<div id="downloadForm" style="display: none">
														<form id="downloadSubmit" method="get"
															action="downloadData">
															<table id="downloadContainer_tb" class="noSpace">
																<tr>
																	<td align="left">
																		<p style="width: 110px">Select organism</p>
																	</td>
																	<td><select class="btn" id="downloadCategory"
																		name="category" style="width: 250px;">
																			<option>------Please Select------</option>
																	</select></td>
																	<td width="20px">
																		<button id="downloadGoBTN" type="submit"
																			class="btn goBTN moduleBTN">
																			<p class="noSpace">
																				<strong>Go</strong>
																			</p>
																		</button>

																	</td>
																</tr>
															</table>
														</form>
													</div>
												</td>
											</tr>
										</table>

									</div>
								</div>
							</td>
							<!-->
<!-->
						</tr>
					</table>
				</div>
			</div>
			<div class="main-menu" style="margin-top: -130px;">
				<nav class="horizontal-nav1 ">
				<ul>
					<li><a href="home">Home</a></li>
					<li><a href="contact">About GeNet</a></li>
					<li><a href="login">Data Contributor Login</a></li>
					<li><a href="help">Help</a></li>
				</ul>
				</nav>
			</div>
		</div>
		<div id="midContainer" style="height: 100%; padding-bottom: 5em;">
			<div style="text-align: left;">
				<h3>Download Data</h3>
			</div>
			<div id="content" style="width: 60%; margin-top: 30px;">
				<form:form id="uploadForm" method="POST" commandName="dataModel"
					action="downloadData" class="form-horizontal" enctype='application/json'>
					<div class="col-sm-offset-1 panel panel-default col-text-align">
						<div class="panel-heading">
							<h3 class="panel-title">
								<spring:message code="label.geneExpressionMatrix"
									text="Select the Files you want to download" />
							</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox"
												name="gene_expression" value="true"/> <spring:message
													code="label.consent2" text="Gene Expression Matrix (.csv)" /></label>

										</div>
									</div>
								</div>
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox" name="gene_info" value="true"/>
												<spring:message code="label.consent2"
													text="Gene Information Table (.csv)" /></label>

										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox"
												name="condition_information" value="true"/> <spring:message
													code="label.consent2"
													text="Condition Information Table (.csv)" />
											</label>

										</div>
									</div>
								</div>
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox"
												name="module_significance" value="true"/> <spring:message
													code="label.consent2"
													text="Module Significance File (.csv)" /></label>

										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox" name="gene_kegg" value="true"/>
												<spring:message code="label.consent2"
													text="Kegg Annotation File (.csv)" /></label>

										</div>
									</div>
								</div>
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox"
												name="gene_kegg_desc" value="true"/> <spring:message
													code="label.consent2" text="Kegg Description File (.csv)" /></label>

										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox" name="gene_pfam" value="true"/>
												<spring:message code="label.consent2"
													text="Pfam Annotation File (.csv)" /></label>

										</div>
									</div>
								</div>
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox"
												name="gene_pfam_desc" value="true"/> <spring:message
													code="label.consent2" text="Pfam Description File (.csv)" /></label>

										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox" name="gene_tu" value="true"/>
												<spring:message code="label.consent2"
													text="Transcriptional unit annotation File (.csv)" /></label>

										</div>
									</div>
								</div>
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox" name="gene_metacyc" value="true"/>
												<spring:message code="label.consent2"
													text="Pathway annotation File (.csv)" /></label>

										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox" name="gene_protein" value="true"/>
												<spring:message code="label.consent2"
													text="Protein Complex Annotation File (.csv)" /></label>

										</div>
									</div>
								</div>
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox"
												name="protein_sequence" value="true"/> <spring:message
													code="label.consent2" text="Protein Sequence File (.fasta)" /></label>

										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-offset-1 col-sm-5 col-text-align">
									<div class="form-group has-feedback">
										<div class="form-check" style="font-weight: bold;">
											<label class="form-check-label"> <input
												class="form-check-input" type="checkbox"
												name="module_enrichment" value="true"/> <spring:message
													code="label.consent2" text="Module Enrichment File (.csv)" /></label>

										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div
									class="col-sm-offset-1 panel col-sm-10 panel-default col-text-align zero-padding">
									<div class="panel-heading">
										<h3 class="panel-title">
											<spring:message code="label.adjacencymatrix"
												text="Gene Correlation Matrix" />
										</h3>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-offset-1 col-sm-5 col-text-align">
												<div class="form-group has-feedback">
													<div class="form-check" style="font-weight: bold;">
														<label class="form-check-label"> <input
															class="form-check-input" type="checkbox"
															name="correlation_matrix" value="true"/> <spring:message
																code="label.consent2" text=".csv file" /></label>

													</div>
												</div>
											</div>
											<div class="col-sm-offset-1 col-sm-5 col-text-align">
												<div class="form-group has-feedback">
													<div class="form-check" style="font-weight: bold;">
														<label class="form-check-label"> <input
															class="form-check-input" type="checkbox"
															name="correlation_matrix_cyto" value="true"/> <spring:message
																code="label.consent2" text=".sif file" /></label>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div
									class="col-sm-offset-1 col-sm-10 zero-padding panel panel-default col-text-align">
									<div class="panel-heading">
										<h3 class="panel-title">
											<spring:message code="label.adjacencymatrix"
												text="Gene Adjacency Matrix" />
										</h3>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-offset-1 col-sm-5 col-text-align">
												<div class="form-group has-feedback">
													<div class="form-check" style="font-weight: bold;">
														<label class="form-check-label"> <input
															class="form-check-input" type="checkbox"
															name="weighted_matrix" value="true"/> <spring:message
																code="label.consent2" text=".csv file" /></label>

													</div>
												</div>
											</div>
											<div class="col-sm-offset-1 col-sm-5 col-text-align">
												<div class="form-group has-feedback">
													<div class="form-check" style="font-weight: bold;">
														<label class="form-check-label"> <input
															class="form-check-input" type="checkbox"
															name="weighted_matrix_cyto" value="true"/> <spring:message
																code="label.consent2" text=".sif file" /></label>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<input type="checkbox" name="all" value="true" style="opacity: 0.0; position: absolute; left: -9999px"/>
							<div class="row">
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-1">
										<button type="button" class="submit" id="selectAllBtn">
											<span><spring:message code="label.selectall"
													text="Select All" /></span>
										</button>
									</div>
									<div class="col-sm-offset-2 col-sm-1">
										<button id="downloadBtn" type="button" class="submit">
											<span><spring:message code="label.download"
													text="Download" /></span>
										</button>
									</div>
									<div class="col-sm-offset-2 col-sm-1">
										<button id="reset" type="reset" class="submit">
											<span><spring:message code="label.reset"
													text="Clear All" /></span>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form:form>
			</div>
			<div />
		</div>
	</div>
	</div>
	<input id="category" type="hidden" value="<%=category%>" />
	<div id="dialogs" style="visibility: hidden;">
		<div id="dialog-message" title="Required data missing">
			<p>Please select an organism</p>
		</div>
		<div id="dialog-message1" title="Required data missing">
			<p>Please provide value for search criteria</p>
		</div>
		<div id="dialog-message2" title="Required data missing">
			<p>Please enter a valid identifier</p>
		</div>
		<div id="dialog-message3" title="Preparing download">
			<p>Please wait while your file is being prepared</p>
		</div>
	</div>
	<script>
	var changeAutoComplete = false;
	var totalRecordsFetched = 0;
	var changeEventCount = 0;
	
	$("[id$=geneValue]").focusout(function(event) {
		changeEventCount = 0;
	});
	$("[id$=geneValue]").keyup(function(event) {
		if(event.which <= 90 && event.which >= 48 || event.which==8 || event.which == 27) {
			changeEventCount = 0;
			checkLastTextBoxValue($("[id$=geneValue]").val(), null);
		} else if (event.which == 40){
			changeEventCount++;
			if(changeEventCount==0 ||changeEventCount==totalRecordsFetched+1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'next');
			}
		} else if (event.which == 38){
			changeEventCount--;
			if(changeEventCount==0 || changeEventCount==-totalRecordsFetched-1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'prev');
			}
		} 
	});	
	
	// 	query expansion for gene Search
	function checkLastTextBoxValue(oldValue, next) {
		oldValue = oldValue.split(" |")[0];
		if (oldValue != "" || next) {
			var type = $("[id$=geneId]").val();
			var organism = $("[id$=geneCategory]").val();
			if (organism != '' && organism != '------Please Select------')
				loadDropDownMenu(oldValue, type, organism, next);
		}
	}
	function loadDropDownMenu(currentQuery, type, organism, next) {
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var resp = xmlhttp.responseText;
				var currentArray = new Array();
				if(resp.length!=0)
					currentArray = resp.split(",");
				var dataList = document.getElementById('genedatalist');
				totalRecordsFetched = currentArray.length;
				if(currentArray.length > 0) {
					$(dataList).empty();
					changeEventCount = 0;
				}
				currentArray.forEach(function(item) {
			        var option = document.createElement('option');
			        option.value = item;
			        dataList.appendChild(option);
			      });
			}
		}
		var request = "Gene_of_Interest?query="+currentQuery+"&type="+type+"&category="+organism+"&job_id=queryExp&size=10";
		  if(next!=null)
			  request = request + "&job_parameter=" + next;
		xmlhttp.open("GET", request, true);
		xmlhttp.send();
	}
</script>
</body>
</html>