<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Browse Data</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="resources/js/index_interaction.js"></script>
<script type="text/javascript" src="resources/js/jquery.blockUI.js"></script>
<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />

<script type="text/javascript">
	function geneBTNClicked() {

		if ($("#geneBTN").width() < 200) {

			$("#genePic").css("visibility", "collapse");
			$("#geneBTN").animate({
				width : "524px"
			});
			$("#formContainer_tb").css("visibility", "visible");
		}
	}
	function pathwayBTNClicked() {
		if ($("#pathwayBTN").width() < 200) {

			$("#pathwayPic").css("visibility", "collapse");
			$("#pathwayBTN").animate({
				width : "524px",
				left : '-=270'
			});
			$("#pathwayContainer_tb").css("visibility", "visible");
			$("#geneBTN").css("z-index", "-1");
			$("#pathwayBTN").css("z-index", "1");
		}

	}
	function closepathwayBTN() {
		if ($("#pathwayBTN").width() > 200) {
			$("#geneBTN").css("z-index", "1");
			$("#pathwayContainer_tb").css("visibility", "collapse");
			$("#pathwayBTN").animate({
				width : "120px",
				left : '+=270'
			});
			$("#pathwayPic").css("visibility", "visible");
			$("#pathwayBTN").css("z-index", "0");
		}
	}
	function closeGeneBTN() {
		if ($("#geneBTN").width() > 200) {
			$("#formContainer_tb").css("visibility", "collapse");
			$("#geneBTN").animate({
				width : "120px"
			});
			$("#genePic").css("visibility", "visible");

		}
	}
</script>
</head>

<body style="background-color: #e5e4e9" onload="loadOrganisms()">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block; width: 100%;">
				<div style="width: 50%; float: left;">
					<img style="padding-left: 30px; height: 100%; width: 70%; max-width: 500px;"
						src="resources/images/indexLogo5.png" />
				</div>
				<div style="position: inherit; margin-top: -37px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="login">Data Contributor Login</a></li>
						<li><a href="help">Help</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer">
			<table style="width: 100%; min-width: 1000px">
				<tr>
					<td align="center">
						<div id="bottunContainer">
							<table style="min-width: 700px; padding-left: 40px">
								<tr>
									<td>
										<div style="padding-left: 90px">
											<table>
												<tr>
													<td style="vertical-align: text-bottom">
														<p class="TXTstyle" onclick="exampleClicked()">Select
															organism</p>
													</td>
													<td style="vertical-align: super"><select class="btn"
														id="category" name="category" onchange="categoryChanged()"
														style="width: 250px; font-size: 14px; color: #003 !important;">
															<option>------Please Select------</option>
													</select></td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td style="padding-left: 10px">
										<div id="geneBTN" class="btn" align="center"
											onclick="geneBTNClicked()" style="cursor: pointer;">
											<span style="font-weight: bold;">Search</span></br> <img
												id="genePic" src="resources/images/gene.png" />
											<div style="position: relative; top: -84px; left: -10px;">
												<form id="geneSubmit" method="post"
													action="Gene_of_Interest">
													<table id="formContainer_tb" class="noSpace"
														style="visibility: collapse">
														<tr>
															<td id="collpaseBTN" onclick="closeGeneBTN()"
																style="width: 40px" align="center"><img
																src="resources/images/collapse.gif" /></td>
															<td onclick="closeGeneBTN()" align="left" width="60px">
																<p style="width: 60px">genes by</p>
															</td>
															<td width="1px"><input type="hidden"
																id="geneCategory" name="category" /></td>
															<td width="100px"><select id="geneId" name="id">
																	<option>Symbol</option>
																	<option>Systematic ID</option>
																	<option>Entrez ID</option>
															</select></td>
															<td width="100px"><input autocomplete="off"
																id="geneValue" name="value" list="genedatalist" title="scroll to the bottom for more options"
																type="text" /> <datalist id="genedatalist"></datalist></td>
															<td onclick="closeGeneBTN()" width="20px">
																<button id="geneGoBTN" type="submit"
																	class="btn goBTN">
																	<p style="visibility: inherit" class="noSpace">
																		<strong>Go</strong>
																	</p>
																</button>

															</td>
															<td></td>
														</tr>
													</table>
												</form>
											</div>
										</div>
										<div id="moduleBTN" class="btn" align="center"
											onclick="moduleBTNClicked()" style="cursor: pointer;">
											<span style="font-weight: bold;">Browse</span></br> <img
												id="modulePic" src="resources/images/module.png" />
											<form id="moduleSubmit" method="post"
												action="Module_of_Interest">
												<input type="hidden" id="moduleCategory" name="category" />
											</form>
										</div>
										<div id="pathwayBTN" class="btn" align="center"
											onclick="pathwayBTNClicked()" style="cursor: pointer;">
											<span style="font-weight: bold;">Search</span></br> <img
												id="pathwayPic" src="resources/images/pathway.png" />
											<div style="position: relative; top: -84px; left: 0px;">
												<form id="pathwaySubmit" method="post"
													action="Pathway_Representation">
													<table id="pathwayContainer_tb" class="noSpace"
														style="visibility: collapse">
														<tr>
															<td width="30px" onclick="closepathwayBTN()"></td>
															<td align="left" width="80px" onclick="closepathwayBTN()">
																<p style="width: 82px; margin-left: 15px">Pathways
																	by</p> <input type="hidden" id="pathwayCategory"
																name="category" />
															</td>

															<td width="210px"><input autocomplete="off"
																style="width: 210px" id="valuePatway" name="value"
																type="text" /></td>
															<td width="20px" onclick="closepathwayBTN()">
																<button id="pathwayGoBTN" type="submit"
																	class="btn goBTN">
																	<p style="visibility: inherit" class="noSpace">
																		<strong>Go</strong>
																	</p>
																</button>

															</td>
															<td id="collpaseBTN" style="width: 40px" align="center"
																onclick="closepathwayBTN()"><img
																src="resources/images/collapseR.gif" /></td>
															<td></td>

														</tr>
													</table>
												</form>
											</div>
										</div>
										<div id="downloadBTN" class="btn" align="center"
											onclick="downloadBTNClicked()" style="cursor: pointer;">
											<span style="font-weight: bold;">Download</span></br> <img
												id="modulePic" src="resources/images/download.png" />
											<form id="downloadSubmit" method="get"
												action="downloadData">
												<input type="hidden" id="downloadCategory" name="category" />
											</form>
										</div>
									</td>
								</tr>

							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center">
						<div style="position: relative; top: -20px">
							<table style="border-collapse: collapse; text-align: center;">
								<tr>
									<td>
										<p style="font-size: 14px">GeNet is optimized to work with the Google Chrome and Mozilla Firefox web browsers</p>
									</td>
								</tr>
								<tr>
									<td>
										<span style="font-size: 12px">&copy;</span>
									<a style="font-size: 12px" href="http://www.mun.ca"
										target="_blank">Memorial University of Newfoundland</a><span style="font-size: 12px; padding-left: 2px">2016</span></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div></div>
	</div>
	<div id="dialogs" style="visibility: hidden;">
		<div id="dialog-message" title="Required data missing">
			<p>Please select an organism</p>
		</div>
		<div id="dialog-message1" title="Required data missing">
			<p>Please provide value for search criteria</p>
		</div>
		<div id="dialog-message2" title="Required data missing">
			<p>Please enter a valid identifier</p>
		</div>
	</div>
</body>
<script type="text/javascript">
	var changeAutoComplete = false;
	var totalRecordsFetched = 0;
	var changeEventCount = 0;
	
	$("[id$=geneValue]").focusout(function(event) {
		changeEventCount = 0;
		if($('#genedatalist option').length > 0){
			$("#geneValue").tooltip("disable");
			$("#geneValue").tooltip("close");
		}
	});
	
	$("[id$=geneValue]").keyup(function(event) {
		if($('#genedatalist option').length > 0){
			$("#geneValue").tooltip("enable");
			$("#geneValue").tooltip("open");
		}		
		if(event.which <= 90 && event.which >= 48 || event.which==8 || event.which == 27) {
			changeEventCount = 0;
			checkLastTextBoxValue($("[id$=geneValue]").val(), null);
		} else if (event.which == 40){
			changeEventCount++;
			if(changeEventCount==0 ||changeEventCount==totalRecordsFetched+1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'next');
			}
		} else if (event.which == 38){
			changeEventCount--;
			if(changeEventCount==0 || changeEventCount==-totalRecordsFetched-1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'prev');
			}
		} else if (event.which == 13){
			if($('#genedatalist option').length > 0){
				$("#geneValue").tooltip("disable");
				$("#geneValue").tooltip("close");
			}
		} 
	});	
	
	// 	query expansion for gene Search
	function checkLastTextBoxValue(oldValue, next) {
		oldValue = oldValue.split(" |")[0];
		if (oldValue != "" || next) {
			var type = $("[id$=geneId]").val();
			var organism = $("[id$=geneCategory]").val();
			if (organism != '' && organism != '------Please Select------')
				loadDropDownMenu(oldValue, type, organism, next);
		}
	}
	
	function loadDropDownMenu(currentQuery, type, organism, next) {
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var resp = xmlhttp.responseText;
				var currentArray = new Array();
				if(resp.length!=0)
					currentArray = resp.split(",");
				var dataList = document.getElementById('genedatalist');
				totalRecordsFetched = currentArray.length;
				if(currentArray.length > 0) {
					$(dataList).empty();
					changeEventCount = 0;
					$("#geneValue").tooltip({
						classes: {
				            "ui-dialog": "custom-tooltip-position"
				        },
				        tooltipClass: "custom-tooltip-class",
				        position: { my: "left+15 bottom+10", at: "right center" }
					});
					$("#geneValue").tooltip("open");
				}
				currentArray.forEach(function(item) {
			        var option = document.createElement("option");
			        option.value = item;
			        dataList.appendChild(option);
		      	});
			}
		}
		var request = "Gene_of_Interest?query="+currentQuery+"&type="+type+"&category="+organism+"&job_id=queryExp&size=10";
		  if(next!=null)
			  request = request + "&job_parameter=" + next;
		xmlhttp.open("GET", request, true);
		xmlhttp.send();
	}
</script>
</html>

