<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home</title>
<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script type="text/javascript">
	$.widget.bridge('uitooltip', $.ui.tooltip);
</script>
<script src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="resources/js/jquery.blockUI.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css">
<link rel="stylesheet" href="resources/css/font-awesome.min.css">
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
	$(function() {
		$('#taxonomyHelp')
				.uitooltip(
						{
							content : function() {
								return "<strong>Rhodobacter capsulatus</strong></br><span style='background-color: yellow;'><em>Taxonomy ID: </em>1061</span><br><em>Inherited blast name: </em><strong>a-proteobacteria</strong><br><em>Rank: </em>species<br>";
							}
						});

		$('#pubmedHelp')
				.uitooltip(
						{
							content : function() {
								return "<strong>REPA: Applying Pathway Analysis to Genome-wide Transcription Factor Binding Data</strong></br>Patra P, Izawa T, Pena Castillo L.</br><span style='background-color: yellow;'>PMID: 27019499</span>";
							}
						});

		$('#keggHelp').uitooltip({
			content : function() {
				return "<img src='resources/images/keggHelp.png'>";
			}
		});
	});
</script>
</head>
<body style="background-color: #e5e4e9">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block; width: 100%">
				<div style="width: 50%; float: left;">
					<a href="home"><img
						style="padding-left: 30px; height: 100%; width: 30%;"
						src="resources/images/Genet.png" /></a>
				</div>
				<div style="position: inherit; margin-top: -21px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="help">Help</a></li>
						<li><a href="logout">Sign Out</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer" style="height: 85%;">
			<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="jobHomePage"><span>Contribute
									Data</span></a></li>
						<c:if test="${(sessionScope.ROLE eq  'Web_Tool_Admin')  }">
							<li><a href="organismToApprove"><span>Approve
										Organism</span></a></li>
						</c:if>
						<li><a href="alljobs"><span>All Import Jobs</span></a></li>
						<li><a href="jobsearch"><span>Search Import Jobs</span></a></li>
						<c:if test="${(sessionScope.ROLE eq  'Web_Tool_Admin')  }">
							<li><a href="downloadLogs"><span>Email Logs</span></a></li>
							<li><a href="serverStatisticsPage"><span>Server
										statistics</span></a></li>
						</c:if>
						<li><a href="reset"><span>Reset Password</span></a></li>
					</ul>
				</div>
			</div>
			</nav>
			<form:form id="uploadForm" method="POST" commandName="uploadModel"
				enctype="multipart/form-data" action="upload"
				class="form-horizontal">
				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10" id="form-message">
								<c:choose>
									<c:when test="${not empty  requestScope.uploadMsg}">
										<span class="success"
											style="margin-left: 20px; font-size: 15px;">${requestScope.uploadMsg }</span>
									</c:when>
									<c:otherwise>
										<form:errors path="*" cssClass="errorblock" element="div" />
									</c:otherwise>
								</c:choose>
							</div>

						</div>

						<div class="form-group">
							<label for="email" class="col-sm-6 control-label"><spring:message
									code="label.email" text="Email Address" /></label>
							<div class="col-sm-5">
								<form:input id="email" readOnly="readOnly" type="email"
									class="form-control" path="email" maxlength="60" size="20" />
							</div>
						</div>
						<div class="form-group">
							<label for="taxonomyId" class="col-sm-6 control-label"><i
								id="taxonomyHelp" title="" class="fa fa-info-circle"
								aria-hidden="true"></i> <spring:message code="label.taxonomy"
									text="Enter NCBI taxonomy ID for your organism" /></label>
							<div class="col-sm-5">
								<form:input id="taxonomyId" class="form-control"
									path="taxonomyId" maxlength="60" size="20" />
							</div>
							<small class="col-sm-offset-6 col-sm-6 text-muted"><spring:message
									code="label.taxonomySearch"
									text="Need to look up your organism's ID? " /><a
								target='_blank' href="https://www.ncbi.nlm.nih.gov/taxonomy"><spring:message
										code="label.clickhere" text="Click Here" /></a></small>
						</div>
						<div class="form-group">
							<label for="pubmedId" class="col-sm-6 control-label"><i
								id="pubmedHelp" title="" class="fa fa-info-circle"
								aria-hidden="true"></i>
							<spring:message code="label.pubmedId"
									text="Enter PubMed ID for associated article" /></label>
							<div class="col-sm-5">
								<form:input id="pubmedId" class="form-control" path="pubmedId"
									maxlength="60" size="20" />
							</div>
							<small class="col-sm-offset-6 col-sm-6 text-muted"><spring:message
									code="label.pubmedSearch"
									text="Need to look up your PubMed Id? " /><a target='_blank'
								href="https://www.ncbi.nlm.nih.gov/pubmed"><spring:message
										code="label.clickhere" text="Click Here" /></a></small>
						</div>
						<div class="col-sm-offset-2 panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">
									<spring:message code="label.geneExpressionMatrix"
										text="Gene Expression Matrix" />
								</h3>
							</div>
							<div class="panel-body">
								<div class="form-group has-feedback">
									<label for="uploadFile" class="col-sm-5 control-label"><spring:message
											code="label.uploadFile" text="Please select file to upload" /></label>
									<div class="col-sm-6">
										<div class="input-group">
											<label class="input-group-addon btn btn-default btn-file">
												<spring:message code="label.selectFile" text="Select File" />
												<form:input id="geneExpression" accept=".csv,.txt,.tsv"
													path="geneExpressionFile" type="file"
													style="display: none;" />
											</label> <input type="text" readOnly="readOnly"
												class="form-control file-radius" /><i
												class="form-control-feedback"
												style="display: none; z-index: 3;"></i>
										</div>
									</div>
									<small class="col-sm-offset-5 col-sm-6 help-block"
										style="display: none;">Please choose a file</small> <small
										class="col-sm-offset-5 col-sm-6 text-muted"><spring:message
											code="label.geneExpFile"
											text="Tab-delimited text file (.csv/.txt/.tsv): genes X conditions. " /><a
										target='_blank'
										href="downloadSamples?fileName=gene_expression"><spring:message
												code="label.DownloadSample" text="Download Sample" /></a></small>
								</div>
								<div class="form-group">
									<label for="correlation" class="col-sm-5 control-label"><spring:message
											code="label.correlation" text="Select correlation to use" /></label>
									<div class="col-sm-3">
										<div class="radio">
											<label> <input type="radio" name="correlation"
												id="correlation1" value="Pearson"> <spring:message
													code="label.pearson" text="Pearson" />
											</label>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="radio">
											<label> <input type="radio" name="correlation"
												id="correlation2" value="BWeightMidcorrelation" checked>
												<spring:message code="label.BWeightMidcorrelation"
													text="Bi Weight Midcorrelation" />
											</label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="networktype" class="col-sm-5 control-label"><spring:message
											code="label.networktype" text="Select your network type" /></label>
									<div class="col-sm-3">
										<div class="radio">
											<label> <input type="radio" name="networktype"
												id="networktype1" value="signed" checked> <spring:message
													code="label.signed" text="Signed" />
											</label>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="radio">
											<label> <input type="radio" name="networktype"
												id="networktype2" value="unsigned"> <spring:message
													code="label.unsigned" text="Unsigned" />
											</label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="threshold" class="col-sm-5 control-label"><spring:message
											code="label.threshold"
											text="Select the soft-thresholding power (Beta-parameter) used to identify gene co-expression modules with WGCNA" /></label>
									<div class="col-sm-5" style="margin-top: 20px;">
										<form:select id="threshold" class="form-control"
											path="threshold">
											<c:forEach begin="1" end="29" varStatus="loop">
												<form:option value="${loop.index}">${loop.index}</form:option>
											</c:forEach>
										</form:select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group has-feedback">
							<label for="geneTable" class="col-sm-6 control-label"><spring:message
									code="label.geneTable" text="Gene Information Table" /></label>
							<div class="col-sm-5">
								<div class="input-group">
									<label class="input-group-addon btn btn-default btn-file">
										<spring:message code="label.selectFile" text="Select File" />
										<form:input id="geneTable" accept=".csv,.txt,.tsv" path="geneTable"
											type="file" style="display: none;" />
									</label> <input type="text" readOnly="readOnly"
										class="form-control file-radius" /><i
										class="form-control-feedback"
										style="display: none; z-index: 3;"></i>
								</div>
							</div>
							<small class="col-sm-offset-6 col-sm-6 help-block"
								style="display: none;">Please choose a file</small> <small
								class="col-sm-offset-6 col-sm-6 text-muted"><spring:message
									code="label.geneTableFile"
									text="Tab-delimited text file (.csv/.txt/.tsv): genes X 6 attributes. " /><a
								target="_blank" href="downloadSamples?fileName=gene_info"><spring:message
										code="label.DownloadSample" text="Download Sample" /></a></small>
						</div>
						<div class="form-group has-feedback">
							<label for="conditionTable" class="col-sm-6 control-label"><spring:message
									code="label.conditionTable"
									text="Condition (sample) Information Table" /></label>
							<div class="col-sm-5">
								<div class="input-group">
									<label class="input-group-addon btn btn-default btn-file">
										<spring:message code="label.selectFile" text="Select File" />
										<form:input id="conditionTable" accept=".csv,.txt,.tsv"
											path="conditionTable" type="file" style="display: none;" />
									</label> <input type="text" readOnly="readOnly"
										class="form-control file-radius" /><i
										class="form-control-feedback"
										style="display: none; z-index: 3;"></i>
								</div>
							</div>
							<small class="col-sm-offset-6 col-sm-6 help-block"
								style="display: none;">Please choose a file</small> <small
								class="col-sm-offset-6 col-sm-6 text-muted"><spring:message
									code="label.conditionTable"
									text="Tab-delimited text file (.csv/.txt/.tsv): condition X 6 attributes. " /><a
								target="_blank"
								href="downloadSamples?fileName=condition_information"><spring:message
										code="label.DownloadSample" text="Download Sample" /></a></small>
						</div>
						<div class="col-sm-offset-2 panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">
									<spring:message code="label.annotations"
										text="Functional Annotation" />
								</h3>
							</div>
							<div class="panel-body">
								<div class="form-group">
									<label for="annotationsText" class="col-sm-9 control-label"><spring:message
											code="label.annotationsText"
											text="Please provide at least one of the following five options:" /></label>
								</div>
								<div class="form-group">
									<label for="keggId" class="col-sm-5 control-label"><i
										id="keggHelp" title="" class="fa fa-info-circle"
										aria-hidden="true"></i>
									<spring:message code="label.keggId"
											text="KEGG Id for your organism" /></label>
									<div class="col-sm-6">
										<form:input id="keggId" class="form-control" path="keggId"
											maxlength="60" size="20" />
									</div>
									<small class="col-sm-offset-5 col-sm-6 help-block"
										style="display: none;">Please provide a valid KEGG Id</small>
									<small class="col-sm-offset-5 col-sm-6 text-muted"><spring:message
											code="label.keggIdSearch"
											text="Need to look up your organism's KEGG ID? " /><a
										target='_blank' href="http://rest.kegg.jp/list/organism"><spring:message
												code="label.clickhere" text="Click Here" /></a></small>
								</div>
								<div class="form-group has-feedback">
									<label for="tuAnnotation" class="col-sm-5 control-label"><spring:message
											code="label.tuAnnotation"
											text="Transcriptional unit annotation file" /></label>
									<div class="col-sm-6" style="margin-top: 10px;">
										<div class="input-group">
											<label class="input-group-addon btn btn-default btn-file">
												<spring:message code="label.selectFile" text="Select File " />
												<form:input id="tuAnnotation" accept=".csv,.txt,.tsv"
													path="tuAnnotationFile" type="file" style="display: none;" />
											</label> <input type="text" readOnly="readOnly"
												class="form-control file-radius" /><i
												class="form-control-feedback"
												style="display: none; z-index: 3;"></i>
										</div>
									</div>
									<small class="col-sm-offset-5 col-sm-6 help-block"
										style="display: none;">Please choose a file</small> <small
										class="col-sm-offset-5 col-sm-7 text-muted"><spring:message
											code="label.tuAnnotation"
											text="Tab-delimited text file (.csv/.txt/.tsv): genes X transcription unit. " /><a
										target="_blank" href="downloadSamples?fileName=gene_tu"><spring:message
												code="label.DownloadSample" text="Download Sample" /></a></small>
								</div>
								<div class="form-group has-feedback">
									<label for="pcAnnotation" class="col-sm-5 control-label"><spring:message
											code="label.pcAnnotation"
											text="Protein complex annotation file" /></label>
									<div class="col-sm-6" style="margin-top: 10px;">
										<div class="input-group">
											<label class="input-group-addon btn btn-default btn-file">
												<spring:message code="label.selectFile" text="Select File " />
												<form:input id="pcAnnotation" accept=".csv,.txt,.tsv"
													path="pcAnnotationFile" type="file" style="display: none;" />
											</label> <input type="text" readOnly="readOnly"
												class="form-control file-radius" /><i
												class="form-control-feedback"
												style="display: none; z-index: 3;"></i>
										</div>
									</div>
									<small class="col-sm-offset-5 col-sm-6 help-block"
										style="display: none;">Please choose a file</small> <small
										class="col-sm-offset-5 col-sm-7 text-muted"><spring:message
											code="label.proteinAnnotation"
											text="Tab-delimited text file (.csv/.txt/.tsv): genes X protein sequence. " /><a
										target="_blank" href="downloadSamples?fileName=gene_protein"><spring:message
												code="label.DownloadSample" text="Download Sample" /></a></small>
								</div>
								<div class="form-group has-feedback">
									<label for="pathwayAnnotation" class="col-sm-5 control-label"><spring:message
											code="label.pathwayAnnotation" text="Pathway annotation file" /></label>
									<div class="col-sm-6">
										<div class="input-group">
											<label class="input-group-addon btn btn-default btn-file">
												<spring:message code="label.selectFile" text="Select File " />
												<form:input id="pathwayAnnotation" accept=".csv,.txt,.tsv"
													path="pathwayAnnotationFile" type="file"
													style="display: none;" />
											</label> <input type="text" readOnly="readOnly"
												class="form-control file-radius" /><i
												class="form-control-feedback"
												style="display: none; z-index: 3;"></i>
										</div>
									</div>
									<small class="col-sm-offset-5 col-sm-6 help-block"
										style="display: none;">Please choose a file</small> <small
										class="col-sm-offset-5 col-sm-7 text-muted"><spring:message
											code="label.pathwayAnnotation"
											text="Tab-delimited text file (.csv/.txt/.tsv): genes X kegg pathway. " /><a
										target="_blank" href="downloadSamples?fileName=gene_metacyc"><spring:message
												code="label.DownloadSample" text="Download Sample" /></a></small>
								</div>
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="form-group has-feedback">
											<label for="pfamAnnotation" class="col-sm-5 control-label"><spring:message
													code="label.pfamAnnotation" text="Pfam annotation file" /></label>
											<div class="col-sm-6">
												<div class="input-group">
													<label class="input-group-addon btn btn-default btn-file">
														<spring:message code="label.selectFile"
															text="Select File " /> <form:input id="pfamAnnotation"
															accept=".csv,.txt,.tsv" path="pfamAnnotationFile" type="file"
															style="display: none;" />
													</label> <input type="text" readOnly="readOnly"
														class="form-control file-radius" /><i
														class="form-control-feedback"
														style="display: none; z-index: 3;"></i>
												</div>
											</div>
											<small class="col-sm-offset-5 col-sm-6 help-block"
												style="display: none;">Please choose a file</small> <small
												class="col-sm-offset-5 col-sm-7 text-muted"><spring:message
													code="label.pfamAnnotation"
													text="Tab-delimited text file (.csv/.txt/.tsv): genes X pfam accession. " /><a
												target="_blank" href="downloadSamples?fileName=gene_pfam"><spring:message
														code="label.DownloadSample" text="Download Sample" /></a></small>
										</div>
										<div class="form-group">
											<label for="or" class="col-sm-6 control-label"><spring:message
													code="label.or" text="OR" /></label>
										</div>
										<div class="form-group has-feedback">
											<label for="proteinSeq" class="col-sm-5 control-label"><spring:message
													code="label.proteinSeq" text="Protein sequence file" /></label>
											<div class="col-sm-6">
												<div class="input-group">
													<label class="input-group-addon btn btn-default btn-file">
														<spring:message code="label.selectFile"
															text="Select File " /> <form:input id="proteinSeq"
															accept=".fasta,.fa,.txt" path="proteinSeqFile" type="file"
															style="display: none;" />
													</label> <input type="text" readOnly="readOnly"
														class="form-control file-radius" /><i
														class="form-control-feedback"
														style="display: none; z-index: 3;"></i>
												</div>
											</div>
											<small class="col-sm-offset-5 col-sm-6 help-block"
												style="display: none;">Please choose a file</small> <small
												class="col-sm-offset-5 col-sm-6 text-muted"><spring:message
													code="label.festaAnnotation"
													text="FASTA file (.fasta/.fa/.txt) with gene symbol in the header. " /><a
												target="_blank" href="downloadSamples?fileName=fasta"><spring:message
														code="label.DownloadSample" text="Download Sample" /></a></small>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group has-feedback">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox" style="font-weight: bold;">
									<input type="checkbox" name="consent" />
									<spring:message code="label.consent1"
										text="I agree to GeNet's "/>
									<a target="_blank" href="contact#privacy"><spring:message
											code="label.consent2"
											text="privacy and data publication policy" /></a>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-4 col-sm-1">
								<button type="submit" class="submit">
									<span><spring:message code="label.submit" text="Submit" /></span>
								</button>
							</div>
							<div class="col-sm-offset-2 col-sm-1">
								<button id="reset" class="submit">
									<span><spring:message code="label.reset" text="Cancel" /></span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
		<div class="row">
			<div class="col-sm-12 centerText">
				&copy; <a href="http://www.mun.ca" target="_blank">Memorial University
					of Newfoundland</a> 2016
			</div>
		</div>
	</div>
	<div></div>
	<script type="text/javascript">
		$(document).on(
				'change',
				':file',
				function() {
					var input = $(this), label = input.val()
							.replace(/\\/g, '/').replace(/.*\//, '');
					input.parent().siblings().val(label);
				});

		var requiredChangeHandler = function() {
			if ($(this).val().length != 0
					&& $(this).val().split('.').pop().toLowerCase() == 'csv') {
				$(this).parents('.has-feedback').removeClass('has-error')
						.addClass('has-success').children('small.help-block')
						.css('display', 'none');
				$(this).parent().siblings('i').removeClass('glyphicon-remove')
						.addClass('glyphicon glyphicon-ok').css('display',
								'block');
			} else {
				$(this).parents('.has-feedback').removeClass('has-success')
						.addClass('has-error').children('small.help-block')
						.css('display', 'block');
				$(this).parent().siblings('i').removeClass('glyphicon-ok')
						.addClass('glyphicon glyphicon-remove').css('display',
								'block');
			}

		};

		$('#geneExpression').change(requiredChangeHandler);
		$('#geneTable').change(requiredChangeHandler);
		$('#conditionTable').change(requiredChangeHandler);

		var optionalChangeHandler = function() {
			var keggId = false;
			var tuAnnotation = false;
			var pcAnnotation = false;
			var pfamAnnotation = false;
			var proteinSeq = false;
			var pathwayAnnotation = false;

			if ($('#keggId').val().length != 0
					&& /^[a-z]+$/i.test($('#keggId').val())) {
				keggId = true;
			}
			if ($('#tuAnnotation').val().length != 0) {
				var fileFormat = $('#tuAnnotation').val().split('.').pop().toLowerCase();
				if(fileFormat == 'csv' || fileFormat == 'txt' || fileFormat == 'tsv')
					tuAnnotation = true;
			}
			if ($('#pcAnnotation').val().length != 0){
				var fileFormat = $('#pcAnnotation').val().split('.').pop().toLowerCase();
				if(fileFormat == 'csv' || fileFormat == 'txt' || fileFormat == 'tsv')
					pcAnnotation = true;
			}
			if ($('#pfamAnnotation').val().length != 0){
				var fileFormat = $('#pfamAnnotation').val().split('.').pop().toLowerCase();
				if(fileFormat == 'csv' || fileFormat == 'txt' || fileFormat == 'tsv')
					pfamAnnotation = true;
			}
			if ($('#proteinSeq').val().length != 0){
				var fileFormat = $('#proteinSeq').val().split('.').pop().toLowerCase();
				if(fileFormat == 'fasta' || fileFormat == 'fa' || fileFormat == 'txt')
					proteinSeq = true;
			}
			if ($('#pathwayAnnotation').val().length != 0){
				var fileFormat = $('#pathwayAnnotation').val().split('.').pop().toLowerCase();
				if(fileFormat == 'csv' || fileFormat == 'txt' || fileFormat == 'tsv')
					pathwayAnnotation = true;
			}
			if (!(keggId || tuAnnotation || pcAnnotation
					|| (pfamAnnotation || proteinSeq) || pathwayAnnotation)) {
				$('#keggId').parents('.has-feedback')
						.removeClass('has-success').addClass('has-error')
						.children('small.help-block').css('display', 'block');
				var blockDiv = $($('#keggId').parents('.has-feedback')
						.children('div')[0]);
				$('#keggId').parents('.has-feedback').children(
						'small.help-block').css('display', 'block');
				$('#keggId').siblings('i').removeClass('glyphicon-ok')
						.addClass('glyphicon glyphicon-remove').css('display',
								'block');

				$('#tuAnnotation').parents('.has-feedback').removeClass(
						'has-success').addClass('has-error').children(
						'small.help-block').css('display', 'block');
				$('#tuAnnotation').parent().siblings('i').addClass(
						'glyphicon-remove').removeClass('glyphicon-ok').css(
						'display', 'block');

				$('#pcAnnotation').parents('.has-feedback').removeClass(
						'has-success').addClass('has-error').children(
						'small.help-block').css('display', 'block');
				$('#pcAnnotation').parent().siblings('i').addClass(
						'glyphicon-remove').removeClass('glyphicon-ok').css(
						'display', 'block');

				$('#pfamAnnotation').parents('.has-feedback').removeClass(
						'has-success').addClass('has-error').children(
						'small.help-block').css('display', 'block');
				$('#pfamAnnotation').parent().siblings('i').addClass(
						'glyphicon-remove').removeClass('glyphicon-ok').css(
						'display', 'block');

				$('#proteinSeq').parents('.has-feedback').removeClass(
						'has-success').addClass('has-error').children(
						'small.help-block').css('display', 'block');
				$('#proteinSeq').parent().siblings('i').addClass(
						'glyphicon-remove').removeClass('glyphicon-ok').css(
						'display', 'block');

				$('#pathwayAnnotation').parents('.has-feedback').removeClass(
						'has-success').addClass('has-error').children(
						'small.help-block').css('display', 'block');
				$('#pathwayAnnotation').parent().siblings('i').addClass(
						'glyphicon-remove').removeClass('glyphicon-ok').css(
						'display', 'block');
			} else {
				$('#keggId').parents('.has-feedback').removeClass('has-error')
						.addClass('has-success').children('small.help-block')
						.css('display', 'none');
				$('#keggId').siblings('i').removeClass('glyphicon-remove')
						.addClass('glyphicon glyphicon-ok').css('display',
								'block');

				$('#tuAnnotation').parents('.has-feedback').addClass(
						'has-success').removeClass('has-error').children(
						'small.help-block').css('display', 'none');
				$('#tuAnnotation').parent().siblings('i').removeClass(
						'glyphicon-remove').addClass('glyphicon glyphicon-ok')
						.css('display', 'block');

				$('#pcAnnotation').parents('.has-feedback').addClass(
						'has-success').removeClass('has-error').children(
						'small.help-block').css('display', 'none');
				$('#pcAnnotation').parent().siblings('i').removeClass(
						'glyphicon-remove').addClass('glyphicon glyphicon-ok')
						.css('display', 'block');

				$('#pfamAnnotation').parents('.has-feedback').addClass(
						'has-success').removeClass('has-error').children(
						'small.help-block').css('display', 'none');
				$('#pfamAnnotation').parent().siblings('i').removeClass(
						'glyphicon-remove').addClass('glyphicon glyphicon-ok')
						.css('display', 'block');

				$('#proteinSeq').parents('.has-feedback').addClass(
						'has-success').removeClass('has-error').children(
						'small.help-block').css('display', 'none');
				$('#proteinSeq').parent().siblings('i').removeClass(
						'glyphicon-remove').addClass('glyphicon glyphicon-ok')
						.css('display', 'block');

				$('#pathwayAnnotation').parents('.has-feedback').addClass(
						'has-success').removeClass('has-error').children(
						'small.help-block').css('display', 'none');
				$('#pathwayAnnotation').parent().siblings('i').removeClass(
						'glyphicon-remove').addClass('glyphicon glyphicon-ok')
						.css('display', 'block');
			}
		};

		$('#keggId').keyup(optionalChangeHandler);
		$('#tuAnnotation').change(optionalChangeHandler);
		$('#pcAnnotation').change(optionalChangeHandler);
		$('#pfamAnnotation').change(optionalChangeHandler);
		$('#proteinSeq').change(optionalChangeHandler);
		$('#pathwayAnnotation').change(optionalChangeHandler);

		$('#uploadForm')
				.bootstrapValidator(
						{
							message : 'Please provide a valid value',
							feedbackIcons : {
								valid : 'glyphicon glyphicon-ok',
								invalid : 'glyphicon glyphicon-remove',
								validating : 'glyphicon glyphicon-refresh'
							},
							fields : {
								taxonomyId : {
									message : 'Please provide a valid Taxonomy Id',
									validators : {
										notEmpty : {
											message : 'Taxonomy Id is required and cannot be empty'
										},
										numeric : {
											message : 'Only numbers are allowed'
										}
									}
								},
								pubmedId : {
									message : 'Please provide a valid PubMed Id',
									validators : {
										notEmpty : {
											message : 'PubMed Id is required and cannot be empty'
										},
										numeric : {
											message : 'Only numbers are allowed'
										}
									}
								},
								keggId : {
									message : 'Please provide a valid KEGG Id',
									validators : {
										regexp : {
											regexp : /^[a-z]+$/i,
											message : 'KEGG id can consist of alphabetical characters only'
										}
									}
								},
								consent : {
									validators : {
										notEmpty : {
											message : 'Consent is required'
										}
									}
								}
							}
						})
				.on(
						'success.form.bv',
						function(event) {
							var geneExpression = false;
							var geneTable = false;
							var conditionTable = false;
							if ($('#geneExpression').val().length != 0){
								var fileFormat = $('#geneExpression').val().split('.').pop().toLowerCase();
								if(fileFormat == 'csv' || fileFormat == 'txt' || fileFormat == 'tsv')
									geneExpression = true;
							} else {
								$('#geneExpression').parents('.has-feedback')
										.addClass('has-error').children(
												'small.help-block').css(
												'display', 'block');
								$('#geneExpression').parent().siblings('i')
										.addClass('glyphicon glyphicon-remove')
										.css('display', 'block');
							}

							if ($('#geneTable').val().length != 0){
								var fileFormat = $('#geneTable').val().split('.').pop().toLowerCase();
								if(fileFormat == 'csv' || fileFormat == 'txt' || fileFormat == 'tsv')
									geneTable = true;
							} else {
								$('#geneTable').parents('.has-feedback')
										.addClass('has-error').children(
												'small.help-block').css(
												'display', 'block');
								$('#geneTable').parent().siblings('i')
										.addClass('glyphicon glyphicon-remove')
										.css('display', 'block');
							}

							if ($('#conditionTable').val().length != 0) {
								var fileFormat = $('#conditionTable').val().split('.').pop().toLowerCase();
								if(fileFormat == 'csv' || fileFormat == 'txt' || fileFormat == 'tsv')
									conditionTable = true;
							} else {
								$('#conditionTable').parents('.has-feedback')
										.addClass('has-error').children(
												'small.help-block').css(
												'display', 'block');
								$('#conditionTable').parent().siblings('i')
										.addClass('glyphicon glyphicon-remove')
										.css('display', 'block');
							}

							var keggId = false;
							var tuAnnotation = false;
							var pcAnnotation = false;
							var pfamAnnotation = false;
							var proteinSeq = false;
							var pathwayAnnotation = false;

							if ($('#keggId').val().length != 0) {
								keggId = true;
							}

							if ($('#tuAnnotation').val().length != 0){
								var fileFormat = $('#tuAnnotation').val().split('.').pop().toLowerCase();
								if(fileFormat == 'csv' || fileFormat == 'txt' || fileFormat == 'tsv')
								tuAnnotation = true;
							}

							if ($('#pcAnnotation').val().length != 0){
								var fileFormat = $('#pcAnnotation').val().split('.').pop().toLowerCase();
								if(fileFormat == 'csv' || fileFormat == 'txt' || fileFormat == 'tsv')
								pcAnnotation = true;
							}

							if ($('#pfamAnnotation').val().length != 0){
								var fileFormat = $('#pfamAnnotation').val().split('.').pop().toLowerCase();
								if(fileFormat == 'csv' || fileFormat == 'txt' || fileFormat == 'tsv')
								pfamAnnotation = true;
							}

							if ($('#proteinSeq').val().length != 0){
								var fileFormat = $('#proteinSeq').val().split('.').pop().toLowerCase();
								if(fileFormat == 'fasta' || fileFormat == 'txt' || fileFormat == 'fa')
								proteinSeq = true;
							}

							if ($('#pathwayAnnotation').val().length != 0){
								var fileFormat = $('#pathwayAnnotation').val().split('.').pop().toLowerCase();
								if(fileFormat == 'csv' || fileFormat == 'txt' || fileFormat == 'tsv')
									pathwayAnnotation = true;
							}

							if (!(keggId || tuAnnotation || pcAnnotation
									|| (pfamAnnotation || proteinSeq) || pathwayAnnotation)) {
								$('#keggId').parents('.has-feedback')
										.removeClass('has-success').addClass(
												'has-error').children(
												'small.help-block').css(
												'display', 'block');
								var blockDiv = $($('#keggId').parents(
										'.has-feedback').children('div')[0]);
								blockDiv.children('i').removeClass(
										'glyphicon-ok').addClass(
										'glyphicon-remove');
								$('#keggId').parent().siblings('i').css(
										'display', 'block');

								$('#tuAnnotation').parents('.has-feedback')
										.addClass('has-error').children(
												'small.help-block').css(
												'display', 'block');
								$('#tuAnnotation').parent().siblings('i')
										.addClass('glyphicon glyphicon-remove')
										.css('display', 'block');

								$('#pcAnnotation').parents('.has-feedback')
										.addClass('has-error').children(
												'small.help-block').css(
												'display', 'block');
								$('#pcAnnotation').parent().siblings('i')
										.addClass('glyphicon glyphicon-remove')
										.css('display', 'block');

								$('#pfamAnnotation').parents('.has-feedback')
										.addClass('has-error').children(
												'small.help-block').css(
												'display', 'block');
								$('#pfamAnnotation').parent().siblings('i')
										.addClass('glyphicon glyphicon-remove')
										.css('display', 'block');

								$('#proteinSeq').parents('.has-feedback')
										.addClass('has-error').children(
												'small.help-block').css(
												'display', 'block');
								$('#proteinSeq').parent().siblings('i')
										.addClass('glyphicon glyphicon-remove')
										.css('display', 'block');

								$('#pathwayAnnotation')
										.parents('.has-feedback').addClass(
												'has-error').children(
												'small.help-block').css(
												'display', 'block');
								$('#pathwayAnnotation').parent().siblings('i')
										.addClass('glyphicon glyphicon-remove')
										.css('display', 'block');
							}

							if (!(geneExpression && geneTable && conditionTable && (keggId
									|| tuAnnotation
									|| pcAnnotation
									|| (pfamAnnotation || proteinSeq) || pathwayAnnotation))) {
								event.preventDefault();
							}
						});

		$('#reset').click(function() {
			window.location.href = 'jobHomePage';
		});
	</script>
</body>
</html>