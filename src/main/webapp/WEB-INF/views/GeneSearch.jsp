<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gene of interest</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="resources/js/interaction.js"></script>
<script type="text/javascript" src="resources/js/jquery.blockUI.js"></script>
<script type="text/javascript"
	src="resources/js/d3.v2_diagramDrawer-V2.js"></script>
<script type="text/javascript" src="resources/js/cytoscapeString.js"></script>
<script type="text/javascript" src="resources/js/cytoscape_web/json2.js"></script>
<script type="text/javascript"
	src="resources/js/cytoscape_web/AC_OETags.js"></script>
<script type="text/javascript"
	src="resources/js/cytoscape_web/cytoscapeweb.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/table.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />

<style type="text/css">
.ui-widget-overlay {
	position: fixed !important;
}
</style>
<%
	String[] recievedData = (String[]) request.getAttribute("geneInfo");
	String[][] expreInfo = (String[][]) request
			.getAttribute("expressionInfo");
	String[][] expreAVGinfo = (String[][]) request
			.getAttribute("avrageExpInfo");
	Integer noOfGenes = (Integer) request.getAttribute("noOfGenes");
	// String ntwrkSize=request.getAttribute("networkSize").toString();
	String moduleConditionValues = request.getAttribute(
			"moduleCondValues").toString();
	String category = (String) request.getAttribute("category");
	//--------

	String[] nodeSchema = (String[]) request
			.getAttribute("net_nodeSchema");
	String[] edgeSchema = (String[]) request
			.getAttribute("net_edgeSchema");
	String[][] networkFullData = (String[][]) request
			.getAttribute("networkData");
	String[][] networkStyleData = (String[][]) request
			.getAttribute("networkStyleData");
	String[][] ConnectedNodesData = (String[][]) request
			.getAttribute("ConnectedNodesData");

	//--------
	String[][] conditionNames = (String[][]) request
			.getAttribute("conditionsAndRanks");
	//String[] conditionNames=new String[expreAVGinfo.length];
	//for(int i=0; i<expreAVGinfo.length;i++)
	//{
	//	conditionNames[i]=expreAVGinfo[i][0];
	//}
	String info = recievedData[5];
%>
<script>
//the following variable store the height of the DIV. this value is used when the div is going to be maximized
var graphContainer_height=0;
var expreContainer_height=0;
var annotationContainer_height=0;
//min_max_DIV gets the div_id that should be minimized/maximized; the height of the container; and the bottom, blue line of the associated btn in the header.
function min_max_DIV(id, heightDIV, btnID)
{

	if(document.getElementById(id).style.visibility!='hidden')
	{
		$("#"+id).animate({height:"1px"});
		document.getElementById(id).style.visibility='hidden';
		document.getElementById(btnID).style.visibility='hidden';
		if(id=='annotationContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Expand Annotation Table');
			$('#' + btnID + ' img').attr('title', 'Expand Annotation Table');
			$('#annotationHeader img').attr('src', 'resources/images/plus.png').attr('height', '12px').attr('title', 'Expand Annotation Table');
		} else if(id=='expreContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Expand Expression Profile');
			$('#' + btnID + ' img').attr('title', 'Expand Expression Profile');
			$('#expreHeader img').attr('src', 'resources/images/plus.png').attr('height', '12px').attr('title', 'Expand Expression Profile');
		} else if(id=='graphContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Expand Gene Network');
			$('#' + btnID + ' img').attr('title', 'Expand Gene Network');
			$('#graphHeader img').attr('src', 'resources/images/plus.png').attr('height', '12px').attr('title', 'Expand Gene Network');
		}
	}
	else
	{
		$("#"+id).animate({height:heightDIV});
		document.getElementById(id).style.visibility='visible';
		document.getElementById(btnID).style.visibility='visible';
		if(id=='annotationContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Collapse Annotation Table');
			$('#' + btnID + ' img').attr('title', 'Collapse Annotation Table');
			$('#annotationHeader img').attr('src', 'resources/images/min.png').attr('height', '4px').attr('title', 'Collapse Annotation Table');
		} else if(id=='expreContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Collapse Expression Profile');
			$('#' + btnID + ' img').attr('title', 'Collapse Expression Profile');
			$('#expreHeader img').attr('src', 'resources/images/min.png').attr('height', '4px').attr('title', 'Collapse Expression Profile');
		} else if(id=='graphContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Collapse Gene Network');
			$('#' + btnID + ' img').attr('title', 'Collapse Gene Network');
			$('#graphHeader img').attr('src', 'resources/images/min.png').attr('height', '4px').attr('title', 'Collapse Gene Network');
		}
	}
	
};

//================================================================================
$(function() {
	btnDIVWidth=240;
	geneBTNClicked();
	//=========================================================
	// store the initial height of the div containers in the assoiciated variables. Note that this height must be set directly in the html not in the css file.
	annotationContainer_height=document.getElementById('annotationContent').style.height;
	document.getElementById('annotationHeader').onclick=function() {min_max_DIV('annotationContent', annotationContainer_height,'annoBTN_line');};
	document.getElementById('annoBTN').onclick=function() {min_max_DIV('annotationContent', annotationContainer_height,'annoBTN_line');};
	document.getElementById('annoBTN_line').onclick=function() {min_max_DIV('annotationContent', annotationContainer_height,'annoBTN_line');};
	<%if (networkFullData != null && networkFullData.length > 0) {%>
		graphContainer_height=document.getElementById('graphContent').style.height;
		document.getElementById('graphHeader').onclick=function() {min_max_DIV('graphContent', graphContainer_height,'graphBTN_line');};
		document.getElementById('graphBTN').onclick=function() {min_max_DIV('graphContent', graphContainer_height,'graphBTN_line');};
		document.getElementById('graphBTN_line').onclick=function() {min_max_DIV('graphContent', graphContainer_height,'graphBTN_line');};
	<%} if(expreInfo != null && expreInfo.length > 0) {%>
		expreContainer_height=document.getElementById('expreContent').style.height;
		document.getElementById('expreHeader').onclick=function() {min_max_DIV('expreContent', expreContainer_height,'expreBTN_line');};
		document.getElementById('expreBTN').onclick=function() {min_max_DIV('expreContent', expreContainer_height,'expreBTN_line');};
		document.getElementById('expreBTN_line').onclick=function() {min_max_DIV('expreContent', expreContainer_height,'expreBTN_line');};
	<%} if (networkFullData == null || ( networkFullData != null && networkFullData.length == 0)) {%>	
			document.getElementById('graphContent').remove();
			$('#graphHeader img').attr('src', '');
			$(document.getElementById('geneNumbers')).text("Graph Representation: No Data Available");
	<%} if (expreInfo == null || ( expreInfo != null && expreInfo.length == 0)) {%>	
		document.getElementById('expreContent').remove();;
		$('#expreHeader img').attr('src', '');
		$(document.getElementById('experssionHeaderLabel')).text("Expression Profile: No Data Available");
	<%}%>
	$('#diagram').scroll(function() {
		$('#diagramTableGeneSearch').scrollLeft($(this).scrollLeft());
	});
	
	$('#diagramTableGeneSearch').scroll(function() {
		$('#diagram').scrollLeft($(this).scrollLeft());
	});
});
//===========================================================

var moduleButtonClicked = false;
var btnDIVWidth=650;

function geneBTNClicked()
{
	var difference=(640-btnDIVWidth)/2;
	btnDIVWidth=640;
	var destination=btnDIVWidth/2-70;
	var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left",(prevLeft+difference)+"px");
	$("#topArrow").animate({"left":destination+"px"}, "slow");
	$("#pathwaySearchForm").css("display","none");
	$("#moduleSearchForm").css("display","none");
	$("#downloadForm").css("display","none");
	$("#geneSearchForm").css("display","table");
	moduleButtonClicked = false;
}
function moduleBTNClicked()
{
	if(moduleButtonClicked) {
		$("#moduleGoBTN").trigger( "click" );
	} else {
		moduleButtonClicked=!moduleButtonClicked;
		var difference=(280-btnDIVWidth)/2;
		btnDIVWidth=280;
		var destination=btnDIVWidth/2+35;
		var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
		$("#topArrow").css("left",(prevLeft+difference)+"px");
		$("#topArrow").animate({"left":destination+"px"}, "slow");
		$("#geneSearchForm").css("display","none");
		$("#pathwaySearchForm").css("display","none");
		$("#downloadForm").css("display","none");
		$("#moduleSearchForm").css("display","table");
		moduleButtonClicked = true;
	}
}

function downloadBTNClicked() {	
	if(moduleButtonClicked) {
		$("#downloadGoBTN").trigger( "click" );
	} else {
		var difference=(640-btnDIVWidth)/2;
		btnDIVWidth=640;
		var destination=btnDIVWidth/2-50;
		var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
		$("#topArrow").css("left",(prevLeft+difference)+"px");
		$("#topArrow").animate({"left":destination+"px"}, "slow");
		$("#pathwaySearchForm").css("display","none");
		$("#moduleSearchForm").css("display","none");
		$("#geneSearchForm").css("display","none");
		$("#downloadForm").css("display","table");		
		moduleButtonClicked = true;
	}
}

function pathwayBTNClicked()
{
	var difference=(625-btnDIVWidth)/2;
	btnDIVWidth=625;
	var destination=btnDIVWidth/2+0;
	var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left",(prevLeft+difference)+"px");
	$("#topArrow").animate({"left":destination+"px"}, "slow");
	$("#geneSearchForm").css("display","none");
	$("#moduleSearchForm").css("display","none");
	$("#downloadForm").css("display","none");
	$("#pathwaySearchForm").css("display","table");
	moduleButtonClicked = false;
}
</script>

</head>

<body style="width: 100%" onload="initializeDocument()">

	<div align="center">
		<div id="main">
			<div id="logo">
				<a href="home"> <img src="resources/images/Genet.png" /></a>
			</div>
			<div id="headContainer" align="center">
				<div class="header-menu">
				<table id="header_tb" class="noSpace">
					<tr>
						<td align="center" style="vertical-align: top;">
							<div id="headerBTNS">
								<div id="headBTNContainer" align="center">
									<table
										style="border-collapse: collapse; padding: 0px; margin: 0px;"
										cellpadding="0px" cellspacing="0px">
										<tr>
											<td align="center">
												<div id="btnCont" style="padding-top: 3px">
													<table id="btns_tbl"
														style="border-collapse: collapse; padding: 0px; margin: 0px; margin-top: 10px;"
														cellpadding="0px" cellspacing="0px">
														<tr>
															<td>
																<div id="geneDIV" onclick="geneBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px">
																	<img src="resources/images/gene_h.png"
																		title="Search Gene" />
																</div>
															</td>
															<td>
																<div id="moduleDIV" onclick="moduleBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/module_h.png"
																		title="Browse Module" />
																	<!-- <form id="moduleSubmit" method="post" action="Module_of_Interest"> -->
																	<!-- </form> -->
																</div>
															</td>
															<td>
																<div id="pathwayDIV" onclick="pathwayBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/pathway_h.png"
																		title="Search Pathway" />
																</div>
															</td>
															<td>
																<div id="downloadDIV" onclick="downloadBTNClicked()"
																	style="padding-bottom: 3px; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/download_h.png"
																		title="Download Data" />
																</div>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div id="bottomLineBTN"
													style="border-bottom: 2px solid #FFFFFF;">
													<div align="left">
														<img id="topArrow"
															style="vertical-align: bottom; text-align: center; position: relative; bottom: -2px; left: 0px"
															src="resources/images/arrowTop.png" />
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div id="geneSearchForm"
													style="display: none; text-align: center">
													<form id="geneSubmit" method="post"
														action="Gene_of_Interest">
														<table id="formContainer_tb" style="width: 650px"
															class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 94px">Find Genes by</p>
																</td>

																<td width="100px"><select id="geneId" name="id">
																		<option>Symbol</option>
																		<option>Systematic ID</option>
																		<option>Entrez ID</option>
																</select></td>
																<td width="100px"><input autocomplete="off"
																	id="geneValue" name="value" list="genedatalist"
																	type="text" /> <datalist id="genedatalist"></datalist>
																</td>
																<td align="left">
																	<p style="width: 76px">in organism</p>
																</td>
																<td width="90px"><select class="btn"
																	id="geneCategory" name="category" style="width: 150px;">
																		<option>------Please Select------</option>
																</select></td>

																<td width="20px">
																	<button id="geneGoBTN" type="submit" class="btn goBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>
																</td>
																<td width="10px" align="center"></td>
															</tr>
														</table>
													</form>
												</div>
												<div id="moduleSearchForm" style="display: none">
													<form id="moduleSubmit" method="post"
														action="Module_of_Interest">
														<table id="moduleContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 110px">Select organism</p>
																</td>
																<td><select class="btn" id="moduleCategory"
																	name="category" style="width: 250px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="moduleGoBTN" type="submit"
																		class="btn goBTN moduleBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>

																</td>
															</tr>
														</table>
													</form>
												</div>
												<div id="pathwaySearchForm" style="display: none">
													<form id="pathwaySubmit" method="post"
														action="Pathway_Representation">
														<table id="pathwayContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 94px">Find pathways</p>
																</td>
																<td width="210px"><input style="width: 210px"
																	id="pathwayValue" name="value" type="text" /></td>
																<td align="center" width="80px">
																	<p style="width: 80px">in organism</p>
																</td>

																<td width="90px"><select class="btn"
																	id="pathwayCategory" name="category"
																	style="width: 150px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="pathwayGoBTN" type="submit"
																		class="btn goBTN">
																		<p style="visibility: inherit" class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>
																</td>
															</tr>
														</table>
													</form>
												</div>
												<div id="downloadForm" style="display: none">
													<form id="downloadSubmit" method="get"
														action="downloadData">
														<table id="downloadContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 110px">Select organism</p>
																</td>
																<td><select class="btn" id="downloadCategory"
																	name="category" style="width: 250px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="downloadGoBTN" type="submit"
																		class="btn goBTN moduleBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>

																</td>
															</tr>
														</table>
													</form>
												</div>
											</td>
										</tr>
									</table>

								</div>
							</div>
						</td>
						<td align="right" class="module-buttons">
							<table class="noSpace">
								<tr>
									<td>
										<div id="expreBTN">
											<img src="resources/images/expr.png"
												title="Collapse Expression Profile" align="middle" />
										</div>
									</td>
									<td width="10px"></td>
									<td>
										<div id="graphBTN">
											<img src="resources/images/graph.png"
												title="Collapse Gene Network" align="middle" />
										</div>
									</td>
									<td width="10px"></td>
									<td>
										<div id="annoBTN">
											<img src="resources/images/anno3.png"
												title="Collapse Annotation Table" align="middle" />
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div id="expreBTN_line">
											<img src="resources/images/line.png"
												title="Collapse Expression Network" align="middle"
												width="32px" height="12px" />
										</div>
									</td>
									<td></td>
									<td>
										<div id="graphBTN_line">
											<img src="resources/images/line.png"
												title="Collapse Gene Network" align="middle" width="32px"
												height="12px" />
										</div>
									</td>
									<td></td>
									<td>
										<div id="annoBTN_line">
											<img src="resources/images/line.png"
												title="Collapse Annotation Table" align="middle"
												width="33px" height="12px" />
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				</div>
			</div>
			<div class="main-menu">
					<nav class="horizontal-nav1 ">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="login">Data Contributor Login</a></li>
						<li><a href="help">Help</a></li>
					</ul>
					</nav>
			</div>
			<div class="no-content"></div>
			<div id="page-contents" style="display: none;">
			<div class="noSpace" id="contents">


				<div id="annotationHeader">
					<table id="aHead_tb" class="noSpace" align="center">
						<tr>
							<td class="paddingLeft" align="left"><label class="noSpace">Annotation
									table</label></td>
							<td align="right" style="padding-right: 20px"><img
								class="noSpace" src="resources/images/min.png"
								title="Collapse Annotation Table" width="12px" height="4px" /></td>
						</tr>
					</table>
				</div>
				<div id="annotationContent"
					style="height: 305px; background-color: #f3f3f3;" class="noSpace">
					<div align="center">
						<table id="bgTable" width="1100px" cellpadding="0" cellspacing="0">
							<tr>
								<td align="center">
									<p class="title">
										<strong>Gene Information</strong>
									</p>
								</td>
								<td id="centralLeft" width="11px"></td>
								<td id="centralRight" width="11px"></td>
								<td width="60%">
									<p class="title">
										<strong>Annotations Table</strong>
									</p>
								</td>

							</tr>
							<tr>
								<td align="center"
									style="padding-left: 4px; vertical-align: top">
									<table cellpadding="0" cellspacing="1" border="0"
										class="nospace">
										<tr>
											<td>
												<p class="title2">Symbol:</p>
											</td>
											<td>
												<p id="symbolValue" class="valueTXT"><%=info.split(",")[0]%></p>
											</td>
										</tr>
										<tr>
											<td>
												<p class="title2">Systematic ID:</p>
											</td>
											<td>
												<p id="systematicIDValue" class="valueTXT"><%=info.split(",")[2]%></p>
											</td>
										</tr>
										<tr>
											<td>
												<p class="title2">Entrez ID:</p>
											</td>
											<td>
												<p id="entrezIDValue" class="valueTXT"><%=info.split(",")[1]%></p>
											</td>
										</tr>
										<tr>
											<td>
												<p class="title2">Type:</p>
											</td>
											<td>
												<p id="typeValue" class="valueTXT"><%=info.split(",")[4]%></p>
											</td>
										</tr>
										<tr>
											<td>
												<p class="title2">Description:</p>
											</td>
											<td>
												<p id="descValue" style="max-width: 300px" class="valueTXT"><%=info.split(",")[3]%></p>
											</td>
										</tr>
										<tr>
											<td>
												<p class="title2">Module Name:</p>
											</td>
											<td id="moduleName" style="max-width: 300px"></td>
										</tr>
										<tr>
											<td>
												<p class="title2">More info:</p>
											</td>
											<td>
												<p id="moreValue" style="max-width: 300px" class="valueTXT">
													<% if(!((info.split(",")[5]).trim().isEmpty())) {%>
														<a target="_blank" href="<%=info.split(",")[5]%>">NCBI</a>
													<%}%>
												</p>
											</td>
										</tr>
									</table>
								</td>
								<td></td>
								<td></td>
								<td width="60%"
									style="padding-bottom: 10px; margin-bottom: 10px; vertical-align: top">
									<table
										style="border-collapse: collapse; margin: 0px; padding: 0px;"
										cellpadding="0px" cellspacing="0px">
										<tr>
											<td>
												<table id="geneAnnoHead" width="100%" align="center">
													<thead>
														<tr>
															<th align="center" width="85px" scope="col"
																style="border-right: 30px solid #f3f3f3;">Source</th>
															<th align="center" scope="col">Annotation</th>

														</tr>
													</thead>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<div
													style="height: 195px; width: 650px; padding-left: 20px; overflow-y: auto; overflow-x: hidden">
													<table id="geneAnno" width="650px" align="center">
														<tbody>
															<tr>
																<td>Pfam</td>
																<td id="pfam"><p><%=recievedData[0]%></p></td>

															</tr>
															<tr>
																<td>KEGG</td>
																<td id="kegg"><p><%=recievedData[1]%></p></td>

															</tr>
															<tr>
																<td>MetaCyc</td>
																<td id="metacyc"><p><%=recievedData[4]%></p></td>

															</tr>
															<tr>
																<td>Transcription Unit (TU)</td>
																<td id="tu"><p><%=recievedData[2]%></p></td>

															</tr>
															<tr>
																<td>Protein Complex (PC)</td>
																<td id="pc"><p><%=recievedData[3]%></p></td>

															</tr>
														</tbody>
													</table>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
				</div>

				<div id="expreHeader">
					<table id="eHead_tb" class="noSpace" align="center">
						<tr>
							<td class="paddingLeft" align="left"><label
								id="experssionHeaderLabel" class="noSpace">Expression
									Profile: Log-ratio / Z-score</label></td>
							<td align="right" style="padding-right: 20px"><img
								class="noSpace" src="resources/images/min.png"
								title="Collapse Expression Network" width="12px" height="4px" /></td>
						</tr>
					</table>
				</div>
				<div id="expreContent" class="noSpace"
					style="height: 600px; padding: 0px; margin: 0px; background-color: #f3f3f3"
					align="center">
					<table style="border-collapse: collapse">
						<tr>
							<td style="vertical-align: top;">
								<table>
									<tr>
										<td>Conditions:</td>
									</tr>
									<tr>
										<td>
											<div id="conditionList" align="left"
												style="overflow: scroll; height: 572px; width: 200px;"></div>
										</td>
									</tr>
								</table>
							</td>
							<td>
								<div style="margin-left: 20px;">
									Max Graph Score: <select id="maxdatavalue"
										class="positivenumbercombo"></select>
								</div>
								<div id="diagram" style="overflow-x: scroll;"></div>
								<div id="diagramTableGeneSearch" style="overflow-x: scroll;"></div>
							</td>
						</tr>
					</table>

				</div>
			</div>

			<div id="graphHeader">
				<table id="gHead_tb" class="noSpace" align="center">
					<tr>
						<td id="graphName" class="paddingLeft" align="left"><label
							id="geneNumbers" class="noSpace">Graph Representation: </label></td>
						<td align="right" style="padding-right: 20px"><img
							class="noSpace" src="resources/images/min.png"
							title="Collapse Gene Network" width="12px" height="4px" /></td>
					</tr>
				</table>
			</div>

			<div id="graphContent" class="noSpace" style="height: 605px"
				align="right">
				<div id="cytoscapeweb" style="height: 600px; z-index: 0"></div>
				<div id="undoDiv"
					style="position: relative; z-index: 2; top: -395px; left: -8px; background-color: #FFF; border-radius: 15px; width: 166px; height: 35px; border: 2px solid #464646;">
					<table class="nospace" width="130px">
						<tr>
							<td><img id="closeUndo" src="resources/images/cb.png"
								onclick="invisUndo()"></img></td>
							<td>
								<button class="submit" style="margin: 0px;"
									onclick="undoClicked()" title="Go back to previous state">
									<span>Undo</span>
								</button>
							</td>
						</tr>
					</table>
				</div>
				<div
					style="position: relative; z-index: 1; top: -392px; left: -8px; background-color: #FFF; border-radius: 15px; width: 166px; height: 100px; border: 2px solid #464646;">
					<div>
						<div align="center">
							<table style="width: 100%;">
								<tr style="height: 30px">
									<td colspan="2" align="center">
										<p>Reset gene network</p> <input id="noOfGenes" type="hidden"
										value="<%=noOfGenes%>" />
									</td>
								</tr>
								<tr style="height: 20px">
									<td colspan="2">
										<div id="geneSlider" style="margin: 0 6% 0 5%;"></div>
									</td>
								</tr>
								<tr style="height: 10px">
									<td colspan="2">
										<div>
											<span style="margin-right: 10%;">20</span> <span
												style="margin-right: 10%;">40</span> <span
												style="margin-right: 10%;">60</span> <span
												style="margin-right: 5%;">80</span> <span>100</span>
										</div>
									</td>
								</tr>
								<tr style="height: 10px">
									<td colspan="2" align="center">
										<p>
											# of Genes:
											<%=noOfGenes%></p>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div
					style="position: relative; z-index: 1; top: -390px; left: -8px; background-color: #FFF; border-radius: 15px; width: 166px; height: 180px; border: 2px solid #464646;">
					<div>
						<div align="center">
							<table>
								<tr style="height: 6px">
									<td colspan="2" align="center">
										<p>Correlation filter</p>
									</td>
								</tr>

								<tr style="height: 40px">
									<td colspan="2" align="center">value &ge;<input
										id="corr_valueBigger" value="0"
										title="Enter a positive value between 0 and 1"
										style="width: 60px" />
									</td>
								</tr>

								<tr align="center">
									<td colspan="2">or</td>
								</tr>

								<tr style="height: 40px">
									<td colspan="2" align="center">value &le;<input
										id="corr_valueLower" value="0"
										title="Enter a negative value between -1 and 0"
										style="width: 60px" />
									</td>
								</tr>

								<tr>
									<td align="center">
										<button class="submit" style="margin: 0px;"
											onclick="clearCorrelation()" title="Clear">
											<span>Clear</span>
										</button>
									</td>
									<td>
										<button class="submit" style="margin: 0px;"
											onclick="updateClicked()"
											title="Update the network based on the entered correlation values">
											<span>Update</span>
										</button>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	<input id="category" type="hidden" value="<%=category%>" />
	<div id="dialogs" style="visibility: hidden;">
		<div id="dialog-message" title="Required data missing">
			<p>Please select an organism</p>
		</div>
		<div id="dialog-message1" title="Required data missing">
			<p>Please provide value for search criteria</p>
		</div>
		<div id="dialog-message2" title="Required data missing">
			<p>Please enter a valid identifier</p>
		</div>
	</div>
	<div class="modal"></div>
	<script type="text/javascript">
//convert java array to javascript array
var columns_and_rank=new Array();
<%if (conditionNames != null && conditionNames.length > 0) {%>
columns_and_rank=new Array(<%=conditionNames.length%>);
<%for (int i = 0; i < conditionNames.length; i++) {%>
columns_and_rank[<%=i%>]=new Array(3);
columns_and_rank[<%=i%>][0]="<%=conditionNames[i][0]%>";	
columns_and_rank[<%=i%>][1]=1;<%--"<%=conditionNames[i][1]%>";--%>
columns_and_rank[<%=i%>][2]=1;
columns_and_rank[<%=i%>][3]="<%=conditionNames[i][1]%>";
columns_and_rank[<%=i%>][4]="<%=conditionNames[i][2]%>";
columns_and_rank[<%=i%>][5]="<%=conditionNames[i][3]%>";
columns_and_rank[<%=i%>][6]="<%=conditionNames[i][4]%>";
columns_and_rank[<%=i%>][7]="<%=conditionNames[i][5]%>";
<%}%>
<%}%>
var expreInfo_js=[];
<%if (expreInfo != null && expreInfo.length > 0) {%>
<%for (int j = 0; j < expreInfo.length; j++) {%>
var temp=[];
temp[0]="<%=expreInfo[j][0]%>";
temp[1]="<%=expreInfo[j][1]%>";
expreInfo_js[<%=j%>]=temp;
<%}%>
<%}%>
var expreAVGinfo_js=[];
<%if (expreInfo != null && expreInfo.length > 0) {%>
<%for (int j = 0; j < expreInfo.length; j++) {%>
var temp2=[];
temp2[0]="<%=expreAVGinfo[j][0]%>";
temp2[1]="<%=expreAVGinfo[j][1]%>";
expreAVGinfo_js[<%=j%>]=temp2;
<%}%>
<%}%>
//store the shown columns
var shownCol=[];
columns_and_rank=columns_and_rank.sort(function(a,b){return b[1]-a[1];});
//if the staandard condition is in the list, remove it from the list
if(columns_and_rank[columns_and_rank.length-1][1]==0)
	{
		columns_and_rank=columns_and_rank.slice(0,columns_and_rank.length-2);
	}
for(var k=0; k<columns_and_rank.length;k++)
	{
		shownCol.push(columns_and_rank[k][0]);
	}

//_________________________________________________________________________________________
//get window width
function GetWidth()
{
        var x = 1000;
        if (self.innerHeight)
        {
                x = self.innerWidth;
        }
        else if (document.documentElement && document.documentElement.clientHeight)
        {
                x = document.documentElement.clientWidth;
        }
        else if (document.body)
        {
                x = document.body.clientWidth;
        }
        return x;
}

//------------------drawing the diagram--------------------------------------------

var windowWidth=GetWidth();
var width=windowWidth-200 - 40;
var diagramWidth = 35 * (shownCol.length -1);
diagramWidth = diagramWidth>width?diagramWidth:width;
$('#diagram').width(width);
$('#diagramTableGeneSearch').width(width + 15);
//--------------------
//alert(conditionsValues_js);
setGraphLimits(4, -4);
initiailize("#diagram","#diagramTableGeneSearch",diagramWidth,480,shownCol,120,"");
addGraph("average",expreAVGinfo_js);
addGraph("<%=recievedData[6]%>",expreInfo_js);
update();
populateConditionList();
//__________________________________________________________________________________________
//---------------------------draw Network (Graph) ---------------------------------
var div_id = "cytoscapeweb";

var options = {
// the location of Cytoscape Web SWF
swfPath: "resources/swf/CytoscapeWeb",
// where you have the Flash installer SWF
flashInstallerPath: "resorces/swf/playerProductInstall"
};

var vis = new org.cytoscapeweb.Visualization(div_id, options);
//.............-convert java arr to js arr for networkFullData-..........................
var networkFullData_js=new Array();
<%if (networkFullData != null && networkFullData.length > 0) {%>
networkFullData_js=new Array(<%=networkFullData.length%>);
<%for (int i = 0; i < networkFullData.length; i++) {%>
	networkFullData_js[<%=i%>]=new Array(<%=networkFullData[i].length%>);
	<%for (int j = 0; j < networkFullData[i].length; j++) {%>
		networkFullData_js[<%=i%>][<%=j%>]="<%=networkFullData[i][j].toString()%>";
		
		<%}%>
<%}%>
<%}%>
//.......................................................................................
//.............-convert java arr to js arr for networkStyleData-..........................
var networkStyleData_js=new Array();
<%if (networkStyleData != null && networkStyleData.length > 0) {%>
networkStyleData_js=new Array(<%=networkStyleData.length%>);
<%for (int i = 0; i < networkStyleData.length; i++) {%>
	networkStyleData_js[<%=i%>]=new Array(<%=networkStyleData[i].length%>);
	<%for (int j = 0; j < networkStyleData[i].length; j++) {%>
	networkStyleData_js[<%=i%>][<%=j%>]="<%=networkStyleData[i][j].toString()%>";
		
		<%}%>
<%}%>
<%}%>
//.......................................................................................
//.............-convert java arr to js arr for nodeSchema-...............................
var nodeSchema_js=new Array(<%=nodeSchema.length%>);
<%for (int i = 0; i < nodeSchema.length; i++) {%>
	nodeSchema_js[<%=i%>]="<%=nodeSchema[i]%>";
<%}%>
//.......................................................................................
//.............-convert java arr to js arr for edgeSchema-...............................
var edgeSchema_js=new Array(<%=edgeSchema.length%>);
<%for (int i = 0; i < edgeSchema.length; i++) {%>
	edgeSchema_js[<%=i%>]="<%=edgeSchema[i]%>";
<%}%>
//.............-convert java arr to js arr for ConnectedNodesData........................
var ConnectedNodesData_js=new Array();
<%if (ConnectedNodesData != null && ConnectedNodesData.length > 0) {%>
ConnectedNodesData_js=new Array(<%=ConnectedNodesData.length%>);
<%for (int i = 0; i < ConnectedNodesData.length; i++) {%>
	ConnectedNodesData_js[<%=i%>]=new Array(3);
	//convert nodesstring to array
	ConnectedNodesData_js[<%=i%>][0]="<%=ConnectedNodesData[i][0].toString()%>";
	<%String[] nodes = ConnectedNodesData[i][1].split(";");%>
	ConnectedNodesData_js[<%=i%>][1]=new Array(<%=nodes.length%>);
	<%for (int j = 0; j < nodes.length; j++) {%>
		<%String[] eachNodeData = nodes[j].split(",");%>
		ConnectedNodesData_js[<%=i%>][1][<%=j%>]=new Array(<%=eachNodeData.length%>);
		<%for (int k = 0; k < eachNodeData.length; k++) {%>
			ConnectedNodesData_js[<%=i%>][1][<%=j%>][<%=k%>]="<%=eachNodeData[k].toString()%>";
		<%}%>
	<%}%>
	//convert the colors string to array
	<%String[] nodesColors = ConnectedNodesData[i][2].split(";");%>
	ConnectedNodesData_js[<%=i%>][2]=new Array(<%=nodesColors.length%>);
	<%for (int j = 0; j < nodesColors.length; j++) {%>
		<%String[] eachNodeColor = nodesColors[j].split(",");%>
		ConnectedNodesData_js[<%=i%>][2][<%=j%>]=new Array(<%=eachNodeColor.length%>);
		<%for (int k = 0; k < eachNodeColor.length; k++) {%>
			ConnectedNodesData_js[<%=i%>][2][<%=j%>][<%=k%>]="<%=eachNodeColor[k].toString()%>";
		<%}%>
	<%}%>
<%}%>
<%}%>
//--------------------------------
//create hyperlink for the module
var aModule=document.createElement("a");
aModule.setAttribute("class","valueTXT");
var moduleNme="<%=recievedData[7]%>";
var moduleConditionValues_js="<%=moduleConditionValues%>";
aModule.setAttribute("href","/genet/Module_Representation?module="+moduleNme+"&conditionsValues="+moduleConditionValues_js+'&category=<%=category%>');
aModule.setAttribute("target", "_self");
aModule.appendChild(document.createTextNode(moduleNme));
var tdModuleName=document.getElementById("moduleName");
tdModuleName.appendChild(aModule);
//........................................................................................................................................................
//expanded nodeds stores the nodes that are clicked and expanded
var expandedNodes=new Array();
//-------------------------------
function invisUndo()
{
	document.getElementById("undoDiv").style.visibility="hidden";
}
document.getElementById("undoDiv").style.visibility="hidden";
//============================= History for undo ==============================================================================
//HistoryOfstates is a array such that each cell, itself, is an array with length 2; the first cell stores the filter value, and the second cell stores the network data.
var HistoryOfstates=new Array();
// stores the sequence of actions
var actionHistory=new Array();
// stores the current state
var currentState=new Array();
currentState.push("0,0");
currentState.push(document.getElementById("symbolValue").innerHTML);
//these variables store the data about the previous action. 
var lastActivity="";
var prevFilterValue=new Array();
//initilize the values for the begining
prevFilterValue.push("0,0");
prevFilterValue.push("0,0");
var prevGraph=new Array();
//prevGraph.push(document.getElementById("symbolValue").innerHTML);
function undoClicked()
{
	if(actionHistory.length==0)
		{
			document.getElementById("undoDiv").style.visibility="hidden";
			//alert("noaction");
			return;
		}
	
	var lastAction=actionHistory.pop();
	if(lastAction=="corrFilter")
		{
		var retrievalState=HistoryOfstates.pop();
		//alert("retrievalState:"+retrievalState);
		currentState[0]=retrievalState[0];
		currentState[1]=retrievalState[1];
		document.getElementById("corr_valueBigger").value=retrievalState[0].split(",")[0];
		document.getElementById("corr_valueLower").value=retrievalState[0].split(",")[1];
		correlationUpdate();
		if(actionHistory.length==0)
		{
			document.getElementById("undoDiv").style.visibility="hidden";
		}
		return;
		//---------
		//document.getElementById("corr_valueBigger").value=prevFilterValue[0].split(",")[0];
		//document.getElementById("corr_valueLower").value=prevFilterValue[0].split(",")[1];
		//prevFilterValue[1]=prevFilterValue[0];
		//correlationUpdate();
		//document.getElementById("undoDiv").style.visibility="hidden";
		//return;
		}
	if(lastAction=="nodeClicked")
		{
		var retrievalState=HistoryOfstates.pop();
		//alert("retrievalStatefor nodes!!:"+retrievalState[1]);
		currentState[0]=retrievalState[0];
		currentState[1]=retrievalState[1];
		expandedNodes=new Array();
		expandedNodes=(retrievalState[1]).split(",");
		//alert("exxx:"+expandedNodes);
		redrawNetwork();
		if(actionHistory.length==0)
		{
			document.getElementById("undoDiv").style.visibility="hidden";
		}
		//
		//expandedNodes=new Array();
		//expandedNodes=prevGraph;
		//redrawNetwork();
		//document.getElementById("undoDiv").style.visibility="hidden";
		return;
		}
	return;
	//==
	
}
//---------------------------------------------------------------------------------------
var netdata=inputNetworkConstructor(networkFullData_js);

var ntwrk_str=toCytoScape(nodeSchema_js, edgeSchema_js, netdata[0], netdata[1]);

var ntwrk=jQuery.parseJSON(ntwrk_str);
var networkStyle1=networkStyle(networkStyleData_js);
var style=jQuery.parseJSON(networkStyle1);
var networkOption={network:ntwrk,visualStyle:style};

vis.draw(networkOption);
//---------------------------

expandedNodes[0]=document.getElementById("symbolValue").innerHTML;

//==================================Node click listener=====================================

vis.addListener("dblclick", "nodes", function(evt) {
    var nodes = evt.target;
	var statusFlag=1;
	if(nodes.data.id==document.getElementById("symbolValue").innerHTML)
	{
		//alert("This gene '"+nodes.data.id+"' cannot be collapsed");
		return;
	}
    //check whether the dbl clicked node is already added or not
	for(var i=0;i<expandedNodes.length;i++)
	{
		if(expandedNodes[i]==nodes.data.id)
		{
			//remove the node
			prevGraph=expandedNodes.concat();
			expandedNodes.splice(i,1);
			statusFlag=2;
			break;
		}
	}
	//if a new node, which is not alredy clicked, is clicked
    if(statusFlag==1)
	{
		statusFlag=3;
    	for(var j=0; j<ConnectedNodesData_js.length;j++)
    	{
    		if(ConnectedNodesData_js[j][0]==nodes.data.id)
    		{
    			prevGraph=expandedNodes.concat();    			
				expandedNodes[expandedNodes.length]=ConnectedNodesData_js[j][0];
    			//newNetwork=newNetwork.concat(ConnectedNodesData_js[j][1]);
    			//newStyle=newStyle.concat(ConnectedNodesData_js[j][2]);
    			//flag 2 means the data is already collected and ready to show
    			statusFlag=2;
				break;
    			//drawww(networkOption2);
    			//this.draw(networkOption2);
    		}
    	}
	}
	//statusFlag means a node should be collapsed or added. In the latter case, the data of the node is already collected
    if(statusFlag==2)
	{
    	//alert(expandedNodes.toString());
    	//saveCurrentAction("nodeClicked",expandedNodes);
    	//lastActivity="nodeClicked";
    	document.getElementById("undoDiv").style.visibility="visible";
    	HistoryOfstates.push(currentState.concat());
    	actionHistory.push("nodeClicked");
    	//alert("the new current status netValue:"+expandedNodes.toString());
    	currentState[1]=expandedNodes.toString();
    	redrawNetwork();
		return;
	}
	//should collect the data by Ajax
	if(statusFlag==3)
	{
		//alert("The genes which are directly connected to '"+document.getElementById("symbolValue").innerHTML+"' can be expanded. To expand this gene, search it directly.");
		loadNodeData(nodes.data.id,document.getElementById("symbolValue").innerHTML);
		
	}

    
});
vis.ready(correlationUpdate);

function loadNodeData(clickedSymbol,mainSymbol)
{
var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  //when the data is ready, do the following
  xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	var resp=xmlhttp.responseText;
	//add the recieved data to the array in order to not download it next time
	var tempNodeData=new Array();
	tempNodeData.push(clickedSymbol);
	tempNodeData.push(stringToArray(resp.split("|")[0]));
	tempNodeData.push(stringToArray(resp.split("|")[1]));
	ConnectedNodesData_js.push(tempNodeData);
	//add the new recieved nodes to the netwrok
	prevGraph=expandedNodes.concat();
	expandedNodes.push(clickedSymbol);
	//lastActivity="nodeClicked";
	document.getElementById("undoDiv").style.visibility="visible";
    HistoryOfstates.push(currentState.concat());
    actionHistory.push("nodeClicked");
   // alert("the new current status netValue:"+expandedNodes.toString());
    currentState[1]=expandedNodes.toString();
	redrawNetwork();
    }
  $.unblockUI();
  }
xmlhttp.open("GET","Gene_of_Interest?clickedSymbol="+clickedSymbol+"&mainSymbol="+mainSymbol+"&flag="+"nodeClicked"+"&job_id="+"network_NCK",true);
$.blockUI({ css: { 
    border: 'none', 
    padding: '15px', 
    backgroundColor: '#000', 
    '-webkit-border-radius': '10px', 
    '-moz-border-radius': '10px', 
    opacity: .5, 
    color: '#fff',
	'z-index': 100} }); 
xmlhttp.send();
}
//---------------------------------------------------------------------------------
function redrawNetwork()
{
	var newStyle=new Array();
    var newNetwork=new Array();
      
	for(var i=0; i<expandedNodes.length;i++)
	{
	for(var j=0; j<ConnectedNodesData_js.length;j++)
	{
		if(ConnectedNodesData_js[j][0]==expandedNodes[i])
		{
			var connectedData = ConnectedNodesData_js[j][1]
			if(newNetwork.length!=0) {
				for(var k=0; k<connectedData.length; k++) {
					var addToNetwork = true;
					for(var l=0; l<newNetwork.length; l++) {
						if(((newNetwork[l][0]==connectedData[k][0] && newNetwork[l][1]==connectedData[k][1]) || (newNetwork[l][0]==connectedData[k][1] && newNetwork[l][1]==connectedData[k][0])))
							addToNetwork = false;
					}
					if(addToNetwork)
						newNetwork.push(connectedData[k]);
				} 
			} else {
				newNetwork=newNetwork.concat(connectedData);
			}
			newStyle=newStyle.concat(ConnectedNodesData_js[j][2]);
			//drawww(networkOption2);
			//this.draw(networkOption2);
		}
	}
	}
	newNetwork=newNetwork.concat(networkFullData_js);
	newStyle=newStyle.concat(networkStyleData_js);
	var netdata2=inputNetworkConstructor(newNetwork);

	var ntwrk_str2=toCytoScape(nodeSchema_js, edgeSchema_js, netdata2[0], netdata2[1]);
	var ntwrk2=jQuery.parseJSON(ntwrk_str2);
	var networkStyle3=networkStyle(newStyle);

	var style2=jQuery.parseJSON(networkStyle3);

	var networkOption2={network:ntwrk2,visualStyle:style2};
	vis.draw(networkOption2);
}
//---------------------------------------------------------------------------------
function stringToArray(str)
{
	var retrunArray=new Array();
	var split1=str.split(";");
	for(var i=0;i<split1.length;i++)
		{
			retrunArray[i]=new Array();
			retrunArray[i]=split1[i].split(",");	
		}
	return retrunArray;
}
//---------------------------------------------------------------------------------

function updateClicked()
{
	
	//
	//prevFilterValue[0]=prevFilterValue[1];
	//prevFilterValue[1]=document.getElementById("corr_valueBigger").value+","+document.getElementById("corr_valueLower").value;
	//lastActivity="corrFilter";
	//document.getElementById("undoDiv").style.visibility="visible";
	//saveCurrentAction("corrFilter", document.getElementById("corr_valueBigger").value+","+document.getElementById("corr_valueLower").value);
	if(correlationUpdate()==1)
		{
		//save the current as a history state one, and store the last action as the current state
		//alert("HistoryOfstates:"+HistoryOfstates);
		//alert("going to insert to history of states:"+currentState);
		HistoryOfstates.push(currentState.concat());
		actionHistory.push("corrFilter");
		document.getElementById("undoDiv").style.visibility="visible";
		currentState[0]=document.getElementById("corr_valueBigger").value+","+document.getElementById("corr_valueLower").value;
		//alert("history after inserting:"+HistoryOfstates);
		}
}
//---------------------------------------------------------------------------------
function correlationUpdate()
{
	var corrValueBigger=document.getElementById("corr_valueBigger").value;
	var corrValueLower=document.getElementById("corr_valueLower").value;
	if((corrValueBigger==""||parseFloat(corrValueBigger)==NaN)&&(corrValueLower=="")||parseFloat(corrValueLower)==NaN)
		{
			alert("Enter correlation values between -1 and 1");
			return -1;
		}
	
	//vis.removeFilter();
	var count=0;
	//alert(corrValueLower);
	vis.filter("edges",function(edge) {
		var tempEdge;
		if(edge.data.correlation!="")
			{
				if(edge.data.correlation>= corrValueBigger||(Math.abs(edge.data.correlation)>=Math.abs(corrValueLower)&&edge.data.correlation<0))
					{
					tempEdge=edge;
					count++;
					}
			}
		
	    return tempEdge;
	}, true);
return 1;
}
function clearCorrelation()
{
	
	document.getElementById("corr_valueBigger").value=0;
	document.getElementById("corr_valueLower").value=0;
	//saveCurrentAction("corrFilter", "0,0");
	//prevFilterValue[0]=prevFilterValue[1];
	//prevFilterValue[1]="0,0";
	//lastActivity="corrFilter";
	//document.getElementById("undoDiv").style.visibility="visible";
	HistoryOfstates.push(currentState);
	actionHistory.push("corrFilter");
	document.getElementById("undoDiv").style.visibility="visible";
	currentState[0]="0,0";
	vis.removeFilter();
	document.getElementById("geneNumbers").innerHTML='Graph Representation: ('+networkFullData_js.length.toString()+' genes)';
}
//=================================================================================
	//=================================================================================
		//=================================================================================
function conditionOvered()
{
	try{//
	var condName=this.getAttribute("id");
	condName=condName.split(":")[1];
	var div=document.getElementById("cond:"+condName);
	div.style.backgroundColor="#dddddd";
	var textLine=document.getElementById("lineText:"+condName);
	textLine.style.fontSize="11px";
	textLine.style.backgroundColor="#121212";
	textLine.fontWeight="bold";
	var line=document.getElementById("line:"+condName);
	line.style.strokeWidth="1.5px";
	line.style.stroke="#626262";
	}catch(err){}
}
function conditionOuted()
{
	try{
	var condName=this.getAttribute("id");
	condName=condName.split(":")[1];
	var div=document.getElementById("cond:"+condName);
	div.style.backgroundColor="#f3f3f3";
	var textLine=document.getElementById("lineText:"+condName);
	textLine.style.fontSize="10px";
	textLine.fontWeight="normal";
	var line=document.getElementById("line:"+condName);
	line.style.strokeWidth="0.4px";
	line.style.stroke="#c7c7c7";
	}catch(err){}
}
function populateConditionList()
{
	//add a div, and a checkbox& text inside this DIV
	for(var i=0; i<columns_and_rank.length;i++)
	{
		var newdiv = document.createElement('div');
		newdiv.addEventListener("mouseover", conditionOvered, false);
		newdiv.addEventListener("mouseout", conditionOuted, false);
		newdiv.setAttribute('id', "cond:"+columns_and_rank[i][0]); 
		newdiv.style.width = "300px";
		var chkBox=document.createElement('input');
		chkBox.setAttribute('id', "chk"+i);
		chkBox.setAttribute('type', "checkbox");
		chkBox.setAttribute('value', i);
		chkBox.onclick=function(){if(this.checked){addColumn(this.value);}else{removeColumn(this.value);}};
		chkBox.checked=true;
		newdiv.appendChild(chkBox);
		var label = document.createElement("label");
		label.htmlFor = "id";
		label.setAttribute('class',"valueTXT hyperLink-conditions");
		label.setAttribute('title',"No. of Replicates: " + columns_and_rank[i][3] + " || Strain: " + columns_and_rank[i][4]
			+ " || Growth Phase: " + columns_and_rank[i][5] + " || Medium: " + columns_and_rank[i][6] + " || Description: " + columns_and_rank[i][7]);
		label.appendChild(document.createTextNode(columns_and_rank[i][0]));
		newdiv.appendChild(label);
		document.getElementById("conditionList").appendChild(newdiv);
	}
}
function addColumn(i)
{
	//shownCol.push(columns_and_rank[i]);
	columns_and_rank[i][2]=1;
	updateShownCol();
	updateColumns(shownCol);
	update();
}
function removeColumn(number)
{
	columns_and_rank[number][2]=0;
	updateShownCol();
	updateColumns(shownCol);
	update();
}
function updateShownCol()
{
	shownCol=[];
	for(var i=0;i<columns_and_rank.length;i++)
	{
		if(columns_and_rank[i][2]==1){
			shownCol.push(columns_and_rank[i][0]);
		}
	}
}
//---------------------------------------------------------------------------
//query expansion for gene Search
var changeAutoComplete = false;
	var totalRecordsFetched = 0;
	var changeEventCount = 0;
	
	$("[id$=geneValue]").focusout(function(event) {
		changeEventCount = 0;
	});
	$("[id$=geneValue]").keyup(function(event) {
		if(event.which <= 90 && event.which >= 48 || event.which==8 || event.which == 27) {
			changeEventCount = 0;
			checkLastTextBoxValue($("[id$=geneValue]").val(), null);
		} else if (event.which == 40){
			changeEventCount++;
			if(changeEventCount==0 ||changeEventCount==totalRecordsFetched+1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'next');
			}
		} else if (event.which == 38){
			changeEventCount--;
			if(changeEventCount==0 || changeEventCount==-totalRecordsFetched-1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'prev');
			}
		} 
	});	
	
	// 	query expansion for gene Search
	function checkLastTextBoxValue(oldValue, next) {
		oldValue = oldValue.split(" |")[0];
		if (oldValue != "" || next) {
			var type = $("[id$=geneId]").val();
			var organism = $("[id$=geneCategory]").val();
			if (organism != '' && organism != '------Please Select------')
				loadDropDownMenu(oldValue, type, organism, next);
		}
	}
	function loadDropDownMenu(currentQuery, type, organism, next) {
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var resp = xmlhttp.responseText;
				var currentArray = new Array();
				if(resp.length!=0)
					currentArray = resp.split(",");
				var dataList = document.getElementById('genedatalist');
				totalRecordsFetched = currentArray.length;
				if(currentArray.length > 0) {
					$(dataList).empty();
					changeEventCount = 0;
				}
				currentArray.forEach(function(item) {
			        var option = document.createElement('option');
			        option.value = item;
			        dataList.appendChild(option);
			      });
			}
		}
		var request = "Gene_of_Interest?query="+currentQuery+"&type="+type+"&category="+organism+"&job_id=queryExp&size=10";
		  if(next!=null)
			  request = request + "&job_parameter=" + next;
		xmlhttp.open("GET", request, true);
		xmlhttp.send();
	}

</script>

</body>
</html>
