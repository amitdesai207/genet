<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sign up</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script type="text/javascript"
	src="resources/js/password_strength_plugin.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />
<script type="text/javascript">
	$(function() {
		$("#dialog-message").dialog({
			autoOpen : false,
			modal : true,
			buttons : {
				Ok : function() {
					$(this).dialog("close");
				}
			}
		});

		$("#dialog-message1").dialog({
			autoOpen : false,
			modal : true,
			buttons : {
				Ok : function() {
					$(this).dialog("close");
				}
			}
		});

		$("#password").passStrength({
			userid : "#userName"
		});

		$("#email").change(function() {
			if ($("#email").val() != "") {
				$.ajax({
					type : "POST",
					url : "checkUserEmail",
					data : $("#email").val(),
					cache : false,
					dataType : "json",
					contentType : "application/json; charset=utf-8",
					mimeType : 'application/json',
				}).done(function(msg) {
					if (msg == true)
						$("#dialog-message").dialog("open");
					else
						$("#userName").val($("#email").val());
				});
			}
		});

		$("#userName").change(
				function() {
					if ($("#userName").val() != ""
							&& $("#userName").val() != $("#email").val()) {
						$.ajax({
							type : "POST",
							url : "checkUserName",
							data : $("#userName").val(),
							cache : false,
							dataType : "json",
							contentType : "application/json; charset=utf-8",
							mimeType : 'application/json',
						}).done(function(msg) {
							if (msg == true)
								$("#dialog-message1").dialog("open");
						});
					}
				});

		$("#rePassword").change(function() {
			if ($("#rePassword").val() == $("#password").val())
				$("#passError").text("");
			else
				$("#passError").text("Password does not match");
		});

	});
</script>
</head>

<body style="background-color: #e5e4e9">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block;">
				<div style="width: 50%; float: left;">
					<a href="home"><img
						style="padding-left: 30px; height: 100%; width: 70%;"
						src="resources/images/indexLogo5.png" /></a>
				</div>
				<div style="position: inherit; margin-top: -37px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="login">Sign In</a></li>
						<li><a href="help">Help</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer" style="height: 400px;">
			<table style="width: 100%; min-width: 1000px">
				<tr>
					<td align="center">
						<div class="container">
							<form:form id="signupForm" method="POST"
								commandName="signupModel" action="signup">
								<div style="height: 80px; margin-top: -20px;">
									<form:errors path="*" cssClass="errorblock" element="div" />
								</div>
								<table class="table-border"
									style="width: 50%; margin-bottom: 30px;">
									<tbody>
										<tr>
											<td colspan="4" align="center"><b>Enter details
													below:</b></td>
										</tr>
										<tr>
											<td><spring:message code="label.firstName"
													text="First Name" />:</td>
											<td><form:input id="firstName" path="firstName"
													maxlength="20" size="20" /></td>
											<td><spring:message code="label.lastName"
													text="Last Name" />:</td>
											<td><form:input id="lastName" path="lastName"
													maxlength="20" size="20" /></td>
										</tr>
										<tr>
											<td><spring:message code="label.email"
													text="Email Address" />:</td>
											<td><form:input id="email" path="email" maxlength="60"
													size="20" /></td>
											<td><spring:message code="label.userName"
													text="UserName" />:</td>
											<td><form:input id="userName" path="userName"
													maxlength="20" size="20" /></td>
										</tr>
										<tr>
											<td><spring:message code="label.password"
													text="Password" />:</td>
											<td><form:input type="password" id="password"
													class="password" path="password" maxlength="20" size="20" /></td>
											<td><spring:message code="label.reEnterPassword"
													text="Re-enter Password" />:</td>
											<td><input type="password" id="rePassword"
												maxlength="20" size="20" /><br /> <span id="passError"
												class="error"></span></td>
										</tr>
										<tr>
											<td colspan="4"><form:checkbox path="sendEmail" /> <spring:message
													code="label.sendEmail" text="Genet can communicate with me" /></td>
										</tr>
										<tr>
											<td colspan="4" align="center"><button class="submit"
													style="margin-left: 45px;" onclick="submitForm()">
													<span><spring:message code="label.signupButton"
															text="Create Account" /></span>
												</button></td>
										</tr>
									</tbody>
								</table>
							</form:form>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center" style="margin: 30px;">
						<div>
							<table style="border-collapse: collapse">
								<tr>
									<td>
										<p style="font-size: 12px">&copy;</p>
									</td>
									<td><a style="font-size: 12px" href="http://www.mun.ca" target="_blank">Memorial
											University of Newfoundland</a></td>
									<td>
										<p style="font-size: 12px; padding-left: 2px">2016</p>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div></div>
	</div>
	<div id="dialogs" style="visibility: hidden;">
		<div id="dialog-message1" title="Username already taken">
			<p>Please provide different username</p>
		</div>
		<div id="dialog-message" title="Email already in records">
			<p>Your email address is already present in our records. Please
				login using username and password</p>
		</div>
	</div>
</body>
</html>
