<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Map.Entry"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Server Stats</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/jquery.bootgrid.js"></script>
<script type="text/javascript" src="resources/js/moderniz.2.8.1.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<link href="resources/css/jquery.bootgrid.css" rel="stylesheet" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="resources/css/font-awesome.min.css">
<script type="text/javascript">
$(function() {
	var grid = $("#grid-basic").bootgrid({
		formatters : {
			"editable": function(column, row)
	        {
				if(row.config!='Number of Running Jobs' && row.config!='List of Running job')
	            return '<input class="form-control" type="text" disabled="disabled" value="'+ row.value +'" id="'+ row.config +'">';
	        },
			"commands": function(column, row)
	        {
				if(row.config!='Number of Running Jobs' && row.config!='List of Running job')
	            return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"" + row.config + "\"><span style=\"font-size: 20px;\" class=\"fa fa-pencil\"></span></button>"+
	            "<button type=\"button\" style=\"margin-left:15px;\" class=\"btn btn-xs btn-default command-save\" data-row-id=\"" + row.config + "\"><span style=\"font-size: 20px;\" class=\"fa fa-floppy-o\"></span></button> ";
	        }
		}
	}).on("loaded.rs.jquery.bootgrid", function(){
	    grid.find(".command-edit").on("click", function(e)
	    {
	        $('input[id="' + $(this).data("row-id") + '"]').prop('disabled', false);
	    }).end().find(".command-save").on("click", function(e)
	    {
	    	var input = $('input[id="' + $(this).data("row-id") + '"]');
	    	input.val();
	    	input.prop('disabled', true);
	    });
	});
});
</script>
</head>
<body style="background-color: #e5e4e9">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block; width: 100%">
				<div style="width: 50%; float: left;">
					<a href="home"><img
						style="padding-left: 30px; height: 100%; width: 30%;"
						src="resources/images/Genet.png" /></a>
				</div>
				<div style="position: inherit; margin-top: -37px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="help">Help</a></li>
						<li><a href="logout">Sign Out</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer" style="height: 85%;">
			<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="jobHomePage"><span>Contribute Data</span></a></li>
						<c:if test="${(sessionScope.ROLE eq  'Web_Tool_Admin')  }">
							<li><a href="organismToApprove"><span>Approve
										Organism</span></a></li>
						</c:if>
						<li><a href="alljobs"><span>All Import Jobs</span></a></li>
						<li><a href="jobsearch"><span>Search Import Jobs</span></a></li>
						<c:if test="${(sessionScope.ROLE eq  'Web_Tool_Admin')  }">
							<li><a href="downloadLogs"><span>Email Logs</span></a></li>
							<li class="active"><a href="serverStatisticsPage"><span>Server
										statistics</span></a></li>
						</c:if>
						<li><a href="reset"><span>Reset Password</span></a></li>
					</ul>
				</div>
			</div>
			</nav>
			<table style="width: 100%; min-width: 1000px">
				<tr style="height: 410px;">
					<td style="vertical-align: top;">
						<div class="container">
							<div style="text-align: center; width: 700px">
								<span><b>Server Statistics / Configurations</b></span>
							</div>
							<div style="overflow: auto; height: 99%;">
								<table id="grid-basic"
									class="table table-condensed table-hover table-striped">
									<thead>
										<tr>
											<th data-column-id="config">Configuration Name</th>
											<th data-column-id="value" data-formatter="editable">Configuration Value</th>
											<th data-column-id="commands" data-formatter="commands" data-sortable="false" data-align="center"></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Number of Running Jobs</td>
											<td>${runningThreads}</td>
										</tr>
										<tr>
											<td>List of Running job</td>
											<td>
												<c:forEach var="runningJob" items="${runningJobsId}">
													<a href="job?id=${runningJob}">${runningJob}</a>,
												</c:forEach>
											</td>
										</tr>
										<c:forEach var="configKeyValue" items="${configs}">
											<tr>
												<td>${configKeyValue.key}</td>
												<td>${configKeyValue.value}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="row">
			<div class="col-sm-12 centerText">
				&copy; <a href="http://www.mun.ca" target="_blank">Memorial University
					of Newfoundland</a> 2016
			</div>
		</div>
	<div></div>
	</div>
</body>
</html>
