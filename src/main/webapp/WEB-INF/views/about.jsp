<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.ArrayList,org.mun.genet.vo.OrganismVo"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>About GeNet</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="resources/js/jquery.bootgrid.js"></script>
<script type="text/javascript" src="resources/js/moderniz.2.8.1.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/bootstrap.css" rel="stylesheet" />
<link href="resources/css/jquery.bootgrid.css" rel="stylesheet" />
<link href="resources/css/table.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />

<script type="text/javascript">
	$(function() {
		$("#contriTable").bootgrid({
			rowCount : 10,
			navigation : 2,
			columnSelection : false,
			sorting : false,
			caseSensitive : false
		}).on("click.rs.jquery.bootgrid", function(e, columns, row) {
			window.open(row["link"]);
		});
	});
</script>
</head>

<body style="background-color: #e5e4e9">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block; width: 100%;">
				<div style="width: 50%; float: left;">
					<a href="home"><img
						style="padding-left: 30px; height: 100%; width: 70%; max-width: 500px;"
						src="resources/images/indexLogo5.png" /></a>
				</div>
				<div style="position: inherit; margin-top: -21px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="login">Data Contributor Login</a></li>
						<li><a href="help">Help</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer" style="height: 100%; padding-bottom: 3em;">
			<div>
				<h2>About GeNET</h2>
			</div>
			<div style="display: inline-block; text-align: justify;">
				<div
					style="float: left; padding-left: 5em; padding-top: 2em; width: 40%">
					<img src="resources/images/st.johns.jpg"
						style="width: 480px; height: 260px;" alt="Genet" />
					<p>
						<b>GeNET use is subject to appropriate acknowledgement
							(citation). Please cite GeNET in your publications as follows:</b><br>
						Pe�a-Castillo, L.; Meruvia-Pastor, O. GeNET: Gene Network
						Visualization. URL: http://bengi.cs.mun.ca/genet/ Accessed on
						MM/YYYY.
					</p>
					<p>
						<b>To cite specific gene co-expression network data in your
							publications:</b><br> Cite the article corresponding to the data
						used.
					</p>
					<p>
						<b>Disclaimer</b><br>GeNET is provided as a free public
						resource.<br>The site, and all content, materials,
						information, software, products and services provided on the site,
						are provided on an "as is" and "as available" basis. We reserve
						the right to make changes or updates to the site at any time
						without notice. Any content, materials, information or software
						downloaded or otherwise obtained through the use of the site is
						done at your own discretion and risk. We expressly disclaims all
						warranties of any kind, whether express or implied, including, but
						not limited to:
					<ol>
						<li>the suitability of the content or the services for any
							purpose</li>
						<li>the availability of the site on an uninterrupted, timely,
							secure, or error-free basis (though we will undertake
							best-efforts to ensure continual uptime and availability of its
							content)</li>
						<li>the accuracy or reliability of any data networks or the
							original data on which they are based (though we strive to reach
							a high level of quality)</li>
						<li>the availability of, and content provided on, third party
							Web sites linked by the site</li>
						<li>the non-infringement on rights such as copyright or other
							intellectual property rights by the use of the service by the
							user</li>
					</ol>
					</p>
					<p id="privacy">
						<b>Privacy & Data Publication Policy</b><br>GeNET is
						committed to user privacy. We do not store details of your
						searches or navigation history. The published data that you upload
						is considered to be for public use and will be made available to
						other users through GeNET web interface. The providers of this
						service may keep records of volume of usage for planning and
						scientific review purposes. Logs of usage may also be kept for the
						purposes of monitoring and improving services. These logs will be
						kept confidential and not made available for other purposes or to
						third parties. The providers of this service do not accept
						responsibility for the consequences of a third party
						illegitimately gaining access to private data during an upload
						session.
					</p>
				</div>
				<div
					style="float: right; padding-left: 2em; margin-right: 2em; width: 48%">
					<p>GeNet is a collaboration between the Bioinformatics Lab and
						the 3D Telepresence and Telecollaboration Lab at Memorial
						University of Newfoundland</p>
					<p>
						<b>Bioinformatics Lab</b><br> <span
							style="padding-left: 35px;">Dr. Lourdes Pe�a-Castillo</span><br>
						<span style="padding-left: 35px;">Department of Computer
							Science</span><br> <span style="padding-left: 35px;">Department
							of Biology</span><br> <span style="padding-left: 35px;">Memorial
							University of Newfoundland</span><br> <span
							style="padding-left: 35px;">St. John's, NL A1B 3X5</span><br>
						<span style="padding-left: 35px;">Canada</span><br> <span
							style="padding-left: 35px;"><a
							href="http://www.cs.mun.ca/~lourdes" target="_blank">www.cs.mun.ca/~lourdes</a></span><br>
						<span style="padding-left: 35px;"><a
							href="mailto:lourdes*at*mun*dot*ca">lourdes *at* mun *dot* ca</a></span><br>
					</p>
					<p>
						<b>3D Telepresence and Telecollaboration Lab</b><br> <span
							style="padding-left: 35px;">Dr. Oscar Meruvia-Pastor</span> <br>
						<span style="padding-left: 35px;">Department of Computer
							Science</span> <br> <span style="padding-left: 35px;">Office
							of the Dean of Science</span> <br> <span
							style="padding-left: 35px;">Memorial University of
							Newfoundland</span> <br> <span style="padding-left: 35px;">St.
							John's, NL A1B 3X5</span> <br> <span style="padding-left: 35px;">Canada</span>
						<br> <span style="padding-left: 35px;"><a
							href="http://www.cs.mun.ca/~omeruvia" target="_blank">www.cs.mun.ca/~omeruvia</a></span>
						<br> <span style="padding-left: 35px;"><a
							href="mailto:oscar*at*mun*dot*ca">oscar *at* mun *dot* ca</a></span> <br>
					</p>
					<p>
						<b>GeNET Web Development</b><br> <span
							style="padding-left: 35px;">Amit Prabhakar Desai</span> <br>
						<span style="padding-left: 35px;">Graduate Student,
							Department of Computer Science</span> <br> <span
							style="padding-left: 35px;">Memorial University of
							Newfoundland</span> <br> <span style="padding-left: 35px;">St.
							John's, NL A1B 3X5, Canada</span> <br> <span
							style="padding-left: 35px;"><a
							href="mailto:apd525*at*mun*dot*ca">apd525 *at* mun *dot* ca</a></span> <br>
						<br> <span style="padding-left: 35px;">Mehdi Razeghin,
							2013 M.Sc.</span><br> <span style="padding-left: 35px;">
							Department of Computer Science</span> <br> <span
							style="padding-left: 35px;">Memorial University of
							Newfoundland</span> <br> <span style="padding-left: 35px;">St.
							John's, NL A1B 3X5, Canada</span> <br>
					</p>
					<div>
						<p>
							<b>Data Contributions</b>
						</p>
						<div style="max-width: 700px">
							<table id="contriTable"
								class="table table-condensed table-hover table-striped"
								style="max-width: 700px">
								<thead>
									<tr>
										<th data-column-id="organism" data-sortable="false"
											data-identifier="true">Organism</th>
										<th data-column-id="link" data-sortable="false"
											data-searchable="false" data-css-class="hyperLink-genes"
											data-formatter="link">Link to Publication(s)</th>
									</tr>
								</thead>
								<%
									ArrayList<OrganismVo> organisms = (ArrayList<OrganismVo>) request.getAttribute("organisms");
								%>
								<c:forEach var="organism" items="${organisms}">
									<tr>
										<td>${organism.name}</td>
										<td><c:forEach var="pubmedId"
												items="${organism.pubmedIds}">
												<a href="${pubmedId}">${pubmedId}</a>
												<br>
											</c:forEach></td>
									</tr>
								</c:forEach>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div style="padding-top: 1em;">
				<center>
					<p style="font-size: 12px">
						&copy; <a style="font-size: 12px" href="http://www.mun.ca"
							target="_blank">Memorial University of Newfoundland</a> 2016
					</p>
				</center>
			</div>
			<div></div>
		</div>
	</div>
</body>
</html>
