<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sign In</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />
<%
	String signupMsg = (String) request.getAttribute("signupMsg");
%>
<script type="text/javascript">
	$(function() {
		$("#account-created").dialog({
			autoOpen : false,
			modal : true,
			buttons : {
				Ok : function() {
					$(this).dialog("close");
				}
			}
		});

		$("#account-active").dialog({
			autoOpen : false,
			modal : true,
			buttons : {
				Ok : function() {
					$(this).dialog("close");
				}
			}
		});
		
		$("#account-missing").dialog({
			autoOpen : false,
			modal : true,
			buttons : {
				Ok : function() {
					$(this).dialog("close");
				}
			}
		});
		
		$("#password-reset").dialog({
			autoOpen : false,
			modal : true,
			buttons : {
				Ok : function() {
					$(this).dialog("close");
				}
			}
		});
		
		$("#password-changed").dialog({
			autoOpen : false,
			modal : true,
			buttons : {
				Ok : function() {
					$(this).dialog("close");
				}
			}
		});
		
		displayDialog();
		
	});

	function displayDialog() {
		if('<%=signupMsg%>'=='account-created')
			$("#account-created").dialog("open");
		else if('<%=signupMsg%>'=='account-active')
			$("#account-active").dialog("open");
		else if('<%=signupMsg%>'=='account-missing')
			$("#account-missing").dialog("open");
		else if('<%=signupMsg%>'=='password-reset')
			$("#password-reset").dialog("open");
		else if('<%=signupMsg%>' == 'password-changed')
			$("#password-changed").dialog("open");
	}

	function clearForm() {
		$("#j_username").val('');
		$("#j_password").val('');
		return false;
	}
	function submitForm() {
		document.getElementById("loginForm").action = "j_spring_security_check";
		document.getElementById("loginForm").submit();
	}
</script>
<style>
.ui-widget-overlay {
	height: 200% !important;
}
</style>
</head>

<body style="background-color: #e5e4e9; height: 100%;">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block;">
				<div style="width: 50%; float: left;">
					<a href="home"><img
						style="padding-left: 30px; height: 100%; width: 70%;"
						src="resources/images/indexLogo5.png" /></a>
				</div>
				<div style="position: inherit; margin-top: -37px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="help">Help</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer" style="height: 1000px;">
			<table style="width: 100%; min-width: 1000px; height: 100%">
				<tr style="height: 100%;">
					<td align="center">
						<div id="bottunContainer" style="width: 100%;">
							<table class="containerTable" style="margin-top: -400px;">
								<tr>
									<td style="width: 70%; padding: 15px; padding-top: 0px;"><div
											style="text-align: justify; padding-left: 20px;">
											<p>
												<b>Publishing Your Own Gene Co-Expression Networks</b><br>
												<br>GeNET encourages users to submit and publish their
												own gene co-expression network data, provided an article
												describing it has already been published and has a PubMed
												entry. To submit data, click on the Data Contributor Login
												link available on the top right corner of GeNET homepage,
												create and/or sign up to your GeNET account, and fill the
												submission form available under the Contribute Data option
												in the navigation bar. Template files and links to external
												resources where to get the required information are provided
												in the submission form.<br> <br> To fill the
												Contribute Data form you will need to gather the following
												information:
											<ol>
												<li>NCBI taxonomy ID of your organism.</li>
												<li>PubMED (PMID) of your published article.</li>
												<li>Gene expression matrix in a tab-delimited text
													file. This file contains the gene expression matrix (genes
													X conditions). Expression measurements should be given as
													log2-ratios with respect to a reference condition or
													Z-scores.</li>
												<li>Details about the parameters used in WGCNA to
													create the co-expression network such as correlation,
													network type and soft-thresholding power (Beta-parameter).</li>
												<li>Gene information table in a tab-delimited text
													file. This file contains a gene information table (genes X
													information fields where information fields are Entrez
													identifier, gene symbol, gene systematic ID, gene
													description, gene type (e.g., tRNA, protein coding),
													module). The field gene symbol must uniquely identify each
													gene and this symbol must be used to refer to the genes in
													the gene expression matrix and other files provided.</li>
												<li>Condition (sample) information table in a
													tab-delimited text file. This file contains a condition
													information table (condition identifier, number of
													replicates, strain, growth conditions, medium, and
													description). The condition identifier must uniquely
													identify each condition and this identifier must be used in
													the column names of the gene expression matrix provided.</li>
												<li>At least one source of functional annotation of the
													genes. Currently supported ways to provide functional
													annotation are the following:</li>
												<ol>
													<li>KEGG ID of your organism.</li>
													<li>A transcriptional unit annotation file. This file
														indicates genes in the same transcriptional unit (gene
														symbol, TU identifier).</li>
													<li>A protein complex annotation file. This file
														indicates genes interacting in a protein complex (gene
														symbol, protein complex identifier).</li>
													<li>A pathway annotation file. This file indicates
														genes acting in a given pathway. Pathway annotations may
														be from any pathway database such as MetaCyc or Reactome
														(gene symbol, pathway name). It can also be used to
														provide GO annotations. If you are providing GO
														annotations, we recommend including the description of the
														GO term in the second column.</li>
													<li>A Pfam annotation file or a FASTA file with the
														protein sequences for automatic annotation with Pfam. The
														Pfam annotation file indicates genes with a given Pfam
														domain (gene symbol, Pfam accession). The FASTA file with
														the protein sequences can only have gene symbols in the
														headers.</li>
												</ol>
											</ol>
											After filling and submitting the form, GeNET will parse the
											files, automatically compute the corresponding correlation
											and adjacency matrices, calculate module enrichment, and
											create a relational database with all the data. Upon
											completion of this process, an email will be sent to the
											submitting user's email with the results of the submission
											and another email will be submitted to the GeNET
											administrator for approval if the submission was successful.
											</p>
											<p>Once data is approved by the GeNET administrator, it
												will be available for public browsing and downloading online
												through GeNET's web site. The data available for downloading
												will include some of the files submitted by the publisher
												and some of the data produced by Genet. A form with all the
												data files available for a selected organism will be
												displayed under the Download Data icon.</p>
											<td style="width: 25%; vertical-align: top;"><div>
													<form:form onsubmit="event.preventDefault();"
														id="loginForm" method="POST" action="">
														<c:if test="${not empty  requestScope.signupMsg}">
															<div class="" style="height: 50px;"></div>
														</c:if>
														<c:if test="${empty  requestScope.signupMsg}">
															<div class="" style="height: 33px;">
																<span class="error" style="font-size: 15px;"> <c:if
																		test="${not empty sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message}">
																		<spring:message
																			text="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message}" />
																		<c:remove var="SPRING_SECURITY_LAST_EXCEPTION"
																			scope="session" />
																	</c:if>
																</span>
															</div>
														</c:if>
														<div class="login-screen" style="padding-left: 22px;">
															<table class="table-border" style="width: 100%">
																<tr>
																	<td colspan="2" align="center">Data Contributor
																		Login</td>
																</tr>
																<tr>
																	<td><spring:message code="label.userName"
																			text="Username" />:</td>
																	<td><input id="j_username" name="j_username"
																		size="20" autocomplete="off" /></td>
																</tr>
																<tr>
																	<td><spring:message code="label.password"
																			text="Password" />:</td>
																	<td><input type="password" id="j_password"
																		name="j_password" size="20" /></td>
																</tr>
																<tr>
																	<td><button class="submit" onclick="submitForm()">
																			<span><spring:message code="label.loginButton"
																					text="Login" /></span>
																		</button></td>
																	<td><button style="margin-left: 45px;"
																			class="submit" onclick="clearForm()">
																			<span><spring:message code="label.clearButton"
																					text="Clear" /></span>
																		</button></td>
																</tr>
																<tr>
																	<td style="padding-left: 0px;" align="center"><a
																		href="signup" target="_self">Create Account!!</a></td>
																	<td align="center"><a href="recovery"
																		target="_self">Forgot your password?</a></td>
																</tr>
															</table>
														</div>
														<div></div>
														<!-- 												</div> -->
														<!-- 											</div> -->
													</form:form>
												</div></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div style="text-align: center;">
			<p style="font-size: 12px">
				&copy; <a style="font-size: 12px" href="http://www.mun.ca"
					target="_blank">Memorial University of Newfoundland</a> 2016
			</p>
		</div>
		<div></div>
	</div>
	<div id="account-created" title="Account Created">
		<p>Your personal information is secure with us. You will receive a
			verification email shortly. Please verify your email</p>
	</div>
	<div id="account-active" title="Account Activated">
		<p>Your account is Active now. Please login</p>
	</div>
	<div id="account-missing" title="Account Missing">
		<p>We don't have your account information. Please signup</p>
	</div>
	<div id="password-reset" title="New password generated">
		<p>Your login credential have been sent to you by email</p>
	</div>
	<div id="password-changed" title="Password Changed">
		<p>Your password has been changed. Please login again</p>
	</div>
</body>
</html>
