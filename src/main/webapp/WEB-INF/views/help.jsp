<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Help</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />
</head>

<body style="background-color: #e5e4e9">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block; width: 100%;">
				<div style="width: 50%; float: left;">
					<a href="home"><img
						style="padding-left: 30px; height: 100%; width: 70%; max-width: 500px;"
						src="resources/images/indexLogo5.png" /></a>
				</div>
				<div style="position: inherit; margin-top: -37px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="login">Data Contributor Login</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer" style="height: 100%; padding-bottom: 3em;">
			<div>
				<h2>Quick GeNET Tutorial</h2>
			</div>
			<div style="padding-left: 5em; padding-right: 5em;">
				<p>
					<b>Data browsing and querying</b><br> <br>GeNET is a
					platform to query and visualize global gene co-expression networks.
					GeNet's web interface allows the user to perform queries for
					individual genes or functional annotations, or to browse the list
					of gene modules. A navigation bar <img
						src="resources/images/navigation.png" alt="Navigation Bar" /> is
					shown at the top of each page to perform the following activities:
				</p>
				<ol>
					<li><b>Search a gene</b><br>
						<p>
						<center>
							<img src="resources/images/search.gene.png" alt="Search Gene" />
						</center> <br>To search a gene for a particular organism, click on the
						Search Gene icon (shown above), then select the organism to query
						from the drop-down menu. After that type the gene identifier in
						the search box, and click the "Go" button or type "Enter". To
						search for a specific gene, the user can type Entrez identifiers,
						systematic gene identifiers, and official gene names.<br> <br>
						A gene-centric page will be then displayed with the annotations,
						expression profile and adjacent genes in the co-expression network
						for the query gene. In the expression profile tab the expression
						of the query gene across all conditions will be shown in red and
						the average expression of the genes in the same module will be
						shown in grey. The initial number of adjacent genes shown in the
						graph representation is 20; however, the user has the choice to
						display up to 100 genes or to filter the edges shown in the graph
						based on the correlation coefficient between gene expression
						profiles
						</p></li>
					<li><b>Search for a pathway over-represented in gene
							co-expression modules</b><br>
						<p>
						<center>
							<img src="resources/images/search.pathway.png" alt="Pathway" />
						</center> <br>To search for a pathway, click on the Search Pathway
						icon (shown above), select the organism and type a keyword to
						search among the pathway descriptions (e.g., metabolism), and
						click "Go" or type "Enter". A table showing all modules enriched
						with a pathway containing the keyword will be displayed (shown in
						the Figure below). To obtain more information for a given module
						click on the corresponding module name.<br> <br>The
						first three columns are self-explanatory, the column titled "#
						genes" indicates the number of genes in this pathway found on the
						corresponding module out of all genes in the pathway. The column
						titled "FDR-Pvalue" indicates the False Discovery Rate (FDR)
						-corrected P value of how significantly enriched the corresponding
						module is with respect to genes from that pathway.
						</p>
						<center>
							<img src="resources/images/pathway.data.png"
								alt="Pathway Data" />
						</center> <br><br></li>
					<li><b>Browse Modules </b><br>
						<center>
							<img src="resources/images/browse.module.png" alt="Module" />
						</center> <br>To browse all the gene co-expression modules identified
						in a organism, click on the Browse Module icon (shown above),
						select the organism and click on the "Go" button.<br> <br>A
						table with all the gene co-expression modules in the selected
						organism will be displayed. The table can be sorted by the module
						name, hub symbol and # of Genes in the module. The hub gene is the
						most connected gene in the module. Samples (experimental
						conditions) used to construct the gene co-expression network will
						be displayed as columns. By clicking on a sample, the modules
						whose genes are significantly higher or lower expressed in that
						condition with respect to all other conditions will be highlighted
						(as shown in the screenshot below).<br> <center><img
						src="resources/images/module.data.png" 
						alt="Module Data" /> </center><br> <br>To go to a gene
						co-expression module page click on the module name. The
						module-centric page will display the twenty most connected genes
						in the module, their expression profile across all conditions and
						the graph representation of the module indicating the functional
						annotations found to be enriched in the module.<br>To see all
						genes in a co-expression module click on "View all genes" button <img
						src="resources/images/viewgenes.button.png" alt="View All Genes" />
						located in the annotation table section. By clicking on this
						button a new page will be opened showing a table (see screenshot
						below) with the identifiers, descriptions and functional
						annotations of all genes in the module. The table can be searched
						and sorted on any column. By click on any gene symbol link, the
						corresponding gene-centric page will be opened.<br> <center><img
						src="resources/images/module.gene.png" 
						alt="Module Gene Data" style="max-width: 1100px"/> </center><br><br><br> <br></li>
					<li><b>Download Data</b><br>
						<center>
							<img src="resources/images/download-help.png" alt="Download Data" />
						</center> <br>To download the data corresponding to an organism,
						select the organism and click on the Download Data icon (shown
						above). A form with all the data files available for the selected
						organism will be displayed. Click on the check mark to the left of
						the files you wish to download and click on the Download button.
						GeNET will produce a single compressed file containing all the
						selected files for download.<br><br><br> <br></li>
				</ol>
				<p>
					<b>Publishing Your Own Gene Co-Expression Networks</b><br> <br>GeNET
					encourages users to submit and publish their own gene co-expression
					network data, provided an article describing it has already been
					published and has a PubMed entry. To submit data, click on the Data
					Contributor Login link available on the top right corner of GeNET
					homepage, create and/or sign up to your GeNET account, and fill the
					submission form available under the Contribute Data option in the
					navigation bar. Template files and links to external resources
					where to get the required information are provided in the
					submission form.<br> <br> To fill the Contribute Data
					form you will need to gather the following information:
				<ol>
					<li>NCBI taxonomy ID of your organism.</li>
					<li>PubMED (PMID) of your published article.</li>
					<li>Gene expression matrix in a tab-delimited text file. This
						file contains the gene expression matrix (genes X conditions).
						Expression measurements should be given as log2-ratios with
						respect to a reference condition or Z-scores.</li>
					<li>Details about the parameters used in WGCNA to create the
						co-expression network such as correlation, network type and
						soft-thresholding power (Beta-parameter).</li>
					<li>Gene information table in a tab-delimited text file. This
						file contains a gene information table (genes X information fields
						where information fields are Entrez identifier, gene symbol, gene
						systematic ID, gene description, gene type (e.g., tRNA, protein
						coding), module). The field gene symbol must uniquely identify
						each gene and this symbol must be used to refer to the genes in
						the gene expression matrix and other files provided.</li>
					<li>Condition (sample) information table in a tab-delimited
						text file. This file contains a condition information table
						(condition identifier, number of replicates, strain, growth
						conditions, medium, and description). The condition identifier
						must uniquely identify each condition and this identifier must be
						used in the column names of the gene expression matrix provided.</li>
					<li>At least one source of functional annotation of the genes.
						Currently supported ways to provide functional annotation are the
						following:</li>
					<ol>
						<li>KEGG ID of your organism.</li>
						<li>A transcriptional unit annotation file. This file
							indicates genes in the same transcriptional unit (gene symbol, TU
							identifier).</li>
						<li>A protein complex annotation file. This file indicates
							genes interacting in a protein complex (gene symbol, protein
							complex identifier).</li>
						<li>A pathway annotation file. This file indicates genes
							acting in a given pathway. Pathway annotations may be from any
							pathway database such as MetaCyc or Reactome (gene symbol,
							pathway name). It can also be used to provide GO annotations. If
							you are providing GO annotations, we recommend including the
							description of the GO term in the second column.</li>
						<li>A Pfam annotation file or a FASTA file with the protein
							sequences for automatic annotation with Pfam. The Pfam annotation
							file indicates genes with a given Pfam domain (gene symbol, Pfam
							accession). The FASTA file with the protein sequences can only
							have gene symbols in the headers.</li>
					</ol>
				</ol>
				After filling and submitting the form, GeNET will parse the files,
				automatically compute the corresponding correlation and adjacency
				matrices, calculate module enrichment, and create a relational
				database with all the data. Upon completion of this process, an
				email will be sent to the submitting user's email with the results
				of the submission and another email will be submitted to the GeNET
				administrator for approval if the submission was successful.
				</p>
				<p>Once data is approved by the GeNET administrator, it will be
					available for public browsing and downloading online through
					GeNET's web site. The data available for downloading will include
					some of the files submitted by the publisher and some of the data
					produced by Genet. A form with all the data files available for a
					selected organism will be displayed under the Download Data icon.</p>
			</div>
			<div></div>
			<div style="padding-top: 1em;">
				<center>
					<p style="font-size: 12px">
						&copy; <a style="font-size: 12px" href="http://www.mun.ca"
							target="_blank">Memorial University of Newfoundland</a> 2016
					</p>
				</center>
			</div>
			<div></div>
		</div>
</body>
</html>
