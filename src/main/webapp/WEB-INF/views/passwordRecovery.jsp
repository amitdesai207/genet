<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Password Recovery</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />
</head>

<body style="background-color: #e5e4e9">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block; width: 100%">
				<div style="width: 50%; float: left;">
					<a href="home"><img
						style="padding-left: 30px; height: 100%; width: 70%;"
						src="resources/images/indexLogo5.png" /></a>
				</div>
				<div style="position: inherit; margin-top: -37px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="login">Sign In</a></li>
						<li><a href="help">Help</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer">
			<table style="width: 100%; min-width: 1000px">
				<tr>
					<td align="center">
						<div id="bottunContainer">
							<table class="containerTable" style="margin-top: -90px;">
								<tr>
									<td><form:form id="emailLogsForm" method="POST"
											commandName="recoveryModel" action="recovery"
											style="padding-top: 70px;">
											<div style="height: 40px;">
												<a style="margin-left: 37px; font-size: 15px; color: red;">${fail}</a>
												<a style="margin-left: 22px; font-size: 15px; color: green;">${success}</a>
											</div>
											<div>
												<a style="margin-left: 40px;">Enter your UserName: </a>
												<form:input type="text" id="emailAddress" path="userName"
													style="margin-left: 20px;" />
												<form:input type="hidden" id="nonce" path="nonce"
													value="${nonce}" />
												<button class="submit" style="margin-left: 25px;">
													<span>Get Password</span>
												</button>
											</div>
										</form:form></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center">
						<div>
							<table style="border-collapse: collapse">
								<tr>
									<td>
										<p style="font-size: 12px">&copy;</p>
									</td>
									<td><a style="font-size: 12px" href="http://www.mun.ca" target="_blank">Memorial
											University of Newfoundland</a></td>
									<td>
										<p style="font-size: 12px; padding-left: 2px">2016</p>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div></div>
	</div>
</body>
</html>
