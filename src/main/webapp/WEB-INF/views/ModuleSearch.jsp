<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Module Search</title>
<script type="text/javascript"
	src="resources/js/d3.v2_diagramDrawer-V2.js"></script>
<script type="text/javascript" src="resources/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.11.min.js"></script>
<script type="text/javascript" src="resources/js/jquery.blockUI.js"></script>
<style type="text/css">
.ui-widget-overlay {
	position: fixed !important;
}

#aHead_sym {
	position: absolute;
	margin-left: 10em;
	margin-top: 3em;
	float: left;
}
</style>
<script type="text/javascript" src="resources/js/interaction.js"></script>
<%
	String[][] modulesData = (String[][]) request
			.getAttribute("modulesData");
	String[] columnNames = (String[]) request
			.getAttribute("columnNames");
	String[] conditions = (String[]) request.getAttribute("conditions");
	String category = (String) request.getAttribute("category");
%>
<script>
var moduleButtonClicked = false;
var btnDIVWidth=650;

function moduleHyperlinkClicked() {
	$.blockUI({
		css : {
			border : 'none',
			padding : '15px',
			backgroundColor : '#000',
			'-webkit-border-radius' : '10px',
			'-moz-border-radius' : '10px',
			opacity : .5,
			color : '#fff',
			'z-index': 100
		}
	});
}

function geneBTNClicked()
{
	var difference=(640-btnDIVWidth)/2;
	btnDIVWidth=640;
	var destination=btnDIVWidth/2-70;
	var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left",(prevLeft+difference)+"px");
	$("#topArrow").animate({"left":destination+"px"}, "slow");
	$("#pathwaySearchForm").css("display","none");
	$("#moduleSearchForm").css("display","none");
	$("#downloadForm").css("display","none");
	$("#geneSearchForm").css("display","table");
	moduleButtonClicked = false;
}
function moduleBTNClicked()
{
	if(moduleButtonClicked) {
		$("#moduleGoBTN").trigger( "click" );
	} else {
		moduleButtonClicked=!moduleButtonClicked;
		var difference=(280-btnDIVWidth)/2;
		btnDIVWidth=280;
		var destination=btnDIVWidth/2+35;
		var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
		$("#topArrow").css("left",(prevLeft+difference)+"px");
		$("#topArrow").animate({"left":destination+"px"}, "slow");
		$("#geneSearchForm").css("display","none");
		$("#pathwaySearchForm").css("display","none");
		$("#downloadForm").css("display","none");
		$("#moduleSearchForm").css("display","table");
		moduleButtonClicked = true;
	}
}

function downloadBTNClicked() {	
	if(moduleButtonClicked) {
		$("#downloadGoBTN").trigger( "click" );
	} else {
		var difference=(640-btnDIVWidth)/2;
		btnDIVWidth=640;
		var destination=btnDIVWidth/2-50;
		var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
		$("#topArrow").css("left",(prevLeft+difference)+"px");
		$("#topArrow").animate({"left":destination+"px"}, "slow");
		$("#pathwaySearchForm").css("display","none");
		$("#moduleSearchForm").css("display","none");
		$("#geneSearchForm").css("display","none");
		$("#downloadForm").css("display","table");		
		moduleButtonClicked = true;
	}	
}

function pathwayBTNClicked()
{
	var difference=(625-btnDIVWidth)/2;
	btnDIVWidth=625;
	var destination=btnDIVWidth/2+0;
	var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left",(prevLeft+difference)+"px");
	$("#topArrow").animate({"left":destination+"px"}, "slow");
	$("#geneSearchForm").css("display","none");
	$("#moduleSearchForm").css("display","none");
	$("#downloadForm").css("display","none");
	$("#pathwaySearchForm").css("display","table");
	moduleButtonClicked = false;
}
</script>
<link rel="stylesheet" href="resources/css/jquery-ui.min.css" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/table.css" rel="stylesheet" type="text/css"
	media="screen" />
</head>
<body onload="initializeDocument()">
	<div align="center">
		<div id="main">
			<div id="logo">
				<a href="home"> <img src="resources/images/Genet.png" /></a>
			</div>
			<div id="headContainer" align="center">
				<div class="header-menu">
				<table id="header_tb" class="noSpace">
					<tr>
						<td align="center" style="vertical-align: top;">
							<div id="headerBTNS">
								<div id="headBTNContainer" align="center">
									<table
										style="border-collapse: collapse; padding: 0px; margin: 0px;"
										cellpadding="0px" cellspacing="0px">
										<tr>
											<td align="center">
												<div id="btnCont" style="padding-top: 3px">
													<table id="btns_tbl"
														style="border-collapse: collapse; padding: 0px; margin: 0px; margin-top: 10px;"
														cellpadding="0px" cellspacing="0px">
														<tr>
															<td>
																<div id="geneDIV" onclick="geneBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px">
																	<img src="resources/images/gene_h.png"
																		title="Search Gene" />
																</div>
															</td>
															<td>
																<div id="moduleDIV" onclick="moduleBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/module_h.png"
																		title="Browse Module" />
																	<!-- <form id="moduleSubmit" method="post" action="Module_of_Interest"> -->
																	<!-- </form> -->
																</div>
															</td>
															<td>
																<div id="pathwayDIV" onclick="pathwayBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/pathway_h.png"
																		title="Search Pathway" />
																</div>
															</td>
															<td>
																<div id="downloadDIV" onclick="downloadBTNClicked()"
																	style="padding-bottom: 3px; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/download_h.png"
																		title="Download Data" />
																</div>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div id="bottomLineBTN"
													style="border-bottom: 2px solid #FFFFFF;">
													<div align="left">
														<img id="topArrow"
															style="vertical-align: bottom; text-align: center; position: relative; bottom: -2px; left: 0px"
															src="resources/images/arrowTop.png" />
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div id="geneSearchForm"
													style="display: none; text-align: center">
													<form id="geneSubmit" method="post"
														action="Gene_of_Interest">
														<table id="formContainer_tb" style="width: 650px"
															class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 94px">Find Genes by</p>
																</td>

																<td width="100px"><select id="geneId" name="id">
																		<option>Symbol</option>
																		<option>Systematic ID</option>
																		<option>Entrez ID</option>
																</select></td>
																<td width="100px"><input autocomplete="off"
																	id="geneValue" name="value" list="genedatalist"
																	type="text" /> <datalist id="genedatalist"></datalist>
																</td>
																<td align="left">
																	<p style="width: 76px">in organism</p>
																</td>
																<td width="90px"><select class="btn"
																	id="geneCategory" name="category" style="width: 150px;">
																		<option>------Please Select------</option>
																</select></td>

																<td width="20px">
																	<button id="geneGoBTN" type="submit" class="btn goBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>
																</td>
																<td width="10px" align="center"></td>
															</tr>
														</table>
													</form>
												</div>
												<div id="moduleSearchForm" style="display: none">
													<form id="moduleSubmit" method="post"
														action="Module_of_Interest">
														<table id="moduleContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 110px">Select organism</p>
																</td>
																<td><select class="btn" id="moduleCategory"
																	name="category" style="width: 250px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="moduleGoBTN" type="submit"
																		class="btn goBTN moduleBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>

																</td>
															</tr>
														</table>
													</form>
												</div>
												<div id="pathwaySearchForm" style="display: none">
													<form id="pathwaySubmit" method="post"
														action="Pathway_Representation">
														<table id="pathwayContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 94px">Find pathways</p>
																</td>
																<td width="210px"><input style="width: 210px"
																	id="pathwayValue" name="value" type="text" /></td>
																<td align="center" width="80px">
																	<p style="width: 80px">in organism</p>
																</td>

																<td width="90px"><select class="btn"
																	id="pathwayCategory" name="category"
																	style="width: 150px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="pathwayGoBTN" type="submit"
																		class="btn goBTN">
																		<p style="visibility: inherit" class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>
																</td>
															</tr>
														</table>
													</form>
												</div>
												<div id="downloadForm" style="display: none">
													<form id="downloadSubmit" method="get"
														action="downloadData">
														<table id="downloadContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 110px">Select organism</p>
																</td>
																<td><select class="btn" id="downloadCategory"
																	name="category" style="width: 250px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="downloadGoBTN" type="submit"
																		class="btn goBTN moduleBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>

																</td>
															</tr>
														</table>
													</form>
												</div>
											</td>
										</tr>
									</table>

								</div>
							</div>
						</td>
						<!-->
<!-->
					</tr>
				</table>
				</div>
			</div>
			<div class="main-menu">
					<nav class="horizontal-nav1 ">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="login">Data Contributor Login</a></li>
						<li><a href="help">Help</a></li>
					</ul>
					</nav>
			</div>
			<div class="no-content"></div>
			<div id="page-contents" style="display: none;">
			<div id="content-wrapper">
				<div>
					<h3>Module Browser</h3>
				</div>
				<div id="symbol-header">
					<table id="aHead_sym" class="noSpace" align="center">
						<thead>
							<tr>
								<th colspan="3">Legends</th>
							</tr>
						</thead>
						<tbody style="text-align: centre;">
							<tr>
								<td style="background-color: rgb(153, 0, 0); width: 20%"></td>
								<td></td>
								<td style="background-color: rgb(240, 128, 128);">Up-Regulated</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="background-color: rgb(0, 102, 153); width: 20%"></td>
								<td></td>
								<td style="background-color: rgb(135, 206, 250);">Down-Regulated</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="background-color: rgb(170, 131, 203); width: 20%"></td>
								<td></td>
								<td style="background-color: rgb(170, 131, 203);">Mixed(Up
									& Down)</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id="content" style="width: 100%" align="center"></div>
			</div>
			</div>
			<div id="end" style="width: 100%; height: 20px"></div>
		</div>
		<!-- <form style="visibility:hidden" id="geneSubmit" method="post" target="_blank" action="Gene_of_Interest"> -->
		<!-- <select name="category"> -->
		<!-- <option>Rhodobacter capsulatus</option> -->
		<!-- </select> -->
		<!-- <select name="id"> -->
		<!-- <option>Symbol</option> -->
		<!-- </select> -->
		<!-- <input id="geneSubmitValue" name="value" type="text"/> -->
		<!-- </form> -->
	</div>
	<input id="category" type="hidden" value="<%=category%>" />
	<div id="dialogs" style="visibility: hidden;">
		<div id="dialog-message" title="Required data missing">
			<p>Please select an organism</p>
		</div>
		<div id="dialog-message1" title="Required data missing">
			<p>Please provide value for search criteria</p>
		</div>
		<div id="dialog-message2" title="Required data missing">
			<p>Please enter a valid identifier</p>
		</div>
	</div>
	<script>
//.............-convert java arr to js arr for modules data-...............................
var modulesData_js=new Array();
var modulesConditionData_js=new Array();
<%for (int i = 0; i < modulesData.length; i++) {%>
	var tempArr=new Array();
	tempArr.push("<%=modulesData[i][0]%>");
	tempArr.push("<%=modulesData[i][1]%>");
	tempArr.push("<%=modulesData[i][2]%>");
	<%String[] conds = modulesData[i][3].split(",");%>
	var tempArr2=new Array();
	<%for (int j = 0; j < conds.length; j++) {%>
		tempArr2.push("<%=conds[j]%>");
	<%}%>
	modulesConditionData_js.push(tempArr2);
	modulesData_js.push(tempArr);
<%}%>
//alert(modulesData_js);
//---------------------------covert colNames array to js one---------------------------
var columnNames_js=new Array();
<%for (int i = 0; i < columnNames.length; i++) {%>
	columnNames_js.push("<%=columnNames[i]%>");
<%}%>
//------------------------------------------------------
//---------------------------covert conditions array to js one---------------------------
var conditions_js=new Array();
<%for (int i = 0; i < conditions.length; i++) {%>
	conditions_js.push("<%=conditions[i]%>");
<%}%>
//------------------------------------------------------
//var dataset = {
//    rowLabel: [],
//    columnLabel: [],
//	verticalColumnLabel: [],
//    value: [],
//    verticalValue:[]
//};
//dataset.columnLabel=columnNames_js;
//dataset.verticalColumnLabel=conditions_js;
//dataset.value=modulesData_js.concat();
//dataset.verticalValue=modulesConditionData_js.concat();
//var width = 600;
//var height = 1200;
//drawModuleTable(divContainer,horizontalColLabel,verticalColLabel,HorData, VerData,x,y,cellWidth,cellHeight,vCellWidth,vCellHeight,strokeColor)
//0,0,110,25,10,50,"#eeeeee",125
drawModuleTable("content",columnNames_js,conditions_js,modulesData_js.concat(), modulesConditionData_js.concat(), '<%=category%>');
//---------------------------------------------------------------------------
//query expansion for gene Search
	var changeAutoComplete = false;
	var totalRecordsFetched = 0;
	var changeEventCount = 0;
	
	$("[id$=geneValue]").focusout(function(event) {
		changeEventCount = 0;
	});
	$("[id$=geneValue]").keyup(function(event) {
		if(event.which <= 90 && event.which >= 48 || event.which==8 || event.which == 27) {
			changeEventCount = 0;
			checkLastTextBoxValue($("[id$=geneValue]").val(), null);
		} else if (event.which == 40){
			changeEventCount++;
			if(changeEventCount==0 ||changeEventCount==totalRecordsFetched+1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'next');
			}
		} else if (event.which == 38){
			changeEventCount--;
			if(changeEventCount==0 || changeEventCount==-totalRecordsFetched-1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'prev');
			}
		} 
	});	
	
	// 	query expansion for gene Search
	function checkLastTextBoxValue(oldValue, next) {
		oldValue = oldValue.split(" |")[0];
		if (oldValue != "" || next) {
			var type = $("[id$=geneId]").val();
			var organism = $("[id$=geneCategory]").val();
			if (organism != '' && organism != '------Please Select------')
				loadDropDownMenu(oldValue, type, organism, next);
		}
	}
	function loadDropDownMenu(currentQuery, type, organism, next) {
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var resp = xmlhttp.responseText;
				var currentArray = new Array();
				if(resp.length!=0)
					currentArray = resp.split(",");
				var dataList = document.getElementById('genedatalist');
				totalRecordsFetched = currentArray.length;
				if(currentArray.length > 0) {
					$(dataList).empty();
					changeEventCount = 0;
				}
				currentArray.forEach(function(item) {
			        var option = document.createElement('option');
			        option.value = item;
			        dataList.appendChild(option);
			      });
			}
		}
		var request = "Gene_of_Interest?query="+currentQuery+"&type="+type+"&category="+organism+"&job_id=queryExp&size=10";
		  if(next!=null)
			  request = request + "&job_parameter=" + next;
		xmlhttp.open("GET", request, true);
		xmlhttp.send();
	}
</script>
</body>
</html>