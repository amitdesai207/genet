<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Module of interest</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="resources/js/interaction.js"></script>
<script type="text/javascript"
	src="resources/js/d3.v2_diagramDrawer-V2.js"></script>
<script type="text/javascript" src="resources/js/cytoscapeString.js"></script>
<script type="text/javascript" src="resources/js/cytoscape_web/json2.js"></script>
<script type="text/javascript"
	src="resources/js/cytoscape_web/AC_OETags.js"></script>
<script type="text/javascript"
	src="resources/js/cytoscape_web/cytoscapeweb.js"></script>
<script type="text/javascript" src="resources/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="resources/js/jquery.colorPicker.js"></script>
<script type="text/javascript" src="resources/js/hashmap.js"></script>

<%
	String info = new String();
	info = "a,a,a,a,a,a";
	String module = (String) request.getAttribute("module");
	String category = (String) request.getAttribute("category");
	String hubSymbol = (String) request.getAttribute("hubSymbol");
	String conditionsValues = (String) request
			.getAttribute("conditionsValues");
	String[][] genesOfModule = (String[][]) request
			.getAttribute("genesOfModule");
	Integer totalNoOfGenesInModule = (Integer) request
			.getAttribute("totalGenes");
	System.out.println(genesOfModule);
	String[][][] genesExpressionInfo = (String[][][]) request
			.getAttribute("genesExpressionInfo");
	System.out.println(genesExpressionInfo);
	String[][] avrageExpression = (String[][]) request
			.getAttribute("avrage");
	System.out.println(avrageExpression);
	String[][] conditionNames = (String[][]) request
			.getAttribute("conditionsAndRanks");
	System.out.println(conditionNames);
	String[][] AnnotationData = (String[][]) request
			.getAttribute("AnnotationData");
	System.out.println(AnnotationData);
	String[] nodeSchema = (String[]) request.getAttribute("nodeSchema");
	System.out.println(nodeSchema);
	String[] edgeSchema = (String[]) request.getAttribute("edgeSchema");
	System.out.println(edgeSchema);
	String[] nodeData = (String[]) request.getAttribute("nodeData");
	System.out.println(nodeData);
	String[][] edgeData = (String[][]) request.getAttribute("edgeData");
	System.out.println(edgeData);
	String[][] networkSchema = (String[][]) request
			.getAttribute("networkSchema");
	System.out.println(networkSchema);
%>
<script>
//9090909009090990000000000000000000000
//the following variable store the height of the DIV. this value is used when the div is going to be maximized
var graphContainer_height=0;
var expreContainer_height=0;
var annotationContainer_height=0;
//min_max_DIV gets the div_id that should be minimized/maximized; the height of the container; and the bottom, blue line of the associated btn in the header.
function min_max_DIV(id, heightDIV, btnID)
{

	if(document.getElementById(id).style.visibility!='hidden')
	{
		$("#"+id).animate({height:"1px"});
		document.getElementById(id).style.visibility='hidden';
		document.getElementById(btnID).style.visibility='hidden';
		if(id=='annotationContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Expand Annotation Table');
			$('#' + btnID + ' img').attr('title', 'Expand Annotation Table');
			$('#annotationHeader img').attr('src', 'resources/images/plus.png').attr('height', '12px').attr('title', 'Expand Annotation Table');
		} else if(id=='expreContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Expand Expression Profile');
			$('#' + btnID + ' img').attr('title', 'Expand Expression Profile');
			$('#expreHeader img').attr('src', 'resources/images/plus.png').attr('height', '12px').attr('title', 'Expand Expression Profile');
		} else if(id=='graphContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Expand Gene Network');
			$('#' + btnID + ' img').attr('title', 'Expand Gene Network');
			$('#graphHeader img').attr('src', 'resources/images/plus.png').attr('height', '12px').attr('title', 'Expand Gene Network');
		}
	}
	else
	{
		$("#"+id).animate({height:heightDIV});
		document.getElementById(id).style.visibility='visible';
		document.getElementById(btnID).style.visibility='visible';
		if(id=='annotationContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Collapse Annotation Table');
			$('#' + btnID + ' img').attr('title', 'Collapse Annotation Table');
			$('#annotationHeader img').attr('src', 'resources/images/min.png').attr('height', '4px').attr('title', 'Collapse Annotation Table');
		} else if(id=='expreContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Collapse Expression Profile');
			$('#' + btnID + ' img').attr('title', 'Collapse Expression Profile');
			$('#expreHeader img').attr('src', 'resources/images/min.png').attr('height', '4px').attr('title', 'Collapse Expression Profile');
		} else if(id=='graphContent') {
			$('#' + btnID.replace('_line', '') + ' img').attr('title', 'Collapse Gene Network');
			$('#' + btnID + ' img').attr('title', 'Collapse Gene Network');
			$('#graphHeader img').attr('src', 'resources/images/min.png').attr('height', '4px').attr('title', 'Collapse Gene Network');
		}
	}
	
};

//================================================================================
window.onload=function()
{
	btnDIVWidth=120;
	var difference=(110-btnDIVWidth)/2;
	btnDIVWidth=110;
	var destination=btnDIVWidth/2+8;
	var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left",(prevLeft+difference)+"px");
	$("#topArrow").animate({"left":destination+"px"}, "slow");
	$("#geneSearchForm").css("display","none");
	$("#pathwaySearchForm").css("display","none");
	//=========================================================
	// store the initial height of the div containers in the assoiciated variables. Note that this height must be set directly in the html not in the css file.
	graphContainer_height=document.getElementById('graphContent').style.height;
	
	expreContainer_height=document.getElementById('expreContent').style.height;
	annotationContainer_height=document.getElementById('annotationContent').style.height;

	//-----------------------------
	//add a click handler for each div container
	document.getElementById('graphHeader').onclick=function() {min_max_DIV('graphContent', graphContainer_height,'graphBTN_line');};
	document.getElementById('annotationHeader').onclick=function() {min_max_DIV('annotationContent', annotationContainer_height,'annoBTN_line');};
	document.getElementById('expreHeader').onclick=function() {min_max_DIV('expreContent', expreContainer_height,'expreBTN_line');};
	//--------------
	//add click handler for the min/max buttons located in the head
	document.getElementById('expreBTN').onclick=function() {min_max_DIV('expreContent', expreContainer_height,'expreBTN_line');};
	document.getElementById('graphBTN').onclick=function() {min_max_DIV('graphContent', graphContainer_height,'graphBTN_line');};
	document.getElementById('annoBTN').onclick=function() {min_max_DIV('annotationContent', annotationContainer_height,'annoBTN_line');};
	
	document.getElementById('expreBTN_line').onclick=function() {min_max_DIV('expreContent', expreContainer_height,'expreBTN_line');};
	document.getElementById('graphBTN_line').onclick=function() {min_max_DIV('graphContent', graphContainer_height,'graphBTN_line');};
	document.getElementById('annoBTN_line').onclick=function() {min_max_DIV('annotationContent', annotationContainer_height,'annoBTN_line');};
	
	$('#diagram').scroll(function() {
		$('#diagramTable').scrollLeft($(this).scrollLeft());
	});
	
	$('#diagramTable').scroll(function() {
		$('#diagram').scrollLeft($(this).scrollLeft());
	});
}
//===========================================================
var moduleButtonClicked = false;
var btnDIVWidth=650;

function geneBTNClicked()
{
	var difference=(640-btnDIVWidth)/2;
	btnDIVWidth=640;
	var destination=btnDIVWidth/2-70;
	var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left",(prevLeft+difference)+"px");
	$("#topArrow").animate({"left":destination+"px"}, "slow");
	$("#pathwaySearchForm").css("display","none");
	$("#moduleSearchForm").css("display","none");
	$("#downloadForm").css("display","none");
	$("#geneSearchForm").css("display","table");
	moduleButtonClicked = false;
}
function moduleBTNClicked()
{
	if(moduleButtonClicked) {
		$("#moduleGoBTN").trigger( "click" );
	} else {
		moduleButtonClicked=!moduleButtonClicked;
		var difference=(280-btnDIVWidth)/2;
		btnDIVWidth=280;
		var destination=btnDIVWidth/2+35;
		var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
		$("#topArrow").css("left",(prevLeft+difference)+"px");
		$("#topArrow").animate({"left":destination+"px"}, "slow");
		$("#geneSearchForm").css("display","none");
		$("#pathwaySearchForm").css("display","none");
		$("#downloadForm").css("display","none");
		$("#moduleSearchForm").css("display","table");
		moduleButtonClicked = true;
	}
}

function downloadBTNClicked() {	
	if(moduleButtonClicked) {
		$("#downloadGoBTN").trigger( "click" );
	} else {
		var difference=(640-btnDIVWidth)/2;
		btnDIVWidth=640;
		var destination=btnDIVWidth/2-50;
		var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
		$("#topArrow").css("left",(prevLeft+difference)+"px");
		$("#topArrow").animate({"left":destination+"px"}, "slow");
		$("#pathwaySearchForm").css("display","none");
		$("#moduleSearchForm").css("display","none");
		$("#geneSearchForm").css("display","none");
		$("#downloadForm").css("display","table");		
		moduleButtonClicked = true;
	}
}

function pathwayBTNClicked()
{
	var difference=(625-btnDIVWidth)/2;
	btnDIVWidth=625;
	var destination=btnDIVWidth/2+0;
	var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left",(prevLeft+difference)+"px");
	$("#topArrow").animate({"left":destination+"px"}, "slow");
	$("#geneSearchForm").css("display","none");
	$("#moduleSearchForm").css("display","none");
	$("#downloadForm").css("display","none");
	$("#pathwaySearchForm").css("display","table");
	moduleButtonClicked = false;
}
</script>
<script>
//---------------------------convert java  array to js array------------------------------------
var conditionsValues_str="<%=conditionsValues%>";
//alert("conditionsValues_str:"+conditionsValues_str);
var conditionsValues_js=new Array();
conditionsValues_js=conditionsValues_str.split(",");
//networkData
var AnnotationData_js=new Array(<%=AnnotationData.length%>);
<%for (int i = 0; i < AnnotationData.length; i++) {%>
	AnnotationData_js[<%=i%>]=new Array(<%=AnnotationData[i].length%>);
	AnnotationData_js[<%=i%>][0]="<%=AnnotationData[i][0].toString()%>";
	AnnotationData_js[<%=i%>][1]="<%=AnnotationData[i][1].toString()%>";
	AnnotationData_js[<%=i%>][2]=new Array();
	var str="<%=AnnotationData[i][2].toString()%>";
	AnnotationData_js[<%=i%>][2]=str.split(",");
	AnnotationData_js[<%=i%>][3]="<%=AnnotationData[i][3].toString()%>";
<%}%>
//genes of modules
var genesOfModule_js=new Array(<%=genesOfModule.length%>);
<%for (int i = 0; i < genesOfModule.length; i++) {%>
	genesOfModule_js[<%=i%>]=new Array(<%=genesOfModule[i].length%>);
	<%for (int j = 0; j < genesOfModule[i].length; j++) {%>
		genesOfModule_js[<%=i%>][<%=j%>]="<%=genesOfModule[i][j].toString()%>";
		
		<%}%>
<%}%>
//edgedata
var edgeData_js=new Array(<%=edgeData.length%>);
<%for (int i = 0; i < edgeData.length; i++) {%>
	edgeData_js[<%=i%>]=new Array(<%=edgeData[i].length%>);
	<%for (int j = 0; j < edgeData[i].length; j++) {%>
		edgeData_js[<%=i%>][<%=j%>]="<%=edgeData[i][j].toString()%>";
		
		<%}%>
<%}%>
//nodeData
var nodeData_js=new Array();
<%for (int j = 0; j < nodeData.length; j++) {%>
	nodeData_js[<%=j%>]="<%=nodeData[j]%>";
<%}%>
//edgeSchema
var edgeSchema_js=new Array();
<%for (int j = 0; j < edgeSchema.length; j++) {%>
	edgeSchema_js[<%=j%>]="<%=edgeSchema[j]%>";
<%}%>
//nodeSchema
var nodeSchema_js=new Array();
<%for (int j = 0; j < nodeSchema.length; j++) {%>
	nodeSchema_js[<%=j%>]="<%=nodeSchema[j]%>";
<%}%>
//networkSchema
var networkStyleData_js=new Array(<%=networkSchema.length%>);
<%for (int i = 0; i < networkSchema.length; i++) {%>
	networkStyleData_js[<%=i%>]=new Array(<%=networkSchema[i].length%>);
	<%for (int j = 0; j < networkSchema[i].length; j++) {%>
		networkStyleData_js[<%=i%>][<%=j%>]="<%=networkSchema[i][j].toString()%>";
		
		<%}%>
<%}%>
// avrage expression 
var expreAVGinfo_js=[];
<%for (int j = 0; j < avrageExpression.length; j++) {%>
var temp2=[];
temp2[0]="<%=avrageExpression[j][0]%>";
temp2[1]="<%=avrageExpression[j][1]%>";
expreAVGinfo_js[<%=j%>]=temp2;
<%}%>
//genesExpressionInfo
var genesExpressionInfo_js=new Array(<%=genesExpressionInfo.length%>);
<%for (int i = 0; i < genesExpressionInfo.length; i++) {%>
	genesExpressionInfo_js[<%=i%>]=new Array(<%=genesExpressionInfo[i].length%>);
	<%for (int j = 0; j < genesExpressionInfo[i].length; j++) {%>
		genesExpressionInfo_js[<%=i%>][<%=j%>]=new Array(2);
		genesExpressionInfo_js[<%=i%>][<%=j%>][0]="<%=genesExpressionInfo[i][j][0].toString()%>";
		genesExpressionInfo_js[<%=i%>][<%=j%>][1]="<%=genesExpressionInfo[i][j][1].toString()%>";
		<%}%>
<%}%>
//conditionNames
var columns_and_rank=new Array(<%=conditionNames.length%>);
<%for (int i = 0; i < conditionNames.length; i++) {%>
columns_and_rank[<%=i%>]=new Array(8);
columns_and_rank[<%=i%>][0]="<%=conditionNames[i][0]%>";	
columns_and_rank[<%=i%>][1]=1; <%--"<%=conditionNames[i][1]%>"; --%>
columns_and_rank[<%=i%>][2]=1;
columns_and_rank[<%=i%>][3]="<%=conditionNames[i][1]%>";
columns_and_rank[<%=i%>][4]="<%=conditionNames[i][2]%>";
columns_and_rank[<%=i%>][5]="<%=conditionNames[i][3]%>";
columns_and_rank[<%=i%>][6]="<%=conditionNames[i][4]%>";
columns_and_rank[<%=i%>][7]="<%=conditionNames[i][5]%>";
<%}%>
//------------------------------------------------
// populate the shown columns based on conditions and their order
var shownCol=[];
columns_and_rank=columns_and_rank.sort(function(a,b){return b[1]-a[1];});
//if the staandard condition is in the list, remove it from the list
if(columns_and_rank[columns_and_rank.length-1][1]==0)
	{
		columns_and_rank=columns_and_rank.slice(0,columns_and_rank.length-2);
	}
for(var k=0; k<columns_and_rank.length;k++)
	{
		shownCol.push(columns_and_rank[k][0]);
	}

  $(function() {    
   //use this method to add new colors to pallete
   //$.fn.colorPicker.addColors(['000', '000', 'fff', 'fff']);
   	for(var i=0; i<AnnotationData_js.length;i++)
	{
   		$('#enrichCol'+i).colorPicker();
	}
  });
</script>

<style type="text/css">
.ui-widget-overlay {
	position: fixed !important;
}
</style>
<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link rel="stylesheet" href="resources/css/colorPicker.css"
	type="text/css" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/table.css" rel="stylesheet" type="text/css"
	media="screen" />
</head>

<body style="cursor: default;" onload="initializeDocument()">

	<div align="center">
		<div id="main">
		<div id="logo">
				<a href="home"> <img src="resources/images/Genet.png" /></a>
			</div>
			<div id="headContainer" align="center">
				<div class="header-menu">
				<table id="header_tb" class="noSpace">
					<tr>
						<td align="center" style="vertical-align: top;">
							<div id="headerBTNS">
								<div id="headBTNContainer" align="center">
									<table
										style="border-collapse: collapse; padding: 0px; margin: 0px;"
										cellpadding="0px" cellspacing="0px">
										<tr>
											<td align="center">
												<div id="btnCont" style="padding-top: 3px">
													<table id="btns_tbl"
														style="border-collapse: collapse; padding: 0px; margin: 0px; margin-top: 10px;"
														cellpadding="0px" cellspacing="0px">
														<tr>
															<td>
																<div id="geneDIV" onclick="geneBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px">
																	<img src="resources/images/gene_h.png"
																		title="Search Gene" />
																</div>
															</td>
															<td>
																<div id="moduleDIV" onclick="moduleBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/module_h.png"
																		title="Browse Module" />
																	<!-- <form id="moduleSubmit" method="post" action="Module_of_Interest"> -->
																	<!-- </form> -->
																</div>
															</td>
															<td>
																<div id="pathwayDIV" onclick="pathwayBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/pathway_h.png"
																		title="Search Pathway" />
																</div>
															</td>
															<td>
																<div id="downloadDIV" onclick="downloadBTNClicked()"
																	style="padding-bottom: 3px; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/download_h.png"
																		title="Download Data" />
																</div>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div id="bottomLineBTN"
													style="border-bottom: 2px solid #FFFFFF;">
													<div align="left">
														<img id="topArrow"
															style="vertical-align: bottom; text-align: center; position: relative; bottom: -2px; left: 0px"
															src="resources/images/arrowTop.png" />
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div id="geneSearchForm"
													style="display: none; text-align: center">
													<form id="geneSubmit" method="post"
														action="Gene_of_Interest">
														<table id="formContainer_tb" style="width: 650px"
															class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 94px">Find Genes by</p>
																</td>

																<td width="100px"><select id="geneId" name="id">
																		<option>Symbol</option>
																		<option>Systematic ID</option>
																		<option>Entrez ID</option>
																</select></td>
																<td width="100px"><input autocomplete="off"
																	id="geneValue" name="value" list="genedatalist"
																	type="text" /> <datalist id="genedatalist"></datalist>
																</td>
																<td align="left">
																	<p style="width: 76px">in organism</p>
																</td>
																<td width="90px"><select class="btn"
																	id="geneCategory" name="category" style="width: 150px;">
																		<option>------Please Select------</option>
																</select></td>

																<td width="20px">
																	<button id="geneGoBTN" type="submit" class="btn goBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>
																</td>
																<td width="10px" align="center"></td>
															</tr>
														</table>
													</form>
												</div>
												<div id="moduleSearchForm" style="display: none">
													<form id="moduleSubmit" method="post"
														action="Module_of_Interest">
														<table id="moduleContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 110px">Select organism</p>
																</td>
																<td><select class="btn" id="moduleCategory"
																	name="category" style="width: 250px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="moduleGoBTN" type="submit"
																		class="btn goBTN moduleBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>

																</td>
															</tr>
														</table>
													</form>
												</div>
												<div id="pathwaySearchForm" style="display: none">
													<form id="pathwaySubmit" method="post"
														action="Pathway_Representation">
														<table id="pathwayContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 94px">Find pathways</p>
																</td>
																<td width="210px"><input style="width: 210px"
																	id="pathwayValue" name="value" type="text" /></td>
																<td align="center" width="80px">
																	<p style="width: 80px">in organism</p>
																</td>

																<td width="90px"><select class="btn"
																	id="pathwayCategory" name="category"
																	style="width: 150px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="pathwayGoBTN" type="submit"
																		class="btn goBTN">
																		<p style="visibility: inherit" class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>
																</td>
															</tr>
														</table>
													</form>
												</div>
												<div id="downloadForm" style="display: none">
													<form id="downloadSubmit" method="get"
														action="downloadData">
														<table id="downloadContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 110px">Select organism</p>
																</td>
																<td><select class="btn" id="downloadCategory"
																	name="category" style="width: 250px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="downloadGoBTN" type="submit"
																		class="btn goBTN moduleBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>

																</td>
															</tr>
														</table>
													</form>
												</div>
											</td>
										</tr>
									</table>

								</div>
							</div>
						</td>
						<td align="right" class="module-buttons">
							<table class="noSpace">
								<tr>
									<td>
										<div id="expreBTN">
											<img src="resources/images/expr.png"
												title="Collapse Expression Network" align="middle" />
										</div>
									</td>
									<td width="10px"></td>
									<td>
										<div id="graphBTN">
											<img src="resources/images/graph.png"
												title="Collapse Gene Network" align="middle" />
										</div>
									</td>
									<td width="10px"></td>
									<td>
										<div id="annoBTN">
											<img src="resources/images/anno3.png"
												title="Collapse Annotation Table" align="middle" />
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div id="expreBTN_line">
											<img src="resources/images/line.png"
												title="Collapse Expression Network" align="middle"
												width="32px" height="12px" />
										</div>
									</td>
									<td></td>
									<td>
										<div id="graphBTN_line">
											<img src="resources/images/line.png"
												title="Collapse Gene Network" align="middle" width="32px"
												height="12px" />
										</div>
									</td>
									<td></td>
									<td>
										<div id="annoBTN_line">
											<img src="resources/images/line.png"
												title="Collapse Annotation Table" align="middle"
												width="33px" height="12px" />
										</div>
									</td>
								</tr>
							</table>
						</td>
						<!-->
<!-->
					</tr>
				</table>
				</div>
			</div>
			<div class="main-menu">
					<nav class="horizontal-nav1 ">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="login">Data Contributor Login</a></li>
						<li><a href="help">Help</a></li>
					</ul>
					</nav>
			</div>
			<div class="no-content"></div>
			<div id="page-contents" style="display: none;">
			<div class="noSpace" id="contents">


				<div id="annotationHeader">
					<table id="aHead_tb" class="noSpace" align="center">
						<tr>
							<td class="paddingLeft" align="left"><label class="noSpace">Annotation
									table</label></td>
							<td align="right" style="padding-right: 20px"><img
								class="noSpace" src="resources/images/min.png"
								title="Collapse Annotation Table" width="12px" height="4px" /></td>
						</tr>
					</table>
				</div>
				<div id="annotationContent" class="noSpace" style="height: 380px">
					<div align="center" style="background-color: #f3f3f3">
						<table id="bgTable" width="1100px" cellpadding="0" cellspacing="0">
							<tr>
								<td align="center">
									<p class="title">
										<strong>Module Information</strong>
									</p>
								</td>
								<td id="centralLeft" width="11px"></td>
								<td id="centralRight" width="11px"></td>
								<td width="60%">
									<p class="title">
										<strong>Associated Genes</strong>
									</p>
								</td>
								<td>
									<button class="submit"
										onclick="window.open('/genet/getModuleGenes?module=<%=module%>&category=<%=category%>')">
										<span>View all genes</span>
									</button>
								</td>
							</tr>
							<tr>
								<td style="padding-left: 4px; vertical-align: top"
									align="center">
									<table cellpadding="0" cellspacing="1" border="0"
										class="nospace">
										<tr>
											<td>
												<p class="title2">Module:</p>
											</td>
											<td>
												<p id="symbolValue" class="valueTXT"><%=module%></p>
											</td>
										</tr>
										<tr>
											<td>
												<p class="title2">Hub symbol:</p>
											</td>
											<td>
												<p id="systematicIDValue" class="valueTXT"><%=hubSymbol%></p>
											</td>
										</tr>
										<tr>
											<td>
												<p class="title2">Total # of genes:</p>
											</td>
											<td>
												<p id="entrezIDValue" class="valueTXT"><%=totalNoOfGenesInModule%></p>
											</td>
										</tr>
										<tr>
											<td>
												<p class="title2"># of genes loaded:</p>
											</td>
											<td>
												<p id="entrezIDValue1" class="valueTXT"><%=genesOfModule.length%></p>
											</td>
										</tr>
									</table>
								</td>
								<td></td>
								<td></td>
								<td
									style="padding-bottom: 20px; margin-bottom: 20px; vertical-align: top">

									<table id="annotationTableHeader" align="center">
										<thead">
											<tr>
												<th align="center" width="110px">Symbol</th>
												<th align="center">Description</th>
											</tr>
										</thead>
									</table>
									<div
										style="height: 280px; overflow: scroll; overflow-x: hidden">
										<table id="annotationTable" align="left">
											<tbody>

											</tbody>
										</table>
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>

				<div id="expreHeader">
					<table id="eHead_tb" class="noSpace" align="center">
						<tr>
							<td class="paddingLeft" align="left"><label class="noSpace">Expression
									Profile: Log-ratio / Z-score</label></td>
							<td align="right" style="padding-right: 20px"><img
								class="noSpace" src="resources/images/min.png"
								title="Collapse Expression Network" width="12px" height="4px" /></td>
						</tr>
					</table>
				</div>
				<div id="expreContent" class="noSpace" align="center"
					style="height: 792px; background-color: #f3f3f3">
					<table style="border-collapse: collapse">
						<tr>
							<td style="vertical-align: top;">
								<table>
									<tr>
										<td>Conditions:</td>
									</tr>
									<tr>
										<td>
											<div id="conditionList" align="left"
												style="overflow: scroll; height: 765px; width: 200px;">
											</div>
										</td>
									</tr>
								</table>
							</td>
							<td>
								<div style="margin-left: 20px;">
									Max Graph Score: <select id="maxdatavalue"
										class="positivenumbercombo"></select>
								</div>
								<div id="diagram" style="overflow-x: scroll;"></div>
								<div id="diagramTable" style="overflow-x: scroll;"></div>
							</td>
						</tr>
					</table>

				</div>
			</div>

			<div id="graphHeader">
				<table id="gHead_tb" class="noSpace" align="center">
					<tr>
						<td id="graphName" class="paddingLeft" align="left"><label
							id="geneNumbers" class="noSpace">Significantly
								over-represented annotations:</label></td>
						<td align="right" style="padding-right: 20px"><img
							class="noSpace" src="resources/images/min.png"
							title="Collapse Gene Network" width="12px" height="4px" /></td>
					</tr>
				</table>
			</div>

			<div id="graphContent" class="noSpace" style="height: 600px"
				align="right">
				<table style="width: 100%; border-collapse: collapse;">
					<tr>
						<td
							style="vertical-align: top; width: 400px; border-right: 2px solid #d5d5d5;">

							<table id="enrichmentTableHeader" align="center">
								<thead>
									<tr>
										<th align="center" width="20px"></th>
										<th align="center" width="40px"
											style="border-bottom: 2px solid #6678b1;">Color</th>
										<th align="center" width="130px"
											style="border-bottom: 2px solid #6678b1;">Name</th>
										<th align="center" style="border-bottom: 2px solid #6678b1;">Description</th>
									</tr>
								</thead>
							</table>
							<div style="height: 570px; overflow: auto; overflow-x: hidden">
								<table id="enrichmentTable" align="center">
									<tbody style="width: 300px; vertical-align: middle;"
										align="center">

									</tbody>
								</table>
							</div>
						</td>
						<td>
							<div id="cytoscapeweb" style="height: 600px; z-index: 0;">
							</div>
						</td>
					</tr>
				</table>

				<div id="undoDiv"
					style="position: relative; z-index: 2; top: -360px; left: -8px; background-color: #FFF; border-radius: 15px; width: 135px; height: 30px; border: 2px solid #464646;">
					<table class="nospace" width="130px">
						<tr>
							<td><img id="closeUndo" src="resources/images/cb.png"
								onclick="invisUndo()"></img></td>
							<td><a onclick="undoClicked()" style="color: #800000;"><u>Undo</u></a>
							</td>
						</tr>
					</table>
				</div>
				<div
					style="position: relative; z-index: 1; top: -392px; left: -8px; background-color: #FFF; border-radius: 15px; width: 166px; height: 100px; border: 2px solid #464646;">
					<div>
						<div align="center">
							<table style="width: 100%;">
								<tr style="height: 30px">
									<td colspan="2" align="center">
										<p>Reset gene network</p> <input id="noOfGenes" type="hidden"
										value="<%=genesOfModule.length%>" />
									</td>
								</tr>
								<tr style="height: 20px">
									<td colspan="2">
										<div id="slider" style="margin: 0 5% 0 3%;"></div>
									</td>
								</tr>
								<tr style="height: 10px">
									<td colspan="2">
										<div>
											<span style="margin-right: 10%;">20</span> <span
												style="margin-right: 10%;">40</span> <span
												style="margin-right: 10%;">60</span> <span
												style="margin-right: 5%;">80</span> <span>100</span>
										</div>
									</td>
								</tr>
								<tr style="height: 10px">
									<td colspan="2" align="center">
										<p>
											# of Genes:
											<%=genesOfModule.length%></p>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div
					style="position: relative; z-index: 1; top: -390px; left: -8px; background-color: #FFF; border-radius: 15px; width: 166px; height: 180px; border: 2px solid #464646;">
					<div>
						<div align="center">
							<table>
								<tr style="height: 6px">
									<td colspan="2" align="center">
										<p>Correlation filter</p>
									</td>
								</tr>

								<tr style="height: 40px">
									<td colspan="2" align="center">value &ge;<input
										id="corr_valueBigger" value="0"
										title="Enter a positive value between 0 and 1"
										style="width: 60px" />
									</td>
								</tr>

								<tr align="center">
									<td colspan="2">or</td>
								</tr>

								<tr style="height: 40px">
									<td colspan="2" align="center">value &le;<input
										id="corr_valueLower" value="0"
										title="Enter a negative value between -1 and 0"
										style="width: 60px" />
									</td>
								</tr>

								<tr>
									<td align="center">
										<button class="submit" style="margin: 0px;"
											onclick="clearCorrelation()" title="Clear">
											<span>Clear</span>
										</button>
									</td>
									<td>
										<button class="submit" style="margin: 0px;"
											onclick="updateClicked()"
											title="Update the network based on the entered correlation values">
											<span>Update</span>
										</button>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
		<form style="visibility: hidden" id="geneSubmitHidden" method="post"
			action="Gene_of_Interest">
			<select name="category">
				<option><%=category%></option>
			</select> <select name="id">
				<option>Symbol</option>
			</select> <input id="valueHidden" name="value" type="text" />
		</form>
	</div>
	<input id="category" type="hidden" value="<%=category%>" />
	<div id="dialogs" style="visibility: hidden;">
		<div id="dialog-message" title="Required data missing">
			<p>Please select an organism</p>
		</div>
		<div id="dialog-message1" title="Required data missing">
			<p>Please provide value for search criteria</p>
		</div>
		<div id="dialog-message2" title="Required data missing">
			<p>Please enter a valid identifier</p>
		</div>
	</div>
	<script>
//========================================= Annotation Section=======================================================
for(var i=0; i<genesOfModule_js.length;i++)
{
	addRow(genesOfModule_js[i][0],genesOfModule_js[i][1]);
}

function addRow(symbol, desc){
var tbody = document.getElementById('annotationTable').getElementsByTagName("tbody")[0];
var row = document.createElement("tr");
var data1 = document.createElement("td");
data1.setAttribute("width", "110px");
var textLabel=document.createElement("label");
textLabel.innerHTML =symbol;
textLabel.setAttribute("class","hyperLink");
data1.appendChild(textLabel);
textLabel.onclick=function(){
	var geneName=document.getElementById("valueHidden");
	geneName.value=symbol;
	geneName.innerHTML=symbol;
	//this.form.target='_blank';return true;
	var geneSubmitForm=document.getElementById("geneSubmitHidden");
	geneSubmitForm.target='_blank';
	geneSubmitForm.submit();
	}

var data2 = document.createElement("td");
data2.appendChild (document.createTextNode(desc));
row.appendChild(data1);
row.appendChild(data2);
tbody.appendChild(row);
}
function geneClicked(symbol)
{
	var geneName=document.getElementById("valueHidden");
	geneName.value=symbol;
	geneName.innerHTML=symbol;
	var geneSubmitForm=document.getElementById("geneSubmitHidden");
	geneSubmitForm.target='_blank';
	geneSubmitForm.submit();
}
//=========================================Expression Info section =================================================
//--------------------
//get window width
function GetWidth()
{
        var x = 1000;
        if (self.innerHeight)
        {
                x = self.innerWidth;
        }
        else if (document.documentElement && document.documentElement.clientHeight)
        {
                x = document.documentElement.clientWidth;
        }
        else if (document.body)
        {
                x = document.body.clientWidth;
        }
        return x;
}

//------------------drawing the diagram--------------------------------------------

var windowWidth=GetWidth();
var width=windowWidth-200 - 40;
var diagramWidth = 35 * (shownCol.length -1);
diagramWidth = diagramWidth>width?diagramWidth:width;
$('#diagram').width(width);
$('#diagramTable').width(width + 15);
//--------------------
//alert(conditionsValues_js);
setGraphLimits(4, -4);
initiailize("#diagram","#diagramTable",diagramWidth,580,shownCol,120,conditionsValues_js);
addGraph("average",expreAVGinfo_js);
//genesExpressionInfo_js
//genesOfModule_js
for(var i=0; i<genesOfModule_js.length; i++)
	{
	addGraph(genesOfModule_js[i][0],genesExpressionInfo_js[i]);
	}

update();
populateConditionList();
// 
function conditionOvered()
{
	//alert(i);
	try{
	
	var condName=this.getAttribute("id");
	condName=condName.split(":")[1];
	var div=document.getElementById("cond:"+condName);
	div.style.backgroundColor="#cccccc";
	var textLine=document.getElementById("lineText:"+condName);
	textLine.style.fontSize="12px";
	textLine.fontWeight="bold";
	var line=document.getElementById("line:"+condName);
	line.style.strokeWidth="1.5px";
	line.style.stroke="#626262";
	}catch(err){}
}
function conditionOuted()
{
	try{
	var condName=this.getAttribute("id");
	condName=condName.split(":")[1];
	var div=document.getElementById("cond:"+condName);
	div.style.backgroundColor="#f3f3f3";
	var textLine=document.getElementById("lineText:"+condName);
	textLine.style.fontSize="10px";
	textLine.fontWeight="normal";
	var line=document.getElementById("line:"+condName);
	line.style.strokeWidth="0.4px";
	line.style.stroke="#c7c7c7";
	}catch(err){}
}
function populateConditionList()
{
	//add a div, and a checkbox& text inside this DIV
	for(var i=0; i<columns_and_rank.length;i++)
	{
		var newdiv = document.createElement('div');
		//newdiv
		newdiv.addEventListener("mouseover", conditionOvered, false);
		newdiv.addEventListener("mouseout", conditionOuted, false);
		newdiv.setAttribute('id', "cond:"+columns_and_rank[i][0]); 
		newdiv.style.width = "300px";
		var chkBox=document.createElement('input');
		chkBox.setAttribute('id', "chk"+i);
		chkBox.setAttribute('type', "checkbox");
		chkBox.setAttribute('value', i);
		chkBox.onclick=function(){if(this.checked){addColumn(this.value);}else{removeColumn(this.value);}};
		chkBox.checked=true;
		newdiv.appendChild(chkBox);
		var label = document.createElement("label");
		label.htmlFor = "id";
		label.setAttribute('class',"valueTXT hyperLink-conditions");
		label.setAttribute('title',"No. of Replicates: " + columns_and_rank[i][3] + " || Strain: " + columns_and_rank[i][4]
			+ " || Growth Phase: " + columns_and_rank[i][5] + " || Medium: " + columns_and_rank[i][6] + " || Description: " + columns_and_rank[i][7]);
		label.appendChild(document.createTextNode(columns_and_rank[i][0]));
		newdiv.appendChild(label);
		document.getElementById("conditionList").appendChild(newdiv);
	}
	$.unblockUI();
}
function addColumn(i)
{
	//shownCol.push(columns_and_rank[i]);
	columns_and_rank[i][2]=1;
	updateShownCol();
	updateColumns(shownCol);
	update();
}
function removeColumn(number)
{
	columns_and_rank[number][2]=0;
	updateShownCol();
	updateColumns(shownCol);
	update();
}
function updateShownCol()
{
	shownCol=[];
	for(var i=0;i<columns_and_rank.length;i++)
	{
		if(columns_and_rank[i][2]==1){
			shownCol.push(columns_and_rank[i][0]);
		}
	}
}
//generating random color
function get_random_color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.round(Math.random() * 100)%15];
    }
    return color;
}
//---------------------------- draw enrichment table ------------------------------
for(var i=0; i<AnnotationData_js.length;i++)
{
	var color=get_random_color();
	addEnrichTableRow(color,AnnotationData_js[i][0],AnnotationData_js[i][1],i, AnnotationData_js[i][3]);
	AnnotationData_js[i].push(color);	
}
function addEnrichTableRow(cell1, cell2, cell3,id, url){
	
	var tbody = document.getElementById('enrichmentTable').getElementsByTagName("tbody")[0];
	var row = document.createElement("tr");
	row.setAttribute("id","enrichRow"+id);
	var data00=document.createElement("td");
	data00.style.width="20px";
	data00.style.paddingLeft="10px";
	var checkbox = document.createElement('input');
	checkbox.type = "checkbox";
	checkbox.id = "enrichCHK"+id;
	data00.appendChild(checkbox);
	data00.setAttribute("onclick","enrichRowClicked("+id+")");
	var data0 = document.createElement("td");

	//create a div to attach color picker to it
	data0.setAttribute("width", "41px");
	var divCP=document.createElement("div");
	divCP.setAttribute("id","enrichDiv"+id);
	//divCP.setAttribute("class","controlset");
	var inputCP=document.createElement("input");
	inputCP.setAttribute("id","enrichCol"+id);
	inputCP.style.visibility="hidden";
	inputCP.setAttribute("name","enrichCol"+id);
	inputCP.setAttribute("type","text");
	inputCP.setAttribute("width", "35px");
	inputCP.setAttribute("value",cell1);
	divCP.appendChild(inputCP);
	data0.appendChild(divCP);
	//	

	var data1 = document.createElement("td");
	data1.setAttribute("width", "130px");
	//var aTagURL="<a target=\"_blank\" href="+url+">"+cell2+"</a>";
	if(url!=""&&url!="undefined")
	{
		var a=document.createElement("a");
		a.setAttribute("target", "blank");
		a.setAttribute("href",url);
		a.appendChild(document.createTextNode(cell2));
		data1.appendChild(a);
	}
	else
	{
		data1.appendChild (document.createTextNode(cell2));
	}
	var data2 = document.createElement("td");
	
	data2.appendChild (document.createTextNode(cell3));
	
	row.appendChild(data00);
	row.appendChild(data0);
	row.appendChild(data1);
	row.appendChild(data2);
	tbody.appendChild(row);
	
}
var pervClicked=-1;
var moduleColor=networkStyleData_js[0][1];
var clickedRowsID=new Array();
var annotationMap = new HashMap();
var selectedNodesForHighlight=new Array();
function findIntersection()
{
	selectedNodesForHighlight=new Array();
	if(clickedRowsID.length==1)
		{
		selectedNodesForHighlight=AnnotationData_js[clickedRowsID[0]][2].concat();
		return;
		}
	if(clickedRowsID.length>=2)
		{
		for(var i=0; i<AnnotationData_js[clickedRowsID[0]][2].length;i++)
			{
				for(var j=0; j<AnnotationData_js[clickedRowsID[1]][2].length;j++)
					{
						if(AnnotationData_js[clickedRowsID[0]][2][i]==AnnotationData_js[clickedRowsID[1]][2][j])
						{
							selectedNodesForHighlight.push(AnnotationData_js[clickedRowsID[0]][2][i]);
							break;
						}
					}
			}
		if(clickedRowsID.length==2)
			{
			return;
			}
			
		for(var i=0; i<selectedNodesForHighlight.length;i++)
			{
				for(var j=2; j<clickedRowsID.length;j++)
					{
						if(findNode(selectedNodesForHighlight[i],AnnotationData_js[clickedRowsID[j]][2])==-1)
							{
								selectedNodesForHighlight.splice(i,1);
								i-=1;
								break;
							}
					}
			}
		}
}

function findNode(nodeName, list)
{
	for(var i=0; i<list.length;i++)
		{
		//alert(list[i]+"||||"+nodeName)
		if(list[i]==nodeName)
			{
			return i;
			}
		}
	return -1;
}
function enrichRowClicked(id)
{
	var selectedNodesForHighlight=new Array();
	var clickedRow=document.getElementById("enrichRow"+id);
	if(clickedRow.bgColor=="#cccccc")
		{
			//alert("removing");
			clickedRow.bgColor="#ffffff";
			//remove its id from clicked rowlist
			//var index=findNode(id,clickedRowsID);
			var chk=document.getElementById("enrichCHK"+id);
			chk.checked=false;
			//clickedRowsID.splice(index,1);
			
			var clickedDIV=document.getElementById("enrichDiv"+id);
			var colorPicker=clickedDIV.lastChild;
			var annoColor=colorPicker.style.backgroundColor;
			for(var j=0; j<networkStyleData_js.length;j++)
			{
				var color = null;
				var networkName = networkStyleData_js[j][0].trim();
				if(annotationMap.has(networkName)) {
					var colorArray = annotationMap.get(networkName);
					var index = colorArray.indexOf(annoColor.trim());
					if (index > -1) {
						colorArray.splice(index, 1);
					}
					if(colorArray.length == 0) {
						annotationMap.remove(networkName);
						color = moduleColor;
					} else {
						annotationMap.set(networkName, colorArray);
						color = colorArray[0];
					}
				} else {
					color = moduleColor;
				}
				networkStyleData_js[j][1]=color;
			}
			var newNetworkStyle=networkStyle(networkStyleData_js);
			var newStyle=jQuery.parseJSON(newNetworkStyle);
			vis.visualStyle(newStyle);
			pervClicked=-1;
			return;
		}
	else{
		clickedRow.bgColor="#cccccc";
		var chk=document.getElementById("enrichCHK"+id);
		chk.checked=true;
		var annoColor;
		var clickedDIV=document.getElementById("enrichDiv"+id);
		var colorPicker=clickedDIV.lastChild;
		annoColor=colorPicker.style.backgroundColor;
		selectedNodesForHighlight=AnnotationData_js[id][2].concat();
		for(var i=0; i<selectedNodesForHighlight.length;i++)
			{
			for(var j=0; j<networkStyleData_js.length;j++)
			{
				var networkName = networkStyleData_js[j][0].trim();
				var networkColor = networkStyleData_js[j][1].trim();
				if(selectedNodesForHighlight[i].trim() == networkName)
				{
					if(networkColor == moduleColor)
						networkStyleData_js[j][1]=annoColor;
					if(annotationMap.has(networkName)) {
						var colorArray = annotationMap.get(networkName);
						colorArray.push(annoColor);
						annotationMap.set(networkName, colorArray);
					} else {
						var colors = new Array();
						colors.push(annoColor);
						annotationMap.set(networkName, colors);
					}	
				}
			}
			}
		var newNetworkStyle=networkStyle(networkStyleData_js);
		var newStyle=jQuery.parseJSON(newNetworkStyle);
		vis.visualStyle(newStyle);
	}
}
function updateNetworkColor()
{
	
	if(clickedRowsID.length==1)
	{
		var clickedDIV=document.getElementById("enrichDiv"+clickedRowsID[0]);
		var colorPicker=clickedDIV.lastChild;
		annoColor=colorPicker.style.backgroundColor;
		for(var j=0; j<networkStyleData_js.length;j++)
		{
		networkStyleData_js[j][1]=moduleColor;
		}
		for(var i=0; i<selectedNodesForHighlight.length;i++)
		{
		for(var j=0; j<networkStyleData_js.length;j++)
			{
			//alert("selectedNodesForHighlight[i]:"+selectedNodesForHighlight[i]+"||"+"networkStyleData_js[j][0]:"+networkStyleData_js[j][0]);
			if(selectedNodesForHighlight[i]==networkStyleData_js[j][0])
				{
				networkStyleData_js[j][1]=annoColor;
				}
			}
		}
		var newNetworkStyle=networkStyle(networkStyleData_js);
		var newStyle=jQuery.parseJSON(newNetworkStyle);
		vis.visualStyle(newStyle);
			//annoColor=AnnotationData_js[clickedRowsID[0]][4];
			//annoColor=clickedRow.
			//selectedNodesForHighlight=AnnotationData_js[id][2].concat();
	}
}
//---------------------------draw Network (Graph) ---------------------------------
var div_id = "cytoscapeweb";

var options = {
// the location of Cytoscape Web SWF
swfPath: "resources/swf/CytoscapeWeb",
// where you have the Flash installer SWF
flashInstallerPath: "resources/swf/playerProductInstall"
};

var vis = new org.cytoscapeweb.Visualization(div_id, options);
//alert(nodeData_js);
var netdata=inputNetworkConstructorForModule(nodeData_js,edgeData_js);
//alert(netdata[0]);
var ntwrk_str=toCytoScape(nodeSchema_js, edgeSchema_js, netdata[0], netdata[1]);

var ntwrk=jQuery.parseJSON(ntwrk_str);
var networkStyle1=networkStyle(networkStyleData_js);
var style=jQuery.parseJSON(networkStyle1);
var networkOption={network:ntwrk,visualStyle:style};

vis.draw(networkOption);
vis.addListener("dblclick", "nodes", function(evt) {
	var nodes=evt.target;
	//alert();
	var geneName=document.getElementById("valueHidden");
	geneName.value=nodes.data.id;
	geneName.innerHTML=nodes.data.id;
	var geneSubmitForm=document.getElementById("geneSubmitHidden");
	geneSubmitForm.target='_blank';
	geneSubmitForm.submit();
});
//----------correlation filter  
function updateClicked()
{
	
	//
	//prevFilterValue[0]=prevFilterValue[1];
	//prevFilterValue[1]=document.getElementById("corr_valueBigger").value+","+document.getElementById("corr_valueLower").value;
	//lastActivity="corrFilter";
	//document.getElementById("undoDiv").style.visibility="visible";
	//saveCurrentAction("corrFilter", document.getElementById("corr_valueBigger").value+","+document.getElementById("corr_valueLower").value);
	if(correlationUpdate()==1)
		{
		//save the current as a history state one, and store the last action as the current state
		//alert("HistoryOfstates:"+HistoryOfstates);
		//alert("going to insert to history of states:"+currentState);
		//HistoryOfstates.push(currentState.concat());
		//actionHistory.push("corrFilter");
		//document.getElementById("undoDiv").style.visibility="visible";
		//currentState[0]=document.getElementById("corr_valueBigger").value+","+document.getElementById("corr_valueLower").value;
		//alert("history after inserting:"+HistoryOfstates);
		}
}
//---------------------------------------------------------------------------------
document.getElementById("undoDiv").style.visibility="hidden";
function correlationUpdate()
{
	var corrValueBigger=document.getElementById("corr_valueBigger").value;
	var corrValueLower=document.getElementById("corr_valueLower").value;
	if((corrValueBigger==""||parseFloat(corrValueBigger)==NaN)&&(corrValueLower=="")||parseFloat(corrValueLower)==NaN)
		{
			alert("Enter correlation values between -1 and 1");
			return -1;
		}
	
	//vis.removeFilter();
	var count=0;
	//alert(corrValueLower);
	vis.filter("edges",function(edge) {
		var tempEdge;
		if(edge.data.correlation!="")
			{
				if(edge.data.correlation>= corrValueBigger||(Math.abs(edge.data.correlation)>=Math.abs(corrValueLower)&&edge.data.correlation<0))
					{
					tempEdge=edge;
					count++;
					}
			}
		
	    return tempEdge;
	}, true);
return 1;
}
function clearCorrelation()
{
	
	document.getElementById("corr_valueBigger").value=0;
	document.getElementById("corr_valueLower").value=0;
	//saveCurrentAction("corrFilter", "0,0");
	//prevFilterValue[0]=prevFilterValue[1];
	//prevFilterValue[1]="0,0";
	//lastActivity="corrFilter";
	//document.getElementById("undoDiv").style.visibility="visible";
	//HistoryOfstates.push(currentState);
	//actionHistory.push("corrFilter");
	//document.getElementById("undoDiv").style.visibility="visible";
	//currentState[0]="0,0";
	vis.removeFilter();
	document.getElementById("geneNumbers").innerHTML='Significantly over-represented annotations: ('+networkFullData_js.length.toString()+' genes)';
}
//---------------------------------------------------------------------------
//query expansion for gene Search
var changeAutoComplete = false;
	var totalRecordsFetched = 0;
	var changeEventCount = 0;
	
	$("[id$=geneValue]").focusout(function(event) {
		changeEventCount = 0;
	});
	$("[id$=geneValue]").keyup(function(event) {
		if(event.which <= 90 && event.which >= 48 || event.which==8 || event.which == 27) {
			changeEventCount = 0;
			checkLastTextBoxValue($("[id$=geneValue]").val(), null);
		} else if (event.which == 40){
			changeEventCount++;
			if(changeEventCount==0 ||changeEventCount==totalRecordsFetched+1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'next');
			}
		} else if (event.which == 38){
			changeEventCount--;
			if(changeEventCount==0 || changeEventCount==-totalRecordsFetched-1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'prev');
			}
		} 
	});	
	
	// 	query expansion for gene Search
	function checkLastTextBoxValue(oldValue, next) {
		oldValue = oldValue.split(" |")[0];
		if (oldValue != "" || next) {
			var type = $("[id$=geneId]").val();
			var organism = $("[id$=geneCategory]").val();
			if (organism != '' && organism != '------Please Select------')
				loadDropDownMenu(oldValue, type, organism, next);
		}
	}
	function loadDropDownMenu(currentQuery, type, organism, next) {
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var resp = xmlhttp.responseText;
				var currentArray = new Array();
				if(resp.length!=0)
					currentArray = resp.split(",");
				var dataList = document.getElementById('genedatalist');
				totalRecordsFetched = currentArray.length;
				if(currentArray.length > 0) {
					$(dataList).empty();
					changeEventCount = 0;
				}
				currentArray.forEach(function(item) {
			        var option = document.createElement('option');
			        option.value = item;
			        dataList.appendChild(option);
			      });
			}
		}
		var request = "Gene_of_Interest?query="+currentQuery+"&type="+type+"&category="+organism+"&job_id=queryExp&size=10";
		  if(next!=null)
			  request = request + "&job_parameter=" + next;
		xmlhttp.open("GET", request, true);
		xmlhttp.send();
	}
</script>
</body>
</html>
