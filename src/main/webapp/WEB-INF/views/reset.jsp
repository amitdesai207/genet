<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reset Password</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script type="text/javascript"
	src="resources/js/password_strength_plugin.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />

<script type="text/javascript">
	$(function() {
		$("#dialog-message").dialog({
			autoOpen : false,
			modal : true,
			buttons : {
				Ok : function() {
					$(this).dialog("close");
				}
			}
		});

		$("#password").passStrength({});

		$("#rePassword").change(function() {
			if ($("#rePassword").val() == $("#password").val())
				$("#passError").text("");
			else
				$("#passError").text("Password does not match");
		});

	});
	function clearForm() {
		$('#password').val('');
		$('#rePassword').val('');
		return false;
	}
	function submitForm() {
		document.getElementById("resetForm").action = "reset";
		document.getElementById("resetForm").submit();
	}
</script>
<% boolean recoveryMode = (Boolean) (request.getAttribute("recoveryMode") == null ? false : true); %>
</head>

<body style="background-color: #e5e4e9">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block; width: 100%">
				<div style="width: 50%; float: left;">
					<a href="home"><img
						style="padding-left: 30px; height: 100%; width: 30%;"
						src="resources/images/Genet.png" /></a>
				</div>
				<div style="position: inherit; margin-top: -37px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="help">Help</a></li>
						<li><a href="logout">Sign Out</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer">
			<table style="width: 100%; min-width: 1000px">
				<tr>
					<td align="center">
						<div id="bottunContainer">
							<table class="containerTable" style="margin-top: -50px;">
								<tr>
									<td><form:form onsubmit="event.preventDefault();"
											commandName="recoveryModel" id="resetForm" method="POST"
											action="">
											<div class="login-screen">
												<table class="table-border">
													<tr>
														<td colspan="2" align="center">Please provide a new
															password</td>
													</tr>
													<tr>
														<td><spring:message code="label.password"
																text="Password" />:</td>
														<td><form:input type="password" id="password"
																class="password" path="userName" maxlength="20"
																size="22" /></td>
													</tr>
													<tr>
														<td><spring:message code="label.reEnterPassword"
																text="Re-enter Password" />:</td>
														<td><input type="password" id="rePassword"
															maxlength="20" size="22" /><br /> <span id="passError"
															class="error"></span></td>
													</tr>
													<tr style="width: 60%">
														<td><button class="submit" onclick="submitForm()">
																<span><spring:message code="label.loginButton"
																		text="Change Password" /></span>
															</button></td>
														<td><button style="margin-left: 0px;" class="submit"
																onclick="clearForm()">
																<span><spring:message code="label.clearButton"
																		text="Clear" /></span>
															</button>
															<%if(!recoveryMode) { %>
															<button style="margin-left: 5px;" class="submit"
																onclick="location.href='jobHomePage'">
																<span><spring:message code="label.clearButton"
																		text="Cancel" /></span>
															</button>
															<%} %>
															</td>
													</tr>
												</table>
											</div>
											<div></div>
											<!-- 												</div> -->
											<!-- 											</div> -->
										</form:form></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center">
						<div>
							<table style="border-collapse: collapse">
								<tr>
									<td>
										<p style="font-size: 12px">&copy;</p>
									</td>
									<td><a style="font-size: 12px" href="http://www.mun.ca"
										target="_blank">Memorial University of Newfoundland</a></td>
									<td>
										<p style="font-size: 12px; padding-left: 2px">2016</p>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<div></div>
	</div>
	<div id="dialogs" style="visibility: hidden;">
		<div id="dialog-message" title="Success">
			<p>Your password is changed</p>
		</div>
	</div>
</body>
</html>
