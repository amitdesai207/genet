<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList,org.mun.genet.vo.OrganismVo"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contributions</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="resources/js/jquery.bootgrid.js"></script>
<script type="text/javascript" src="resources/js/moderniz.2.8.1.js"></script>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/table.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/bootstrap.css" rel="stylesheet" />
<link href="resources/css/jquery.bootgrid.css" rel="stylesheet" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css"
	media="screen" />

<script type="text/javascript">
	$(function() {
		$("#contriTable").bootgrid({
			rowCount : 10,
			navigation : 2,
			columnSelection : false,
			sorting : false,
			caseSensitive : false
		}).on("click.rs.jquery.bootgrid", function(e, columns, row) {
			window.open(row["link"]);
		});
	});
</script>
<style>
#contriTable.column #contriTable.text {
	color: #f00 !important;
}

#contriTable.cell {
	font-weight: bold;
}
</style>
</head>

<body style="background-color: #e5e4e9">
	<div id="content" style="width: 100%; min-width: 1000px">
		<div style="padding-top: 20px;">
			<div style="display: inline-block;">
				<div style="width: 50%; float: left;">
					<a href="home"><img
						style="padding-left: 30px; height: 100%; width: 70%;"
						src="resources/images/indexLogo5.png" /></a>
				</div>
				<div style="position: inherit; margin-top: -21px; float: right;">
					<nav class="horizontal-nav full-width horizontalNav-notprocessed">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="login">Data Contributor Login</a></li>
						<li><a href="help">Help</a></li>
					</ul>
					</nav>
				</div>
			</div>
		</div>
		<div id="midContainer" style="height: 100%; padding-bottom: 3em;">
			<div>
				<h2>Contributors</h2>
			</div>
			<div style="padding-left: 5em; padding-right: 5em;">
				<div style="display: inline-block;">
					<div style="width: 50%; float: left;">
						<p>
							<b>GeNET Concept Development</b><br> <span
								style="padding-left: 35px;">Dr. Lourdes Pe�a-Castillo</span><br>
							<span style="padding-left: 35px;">Department of Computer
								Science & Department of Biology</span><br> <span
								style="padding-left: 35px;">Memorial University of
								Newfoundland</span><br> <span style="padding-left: 35px;">St.
								John's, NL A1B 3X5, Canada</span><br> <span
								style="padding-left: 35px;"><a
								href="http://www.cs.mun.ca/~lourdes" target="_blank">www.cs.mun.ca/~lourdes</a></span><br>
							<span style="padding-left: 35px;"><a
								href="mailto:lourdes*at*mun*dot*ca">lourdes *at* mun *dot*
									ca</a></span><br> <br> <span style="padding-left: 35px;">Dr.
								Oscar Meruvia-Pastor</span> <br> <span style="padding-left: 35px;">Department
								of Computer Science</span> <br> <span style="padding-left: 35px;">Office
								of Dean of Science</span> <br> <span style="padding-left: 35px;">Memorial
								University of Newfoundland</span> <br> <span
								style="padding-left: 35px;">St. John's, NL A1B 3X5,
								Canada</span> <br> <span style="padding-left: 35px;"><a
								href="http://www.cs.mun.ca/~omeruvia" target="_blank">www.cs.mun.ca/~omeruvia</a></span>
							<br> <span style="padding-left: 35px;"><a
								href="mailto:oscar*at*mun*dot*ca">oscar *at* mun *dot* ca</a></span> <br>
						</p>
						<p>
							<b>GeNET Web Development</b><br> <span
								style="padding-left: 35px;">Amit Prabhakar Desai</span> <br>
							<span style="padding-left: 35px;">Graduate Student,
								Department of Computer Science</span> <br> <span
								style="padding-left: 35px;">Memorial University of
								Newfoundland</span> <br> <span style="padding-left: 35px;">St.
								John's, NL A1B 3X5, Canada</span> <br> <span
								style="padding-left: 35px;"><a
								href="mailto:apd525*at*mun*dot*ca">apd525 *at* mun *dot* ca</a></span>
							<br> <br> <span style="padding-left: 35px;">Mehdi
								Razeghin, 2013 M.Sc.</span><br> <span style="padding-left: 35px;">
								Department of Computer Science</span> <br> <span
								style="padding-left: 35px;">Memorial University of
								Newfoundland</span> <br> <span style="padding-left: 35px;">St.
								John's, NL A1B 3X5, Canada</span> <br>
						</p>
					</div>
					<div style="width: 48%; float: right;">
						<p>
							<b>Data Contributions</b>
						</p>
						<div style="max-width: 700px">
							<table id="contriTable"
								class="table table-condensed table-hover table-striped"
								style="max-width: 700px">
								<thead>
									<tr>
										<th data-column-id="organism" data-sortable="false"
											data-identifier="true">Organism</th>
										<th data-column-id="link" data-sortable="false"
											data-searchable="false" data-css-class="hyperLink-genes"
											data-formatter="link">Link to Publication(s)</th>
									</tr>
								</thead>
								<%
									ArrayList<OrganismVo> organisms = (ArrayList<OrganismVo>) request.getAttribute("organisms");
								%>
								<c:forEach var="organism" items="${organisms}">
									<tr>
										<td>${organism.name}</td>
										<td><c:forEach var="pubmedId"
												items="${organism.pubmedIds}">
												<a href="${pubmedId}">${pubmedId}</a>
												<br>
											</c:forEach></td>
									</tr>
								</c:forEach>
								<tbody>
								</tbody>
							</table>
						</div>
						<p>
							<b>GeNET use is subject to appropriate acknowledgement
								(citation). Please cite GeNET in your publications as follows:</b><br>
							Pe�a-Castillo, L.; Meruvia-Pastor, O. GeNET: Gene Network
							Visualization. URL: http://bengi.cs.mun.ca/genet/ Accessed on
							MM/YYYY.
						</p>
						<p>
							<b>To cite specific gene co-expression network data in your
								publications:</b><br> Cite the article corresponding to the
							data used.
						</p>
					</div>
				</div>
			</div>
		</div>
		<div style="padding-top: 1em;">
			<center>
				<p style="font-size: 12px">
					&copy; <a style="font-size: 12px" href="http://www.mun.ca"
						target="_blank">Memorial University of Newfoundland</a> 2016
				</p>
			</center>
		</div>
		<div></div>
	</div>
</body>
</html>
