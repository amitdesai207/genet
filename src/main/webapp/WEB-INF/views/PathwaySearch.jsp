<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pathway Search</title>

<script type="text/javascript" src="resources/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="resources/js/interaction.js"></script>

<%
String[][] rowsOfTable=(String[][])request.getAttribute("rowsOfTable");
String category=(String)request.getAttribute("category");
%>
<script>

//------------------
window.onload=function()
{
	$( document ).tooltip();
	btnDIVWidth=110;
	//$("#topArrow").animate({"left":60+"px"}, "slow");
	pathwayBTNClicked();
}

var moduleButtonClicked = false;
var btnDIVWidth=650;

function geneBTNClicked()
{
	var difference=(640-btnDIVWidth)/2;
	btnDIVWidth=640;
	var destination=btnDIVWidth/2-70;
	var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left",(prevLeft+difference)+"px");
	$("#topArrow").animate({"left":destination+"px"}, "slow");
	$("#pathwaySearchForm").css("display","none");
	$("#moduleSearchForm").css("display","none");
	$("#downloadForm").css("display","none");
	$("#geneSearchForm").css("display","table");
	moduleButtonClicked = false;
}
function moduleBTNClicked()
{
	if(moduleButtonClicked) {
		$("#moduleGoBTN").trigger( "click" );
	} else {
		moduleButtonClicked=!moduleButtonClicked;
		var difference=(280-btnDIVWidth)/2;
		btnDIVWidth=280;
		var destination=btnDIVWidth/2+35;
		var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
		$("#topArrow").css("left",(prevLeft+difference)+"px");
		$("#topArrow").animate({"left":destination+"px"}, "slow");
		$("#geneSearchForm").css("display","none");
		$("#pathwaySearchForm").css("display","none");
		$("#downloadForm").css("display","none");
		$("#moduleSearchForm").css("display","table");
		moduleButtonClicked = true;
	}
}

function downloadBTNClicked() {	
	if(moduleButtonClicked) {
		$("#downloadGoBTN").trigger( "click" );
	} else {
		var difference=(640-btnDIVWidth)/2;
		btnDIVWidth=640;
		var destination=btnDIVWidth/2-50;
		var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
		$("#topArrow").css("left",(prevLeft+difference)+"px");
		$("#topArrow").animate({"left":destination+"px"}, "slow");
		$("#pathwaySearchForm").css("display","none");
		$("#moduleSearchForm").css("display","none");
		$("#geneSearchForm").css("display","none");
		$("#downloadForm").css("display","table");		
		moduleButtonClicked = true;
	}
}

function pathwayBTNClicked()
{
	var difference=(625-btnDIVWidth)/2;
	btnDIVWidth=625;
	var destination=btnDIVWidth/2+0;
	var prevLeft=parseFloat(document.getElementById("topArrow").style.left);
	$("#topArrow").css("left",(prevLeft+difference)+"px");
	$("#topArrow").animate({"left":destination+"px"}, "slow");
	$("#geneSearchForm").css("display","none");
	$("#moduleSearchForm").css("display","none");
	$("#downloadForm").css("display","none");
	$("#pathwaySearchForm").css("display","table");
	moduleButtonClicked = false;
	downloadButtonClicked = false;
}
</script>

<style type="text/css">
.ui-widget-overlay {
    position: fixed !important;
}
</style>

<link rel="stylesheet" href="resources/css/jquery-ui.min.css"
	type="text/css" media="screen" />
<link href="resources/css/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/theme.css" rel="stylesheet" type="text/css"
	media="screen" />
<link href="resources/css/table.css" rel="stylesheet" type="text/css" media="screen" />
<link href="resources/css/general.css" rel="stylesheet" type="text/css" media="screen" />
</head>

<body onload="initializeDocument()">

	<div align="center">
		<div id="main">
<div id="logo">
				<a href="home"> <img src="resources/images/Genet.png" /></a>
			</div>
			<div id="headContainer" align="center">
				<div class="header-menu">
				<table id="header_tb" class="noSpace">
					<tr>
						<td align="center" style="vertical-align: top;">
							<div id="headerBTNS">
								<div id="headBTNContainer" align="center">
									<table
										style="border-collapse: collapse; padding: 0px; margin: 0px;"
										cellpadding="0px" cellspacing="0px">
										<tr>
											<td align="center">
												<div id="btnCont" style="padding-top: 3px">
													<table id="btns_tbl"
														style="border-collapse: collapse; padding: 0px; margin: 0px; margin-top: 10px;"
														cellpadding="0px" cellspacing="0px">
														<tr>
															<td>
																<div id="geneDIV" onclick="geneBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px">
																	<img src="resources/images/gene_h.png"
																		title="Search Gene" />
																</div>
															</td>
															<td>
																<div id="moduleDIV" onclick="moduleBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/module_h.png"
																		title="Browse Module" />
																	<!-- <form id="moduleSubmit" method="post" action="Module_of_Interest"> -->
																	<!-- </form> -->
																</div>
															</td>
															<td>
																<div id="pathwayDIV" onclick="pathwayBTNClicked()"
																	style="padding-bottom: 3px; border-right: 1px solid #ccc; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/pathway_h.png"
																		title="Search Pathway" />
																</div>
															</td>
															<td>
																<div id="downloadDIV" onclick="downloadBTNClicked()"
																	style="padding-bottom: 3px; padding-right: 8px; padding-left: 8px">
																	<img src="resources/images/download_h.png"
																		title="Download Data" />
																</div>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div id="bottomLineBTN"
													style="border-bottom: 2px solid #FFFFFF;">
													<div align="left">
														<img id="topArrow"
															style="vertical-align: bottom; text-align: center; position: relative; bottom: -2px; left: 0px"
															src="resources/images/arrowTop.png" />
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div id="geneSearchForm"
													style="display: none; text-align: center">
													<form id="geneSubmit" method="post"
														action="Gene_of_Interest">
														<table id="formContainer_tb" style="width: 650px"
															class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 94px">Find Genes by</p>
																</td>

																<td width="100px"><select id="geneId" name="id">
																		<option>Symbol</option>
																		<option>Systematic ID</option>
																		<option>Entrez ID</option>
																</select></td>
																<td width="100px"><input autocomplete="off"
																	id="geneValue" name="value" list="genedatalist"
																	type="text" /> <datalist id="genedatalist"></datalist>
																</td>
																<td align="left">
																	<p style="width: 76px">in organism</p>
																</td>
																<td width="90px"><select class="btn"
																	id="geneCategory" name="category" style="width: 150px;">
																		<option>------Please Select------</option>
																</select></td>

																<td width="20px">
																	<button id="geneGoBTN" type="submit" class="btn goBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>
																</td>
																<td width="10px" align="center"></td>
															</tr>
														</table>
													</form>
												</div>
												<div id="moduleSearchForm" style="display: none">
													<form id="moduleSubmit" method="post"
														action="Module_of_Interest">
														<table id="moduleContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 110px">Select organism</p>
																</td>
																<td><select class="btn" id="moduleCategory"
																	name="category" style="width: 250px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="moduleGoBTN" type="submit"
																		class="btn goBTN moduleBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>

																</td>
															</tr>
														</table>
													</form>
												</div>
												<div id="pathwaySearchForm" style="display: none">
													<form id="pathwaySubmit" method="post"
														action="Pathway_Representation">
														<table id="pathwayContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 94px">Find pathways</p>
																</td>
																<td width="210px"><input style="width: 210px"
																	id="pathwayValue" name="value" type="text" /></td>
																<td align="center" width="80px">
																	<p style="width: 80px">in organism</p>
																</td>

																<td width="90px"><select class="btn"
																	id="pathwayCategory" name="category"
																	style="width: 150px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="pathwayGoBTN" type="submit"
																		class="btn goBTN">
																		<p style="visibility: inherit" class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>
																</td>
															</tr>
														</table>
													</form>
												</div>
												<div id="downloadForm" style="display: none">
													<form id="downloadSubmit" method="get"
														action="downloadData">
														<table id="downloadContainer_tb" class="noSpace">
															<tr>
																<td align="left">
																	<p style="width: 110px">Select organism</p>
																</td>
																<td><select class="btn" id="downloadCategory"
																	name="category" style="width: 250px;">
																		<option>------Please Select------</option>
																</select></td>
																<td width="20px">
																	<button id="downloadGoBTN" type="submit"
																		class="btn goBTN moduleBTN">
																		<p class="noSpace">
																			<strong>Go</strong>
																		</p>
																	</button>

																</td>
															</tr>
														</table>
													</form>
												</div>
											</td>
										</tr>
									</table>

								</div>
							</div>
						</td>
						<!-->
<!-->
					</tr>
				</table>
				</div>
			</div>
			<div class="main-menu">
					<nav class="horizontal-nav1 ">
					<ul>
						<li><a href="home">Home</a></li>
						<li><a href="contact">About GeNet</a></li>
						<li><a href="login">Data Contributor Login</a></li>
						<li><a href="help">Help</a></li>
					</ul>
					</nav>
			</div>
			<div class="noSpace" id="contents" align="center"
				style="width: 100%; padding-bottom: 40px">
				<div>
					<table id="pathwayTable" style="min-width: 1000px">
						<thead>
							<tr>
								<th style="width: 150px">
									<div id="ModuleNameHD">Module name</div>
								</th>
								<th style="width: 200px">Annotation ID</th>
								<th style="width: 350px">Description</th>
								<th style="width: 100px">
									<div id="ModuleGeneHD" title="Number of genes in module with annotation/number of genes in module">#genes</div>
								</th>
								<th style="width: 90px">FDR-PValue</th>
							</tr>
						</thead>
						<tbody id="tbodyRows">
						</tbody>
					</table>
				</div>
				<form id="moduleSubmit" method="post" action="Module_Representation">
				</form>
			</div>
		</div>
	</div>
	<input id="category" type="hidden" value="<%=category%>" />
	<div id="dialogs" style="visibility: hidden;">
		<div id="dialog-message" title="Required data missing">
			<p>Please select an organism</p>
		</div>
		<div id="dialog-message1" title="Required data missing">
			<p>Please provide value for search criteria</p>
		</div>
		<div id="dialog-message2" title="Required data missing">
			<p>Please enter a valid identifier</p>
		</div>
	</div>
	<script type="text/javascript">
//covert java array to javascript array
var rowsOfTable_js=new Array();
<%for (int i=0;i<rowsOfTable.length;i++){%>
	<%System.out.println("i>>:"+i+"||");%>
	rowsOfTable_js[<%=i%>]=new Array(<%=rowsOfTable[i].length%>);
	
	<%for(int j=0; j<rowsOfTable[i].length;j++){%>
	<%System.out.println("ij>>:"+j+"-"+rowsOfTable[i].length+"||"+rowsOfTable[i][j]);%>
		rowsOfTable_js[<%=i%>][<%=j%>]="<%=rowsOfTable[i][j].toString()%>";
	<%}%>
<%}%>
var tBody=document.getElementById("tbodyRows");
for(var i=0; i<rowsOfTable_js.length;i++)
{
	var row=document.createElement("tr");
	var moduleName=document.createElement("td");
	var a=document.createElement("a");
	
	a.setAttribute("class","pathwayTable");
	a.setAttribute("href","/genet/Module_Representation?module="+rowsOfTable_js[i][0]+"&conditionsValues="+rowsOfTable_js[i][5]+'&category=<%=category%>');
	a.setAttribute("target", "_self");
	//genet/Module_Representation?module="+HorData[j][i]+"&conditionsValues="+varticData[j].toString()
	//moduleName.setAttribute("onclick","moduleClicked("+i+")");
	
	a.appendChild(document.createTextNode(rowsOfTable_js[i][0]));
	moduleName.appendChild(a);
	row.appendChild(moduleName);
	for(var j=1; j<rowsOfTable_js[i].length-1;j++)
	{
		var cell=document.createElement("td");
		cell.appendChild(document.createTextNode(rowsOfTable_js[i][j]));
		row.appendChild(cell);
	}
	tBody.appendChild(row);
}
function moduleClicked(i)
{
	alert(rowsOfTable_js[i][0]);
}
//---------------------------------------------------------------------------
//query expansion for gene Search
var changeAutoComplete = false;
	var totalRecordsFetched = 0;
	var changeEventCount = 0;
	
	$("[id$=geneValue]").focusout(function(event) {
		changeEventCount = 0;
	});
	$("[id$=geneValue]").keyup(function(event) {
		if(event.which <= 90 && event.which >= 48 || event.which==8 || event.which == 27) {
			changeEventCount = 0;
			checkLastTextBoxValue($("[id$=geneValue]").val(), null);
		} else if (event.which == 40){
			changeEventCount++;
			if(changeEventCount==0 ||changeEventCount==totalRecordsFetched+1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'next');
			}
		} else if (event.which == 38){
			changeEventCount--;
			if(changeEventCount==0 || changeEventCount==-totalRecordsFetched-1){
				checkLastTextBoxValue($("[id$=geneValue]").val(), 'prev');
			}
		} 
	});	
	
	// 	query expansion for gene Search
	function checkLastTextBoxValue(oldValue, next) {
		oldValue = oldValue.split(" |")[0];
		if (oldValue != "" || next) {
			var type = $("[id$=geneId]").val();
			var organism = $("[id$=geneCategory]").val();
			if (organism != '' && organism != '------Please Select------')
				loadDropDownMenu(oldValue, type, organism, next);
		}
	}
	function loadDropDownMenu(currentQuery, type, organism, next) {
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var resp = xmlhttp.responseText;
				var currentArray = new Array();
				if(resp.length!=0)
					currentArray = resp.split(",");
				var dataList = document.getElementById('genedatalist');
				totalRecordsFetched = currentArray.length;
				if(currentArray.length > 0) {
					$(dataList).empty();
					changeEventCount = 0;
				}
				currentArray.forEach(function(item) {
			        var option = document.createElement('option');
			        option.value = item;
			        dataList.appendChild(option);
			      });
			}
		}
		var request = "Gene_of_Interest?query="+currentQuery+"&type="+type+"&category="+organism+"&job_id=queryExp&size=10";
		  if(next!=null)
			  request = request + "&job_parameter=" + next;
		xmlhttp.open("GET", request, true);
		xmlhttp.send();
	}

</script>

</body>

</html>
