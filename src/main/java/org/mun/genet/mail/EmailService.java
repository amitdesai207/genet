package org.mun.genet.mail;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mun.genet.configurer.Configuration;
import org.mun.genet.configurer.NotificationStatusType;
import org.mun.genet.configurer.NotificationType;
import org.mun.genet.daos.NotificationService;
import org.mun.genet.encryptors.SymmetricAesEncryptor;
import org.mun.genet.vo.JobVo;
import org.mun.genet.vo.NotificationVo;
import org.mun.genet.vo.UserVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.Template;
import freemarker.template.TemplateException;

@Component
public class EmailService {

	@Autowired
	private Configuration dbConfiguration;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private SymmetricAesEncryptor aesEncryptor;

	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

	public void saveNotificationMail(JobVo jobVo, NotificationType type) {

		if (Boolean.parseBoolean(dbConfiguration.getConfigurationByName("mail.enable"))) {
			Email mail = new Email();
			Map<String, Object> templateVars = new HashMap<String, Object>();

			mail.from = dbConfiguration.getConfigurationByName("mail.mailid");
			mail.to = jobVo.getUserEmail();
			mail.cc = dbConfiguration.getConfigurationByName("mail.mailccid");
			if (NotificationType.JOB_SUBMIT.isEqual(type.toString())) {
				mail.subject = getLocaleMessage("email.subjectSubmit");
				templateVars.put("userName", getLocaleMessage("email.userName"));
				templateVars.put("jobId", jobVo.getDisplayId());
				templateVars.put("signinLink", getLocaleMessage("serverHostName") + getLocaleMessage("loginUrl"));
				mail.bodyContent = getBodyContent(getLocaleMessage("email.submitTemplate"), templateVars);
				logger.info(jobVo.getUserEmail() + ": queued job submit mail");
			} else if (NotificationType.JOB_COMPLETE.isEqual(type.toString())) {
				mail.subject = getLocaleMessage("email.subjectComplete");
				templateVars.put("userName", getLocaleMessage("email.userName"));
				templateVars.put("jobId", jobVo.getDisplayId());
				templateVars.put("signinLink", getLocaleMessage("serverHostName") + getLocaleMessage("loginUrl"));
				mail.bodyContent = getBodyContent(getLocaleMessage("email.completeTemplate"), templateVars);
				logger.info(jobVo.getUserEmail() + ": queued job complete mail");
			} else if (NotificationType.JOB_ERROR.isEqual(type.toString())) {
				mail.subject = getLocaleMessage("email.subjectError");
				templateVars.put("userName", getLocaleMessage("email.userName"));
				templateVars.put("jobId", jobVo.getDisplayId());
				templateVars.put("signinLink", getLocaleMessage("serverHostName") + getLocaleMessage("loginUrl"));
				mail.bodyContent = getBodyContent(getLocaleMessage("email.errorTemplate"), templateVars);
				logger.info(jobVo.getUserEmail() + ": queued job error mail");
			}

			saveNotificationObject(mail);
		}
	}

	public NotificationVo createDownloadLogMail(String email) {
		NotificationVo notification = new NotificationVo();
		notification.setSender(dbConfiguration.getConfigurationByName("mail.mailid"));
		notification.setReceiver(email);
		notification.setSubject(getLocaleMessage("email.subjectDownloadLog"));
		notification.setMessageToSend(getBodyContent(getLocaleMessage("email.downloadLogTemplate"), null));
		logger.info("Log download mail created successfully");
		return notification;
	}
	
	public NotificationVo createSQLExceptionMail(String email, String exception) {
		NotificationVo notification = new NotificationVo();
		notification.setSender(dbConfiguration.getConfigurationByName("mail.mailid"));
		notification.setReceiver(email);
		notification.setSubject(getLocaleMessage("email.subjectSQLEmail"));
		Map<String, Object> templateVars = new HashMap<String, Object>();
		templateVars.put("exception", exception);
		notification.setMessageToSend(getBodyContent(getLocaleMessage("email.SQLExceptionTemplate"), templateVars));
		logger.info("SQL Exception mail created successfully");
		return notification;
	}

	public void createPasswordRecoveryMail(UserVo userVo) {
		Email mail = new Email();
		mail.from = dbConfiguration.getConfigurationByName("mail.mailid");
		mail.subject = getLocaleMessage("email.subjectPasswordRecovery");
		mail.to = userVo.getUserEmail();
		Map<String, Object> templateVars = new HashMap<String, Object>();
		templateVars.put("userName", getLocaleMessage("email.userName"));
		templateVars.put("password",
				aesEncryptor.decrypt(userVo.getPassword(), dbConfiguration.getConfigurationByName("DBEntryEncrptKey")));
		templateVars.put("signinLink", getLocaleMessage("serverHostName") + getLocaleMessage("loginUrl"));
		mail.bodyContent = getBodyContent(getLocaleMessage("email.passwordRecoveryTemplate"), templateVars);
		logger.info("Password Recovery mail for user: " + userVo.getUserName() + "saved successfully");
		saveNotificationObject(mail);
	}

	public void createActivationMail(UserVo userVo) {
		Email mail = new Email();
		mail.from = dbConfiguration.getConfigurationByName("mail.mailid");
		mail.subject = getLocaleMessage("email.subjectActivationEmail");
		mail.to = userVo.getUserEmail();
		Map<String, Object> templateVars = new HashMap<String, Object>();
		templateVars.put("userName", getLocaleMessage("email.userName"));
		templateVars.put("activationLink",
				getLocaleMessage("serverHostName") + getLocaleMessage("activationUrl") + userVo.getActivation());
		templateVars.put("signinLink", getLocaleMessage("serverHostName") + getLocaleMessage("loginUrl"));
		mail.bodyContent = getBodyContent(getLocaleMessage("email.activationTemplate"), templateVars);
		logger.info("Activation mail for user: " + userVo.getUserName() + "saved successfully");
		saveNotificationObject(mail);
	}

	private String getLocaleMessage(String messageCode) {
		return dbConfiguration.getConfigurationByName(messageCode);
	}

	private String getBodyContent(String bodyTemplate, final Map<String, Object> templateVars) {
		freemarker.template.Configuration configuration = new freemarker.template.Configuration();
		configuration.setClassForTemplateLoading(this.getClass(), "/");

		StringBuffer content = new StringBuffer();
		try {
			Template freemarkerTemplate = configuration.getTemplate(bodyTemplate);
			content.append(FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerTemplate, templateVars));
		} catch (IOException e) {
			logger.warn("Error while sending mail: " + e.getMessage());
		} catch (TemplateException e) {
			logger.warn("Error while reading mail template: " + e.getMessage());
		}

		return content.toString();
	}

	private void saveNotificationObject(final Email mail) {
		NotificationVo notificationVo = new NotificationVo();
		notificationVo.setCopiedReceiver(mail.cc);
		notificationVo.setMessageToSend(mail.bodyContent);
		notificationVo.setNumberOfRetries(0);
		notificationVo.setReceiver(mail.to);
		notificationVo.setStatus(NotificationStatusType.PENDING.toString());
		notificationVo.setSender(mail.from);
		notificationVo.setSubject(mail.subject);
		notificationService.createNotification(notificationVo);
	}

	public void createPubmedIdMail(String userEmail) {
		Email mail = new Email();
		mail.from = dbConfiguration.getConfigurationByName("mail.mailid");
		mail.cc = dbConfiguration.getConfigurationByName("mail.mailccid");
		mail.subject = getLocaleMessage("email.pubmedId");
		mail.to = userEmail;
		Map<String, Object> templateVars = new HashMap<String, Object>();
		templateVars.put("userName", getLocaleMessage("email.userName"));
		templateVars.put("signinLink", getLocaleMessage("serverHostName") + getLocaleMessage("loginUrl"));
		mail.bodyContent = getBodyContent(getLocaleMessage("email.pubmedIdTemplate"), templateVars);
		logger.info("Invalid PubMedId mail for user: " + userEmail + "saved successfully");
		saveNotificationObject(mail);
	}

	private class Email {
		public String from;
		public String to;
		public String cc;
		public String subject;
		public String bodyContent;
	}

}
