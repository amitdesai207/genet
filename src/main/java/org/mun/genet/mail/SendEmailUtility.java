package org.mun.genet.mail;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.mun.genet.vo.NotificationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

@Component
public class SendEmailUtility {
	@Autowired
	private JavaMailSender mailSender;

	public void sendMail(final NotificationVo mail, final File attachmentFile)
			throws Exception {

		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {

				MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
				String[] mailTo = mail.getReceiver().split(";[\\s]*");
				message.setTo(mailTo);
				if(mail.getCopiedReceiver()!=null && !mail.getCopiedReceiver().trim().isEmpty())
					message.setBcc(mail.getCopiedReceiver());
				message.setFrom(mail.getSender());
				message.setSubject(mail.getSubject());
				message.setText(mail.getMessageToSend(), true);
				if (attachmentFile != null)
					message.addAttachment(attachmentFile.getName(), attachmentFile);
				message.addInline("genet_logo", new ClassPathResource("mail/genet_logo.png"));
				message.addInline("footer", new ClassPathResource("mail/footer.gif"));
			}
		};
		this.mailSender.send(preparator);
	}

}
