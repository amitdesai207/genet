package org.mun.genet.jobreceiver;

public enum NetworkType {
	SIGNED(0), UNSIGNED(1);

	private int type;

	private NetworkType(int type) {
		this.type = type;
	}

	public static NetworkType getNetworkType(int input) {
		for (NetworkType myType : NetworkType.values()) {
			if (myType.type == input) {
				return myType;
			}
		}
		return null;
	}

	public static NetworkType getNetworkType(String name) {
		for (NetworkType myType : NetworkType.values()) {
			if (myType.name().equalsIgnoreCase(name)) {
				return myType;
			}
		}
		return null;
	}

	public boolean isEqual(Object type) {
		if (type == null) {
			return false;
		}
		if (!(type instanceof Integer)) {
			return false;
		}
		if (this.type == (int) type) {
			return true;
		}

		return false;
	}

	public int getType() {
		return type;
	}
}
