package org.mun.genet.jobreceiver;

public enum CorrelationType {
	PEARSON(0), BWEIGHTMIDCORRELATION(1);

	private int type;

	private CorrelationType(int type) {
		this.type = type;
	}

	public static CorrelationType getCorrelationType(int input) {
		for (CorrelationType myType : CorrelationType.values()) {
			if (myType.type == input) {
				return myType;
			}
		}
		return null;
	}

	public static CorrelationType getCorrelationType(String name) {
		for (CorrelationType myType : CorrelationType.values()) {
			if (myType.name().equalsIgnoreCase(name)) {
				return myType;
			}
		}
		return null;
	}

	public boolean isEqual(Object type) {
		if (type == null) {
			return false;
		}
		if (!(type instanceof Integer)) {
			return false;
		}
		if (this.type == (int) type) {
			return true;
		}

		return false;
	}

	public int getType() {
		return type;
	}
}
