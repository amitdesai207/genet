package org.mun.genet.jobreceiver;

public class EmptyException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EmptyException(Exception e) {
		super(e);
	}

	public EmptyException(Exception e, String entityName, int rowCounter) {
		// TODO Auto-generated constructor stub
	}

}
