package org.mun.genet.jobreceiver;

import org.mun.genet.configurer.ImportJobStatusType;

public class AdapterException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String entityName;
	private int lineNo;
	private ImportJobStatusType failedStatus;

	public AdapterException(Exception e) {
		super(e);
	}

	public AdapterException(String msg) {
		super(msg);
	}

	public AdapterException(Exception e, String entityName, int lineNo) {
		super(e);
		this.entityName = entityName;
		this.lineNo = lineNo;
	}
	
	public AdapterException(Exception e, String entityName, int lineNo, ImportJobStatusType failedStatus) {
		super(e);
		this.entityName = entityName;
		this.lineNo = lineNo;
		this.failedStatus = failedStatus;
	}

	public String getEntityId() {
		return entityName;
	}

	public int getLineNo() {
		return lineNo;
	}

	public void setLineNo(int lineNo) {
		this.lineNo = lineNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getEntityName() {
		return entityName;
	}

	public ImportJobStatusType getFailedStatus() {
		return failedStatus;
	}
}
