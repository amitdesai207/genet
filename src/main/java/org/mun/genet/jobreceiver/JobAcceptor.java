package org.mun.genet.jobreceiver;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

import org.mun.genet.configurer.Configuration;
import org.mun.genet.configurer.ImportJobStatusType;
import org.mun.genet.configurer.NotificationType;
import org.mun.genet.daos.FileStoreService;
import org.mun.genet.daos.JobService;
import org.mun.genet.mail.EmailService;
import org.mun.genet.model.UploadModel;
import org.mun.genet.processor.ImportDataFiles;
import org.mun.genet.vo.FileStoreVo;
import org.mun.genet.vo.JobVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class JobAcceptor {
	private static Logger logger = LoggerFactory.getLogger(JobAcceptor.class);
	private static String CSV_FILE_EXTENSION = ".csv";

	@Autowired
	private JobService jobService;
	@Autowired
	private FileStoreService fileStoreService;
	@Autowired
	private EmailService emailService;
	@Autowired
	private Configuration configuration;

	public String acceptJobs(UploadModel uploadModel) throws UploadException {
		MultipartFile inputFile = null;
		JobVo jobVo = null;

		try {
			jobVo = new JobVo();
			jobVo.setUserEmail(uploadModel.getEmail());

			jobVo.setStatus(ImportJobStatusType.UPLOADING.toString());

			jobVo.setClientIP(uploadModel.getClientIP());
			jobVo.setBrowser(uploadModel.getBrowser());
			jobVo.setOverrideExistingDatabase(false);

			jobVo.setCorrelation(CorrelationType.getCorrelationType(uploadModel.getCorrelation()).getType());
			jobVo.setKeggId(uploadModel.getKeggId());
			jobVo.setNetworkType(NetworkType.getNetworkType(uploadModel.getNetworktype()).getType());
			jobVo.setPubmedId(uploadModel.getPubmedId());
			jobVo.setTaxonomyId(uploadModel.getTaxonomyId());
			jobVo.setThreshold(uploadModel.getThreshold());
			jobVo.setOrganism(uploadModel.getOrganismName());

			jobVo = jobService.addJob(jobVo);
			FileStoreVo fileStore = fileStoreService.storeFile(inputFile, uploadModel.getEmail());
			fileStoreService.setDisplayId(fileStore.getId());

			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL resource = classLoader.getResource("GENE_PFAM_DESC.csv");
			fileStoreService.storeExistingFileInFileRespository(new FileInputStream(resource.getFile()),
					fileStore.getFilePath(), ImportDataFiles.GENE_PFAM_DESC.toString() + CSV_FILE_EXTENSION);
			fileStoreService.storeFileInFileRespository(uploadModel.getGeneExpressionFile(), fileStore.getFilePath(),
					ImportDataFiles.GENE_EXPRESSION.toString() + CSV_FILE_EXTENSION);
			fileStoreService.storeFileInFileRespository(uploadModel.getGeneTable(), fileStore.getFilePath(),
					ImportDataFiles.GENE_INFO.toString() + CSV_FILE_EXTENSION);
			fileStoreService.storeFileInFileRespository(uploadModel.getConditionTable(), fileStore.getFilePath(),
					ImportDataFiles.CONDITION_INFORMATION.toString() + CSV_FILE_EXTENSION);
			if (!uploadModel.getTuAnnotationFile().isEmpty()) {
				fileStoreService.storeFileInFileRespository(uploadModel.getTuAnnotationFile(), fileStore.getFilePath(),
						ImportDataFiles.GENE_TU.toString() + CSV_FILE_EXTENSION);
			}
			if (!uploadModel.getPcAnnotationFile().isEmpty()) {
				fileStoreService.storeFileInFileRespository(uploadModel.getPcAnnotationFile(), fileStore.getFilePath(),
						ImportDataFiles.GENE_PROTEIN.toString() + CSV_FILE_EXTENSION);
			}
			if (!uploadModel.getPfamAnnotationFile().isEmpty()) {
				fileStoreService.storeFileInFileRespository(uploadModel.getPfamAnnotationFile(),
						fileStore.getFilePath(), ImportDataFiles.GENE_PFAM.toString() + CSV_FILE_EXTENSION);
			}
			if (!uploadModel.getProteinSeqFile().isEmpty()) {
				fileStoreService.storeFileInFileRespository(uploadModel.getProteinSeqFile(), fileStore.getFilePath(),
						configuration.getConfigurationByName("proteinSequenceFastaFileName"));
			}
			if (!uploadModel.getPathwayAnnotationFile().isEmpty()) {
				fileStoreService.storeFileInFileRespository(uploadModel.getPathwayAnnotationFile(),
						fileStore.getFilePath(), ImportDataFiles.GENE_METACYC.toString() + CSV_FILE_EXTENSION);
			}

			jobService.updateFileStoreId(jobVo.getId(), fileStore.getId());
			try {
				emailService.saveNotificationMail(jobVo, NotificationType.JOB_SUBMIT);
			} catch (Exception ex) {
				logger.warn(jobVo.getUserEmail() + ": Genet Import request submit mail failed");
			}

			String jobDisplayId = jobVo.getDisplayId();
			return jobDisplayId;
		} catch (IOException ex) {
			throw new UploadException(ex);
		} catch (Exception ex) {
			throw new UploadException(ex);
		}
	}

}
