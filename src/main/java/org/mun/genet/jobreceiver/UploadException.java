package org.mun.genet.jobreceiver;

public class UploadException extends RuntimeException {

	private static final long serialVersionUID = -272860127921000898L;

	public UploadException(Exception e) {
		super(e);
	}
	
	public UploadException(String msg){
		super(msg);
	}
}