package org.mun.genet.endpoints;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mun.genet.auth.OrganismSessionManager;
import org.mun.genet.configurer.Configuration;
import org.mun.genet.model.Module_DB;
import org.mun.genet.model.Pathway_DB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class PathwayController {

	@Autowired
	private OrganismSessionManager sessionManager;
	@Autowired
	private Configuration configuration;

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@RequestMapping(value = "/Pathway_Representation", method = RequestMethod.GET)
	public void notSupported(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@RequestMapping(value = "/Pathway_Representation", method = RequestMethod.POST)
	public String pathwayRepresentation(HttpServletRequest request, HttpServletResponse response) {

		// get the user's query
		String organism = sessionManager.getSessionOrganism(request.getSession(true), request.getParameter("category"));
		if (organism == null)
			return "home";
		String pathwayDesc_ID = request.getParameter("value");
		boolean selectAll = false;
		if (pathwayDesc_ID.equalsIgnoreCase("*"))
			selectAll = true;

		Pathway_DB pathway_info = new Pathway_DB(organism);

		// -----collect the information for pfam
		// pathways-------------------------------------------------------------
		String[][] pfamPathways = pathway_info.getPfamPathways(pathwayDesc_ID, selectAll);
		ArrayList<ArrayList<String>> pfams_Module_AllInfo = new ArrayList<ArrayList<String>>();
		if (pfamPathways != null) {
			if (pfamPathways.length > 0) {

				for (int i = 0; i < pfamPathways.length; i++) {
					String[][] modulesInfo = pathway_info.modulesWithPfamID(pfamPathways[i][0]);
					if (modulesInfo != null) {
						if (modulesInfo.length > 0) {
							for (int j = 0; j < modulesInfo.length; j++) {
								String geneNO = pathway_info.geneNOwithSameModule_PfamPathway(modulesInfo[j][0],
										pfamPathways[i][0]);
								ArrayList<String> row = new ArrayList<String>();
								row.add(modulesInfo[j][0]);
								row.add(pfamPathways[i][0]);
								row.add(pfamPathways[i][1]);
								row.add(geneNO);
								row.add(modulesInfo[j][1]);
								pfams_Module_AllInfo.add(row);
							}
						}
					}
				}
			}
		}
		// -----collect the information for KEGG
		// pathways-------------------------------------------------------------
		String[][] keggPathways = pathway_info.getKEGGPathways(pathwayDesc_ID, selectAll);
		ArrayList<ArrayList<String>> kegg_Module_AllInfo = new ArrayList<ArrayList<String>>();
		if (keggPathways != null) {
			if (keggPathways.length > 0) {

				for (int i = 0; i < keggPathways.length; i++) {
					String[][] modulesInfo = pathway_info.modulesWithKEGGID(keggPathways[i][0]);
					if (modulesInfo != null) {
						if (modulesInfo.length > 0) {
							for (int j = 0; j < modulesInfo.length; j++) {
								String geneNO = pathway_info.geneNOwithSameModule_KEGGPathway(modulesInfo[j][0],
										keggPathways[i][0]);
								ArrayList<String> row = new ArrayList<String>();
								row.add(modulesInfo[j][0]);
								row.add(keggPathways[i][0]);
								row.add(keggPathways[i][1]);
								row.add(geneNO);
								row.add(modulesInfo[j][1]);
								kegg_Module_AllInfo.add(row);
							}
						}
					}
				}
			}
		}
		// -----collect the information for MetaCyc
		// pathways-------------------------------------------------------------
		String[][] metaPathways = pathway_info.getMetaCycPathways(pathwayDesc_ID, selectAll);
		ArrayList<ArrayList<String>> meta_Module_AllInfo = new ArrayList<ArrayList<String>>();
		if (metaPathways != null) {
			if (metaPathways.length > 0) {

				for (int i = 0; i < metaPathways.length; i++) {
					String[][] modulesInfo = pathway_info.modulesWithMetaCycID(metaPathways[i][0]);
					if (modulesInfo != null) {
						if (modulesInfo.length > 0) {
							for (int j = 0; j < modulesInfo.length; j++) {
								String geneNO = pathway_info.geneNOwithSameModule_MetaCycPathway(modulesInfo[j][0],
										metaPathways[i][0]);
								ArrayList<String> row = new ArrayList<String>();
								row.add(modulesInfo[j][0]);
								row.add(metaPathways[i][0]);
								row.add(metaPathways[i][1]);
								row.add(geneNO);
								row.add(modulesInfo[j][1]);
								meta_Module_AllInfo.add(row);
							}
						}
					}
				}
			}
		}
		// -----collect the information for PC
		// pathways-------------------------------------------------------------
		String[][] pcPathways = pathway_info.getPCPathways(pathwayDesc_ID, selectAll);
		ArrayList<ArrayList<String>> pc_Module_AllInfo = new ArrayList<ArrayList<String>>();
		if (pcPathways != null) {
			if (pcPathways.length > 0) {

				for (int i = 0; i < pcPathways.length; i++) {
					String[][] modulesInfo = pathway_info.modulesWithPCID(pcPathways[i][0]);
					if (modulesInfo != null) {
						if (modulesInfo.length > 0) {
							for (int j = 0; j < modulesInfo.length; j++) {
								String geneNO = pathway_info.geneNOwithSameModule_PCPathway(modulesInfo[j][0],
										pcPathways[i][0]);
								ArrayList<String> row = new ArrayList<String>();
								row.add(modulesInfo[j][0]);
								row.add(pcPathways[i][0]);
								row.add(pcPathways[i][1]);
								row.add(geneNO);
								row.add(modulesInfo[j][1]);
								pc_Module_AllInfo.add(row);
							}
						}
					}
				}
			}
		}
		// -----collect the information for TU
		// pathways-------------------------------------------------------------
		String[][] tuPathways = pathway_info.getTUPathways(pathwayDesc_ID, selectAll);
		ArrayList<ArrayList<String>> tu_Module_AllInfo = new ArrayList<ArrayList<String>>();
		if (tuPathways != null) {
			if (tuPathways.length > 0) {

				for (int i = 0; i < tuPathways.length; i++) {
					String[][] modulesInfo = pathway_info.modulesWithTUID(tuPathways[i][0]);
					if (modulesInfo != null) {
						if (modulesInfo.length > 0) {
							for (int j = 0; j < modulesInfo.length; j++) {
								String geneNO = pathway_info.geneNOwithSameModule_TUPathway(modulesInfo[j][0],
										tuPathways[i][0]);
								ArrayList<String> row = new ArrayList<String>();
								row.add(modulesInfo[j][0]);
								row.add(tuPathways[i][0]);
								row.add(tuPathways[i][1]);
								row.add(geneNO);
								row.add(modulesInfo[j][1]);
								tu_Module_AllInfo.add(row);
							}
						}
					}
				}
			}
		}
		// create the return data for constructing the table
		int length = tu_Module_AllInfo.size() + pc_Module_AllInfo.size() + meta_Module_AllInfo.size()
				+ pfams_Module_AllInfo.size() + kegg_Module_AllInfo.size();
		String[][] rowsOfTable = new String[length][6];
		int rowCount = 0;
		Module_DB module_info = new Module_DB(organism, configuration);
		for (int i = 0; i < pfams_Module_AllInfo.size(); i++) {

			for (int j = 0; j < pfams_Module_AllInfo.get(i).size(); j++) {
				rowsOfTable[rowCount][j] = pfams_Module_AllInfo.get(i).get(j);

			}
			rowsOfTable[rowCount][5] = module_info.moduleConditionsValues(rowsOfTable[rowCount][0]);
			rowCount++;
		}
		for (int i = 0; i < tu_Module_AllInfo.size(); i++) {

			for (int j = 0; j < tu_Module_AllInfo.get(i).size(); j++) {
				rowsOfTable[rowCount][j] = tu_Module_AllInfo.get(i).get(j);

			}
			rowsOfTable[rowCount][5] = module_info.moduleConditionsValues(rowsOfTable[rowCount][0]);
			rowCount++;
		}
		for (int i = 0; i < pc_Module_AllInfo.size(); i++) {
			for (int j = 0; j < pc_Module_AllInfo.get(i).size(); j++) {
				rowsOfTable[rowCount][j] = pc_Module_AllInfo.get(i).get(j);

			}
			rowsOfTable[rowCount][5] = module_info.moduleConditionsValues(rowsOfTable[rowCount][0]);
			rowCount++;
		}
		for (int i = 0; i < meta_Module_AllInfo.size(); i++) {
			for (int j = 0; j < meta_Module_AllInfo.get(i).size(); j++) {
				rowsOfTable[rowCount][j] = meta_Module_AllInfo.get(i).get(j);

			}
			rowsOfTable[rowCount][5] = module_info.moduleConditionsValues(rowsOfTable[rowCount][0]);
			rowCount++;
		}
		for (int i = 0; i < kegg_Module_AllInfo.size(); i++) {
			for (int j = 0; j < kegg_Module_AllInfo.get(i).size(); j++) {
				rowsOfTable[rowCount][j] = kegg_Module_AllInfo.get(i).get(j);

			}
			rowsOfTable[rowCount][5] = module_info.moduleConditionsValues(rowsOfTable[rowCount][0]);
			rowCount++;
		}
		request.setAttribute("rowsOfTable", rowsOfTable);
		request.setAttribute("category", organism);
		// send back the information and call the UI page for showing them
		return "PathwaySearch";
	}
}
