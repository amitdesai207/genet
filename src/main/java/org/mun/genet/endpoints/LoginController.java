package org.mun.genet.endpoints;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mun.genet.auth.OrganismSessionManager;
import org.mun.genet.daos.OrganismService;
import org.mun.genet.daos.UserService;
import org.mun.genet.model.UploadModel;
import org.mun.genet.vo.OrganismVo;
import org.mun.genet.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Login controller for login Page
 */
@Controller
public class LoginController {

	@Autowired
	private OrganismService organismService;
	@Autowired
	private UserService userService;
	@Autowired
	private OrganismSessionManager sessionManager;

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home() {
		return "home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(@RequestParam(value = "error", required = false) boolean error, ModelMap model) {
		return "login";
	}

	@RequestMapping(value = "/denied", method = RequestMethod.GET)
	public String deniedPage() {
		return "denied";
	}

	@RequestMapping(value = "/organismList", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public List<OrganismVo> getAllOrganisms(HttpServletRequest request) {
		String role = (String) request.getSession().getAttribute("ROLE");
		List<String> organisms = organismService.getOrganismList(role);
		String sessionOrganism = sessionManager.getSessionOrganism(request.getSession(true), null);
		List<OrganismVo> organismVos = new ArrayList<>();
		for (String organism : organisms) {
			OrganismVo organismVo = new OrganismVo();
			organismVo.setName(organism);
			if (sessionOrganism != null && sessionOrganism.equalsIgnoreCase(organism))
				organismVo.setSelected(true);
			organismVos.add(organismVo);
		}
		return organismVos;
	}

	@RequestMapping(value = "/organismToApprove", method = RequestMethod.GET)
	public @ModelAttribute("organisms") List<ArrayList<String>> getAllOrganismsToApprove() {
		List<ArrayList<String>> organisms = organismService.getOrganismToApprove();
		return organisms;
	}

	@RequestMapping(value = "/approveOrganism", method = RequestMethod.POST)
	@ResponseBody
	public Boolean approveOrganism(@RequestParam String id, @RequestParam(required = false) boolean reject) {
		if (!reject) {
			organismService.approveOrganism(id);
			// downloadJobService.addDownloadJob(organismName);
		} else
			organismService.rejectOrganism(id);
		return true;
	}

	@RequestMapping(value = "/jobHomePage", method = RequestMethod.GET)
	public String adminHome(Model model) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userName = authentication.getName();
		UserVo userVo = new UserVo();
		userVo.setUserName(userName);
		userVo = userService.isUsernameAuthentic(userVo);
		UploadModel uploadModel = new UploadModel();
		uploadModel.setEmail(userVo.getUserEmail());
		model.addAttribute("uploadModel", uploadModel);
		return "adminHome";
	}

	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String contact(HttpServletRequest request) {
		List<OrganismVo> organisms = organismService.getOrganisms();
		request.setAttribute("organisms", organisms);
		return "about";
	}

	@RequestMapping(value = "/activate", method = RequestMethod.GET)
	public String activateAccount(@RequestParam String key, HttpServletRequest request) {
		if (userService.activateUser(key))
			request.setAttribute("signupMsg", "account-active");
		else
			request.setAttribute("signupMsg", "account-missing");
		return "login";
	}

	@RequestMapping(value = "/help", method = RequestMethod.GET)
	public String help() {
		return "help";
	}
}
