package org.mun.genet.endpoints;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class RequestLogger extends HandlerInterceptorAdapter {

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		response.addHeader("X-FRAME-OPTIONS", "DENY");
		response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
		response.setDateHeader("Expires", 0);
		super.postHandle(request, response, handler, modelAndView);
	}
}
