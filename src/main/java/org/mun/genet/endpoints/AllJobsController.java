package org.mun.genet.endpoints;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mun.genet.daos.JobService;
import org.mun.genet.daos.UserService;
import org.mun.genet.daos.entities.Role.UserRoles;
import org.mun.genet.vo.JobVo;
import org.mun.genet.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class AllJobsController {

	@Autowired
	private JobService jobService;
	@Autowired
	private UserService userService;

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}
	
	@RequestMapping(value = "/alljobs", method = RequestMethod.GET)
	public @ModelAttribute("jobs")
	Collection<JobVo> getAllJobs(HttpServletRequest request) {
		String role = (String) request.getSession().getAttribute("ROLE");
		Collection<JobVo> jobs = null;
		if (UserRoles.Web_Tool_Admin.name().equals(role)) {
			jobs = jobService.findAll();
		} else {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String userName = authentication.getName();
			UserVo userVo = new UserVo();
			userVo.setUserName(userName);
			userVo = userService.isUsernameAuthentic(userVo);
			jobs = jobService.findAllJobsForUser(userVo.getUserEmail(), role);
		}
		request.setAttribute("accessRole", role);
		return jobs;
	}

}
