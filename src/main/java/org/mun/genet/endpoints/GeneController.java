package org.mun.genet.endpoints;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.mun.genet.auth.OrganismSessionManager;
import org.mun.genet.configurer.Configuration;
import org.mun.genet.model.Gene_DB;
import org.mun.genet.model.Module_DB;
import org.mun.genet.vo.GeneInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class GeneController {

	@Autowired
	private OrganismSessionManager sessionManager;
	@Autowired
	private Configuration configuration;

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/Gene_of_Interest", method = RequestMethod.GET)
	public void getGeneInfo(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String job_id = "";
		try {
			job_id = request.getParameter("job_id");

			if (job_id.compareTo("queryExp") == 0) {
				String organism = sessionManager.getSessionOrganism(request.getSession(true),
						request.getParameter("category"));
				if (organism == null)
					return;
				String job_parameter = request.getParameter("job_parameter");
				int size = Integer.parseInt(request.getParameter("size"));
				String genes = "";
				GeneInfoVo[] geneList = null;
				HttpSession currentSesion = request.getSession();
				if (job_parameter == null) {
					String query = request.getParameter("query");
					String type = request.getParameter("type");
					Gene_DB gene_Info = new Gene_DB(organism, configuration);
					ArrayList<GeneInfoVo> geneInfos = gene_Info.genesList(type, query);
					currentSesion.setAttribute(organism + "GeneList", geneInfos);
					currentSesion.setAttribute(organism + "GeneListStartIndex", size);
					geneList = Arrays.copyOfRange(geneInfos.toArray(new GeneInfoVo[0]), 0, size);
				} else {
					ArrayList<GeneInfoVo> geneInfos = (ArrayList<GeneInfoVo>) currentSesion
							.getAttribute(organism + "GeneList");
					int startingIndex = (Integer) currentSesion.getAttribute(organism + "GeneListStartIndex");
					int endIndex = 0;
					geneList = new GeneInfoVo[0];
					if (job_parameter.trim().equalsIgnoreCase("next")) {
						if (startingIndex == geneInfos.size()) {
							startingIndex = 0;
						}
						endIndex = startingIndex + size;
						if (endIndex > geneInfos.size()) {
							endIndex = geneInfos.size();
						}
						currentSesion.setAttribute(organism + "GeneListStartIndex", endIndex);
					} else if (job_parameter.trim().equalsIgnoreCase("prev")) {
						if (startingIndex == geneInfos.size())
							endIndex = startingIndex - size - (geneInfos.size() % size);
						else
							endIndex = startingIndex - size;
						if (endIndex == 0) {
							endIndex = geneInfos.size();
							startingIndex = geneInfos.size() - (geneInfos.size() % size);
						} else
							startingIndex = endIndex - size;
						currentSesion.setAttribute(organism + "GeneListStartIndex", endIndex);
					}
					if (endIndex != 0)
						geneList = Arrays.copyOfRange(geneInfos.toArray(new GeneInfoVo[0]), startingIndex, endIndex);
				}
				for (GeneInfoVo gene : geneList) {
					if (gene != null)
						genes = genes + gene.getSearchCol1() + " |" + gene.getSearchCol2().replace(",", " -") + ",";
				}
				if (!genes.isEmpty()) {
					genes = genes.substring(0, genes.length() - 1);
				}
				response.setContentType("text/xml");
				response.setHeader("Cache-Control", "no-cache");
				response.getWriter().write(genes);
				return;
			}

			if (job_id.compareTo("network_NCK") == 0) {
				String flag = request.getParameter("flag");
				String mainSymbol = request.getParameter("mainSymbol");
				String clickedSymbol = request.getParameter("clickedSymbol");
				String organism = sessionManager.getSessionOrganism(request.getSession(true),
						request.getParameter("category"));
				if (organism == null)
					return;
				if (flag.compareTo("nodeClicked") == 0) {
					Gene_DB getInfo = new Gene_DB(organism, configuration);
					Set<String> symbol = new HashSet<>();
					symbol.add(clickedSymbol);
					ArrayList<ArrayList<String>> data = getInfo.readConnectedNodeData(symbol, mainSymbol);
					String[][] connectedNodes = null;
					response.setContentType("text/xml");
					response.setHeader("Cache-Control", "no-cache");
					if (data.size() == 0)
						response.getWriter().write("|");
					else
						connectedNodes = new String[data.size()][data.get(0).size()];
					for (int i = 0; i < data.size(); i++) {
						for (int j = 0; j < data.get(0).size(); j++) {
							connectedNodes[i][j] = data.get(i).get(j);
							if (j == 1)
								symbol.add(data.get(i).get(j));
						}
					}
					Map<String, String> nodeColors = getInfo.nodeColors(symbol);
					String[][] symbolColors = new String[connectedNodes.length + 1][2];
					symbolColors[0][0] = clickedSymbol;
					symbolColors[0][1] = nodeColors.get(clickedSymbol);
					int i = 1;
					for (String[] networkData : connectedNodes) {
						if (symbol.contains(networkData[0])) {
							symbolColors[i][0] = networkData[1];
							symbolColors[i][1] = nodeColors.get(networkData[1]);
						} else {
							symbolColors[i][0] = networkData[0];
							symbolColors[i][1] = nodeColors.get(networkData[0]);
						}
						i++;
					}
					response.getWriter().write(arrayToString(connectedNodes) + "|" + arrayToString(symbolColors));
				}
			}
		} catch (Exception e) {
			response.sendRedirect("home");
		}

	}

	@RequestMapping(value = "/Gene_of_Interest", method = RequestMethod.POST)
	public String geneOfInterest(HttpServletRequest request, HttpServletResponse response) {
		// create an instance of Gene_DB to collect the information about the
		// gene of interest
		String organism = sessionManager.getSessionOrganism(request.getSession(true), request.getParameter("category"));
		if (organism == null)
			return "home";
		Gene_DB getInfo = new Gene_DB(organism, configuration);
		String geneValue = request.getParameter("noOfGenes");
		Integer limit = 20;
		if (geneValue != null)
			limit = Integer.parseInt(geneValue);
		// results is an array with size 7 that stores all the information which
		// are related to the section Gene Info in the user interface
		String[] results = getInfo.geneInfoCollector(request.getParameter("id"), request.getParameter("value"));
		results[0] = addPFAMLink(results[0]);
		results[1] = addKeggLink(results[1]);
		// set attribute for basic info of gene (section 1)
		request.setAttribute("geneInfo", results);
		// get the symbol id of the searched gene
		String symbol = results[6];
		String module = results[7];
		// get the systematic id of the searched gene because, it is required
		// for KEGG website link(it is more safe)
		if (symbol.compareTo("N/A") == 0) {
			request.setAttribute("category", organism);
			return "noGeneFound";
		}

		// --------------------------------Expression table
		// -----------------------------------------------------
		// collect the information about the section Expression table
		request.setAttribute("expressionInfo", getInfo.geneExperssionInfo(symbol));
		// calculate the average value of genes for each condition; the
		// return value of avrageExpression_calculation is an array
		// containing the average of all conditions
		int noOfGenes = getInfo.getTotalNoOfGenesInModule(module);
		request.setAttribute("avrageExpInfo", getInfo.avrageExpression_calculation(module, noOfGenes));
		// get and set the conditions names and their order
		request.setAttribute("conditionsAndRanks", getInfo.getConditions());
		String[][] dataOfNetwork = null;
		if (module != null && !module.trim().isEmpty())
			dataOfNetwork = getInfo.networkData(symbol, limit);
		// the first element would be the central gene symbol, the second
		// element: other genes data that are connected to first element
		// value;the third one is the color values of the second elements
		Set<String> symbols = new HashSet<>();
		if (dataOfNetwork != null) {
			for (String[] data : dataOfNetwork) {
				symbols.add(data[1]);
			}
		}
		Map<String, ArrayList<String>> connectedNodesMap = new HashMap<>();
		ArrayList<ArrayList<String>> connectedNodes = new ArrayList<>();
		if (symbols.size() != 0)
			connectedNodes = getInfo.readConnectedNodeData(symbols, symbol);
		for (ArrayList<String> connectedNode : connectedNodes) {
			if (connectedNodesMap.containsKey(connectedNode.get(0))) {
				ArrayList<String> nodeArray = connectedNodesMap.get(connectedNode.get(0));
				nodeArray.add(StringUtils.arrayToCommaDelimitedString(connectedNode.toArray()));
				symbols.add(connectedNode.get(0));
				symbols.add(connectedNode.get(1));
			} else {
				ArrayList<String> nodeArray = new ArrayList<>();
				nodeArray.add(StringUtils.arrayToCommaDelimitedString(connectedNode.toArray()));
				connectedNodesMap.put(connectedNode.get(0), nodeArray);
				symbols.add(connectedNode.get(0));
				symbols.add(connectedNode.get(1));
			}
		}

		if (dataOfNetwork != null) {
			for (String[] data : dataOfNetwork) {
				symbols.add(data[0]);
			}
		}
		Map<String, String> nodeColors = new HashMap<>();
		if (!symbols.isEmpty())
			nodeColors = getInfo.nodeColors(symbols);
		String[][] ConnectedNodesData = new String[connectedNodesMap.size()][3];
		int i = 0;
		for (Entry<String, ArrayList<String>> connectedNode : connectedNodesMap.entrySet()) {
			ConnectedNodesData[i][0] = connectedNode.getKey();
			ConnectedNodesData[i][1] = StringUtils.arrayToDelimitedString(connectedNode.getValue().toArray(), ";");
			ArrayList<String> symbolColors = new ArrayList<>();
			symbolColors.add(connectedNode.getKey() + "," + nodeColors.get(connectedNode.getKey()));
			for (String childNode : connectedNode.getValue()) {
				String[] childNodeValues = childNode.split(",");
				symbolColors.add(childNodeValues[1] + "," + nodeColors.get(childNodeValues[1]));
			}
			ConnectedNodesData[i][2] = StringUtils.arrayToDelimitedString(symbolColors.toArray(), ";");
			i++;
		}
		// get module conditions' values to create a hyperlink
		Module_DB module_info = new Module_DB(organism, configuration);
		String condValues = module_info.moduleConditionsValues(module);
		request.setAttribute("moduleCondValues", condValues);
		request.setAttribute("ConnectedNodesData", ConnectedNodesData);
		String[] nodeSchema = { "label" };
		String[] edgeSchema = { "weight", "correlation" };
		request.setAttribute("net_nodeSchema", nodeSchema);
		request.setAttribute("net_edgeSchema", edgeSchema);
		request.setAttribute("networkData", dataOfNetwork);
		String[][] symbolColors = null;
		if (dataOfNetwork != null) {
			symbolColors = new String[dataOfNetwork.length + 1][2];
			symbolColors[0][0] = symbol;
			symbolColors[0][1] = nodeColors.get(symbol);
			i = 1;
			for (String[] networkData : dataOfNetwork) {
				if (networkData[0].equals(symbol)) {
					symbolColors[i][0] = networkData[1];
					symbolColors[i][1] = nodeColors.get(networkData[1]);
				} else {
					symbolColors[i][0] = networkData[0];
					symbolColors[i][1] = nodeColors.get(networkData[0]);
				}
				i++;
			}
		}
		request.setAttribute("networkStyleData", symbolColors);
		// send back the information and call the UI page for showing them
		request.setAttribute("noOfGenes", limit);
		request.setAttribute("category", organism);
		return "GeneSearch";

	}

	String[][] removeSpecificElement(String[][] data, String node) {

		for (int i = 0; i < data.length; i++) {
			if (data[i][0].compareTo(node) == 0 || data[i][1].compareTo(node) == 0) {

			}
		}
		return data;
	}

	String arrayToString(String[][] data) {
		String return_str = "";
		for (int j = 0; j < data.length; j++) {
			for (int k = 0; k < data[j].length; k++) {
				return_str += data[j][k];
				if (k < data[j].length - 1) {
					return_str += ",";
				}
			}
			if (j < data.length - 1) {
				return_str += ";";
			}
		}
		return return_str;
	}

	String addPFAMLink(String pfams_str) {
		String rtrn_str = "";
		String[] pfams = pfams_str.replace(" ", "").split(",");
		String pfamLinkServer = configuration.getConfigurationByName("pfamLinkServer");
		for (int i = 0; i < pfams.length; i++) {
			rtrn_str += "<a target=\"_blank\" href=" + pfamLinkServer + "/family?acc=" + pfams[i] + ">" + pfams[i]
					+ "</a>";
			if (i < pfams.length - 1) {
				rtrn_str += ",";
			}
		}
		return rtrn_str;
	}

	String addKeggLink(String keggStr) {
		StringBuffer rtrn_str = new StringBuffer();
		List<String> pathwayIds = Arrays.asList(keggStr.replace(" ", "").split(","));
		String keggLinkServer = configuration.getConfigurationByName("keggLinkServer");
		for (String pathwayId : pathwayIds) {
			rtrn_str.append("<a target=\"_blank\" href=\"" + keggLinkServer + "/kegg-bin/show_pathway?@" + pathwayId
					+ "/default%3dyellow/\" >" + pathwayId + "</a>,");
		}
		if (rtrn_str.length() != 0)
			return rtrn_str.substring(0, rtrn_str.length() - 1);
		else
			return "";
	}

	int isExist(String key, ArrayList<ArrayList<String>> strArray) {
		for (int i = 0; i < strArray.size(); i++) {
			if (strArray.get(i).get(0).compareTo(key) == 0) {
				return i;
			}
		}
		return -1;
	}

	// this function creates the netwrk data
	ArrayList<String[][]> inputNetworkConstructor(String[][] data) {
		// nodes only have id and label
		ArrayList<ArrayList<String>> nodes = new ArrayList<ArrayList<String>>();
		// edges are assumed to have id, source, target, weight, correlation
		ArrayList<ArrayList<String>> edges = new ArrayList<ArrayList<String>>();
		for (int i = 0; i < data.length; i++) {
			// the value of id and label are set to be equal
			if (isExist(data[i][0], nodes) < 0) {
				ArrayList<String> temp = new ArrayList<String>();
				// node id
				temp.add(data[i][0]);
				// node label
				temp.add(data[i][0]);
				nodes.add(temp);
			}
			if (isExist(data[i][1], nodes) < 0) {
				ArrayList<String> temp = new ArrayList<String>();
				// node id
				temp.add(data[i][1]);
				// node label
				temp.add(data[i][1]);
				nodes.add(temp);
			}
			ArrayList<String> temp = new ArrayList<String>();
			// edge id
			temp.add(data[i][0] + "to" + data[i][1]);
			// edge source
			temp.add(data[i][0]);
			// edge target
			temp.add(data[i][1]);
			// edge weight
			temp.add(data[i][2]);
			// edge correlation
			temp.add(data[i][3]);
			edges.add(temp);
		}

		ArrayList<String[][]> returnResult = new ArrayList<String[][]>();

		returnResult.add(ArrayListToArray(nodes));
		returnResult.add(ArrayListToArray(edges));
		return returnResult;
	}

	String[][] ArrayListToArray(ArrayList<ArrayList<String>> data) {
		String[][] returnData = new String[data.size()][data.get(0).size()];
		for (int i = 0; i < data.size(); i++) {
			for (int j = 0; j < data.get(0).size(); j++) {
				returnData[i][j] = data.get(i).get(j);
			}
		}
		return returnData;
	}
}
