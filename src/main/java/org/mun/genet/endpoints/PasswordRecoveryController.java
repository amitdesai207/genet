package org.mun.genet.endpoints;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mun.genet.auth.SessionCache;
import org.mun.genet.daos.UserService;
import org.mun.genet.mail.EmailService;
import org.mun.genet.model.RecoveryModel;
import org.mun.genet.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PasswordRecoveryController {

	@Autowired
	private EmailService emailService;
	@Autowired
	private UserService userService;

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@RequestMapping(value = "/recovery", method = RequestMethod.GET)
	public String initForm(Model model, HttpServletRequest request) {
		RecoveryModel recoveryModel = new RecoveryModel();
		setNewNonce(recoveryModel, request);
		model.addAttribute("recoveryModel", recoveryModel);
		return "passwordRecovery";
	}

	@RequestMapping(value = "/recovery", method = RequestMethod.POST)
	ModelAndView submitEmail(@ModelAttribute("recoveryModel") RecoveryModel recoveryModel, BindingResult result,
			HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("passwordRecovery");
		if (recoveryModel.getNonce() == null || recoveryModel.getNonce().isEmpty()
				|| !recoveryModel.getNonce().equals(request.getSession().getAttribute("recoveryNonce"))) {
			return mav.addObject("fail", "Invalid request, please refresh the page and try again");
		} else {
			UserVo user = new UserVo();
			user.setUserName(recoveryModel.getUserName());
			user = userService.isUsernameAuthentic(user);
			if (user == null)
				mav.addObject("fail", "Sorry we don't have your information with us. Please register");
			else {
				user = userService.resetPassword(user);
				emailService.createPasswordRecoveryMail(user);
				request.setAttribute("signupMsg", "password-reset");
				mav.addObject("success", "Recovery mail sent successfully");
				mav.setViewName("login");
			}
			return mav;
		}
	}

	private void setNewNonce(RecoveryModel model, HttpServletRequest request) {
		model.setNonce(UUID.randomUUID().toString());
		request.getSession().setAttribute("recoveryNonce", model.getNonce());
	}

	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public String resetInitForm(Model model, HttpServletRequest request) {
		RecoveryModel recoveryModel = new RecoveryModel();
		model.addAttribute("recoveryModel", recoveryModel);
		boolean recoveryMode = (Boolean) (request.getSession().getAttribute("recoveryMode") == null ? false : true);
		if (recoveryMode) {
			request.setAttribute("recoveryMode", true);
			request.getSession().removeAttribute("recoveryMode");
		}
		return "reset";
	}

	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public String resetUserPassword(@ModelAttribute("recoveryModel") RecoveryModel recoveryModel,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserVo userVo = new UserVo();
		userVo.setUserName((String) authentication.getPrincipal());
		userVo.setPassword(recoveryModel.getUserName());
		userService.resetUserPassword(userVo);
		SessionCache.processLogout((String) authentication.getPrincipal());
		SecurityContextHolder.clearContext();
		request.setAttribute("signupMsg", "password-changed");
		return "login";
	}
}
