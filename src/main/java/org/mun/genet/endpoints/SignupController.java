package org.mun.genet.endpoints;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mun.genet.commandvalidators.SignupValidator;
import org.mun.genet.configurer.Configuration;
import org.mun.genet.daos.UserService;
import org.mun.genet.daos.entities.Role.UserRoles;
import org.mun.genet.encryptors.SymmetricAesEncryptor;
import org.mun.genet.mail.EmailService;
import org.mun.genet.model.SignupModel;
import org.mun.genet.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Login controller for login Page
 */
@Controller
public class SignupController {

	@Autowired
	SignupValidator signupValidator;
	@Autowired
	Configuration configuration;
	@Autowired
	SymmetricAesEncryptor aesEncryptor;
	@Autowired
	EmailService emailService;

	@Autowired
	UserService userService;

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@RequestMapping(value = "/checkUserEmail", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public boolean checkUserEmailIsPresent(@RequestBody String email) {
		UserVo userVo = new UserVo();
		userVo.setUserEmail(email);
		return userService.isUserEmailPresent(userVo);
	}

	@RequestMapping(value = "/checkUserName", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public boolean checkUserNameIsPresent(@RequestBody String userName) {
		UserVo userVo = new UserVo();
		userVo.setUserName(userName);
		return userService.isUsernamePresent(userVo);
	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model) {
		SignupModel signupModel = new SignupModel();
		model.addAttribute("signupModel", signupModel);
		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String processSubmit(@ModelAttribute("signupModel") SignupModel signupModel, BindingResult result,
			HttpServletRequest request) {
		signupValidator.validate(signupModel, result);
		if (result.hasErrors()) {
			return "signup";
		} else {
			UserVo userVo = new UserVo();
			userVo.setFirstName(signupModel.getFirstName());
			userVo.setLastName(signupModel.getLastName());
			userVo.setUserEmail(signupModel.getEmail());
			userVo.setUserName(signupModel.getUserName());
			userVo.setPassword(aesEncryptor.encrypt(signupModel.getPassword(),
					configuration.getConfigurationByName("DBEntryEncrptKey")));
			userVo.setDateCreated(new Date());
			userVo.setConsent(signupModel.isSendEmail());
			userVo.setLastUpdated(new Date());
			userVo.setRole(UserRoles.OTHER.getUserRole());
			userVo.setActive(false);
			userVo.setActivation(UUID.randomUUID().toString());
			userService.addUser(userVo);
			emailService.createActivationMail(userVo);
			request.setAttribute("signupMsg", "account-created");
		}
		return "login";
	}
}
