package org.mun.genet.endpoints;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.mun.genet.configurer.ConfigCache;
import org.mun.genet.daos.JobService;
import org.mun.genet.vo.JobVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ServerStatController {

	@Autowired
	private JobService jobService;
	@Autowired
	private ConfigCache dbConfigCache;;

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@RequestMapping(value = "/serverStatisticsPage", method = RequestMethod.GET)
	public ModelAndView initForm(Model model) {
		ModelAndView serverStatModelView = new ModelAndView();
		// Check server load
		int activeJobsCount = jobService.findActiveJobCount();
		List<JobVo> runningJobVos = jobService.findAllRunningJobs();
		Iterator<JobVo> itr = runningJobVos.iterator();
		List<String> runningJobId = new ArrayList<String>();
		while (itr.hasNext()) {

			runningJobId.add(itr.next().getDisplayId().toString());
		}

		serverStatModelView.addObject("runningThreads", activeJobsCount);
		serverStatModelView.addObject("runningJobsId", runningJobId);
		serverStatModelView.addObject("configs", dbConfigCache.getAllConfigs().entrySet());
		return serverStatModelView;
	}

}
