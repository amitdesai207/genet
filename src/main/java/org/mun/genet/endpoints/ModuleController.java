package org.mun.genet.endpoints;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mun.genet.auth.OrganismSessionManager;
import org.mun.genet.configurer.Configuration;
import org.mun.genet.model.Gene_DB;
import org.mun.genet.model.Module_DB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class ModuleController {

	@Autowired
	private OrganismSessionManager sessionManager;
	@Autowired
	private Configuration configuration;

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@RequestMapping(value = "/Module_of_Interest", method = RequestMethod.GET)
	public void notSupported(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@RequestMapping(value = "/Module_of_Interest", method = RequestMethod.POST)
	public String moduleOfInterest(HttpServletRequest request, HttpServletResponse response) {
		// get the data for the table of modules
		String organism = sessionManager.getSessionOrganism(request.getSession(true), request.getParameter("category"));
		if (organism == null)
			return "home";
		Module_DB moduleInfo = new Module_DB(organism, configuration);
		request.setAttribute("modulesData", moduleInfo.getModuleList_plus_data());
		String[] colNames = { "Module Name", "Hub Symbol", "# of Genes" };
		request.setAttribute("columnNames", colNames);
		request.setAttribute("conditions", moduleInfo.getOrderedConsitions());
		request.setAttribute("category", organism);
		return "ModuleSearch";
	}

	@RequestMapping(value = "/Module_Representation", method = RequestMethod.GET)
	public String moduleRepresentation(HttpServletRequest request, HttpServletResponse response) {
		String organism = sessionManager.getSessionOrganism(request.getSession(true), request.getParameter("category"));
		if (organism == null)
			return "home";
		String module = request.getParameter("module");
		String conditionsValues = request.getParameter("conditionsValues");
		String geneValue = request.getParameter("geneValue");
		Integer limit = 20;
		if (geneValue != null)
			limit = Integer.parseInt(geneValue);
		Module_DB module_Info = new Module_DB(organism, configuration);
		// get the module information (Hub andcolor)
		String[] hubAndColor = module_Info.getHubgeneAndColorCode(module);
		// get the genes + their description
		String[][] genesOfModule = module_Info.getGenes_Description(module, limit);
		Set<String> symbols = new HashSet<>();
		for (String[] gene : genesOfModule) {
			symbols.add(gene[0]);
		}
		// get the average expression
		Gene_DB gene_Info = new Gene_DB(organism, configuration);
		Integer noOfGenesInModule = gene_Info.getTotalNoOfGenesInModule(module);
		String[][] avrage = gene_Info.avrageExpression_calculation(module, noOfGenesInModule);
		// get the expressions of genes
		String[][][] genesExpressionInfo = gene_Info.geneExperssionInfo(symbols);
		// ==================================================================================================
		// ------------------- constructing the data for module network
		String[] nodeSchema = { "label" };
		String[] edgeSchema = { "weight", "correlation" };

		// constructing nodes Data and network schema
		String[] nodes = new String[genesOfModule.length];
		String[][] networkSchema = new String[genesOfModule.length][2];

		for (int i = 0; i < genesOfModule.length; i++) {

			nodes[i] = genesOfModule[i][0];

			networkSchema[i][0] = genesOfModule[i][0];
			networkSchema[i][1] = hubAndColor[1];
		}
		// collecting edges data
		String[][] edgeData = gene_Info.networkDataForModuleGenes(symbols);
		ArrayList<String[]> AnnotationData = module_Info.getModuleEnrichemnt(module, symbols);
		// convert arraylist to Array
		String[][] networkDataArr = new String[AnnotationData.size()][4];
		for (int i = 0; i < AnnotationData.size(); i++) {

			networkDataArr[i][0] = AnnotationData.get(i)[0];
			networkDataArr[i][1] = AnnotationData.get(i)[1];
			if (AnnotationData.get(i)[2].length() > 1) {
				networkDataArr[i][2] = AnnotationData.get(i)[2];
			} else {
				networkDataArr[i][2] = "--NO DATA--";
			}
			networkDataArr[i][3] = AnnotationData.get(i)[3];
		}

		request.setAttribute("conditionsValues", conditionsValues);
		request.setAttribute("networkSchema", networkSchema);
		request.setAttribute("nodeSchema", nodeSchema);
		request.setAttribute("edgeSchema", edgeSchema);
		request.setAttribute("nodeData", nodes);
		request.setAttribute("edgeData", edgeData);
		request.setAttribute("AnnotationData", networkDataArr);
		request.setAttribute("genesExpressionInfo", genesExpressionInfo);
		request.setAttribute("avrage", avrage);
		request.setAttribute("module", module);
		request.setAttribute("hubSymbol", hubAndColor[0]);
		request.setAttribute("moduleColor", hubAndColor[1]);
		request.setAttribute("genesOfModule", genesOfModule);
		request.setAttribute("totalGenes", noOfGenesInModule);
		// get and send the condition and their order
		request.setAttribute("conditionsAndRanks", module_Info.getConditionInfo());
		request.setAttribute("category", organism);
		return "Module";
	}

	@RequestMapping(value = "/getModuleGenes", method = RequestMethod.GET)
	public String getModuleGenes(HttpServletRequest request, HttpServletResponse response) {
		String organism = sessionManager.getSessionOrganism(request.getSession(true), request.getParameter("category"));
		if (organism == null)
			return "home";
		String module = request.getParameter("module");
		if (module != null) {
			Module_DB module_Info = new Module_DB(organism, configuration);
			request.setAttribute("genes", module_Info.getGeneInfoInModule(module));
			request.setAttribute("category", organism);
			request.setAttribute("module", module);
		}
		return "ModuleGenes";
	}

}
