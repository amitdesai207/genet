package org.mun.genet.endpoints;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mun.genet.commandvalidators.SQLInjectionValidator;
import org.mun.genet.daos.JobResultService;
import org.mun.genet.daos.JobService;
import org.mun.genet.daos.SdkExceptionService;
import org.mun.genet.daos.entities.Role.UserRoles;
import org.mun.genet.vo.JobResultVo;
import org.mun.genet.vo.JobVo;
import org.mun.genet.vo.SdkExceptionVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class JobPageController {
	Logger logger = LoggerFactory.getLogger(JobPageController.class);

	@Autowired
	private JobService jobService;
	@Autowired
	private JobResultService jobResultService;
	@Autowired
	private SdkExceptionService sdkExceptionService;
	@Autowired
	private SQLInjectionValidator sqlInjectionValidator;

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@RequestMapping(value = "/job", method = RequestMethod.GET)
	public @ModelAttribute("job") JobVo getJob(@RequestParam String id, HttpServletRequest request) {
		String role = (String) request.getSession().getAttribute("ROLE");
		JobVo jobVo = jobService.findByDisplayId(id, role);
		return jobVo;
	}

	@RequestMapping(value = "/jobresult", method = RequestMethod.GET)
	public @ModelAttribute("jobresults") Collection<JobResultVo> getJobResult(@RequestParam String id) {
		return jobResultService.findAllResultsForJob(id);
	}

	@RequestMapping(value = "/joberrors", method = RequestMethod.GET)
	public @ModelAttribute("joberrors") Collection<SdkExceptionVo> getJobErrors(@RequestParam String id) {
		return sdkExceptionService.findAllSdkExceptionsForJob(id);
	}

	@RequestMapping(value = "/jobbyuser", method = RequestMethod.GET)
	public @ModelAttribute("job") Collection<JobVo> getJobsByUser(@RequestParam String userMail,
			HttpServletRequest request) {
		String role = (String) request.getSession().getAttribute("ROLE");
		return jobService.findAllJobsForUser(userMail, role);
	}

	@RequestMapping(value = "/jobsearch", method = RequestMethod.GET)
	public @ModelAttribute("jobs") Collection<JobVo> getJobSearch(@RequestParam(required = false) String value,
			@RequestParam(required = false) String by, HttpServletRequest request) {
		Collection<JobVo> jobs = new ArrayList<JobVo>();
		String role = (String) request.getSession().getAttribute("ROLE");
		if (by == null) {
			return jobs;
		}
		if (!value.isEmpty()) {
			sqlInjectionValidator.validate(value, null);
		}
		try {
			if ("job".equalsIgnoreCase(by)) {
				jobs.add(jobService.findByDisplayId(value, role));
				return jobs;
			} else if ("user".equalsIgnoreCase(by)) {
				return getJobsByUser(value, request);
			}
		} catch (Exception e) {
			logger.warn("Error while retrieving jobs ", e);
		}
		return jobs;
	}

	@RequestMapping(value = "/stopJob", method = RequestMethod.POST)
	@ResponseBody
	public Boolean stopJob(@RequestParam String id, HttpServletRequest request) {
		jobService.stopJobByUser(id);
		return true;
	}

	@RequestMapping(value = "/approveOverrideJob", method = RequestMethod.POST)
	@ResponseBody
	public Boolean approveOverrideJob(@RequestParam String id, HttpServletRequest request) {
		String role = (String) request.getSession().getAttribute("ROLE");
		if (UserRoles.Web_Tool_Admin.name().equals(role)) {
			jobService.approveOverrideJob(id);
			jobService.markJobAsPending(id);
			return true;
		}
		return false;
	}

	@RequestMapping(value = "/rejectOverrideJob", method = RequestMethod.POST)
	@ResponseBody
	public Boolean rejectOverrideJob(@RequestParam String id, HttpServletRequest request) {
		String role = (String) request.getSession().getAttribute("ROLE");
		if (UserRoles.Web_Tool_Admin.name().equals(role)) {
			jobService.markJobAsFailed(id);
			return true;
		}
		return false;
	}

}