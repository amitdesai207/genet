package org.mun.genet.endpoints;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mun.genet.commandvalidators.UploadCommandValidator;
import org.mun.genet.jobreceiver.JobAcceptor;
import org.mun.genet.model.UploadModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class UploadController {
	private Logger logger = LoggerFactory.getLogger(UploadController.class);

	@Autowired
	private UploadCommandValidator uploadCommandValidator;
	@Autowired
	private JobAcceptor jobAcceptor;

	public static final String STATUS = "Received";

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String processSubmit(@ModelAttribute("uploadModel") UploadModel uploadModel, BindingResult result,
			HttpServletRequest request) {
		uploadCommandValidator.validate(uploadModel, result);
		if (!result.hasErrors()) {
			try {
				uploadModel.setClientIP(request.getRemoteAddr());
				uploadModel.setBrowser(request.getHeader("user-agent"));
				jobAcceptor.acceptJobs(uploadModel);
				request.setAttribute("uploadMsg",
						"File uploaded sucessfully. Please check job statistics for the status of job");
			} catch (Exception ex) {
				request.setAttribute("uploadMsg", "File upload failed. Please contact system admin for more details");
				logger.warn("File upload failed ", ex);
			}
		}
		return "adminHome";
	}

}