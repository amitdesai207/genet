package org.mun.genet.endpoints;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.mun.genet.auth.OrganismSessionManager;
import org.mun.genet.commandvalidators.EmailValidator;
import org.mun.genet.configurer.Configuration;
import org.mun.genet.daos.OrganismService;
import org.mun.genet.encryptors.GZipCompressor;
import org.mun.genet.mail.EmailService;
import org.mun.genet.mail.SendEmailUtility;
import org.mun.genet.model.DownloadDataModel;
import org.mun.genet.model.DownloadLogsModel;
import org.mun.genet.processor.ImportDataFiles;
import org.mun.genet.vo.NotificationVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DownloadController {

	@Autowired
	private OrganismSessionManager sessionManager;
	@Autowired
	private EmailValidator emailValidator;
	@Autowired
	private GZipCompressor Gcompress;
	@Autowired
	private EmailService emailService;
	@Autowired
	private SendEmailUtility emailUtility;
	@Autowired
	private Configuration configuration;
	@Autowired
	private OrganismService organismService;

	private static String CSV_FILE_EXTENSION = ".csv";
	private static String CYTO_FILE_EXTENSION = ".sif";
	private static Logger logger = LoggerFactory.getLogger(DownloadController.class);

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleResourceNotFoundException(HttpServletResponse response) throws IOException {
		response.sendRedirect("home");
	}

	@RequestMapping(value = "/downloadLogs", method = RequestMethod.GET)
	public String initForm(Model model, HttpServletRequest request) {
		DownloadLogsModel downloadModel = new DownloadLogsModel();
		setNewNonce(downloadModel, request);
		model.addAttribute("downloadModel", downloadModel);
		return "downloadLogs";
	}

	@RequestMapping(value = "/downloadLogs", method = RequestMethod.POST)
	ModelAndView submitEmail(@ModelAttribute("downloadModel") DownloadLogsModel downloadModel, BindingResult result,
			HttpServletRequest request) throws Exception {
		emailValidator.validate(downloadModel.getEmailAddress(), result);
		ModelAndView mav = new ModelAndView();
		if (result.hasErrors()) {
			logger.info("email address has errors");
			String errorMessage = "";
			for (ObjectError oe : result.getAllErrors()) {
				errorMessage += oe.getDefaultMessage();
			}
			return mav.addObject("fail", errorMessage);
		} else if (downloadModel.getNonce() == null || downloadModel.getNonce().isEmpty()
				|| !downloadModel.getNonce().equals(request.getSession().getAttribute("downloadLogsNonce"))) {
			return mav.addObject("fail", "Invalid request, please refresh the page and try again");
		} else {
			try {
				File temp = File.createTempFile("GenetImportLog", ".gz");
				String inputFile = configuration.getConfigurationByName("mail.logFilePath");
				Gcompress.compress(inputFile, temp.getAbsolutePath());
				NotificationVo mail = emailService.createDownloadLogMail(downloadModel.getEmailAddress());
				emailUtility.sendMail(mail, temp);
				temp.delete();

			} catch (IOException e) {
				logger.debug("Error while reading file: " + e.getMessage());
			}
			mav.addObject("success", "Mail sent successfully");
			return mav;
		}
	}

	private void setNewNonce(DownloadLogsModel model, HttpServletRequest request) {
		model.setNonce(UUID.randomUUID().toString());
		request.getSession().setAttribute("downloadLogsNonce", model.getNonce());
	}

	@RequestMapping(value = "/downloadSamples", method = RequestMethod.GET)
	public void downloadSamples(@RequestParam String fileName, HttpServletResponse response) throws Exception {
		InputStream inputStream = null;
		try {
			String fileToDownload = null;
			if (fileName.trim().equals("fasta"))
				fileToDownload = configuration.getConfigurationByName("proteinSequenceFastaFileName");
			else {
				fileToDownload = ImportDataFiles.getImportDataFiles(fileName).toString() + CSV_FILE_EXTENSION;
			}
			String inputFile = configuration.getConfigurationByName("download.sampleFilePath");
			inputStream = new FileInputStream(inputFile + fileToDownload);
			response.setContentType("multipart/text");
			response.setHeader("Content-Disposition", "attachment;filename=" + fileToDownload);
			IOUtils.copy(inputStream, response.getOutputStream());
			response.flushBuffer();
		} catch (Exception ex) {
			logger.info("Error writing file to output stream. " + ex.getMessage());
			try {
				response.getWriter().write("File Not Available. The file you are trying to access is not present.");
			} catch (IOException e) {
				logger.warn("Unable to Write response");
			}
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (Exception e) {
					logger.warn("Unable to close inputStream.");
				}
				inputStream = null;
			}
		}
	}

	@RequestMapping(value = "/downloadData", method = RequestMethod.GET)
	public String downloadData(Model model, @RequestParam String category, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String organism = sessionManager.getSessionOrganism(request.getSession(true), category);
		if (organism == null)
			return "home";
		DownloadDataModel dataModel = new DownloadDataModel();
		String filePath = organismService.getFileStorePath(organism);
		File[] csvFiles = new File(filePath).listFiles();
		for (File csvFile : csvFiles) {
			String nodeName = csvFile.getName().replace(CSV_FILE_EXTENSION, "").replace(CYTO_FILE_EXTENSION, "");
			if (nodeName.equals(configuration.getConfigurationByName("proteinSequenceFastaFileName")))
				dataModel.setProtein_sequence(true);
			else {
				ImportDataFiles importFile = ImportDataFiles.getImportDataFiles(nodeName);
				if (importFile != null)
					dataModel.setImportDataFileInModel(importFile.toString());
			}
		}
		model.addAttribute("dataModel", dataModel);
		model.addAttribute("category", organism);
		return "downloadData";
	}

	@RequestMapping(value = "/downloadData", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public String createDownloadData(@RequestBody DownloadDataModel dataModel, HttpServletRequest request)
			throws Exception {
		try {
			String organism = sessionManager.getSessionOrganism(request.getSession(true), null);
			String filePath = organismService.getFileStorePath(organism);
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yy-hh-mm");
			String fileName = null;
			if (!dataModel.isAll())
				fileName = "Genet_" + organism.replace(" ", "_") + dateFormat.format(new Date());
			else
				fileName = "Genet_" + organism.replace(" ", "_") + "_ALL";
			String fileBase = configuration.getConfigurationByName("file.storage.path");
			File downloadFolder = new File(fileBase + File.separator + "download" + File.separator);
			FileUtils.forceMkdir(downloadFolder);
			File downloadFile = new File(downloadFolder.getAbsolutePath() + File.separator + fileName + ".tar.gz");
			if (!dataModel.isAll()) {
				List<String> fileList = dataModel.getImportDataFilePathsInModel(filePath,
						configuration.getConfigurationByName("proteinSequenceFastaFileName"));
				Gcompress.compressMultipleFiles(fileList, downloadFile.getAbsolutePath());
			}
			return fileName;
		} catch (Exception ex) {
			logger.error(ex.toString());
			return "";
		}
	}

	@RequestMapping(value = "/downloadFile", method = RequestMethod.GET)
	public void fetchFileToDownloadData(@RequestParam String fileName, HttpServletResponse response) throws Exception {
		String fileBase = configuration.getConfigurationByName("file.storage.path");
		File downloadFile = new File(fileBase + File.separator + "download" + File.separator + fileName + ".tar.gz");
		FileInputStream inputStream = new FileInputStream(downloadFile);
		response.setContentType("multipart/text");
		response.setHeader("Content-Disposition", "attachment;filename=" + fileName + ".tar.gz");
		IOUtils.copy(inputStream, response.getOutputStream());
		inputStream.close();
		if(!fileName.contains("_ALL"))
			downloadFile.delete();
		response.flushBuffer();
	}

}
