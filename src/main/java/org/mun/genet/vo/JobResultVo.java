package org.mun.genet.vo;

import java.util.Date;

public class JobResultVo {
	private Long id;
	private String entity;
	private int recordsFailed;
	private int recordsReceived;
	private int recordsSuccessful;
	private Long jobId;
	private Date dateCreated;
	private Date lastUpdated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public int getRecordsFailed() {
		return recordsFailed;
	}

	public void setRecordsFailed(int recordsFailed) {
		this.recordsFailed = recordsFailed;
	}

	public int getRecordsReceived() {
		return recordsReceived;
	}

	public void setRecordsReceived(int recordsReceived) {
		this.recordsReceived = recordsReceived;
	}

	public int getRecordsSuccessful() {
		return recordsSuccessful;
	}

	public void setRecordsSuccessful(int recordsSuccessful) {
		this.recordsSuccessful = recordsSuccessful;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

}
