package org.mun.genet.vo;

import java.util.Date;

public class JobResultSummaryVo {

	private String id;
	private Date dateCreated;
	private Double successPercentage;
	private Long recordsSent;
	private Long recordsSuccessful;
	private Long recordsFailed;
	private String sourceSystem;
	private String userEmail;
	private String entitiesFailed;

	public JobResultSummaryVo() {
	}

	public JobResultSummaryVo(String id, Date dateCreated, String userEmail, String sourceSystem, Long recordsSent,
			Long recordsSuccessful, Long recordsFailed, String entitiesFailed) {

		super();
		this.id = id;
		this.dateCreated = dateCreated;
		this.userEmail = userEmail;
		this.sourceSystem = sourceSystem;
		this.recordsSent = recordsSent;
		this.recordsSuccessful = recordsSuccessful;
		this.recordsFailed = recordsFailed;
		this.entitiesFailed = entitiesFailed;
	}

	public JobResultSummaryVo(String id, Long recordsFailed) {

		this.id = id;
		this.recordsFailed = recordsFailed;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public Long getRecordsReceived() {

		if (recordsSent == null) {
			return new Long(0);
		} else {
			return recordsSent;
		}
	}

	public void setRecordsReceived(Long recordsReceived) {
		this.recordsSent = recordsReceived;
	}

	public Long getRecordsSuccessful() {

		if (recordsSuccessful == null) {
			return new Long(0);
		} else {
			return recordsSuccessful;
		}
	}

	public void setRecordsSuccessful(Long recordsSuccessful) {
		this.recordsSuccessful = recordsSuccessful;
	}

	public Long getRecordsFailed() {

		if (recordsFailed == null) {
			return new Long(0);
		} else {
			return recordsFailed;
		}
	}

	public void setRecordsFailed(Long recordsFailed) {
		this.recordsFailed = recordsFailed;
	}

	public Double getSuccessPercentage() {

		if (recordsSent != null && recordsSent > 0) {
			return ((double) recordsSuccessful / (double) recordsSent) * 100;
		} else {
			return 0.0d;
		}
	}

	public void setSuccessPercentage(Double successPercentage) {
		this.successPercentage = successPercentage;
	}

	public String getEntitiesFailed() {
		return entitiesFailed;
	}

	public void setEntitiesFailed(String entitiesFailed) {
		this.entitiesFailed = entitiesFailed;
	}

	@Override
	public String toString() {
		return "JobResultSummaryVo [id=" + id + ", dateCreated=" + dateCreated + ", userEmail=" + userEmail
				+ ", sourceSystem=" + sourceSystem + ", recordsReceived=" + recordsSent + ", recordsSuccessful="
				+ recordsSuccessful + ", recordsFailed=" + recordsFailed + ", successPercentage=" + successPercentage
				+ ", entitiesFailed=" + entitiesFailed + "]";
	}

}
