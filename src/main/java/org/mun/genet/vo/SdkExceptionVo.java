package org.mun.genet.vo;

import java.util.Date;

public class SdkExceptionVo {

	private Long id;
	private String causeMessage;
	private String customMessage;
	private String entity;
	private int lineNo;
	private String errDetail;
	private Date dateCreated;
	private Date lastUpdated;
	private Long jobId;
	private Long downloadJobId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCauseMessage() {
		return causeMessage;
	}

	public void setCauseMessage(String causeMessage) {
		this.causeMessage = causeMessage;
	}

	public String getCustomMessage() {
		return customMessage;
	}

	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public int getErrCode() {
		return lineNo;
	}

	public void setErrCode(int errCode) {
		this.lineNo = errCode;
	}

	public String getErrDetail() {
		return errDetail;
	}

	public void setErrDetail(String errDetail) {
		this.errDetail = errDetail;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public int getLineNo() {
		return lineNo;
	}

	public void setLineNo(int lineNo) {
		this.lineNo = lineNo;
	}

	public Long getDownloadJobId() {
		return downloadJobId;
	}

	public void setDownloadJobId(Long downloadJobId) {
		this.downloadJobId = downloadJobId;
	}

}
