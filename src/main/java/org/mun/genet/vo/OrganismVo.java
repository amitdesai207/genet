package org.mun.genet.vo;

import java.util.List;

public class OrganismVo {
	private String name;
	private List<String> pubmedIds;
	private boolean selected;
	private Long filePathId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getPubmedIds() {
		return pubmedIds;
	}

	public void setPubmedIds(List<String> pubmedIds) {
		this.pubmedIds = pubmedIds;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Long getFilePathId() {
		return filePathId;
	}

	public void setFilePathId(Long filePathId) {
		this.filePathId = filePathId;
	}

}
