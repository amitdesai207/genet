package org.mun.genet.vo;

import java.util.List;
import java.util.Map;

public class Record {
	protected String pmcid;
	protected String pmid;
	protected String doi;
	protected boolean live;
	protected String status;
	protected String errmsg;
	protected List<Map<String, String>> versions;

	public String getPmcid() {
		return pmcid;
	}

	public void setPmcid(String pmcid) {
		this.pmcid = pmcid;
	}

	public String getPmid() {
		return pmid;
	}

	public void setPmid(String pmid) {
		this.pmid = pmid;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public boolean isLive() {
		return live;
	}

	public void setLive(boolean live) {
		this.live = live;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public List<Map<String, String>> getVersions() {
		return versions;
	}

	public void setVersions(List<Map<String, String>> versions) {
		this.versions = versions;
	}
}
