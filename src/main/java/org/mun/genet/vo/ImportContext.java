package org.mun.genet.vo;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.mun.genet.configurer.Configuration;
import org.mun.genet.daos.SdkExceptionService;
import org.mun.genet.mail.EmailService;
import org.mun.genet.mail.SendEmailUtility;
import org.mun.genet.model.JobStatistics;
import org.mun.genet.processor.ImportDataFiles;

/**
 * Contains job, SDK session context etc.
 * 
 */
public class ImportContext {
	private JobVo job;
	private Map<ImportDataFiles, JobStatistics> statistics = new LinkedHashMap<>();
	private DataSource dataSource;
	private SdkExceptionService sdkExceptionService;
	private String organism;
	private DownloadJobVo downloadJob;
	private List<ImportDataFiles> filesUploaded;
	private EmailService emailService;
	private SendEmailUtility emailUtility;
	private Configuration configuration;

	public List<ImportDataFiles> getFilesUploaded() {
		return filesUploaded;
	}

	public void setFilesUploaded(List<ImportDataFiles> filesUploaded) {
		this.filesUploaded = filesUploaded;
	}

	public ImportContext(JobVo job) {
		super();
		this.job = job;
	}

	public ImportContext(DownloadJobVo downloadJob) {
		super();
		this.downloadJob = downloadJob;
		this.organism = downloadJob.getOrganism();
	}

	public JobVo getJob() {
		return job;
	}

	public Long getJobId() {
		return job.getId();
	}

	public Long getDataPath() {
		return job.getDataPath();
	}

	public String getUserEmail() {
		return job.getUserEmail();
	}

	public Map<ImportDataFiles, JobStatistics> getStatistics() {
		return statistics;
	}

	public void setStatistics(Map<ImportDataFiles, JobStatistics> statistics) {
		this.statistics = statistics;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setJob(JobVo job) {
		this.job = job;
	}

	public SdkExceptionService getSdkExceptionService() {
		return sdkExceptionService;
	}

	public void setSdkExceptionService(SdkExceptionService sdkExceptionService) {
		this.sdkExceptionService = sdkExceptionService;
	}

	public String getOrganism() {
		return organism;
	}

	public void setOrganism(String organism) {
		this.organism = organism;
	}

	public DownloadJobVo getDownloadJob() {
		return downloadJob;
	}

	public void setDownloadJob(DownloadJobVo downloadJob) {
		this.downloadJob = downloadJob;
	}

	public EmailService getEmailService() {
		return emailService;
	}

	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	public SendEmailUtility getEmailUtility() {
		return emailUtility;
	}

	public void setEmailUtility(SendEmailUtility emailUtility) {
		this.emailUtility = emailUtility;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

}
