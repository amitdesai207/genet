package org.mun.genet.vo;

import java.io.Serializable;

public class GeneInfoVo implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public GeneInfoVo(String SearchCol1, String SearchCol2) {
		this.SearchCol1 = SearchCol1;
		this.SearchCol2 = SearchCol2;
	}

	private String SearchCol1;

	private String SearchCol2;

	public String getSearchCol1() {
		return SearchCol1;
	}

	public void setSearchCol1(String searchCol1) {
		SearchCol1 = searchCol1;
	}

	public String getSearchCol2() {
		return SearchCol2;
	}

	public void setSearchCol2(String searchCol2) {
		SearchCol2 = searchCol2;
	}
}
