package org.mun.genet.configurer;

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.mun.genet.initialize.Initializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ConfigCache implements Initializable {

	private static Logger logger = LoggerFactory.getLogger(ConfigCache.class);;
	private Properties cachedProperties = new Properties();

	public void initialize(ApplicationContext context) {
		InputStream input = null;
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL resource = classLoader.getResource("app.properties");
			input = new FileInputStream(resource.getFile());
			// load a properties file
			cachedProperties.load(input);
		} catch (IOException ex) {
			logger.error("Unable to load properties file: ", ex);
		} finally {
			safeClose(input);
		}
		logger.info("Configuration initialized");
	}

	private void safeClose(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (IOException e) {
				logger.warn("Unable to close " + closeable);
			}
		}
	}

	public synchronized String getConfigurationByName(String key) {
		if (cachedProperties == null) {
			initialize(null);
		}
		return (String) cachedProperties.get(key);
	}

	public synchronized void setValueFromCache(String key, String value) {
		cachedProperties.setProperty(key, value);
	}

	public synchronized Properties getAllConfigs() {
		return cachedProperties;
	}

}
