package org.mun.genet.configurer;

import org.mun.genet.processor.JobQueueImpl;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class SchedulerJob extends QuartzJobBean {
	private JobQueueImpl jobQueueImpl;

	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		jobQueueImpl.runQueuedJobs();
	}

	public void setJobQueueImpl(JobQueueImpl jobQueueImpl) {
		this.jobQueueImpl = jobQueueImpl;
	}

}