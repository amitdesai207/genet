package org.mun.genet.configurer;

public enum NotificationStatusType {
	PENDING("PENDING"),
	RETRY_NEEDED("RETRY_NEEDED"),
	FAILED("FAILED"),
	SENT("SENT");

	private String type;

	private NotificationStatusType(String type) {
		this.type = type;
	}

	public NotificationStatusType getNotificationStatusType(String input) {
		for (NotificationStatusType myType : NotificationStatusType.values()) {
			if (myType.type.equalsIgnoreCase(input)) {
				return myType;
			}
		}
		return null;
	}

	public boolean isEqual(Object type) {
		if (type == null) {
			return false;
		}
		if (!(type instanceof String)) {
			return false;
		}
		if (this.type.equalsIgnoreCase((String) type)) {
			return true;
		}

		return false;
	}

	public String toString() {
		return type;
	}


}
