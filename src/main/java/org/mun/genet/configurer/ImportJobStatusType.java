package org.mun.genet.configurer;

public enum ImportJobStatusType {
	SAVED("SAVED"),
	UPLOADING("UPLOADING"),
	PENDING("PENDING"),
	RUNNING("RUNNING"),
	FAILED("FAILED"),
	SUCCESSFUL("SUCCESSFUL"),
	INTERRUPTED("INTERRUPTED"),
	FAILED_DUPLICATE_ORGANISM("FAILED - DUPLICATE ORGANISM");

	private String type;

	private ImportJobStatusType(String type) {
		this.type = type;
	}

	public ImportJobStatusType getImportJobStatusType(String input) {
		for (ImportJobStatusType myType : ImportJobStatusType.values()) {
			if (myType.type.equalsIgnoreCase(input)) {
				return myType;
			}
		}
		return null;
	}

	public boolean isEqual(Object type) {
		if (type == null) {
			return false;
		}
		if (!(type instanceof String)) {
			return false;
		}
		if (this.type.equalsIgnoreCase((String) type)) {
			return true;
		}

		return false;
	}

	public String toString() {
		return type;
	}

}
