package org.mun.genet.configurer;

import java.util.Iterator;
import java.util.List;

import org.mun.genet.daos.FileStoreService;
import org.mun.genet.daos.JobService;
import org.mun.genet.initialize.Initializable;
import org.mun.genet.vo.JobVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class DBIntigrityChecker implements Initializable {
	@Autowired
	private JobService jobService;
	@Autowired
	private FileStoreService fileStoreService;

	@Override
	public void initialize(ApplicationContext context) {
		List<JobVo> runningJobs = jobService.findAllRunningJobs();
		Iterator<JobVo> runningJobItr = runningJobs.iterator();
		JobVo job = null;
		while (runningJobItr.hasNext()) {
			job = runningJobItr.next();
			jobService.markJobAsInturrupted(job.getId());
			fileStoreService.setCompeletedDate(job.getDataPath());
		}

	}

}
