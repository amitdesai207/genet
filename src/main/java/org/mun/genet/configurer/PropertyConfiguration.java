package org.mun.genet.configurer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PropertyConfiguration implements Configuration {

	public static Configuration config;

	@Autowired
	private ConfigCache dbConfigCache;

	public PropertyConfiguration() {
		config = this;
	}

	@Override
	public String getConfigurationByName(String name) throws IllegalArgumentException {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException();
		}
		return dbConfigCache.getConfigurationByName(name);
	}

}
