package org.mun.genet.configurer;

import java.util.Properties;

import org.mun.genet.initialize.Initializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

@Component
public class EmailConfigurer implements Initializable {

	@Autowired
	private JavaMailSenderImpl mailSender;
	@Autowired
	private Configuration configuration;

	@Override
	public void initialize(ApplicationContext context) {
		mailSender.setHost(configuration.getConfigurationByName("mail.server.name"));
		mailSender.setPort(Integer.parseInt(configuration
				.getConfigurationByName("mail.server.port")));
		if (configuration.getConfigurationByName("mail.user") != null)
			mailSender.setUsername(configuration.getConfigurationByName("mail.user"));
		if (configuration.getConfigurationByName("mail.password") != null)
			mailSender.setPassword(configuration.getConfigurationByName("mail.password"));
		Properties javaMailProperties = new Properties();
		javaMailProperties.put("mail.smtp.auth",
				Boolean.parseBoolean(configuration.getConfigurationByName("mail.smtp.auth")));
		javaMailProperties.put("mail.smtp.starttls.enable", Boolean.parseBoolean(configuration
				.getConfigurationByName("mail.smtp.starttls.enable")));
		mailSender.setJavaMailProperties(javaMailProperties);
	}

}
