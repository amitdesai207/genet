package org.mun.genet.configurer;

public interface Configuration {

	public String getConfigurationByName(String name) throws IllegalArgumentException;
	
}
