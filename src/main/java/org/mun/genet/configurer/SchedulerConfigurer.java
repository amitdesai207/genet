package org.mun.genet.configurer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.mun.genet.cleanup.PasswordResetJob;
import org.mun.genet.cleanup.PruningJob;
import org.mun.genet.initialize.Initializable;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.CronTriggerBean;
import org.springframework.scheduling.quartz.JobDetailBean;
import org.springframework.scheduling.quartz.SimpleTriggerBean;
import org.springframework.stereotype.Component;

@Component
public class SchedulerConfigurer implements Initializable {
	private final static Logger logger = LoggerFactory.getLogger(SchedulerConfigurer.class);

	@Autowired
	Scheduler scheduler;
	@Autowired
	Configuration config;

	@Override
	public void initialize(ApplicationContext context) {
		try {
			CronTriggerBean cronTriggerBean = buildCronTriggerBeanForPruningJobs(context);
			CronTriggerBean cronTriggerPasswordResetBean = buildCronTriggerBeanForPasswordReset(context);
			SimpleTriggerBean simpleTriggerBean = buildSimpleTriggerBean(context);
			try {
				scheduler.scheduleJob(simpleTriggerBean.getJobDetail(), simpleTriggerBean);
				scheduler.scheduleJob(cronTriggerBean.getJobDetail(), cronTriggerBean);
				scheduler.scheduleJob(cronTriggerPasswordResetBean.getJobDetail(), cronTriggerPasswordResetBean);
				logger.info("Scheduled PrunningJob " + cronTriggerBean);
			} catch (SchedulerException e) {
				logger.error("Error scheduling jobs ", e);
			}
		} catch (Exception e) {
			logger.error("Error while initializing cron jobs.", e);
		}
	}

	private SimpleTriggerBean buildSimpleTriggerBean(ApplicationContext context) throws Exception {
		JobDetailBean jobDetail = new JobDetailBean();
		jobDetail.setName("schedulerJob");
		jobDetail.setJobClass(SchedulerJob.class);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("jobQueueImpl", context.getBean("jobQueueImpl"));
		jobDetail.setJobDataAsMap(data);

		SimpleTriggerBean simpleTriggerBean = new SimpleTriggerBean();
		long interval = Long.parseLong(config.getConfigurationByName("scheduler.trigger_interval"));
		simpleTriggerBean.setRepeatInterval(interval * 1000);
		simpleTriggerBean.setName("simpleTriggerBean");
		simpleTriggerBean.setStartDelay(1000l);
		simpleTriggerBean.setStartTime(new Date());
		simpleTriggerBean.setJobDetail(jobDetail);

		return simpleTriggerBean;
	}

	public CronTriggerBean buildCronTriggerBeanForPruningJobs(ApplicationContext context) throws Exception {
		JobDetailBean jobDetail = new JobDetailBean();
		jobDetail.setName("prunOldFilesJob");
		jobDetail.setJobClass(PruningJob.class);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("remover", context.getBean("fileStoreServiceImpl"));
		jobDetail.setJobDataAsMap(data);

		CronTriggerBean cronTriggerBean = new CronTriggerBean();
		String cronExpression = config.getConfigurationByName("storage.cleanup.frequency");
		cronTriggerBean.setCronExpression(cronExpression);
		cronTriggerBean.setName("PrunTriggerBean");
		cronTriggerBean.setJobDetail(jobDetail);

		return cronTriggerBean;
	}
	
	public CronTriggerBean buildCronTriggerBeanForPasswordReset(ApplicationContext context) throws Exception {
		JobDetailBean jobDetail = new JobDetailBean();
		jobDetail.setName("passwordResetJob");
		jobDetail.setJobClass(PasswordResetJob.class);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("resetter", context.getBean("userServiceImpl"));
		jobDetail.setJobDataAsMap(data);

		CronTriggerBean cronTriggerBean = new CronTriggerBean();
		String cronExpression = config.getConfigurationByName("password.reset.frequency");
		cronTriggerBean.setCronExpression(cronExpression);
		cronTriggerBean.setName("PasswordTriggerBean");
		cronTriggerBean.setJobDetail(jobDetail);

		return cronTriggerBean;
	}

}
