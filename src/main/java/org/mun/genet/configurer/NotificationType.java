package org.mun.genet.configurer;

public enum NotificationType {

	JOB_SUBMIT("JOB_SUBMIT"),
	JOB_COMPLETE("JOB_COMPLETE"),
	JOB_ERROR("JOB_ERROR");

	private String type;

	private NotificationType(String type) {
		this.type = type;
	}

	public NotificationType getNotificationType(String input) {
		for (NotificationType myType : NotificationType.values()) {
			if (myType.type.equalsIgnoreCase(input)) {
				return myType;
			}
		}
		return null;
	}

	public boolean isEqual(Object type) {
		if (type == null) {
			return false;
		}
		if (!(type instanceof String)) {
			return false;
		}
		if (this.type.equalsIgnoreCase((String) type)) {
			return true;
		}

		return false;
	}

	public String toString() {
		return type;
	}

}