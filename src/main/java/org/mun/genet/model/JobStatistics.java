package org.mun.genet.model;

public class JobStatistics {

	private int recordsReceivedCount;
	private int recordsSuccessfulCount;
	private int countOfDefaultValues;

	public int getRecordsReceivedCount() {
		return recordsReceivedCount;
	}

	public void setRecordsReceivedCount(int recordsReceivedCount) {
		this.recordsReceivedCount = recordsReceivedCount;
	}

	public int getRecordsSuccessfulCount() {
		return recordsSuccessfulCount;
	}

	public void setRecordsSuccessfulCount(int recordsSuccessfulCount) {
		this.recordsSuccessfulCount = recordsSuccessfulCount;
	}

	public int getCountOfDefaultValues() {
		return countOfDefaultValues;
	}

	public void setCountOfDefaultValues(int countOfDefaultValues) {
		this.countOfDefaultValues = countOfDefaultValues;
	}

}
