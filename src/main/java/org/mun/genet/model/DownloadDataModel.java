package org.mun.genet.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.mun.genet.processor.ImportDataFiles;

public class DownloadDataModel {
	private static String CSV_FILE_EXTENSION = ".csv";
	private static String CYTO_FILE_EXTENSION = ".sif";

	private boolean condition_information = false;
	private boolean gene_info = false;
	private boolean gene_expression = false;
	private boolean module_significance = false;
	private boolean gene_pfam_desc = false;
	private boolean gene_pfam = false;
	private boolean gene_kegg_desc = false;
	private boolean gene_kegg = false;
	private boolean gene_metacyc = false;
	private boolean gene_protein = false;
	private boolean gene_tu = false;
	private boolean module_enrichment = false;
	private boolean correlation_matrix = false;
	private boolean correlation_matrix_cyto = false;
	private boolean weighted_matrix = false;
	private boolean weighted_matrix_cyto = false;
	private boolean protein_sequence = false;
	private boolean all = false;

	public boolean isCondition_information() {
		return condition_information;
	}

	public void setCondition_information(boolean condition_information) {
		this.condition_information = condition_information;
	}

	public boolean isGene_info() {
		return gene_info;
	}

	public void setGene_info(boolean gene_info) {
		this.gene_info = gene_info;
	}

	public boolean isGene_expression() {
		return gene_expression;
	}

	public void setGene_expression(boolean gene_expression) {
		this.gene_expression = gene_expression;
	}

	public boolean isModule_significance() {
		return module_significance;
	}

	public void setModule_significance(boolean module_significance) {
		this.module_significance = module_significance;
	}

	public boolean isGene_pfam_desc() {
		return gene_pfam_desc;
	}

	public void setGene_pfam_desc(boolean gene_pfam_desc) {
		this.gene_pfam_desc = gene_pfam_desc;
	}

	public boolean isGene_pfam() {
		return gene_pfam;
	}

	public void setGene_pfam(boolean gene_pfam) {
		this.gene_pfam = gene_pfam;
	}

	public boolean isGene_kegg_desc() {
		return gene_kegg_desc;
	}

	public void setGene_kegg_desc(boolean gene_kegg_desc) {
		this.gene_kegg_desc = gene_kegg_desc;
	}

	public boolean isGene_kegg() {
		return gene_kegg;
	}

	public void setGene_kegg(boolean gene_kegg) {
		this.gene_kegg = gene_kegg;
	}

	public boolean isGene_metacyc() {
		return gene_metacyc;
	}

	public void setGene_metacyc(boolean gene_metacyc) {
		this.gene_metacyc = gene_metacyc;
	}

	public boolean isGene_protein() {
		return gene_protein;
	}

	public void setGene_protein(boolean gene_protein) {
		this.gene_protein = gene_protein;
	}

	public boolean isGene_tu() {
		return gene_tu;
	}

	public void setGene_tu(boolean gene_tu) {
		this.gene_tu = gene_tu;
	}

	public boolean isModule_enrichment() {
		return module_enrichment;
	}

	public void setModule_enrichment(boolean module_enrichment) {
		this.module_enrichment = module_enrichment;
	}

	public boolean isCorrelation_matrix() {
		return correlation_matrix;
	}

	public void setCorrelation_matrix(boolean correlation_matrix) {
		this.correlation_matrix = correlation_matrix;
	}

	public boolean isCorrelation_matrix_cyto() {
		return correlation_matrix_cyto;
	}

	public void setCorrelation_matrix_cyto(boolean correlation_matrix_cyto) {
		this.correlation_matrix_cyto = correlation_matrix_cyto;
	}

	public boolean isWeighted_matrix() {
		return weighted_matrix;
	}

	public void setWeighted_matrix(boolean weighted_matrix) {
		this.weighted_matrix = weighted_matrix;
	}

	public boolean isWeighted_matrix_cyto() {
		return weighted_matrix_cyto;
	}

	public void setWeighted_matrix_cyto(boolean weighted_matrix_cyto) {
		this.weighted_matrix_cyto = weighted_matrix_cyto;
	}

	public boolean isProtein_sequence() {
		return protein_sequence;
	}

	public void setProtein_sequence(boolean protein_sequence) {
		this.protein_sequence = protein_sequence;
	}

	public void setImportDataFileInModel(String dataFile) {
		if (ImportDataFiles.CONDITION_INFORMATION.isEqual(dataFile))
			this.condition_information = true;
		if (ImportDataFiles.CORRELATION_MATRIX.isEqual(dataFile)) {
			this.correlation_matrix = true;
			this.correlation_matrix_cyto = true;
		}
		if (ImportDataFiles.GENE_EXPRESSION.isEqual(dataFile))
			this.gene_expression = true;
		if (ImportDataFiles.GENE_INFO.isEqual(dataFile))
			this.gene_info = true;
		if (ImportDataFiles.GENE_KEGG.isEqual(dataFile))
			this.gene_kegg = true;
		if (ImportDataFiles.GENE_KEGG_DESC.isEqual(dataFile))
			this.gene_kegg_desc = true;
		if (ImportDataFiles.GENE_METACYC.isEqual(dataFile))
			this.gene_metacyc = true;
		if (ImportDataFiles.GENE_PFAM.isEqual(dataFile))
			this.gene_pfam = true;
		if (ImportDataFiles.GENE_PFAM_DESC.isEqual(dataFile))
			this.gene_pfam_desc = true;
		if (ImportDataFiles.GENE_PROTEIN.isEqual(dataFile))
			this.gene_protein = true;
		if (ImportDataFiles.GENE_TU.isEqual(dataFile))
			this.gene_tu = true;
		if (ImportDataFiles.MODULE_ENRICHMENT.isEqual(dataFile))
			this.module_enrichment = true;
		if (ImportDataFiles.MODULE_SIGNIFICANCE.isEqual(dataFile))
			this.module_significance = true;
		if (ImportDataFiles.WEIGHTED_MATRIX.isEqual(dataFile)) {
			this.weighted_matrix = true;
			this.weighted_matrix_cyto = true;
		}
	}

	public List<String> getImportDataFilePathsInModel(String folderPath, String seqFileName) {
		String baseFolder = folderPath + File.separator;
		List<String> fileList = new ArrayList<>();
		if (condition_information)
			fileList.add(baseFolder + ImportDataFiles.CONDITION_INFORMATION + CSV_FILE_EXTENSION);
		if (correlation_matrix)
			fileList.add(baseFolder + ImportDataFiles.CORRELATION_MATRIX + CSV_FILE_EXTENSION);
		if (correlation_matrix_cyto)
			fileList.add(baseFolder + ImportDataFiles.CORRELATION_MATRIX + CYTO_FILE_EXTENSION);
		if (gene_expression)
			fileList.add(baseFolder + ImportDataFiles.GENE_EXPRESSION + CSV_FILE_EXTENSION);
		if (gene_info)
			fileList.add(baseFolder + ImportDataFiles.GENE_INFO + CSV_FILE_EXTENSION);
		if (gene_kegg)
			fileList.add(baseFolder + ImportDataFiles.GENE_KEGG + "_v2" + CSV_FILE_EXTENSION);
		if (gene_kegg_desc)
			fileList.add(baseFolder + ImportDataFiles.GENE_KEGG_DESC + CSV_FILE_EXTENSION);
		if (gene_metacyc)
			fileList.add(baseFolder + ImportDataFiles.GENE_METACYC + CSV_FILE_EXTENSION);
		if (gene_pfam)
			fileList.add(baseFolder + ImportDataFiles.GENE_PFAM + CSV_FILE_EXTENSION);
		if (gene_pfam_desc)
			fileList.add(baseFolder + ImportDataFiles.GENE_PFAM_DESC + CSV_FILE_EXTENSION);
		if (gene_protein)
			fileList.add(baseFolder + ImportDataFiles.GENE_PROTEIN + CSV_FILE_EXTENSION);
		if (gene_tu)
			fileList.add(baseFolder + ImportDataFiles.GENE_TU + CSV_FILE_EXTENSION);
		if (module_enrichment)
			fileList.add(baseFolder + ImportDataFiles.MODULE_ENRICHMENT + CSV_FILE_EXTENSION);
		if (module_significance)
			fileList.add(baseFolder + ImportDataFiles.MODULE_SIGNIFICANCE + CSV_FILE_EXTENSION);
		if (weighted_matrix)
			fileList.add(baseFolder + ImportDataFiles.WEIGHTED_MATRIX + CSV_FILE_EXTENSION);
		if (weighted_matrix_cyto)
			fileList.add(baseFolder + ImportDataFiles.WEIGHTED_MATRIX + CYTO_FILE_EXTENSION);
		if (protein_sequence)
			fileList.add(baseFolder + seqFileName);
		return fileList;
	}

	public boolean isAll() {
		return all;
	}

	public void setAll(boolean all) {
		this.all = all;
	}
}