package org.mun.genet.model;

import java.util.ArrayList;

public class CytoScape {
	String[] _node_Schema;
	String[] _edge_Schema;
	//
	public String toCytoScape(String[] node_Schema, String[] edge_Schema, String[][] node_Data, String[][] edge_Data)
	{
		String cyto="";
		cyto="{"+dataSchema(nodeSchema(node_Schema),edgeSchema(edge_Schema))+","+graphData(nodeData(node_Data),edgeData(edge_Data))+"}";
		return cyto;
	}
	String graphData(String node_data, String edge_data)
	{
		String data="";
		if(node_data.compareTo("")>0 && edge_data.compareTo("")>0)
		{
			data="\"data\":{"+node_data+","+edge_data+"}";
		}
		else
		{
			if(node_data.compareTo("")>0)
			{
				data="\"data\":{"+node_data+"}";
			}
			else
			{
				if(edge_data.compareTo("")>0)
				{
					data="\"data\":{"+edge_data+"}";
				}
			}
		}
		return data;	
	}
	String nodeData(String[][] data)
	{
		//data should be in this format:  data[i][0]=id  , data[i][j]=associated value to _nodeSchema[j]
		String nodes="";
		if(data.length>0)
		{
			
			nodes="\"nodes\":[";
			for(int i=0; i<data.length;i++)
			{
				nodes+="{\"id\":\""+data[i][0]+"\"";
				for(int j=0; j<_node_Schema.length;j++)
				{	
					nodes+=",\""+_node_Schema[j]+"\":\""+data[i][j+1]+"\"";
				}
				nodes+="}";

				if((i+1)<data.length)
				{
					nodes+=",";
				}
			}
			nodes+="]";
		}
		return nodes;
	}
	String edgeData(String[][] data)
	{
		//data: {nodes: [ { id: "1" }, { id: "2" } ],edges: [ { id: "2to1", target: "1", source: "2" } ]}
		//data should be in this format:  data[i][0]=id  , data[i][j]=associated value to _nodeSchema[j]
		String edges="";
		if(data.length>0)
		{
			edges="\"edges\":[";
			for(int i=0; i<data.length;i++)
			{
				edges+="{\"id\":\""+data[i][0]+"\","+"\"target\":\""+data[i][1]+"\","+"\"source\":\""+data[i][2]+"\"";
				for(int j=0; j<_edge_Schema.length;j++)
				{
					
					edges+=",\""+_edge_Schema[j]+"\":\""+data[i][3]+"\"";
				}
				edges+="}";

				if((i+1)<data.length)
				{
					edges+=",";
				}
			}
			edges+="]";
		}
		return edges;
	}
		

	
	//this function create an string like this one: "dataSchema":{"nodes":[{"name":"label","type":"string"}],"edges":[{"name":"weight","type":"string"}]}
	String dataSchema(String node_Schema, String edge_Schema)
	{
		String schema="";
		if(node_Schema.compareTo("")>0 && edge_Schema.compareTo("")>0)
		{
			schema="\"dataSchema\":{"+node_Schema+","+edge_Schema+"}";
		}
		else
		{
			if(node_Schema.compareTo("")>0)
			{
				schema="\"dataSchema\":{"+node_Schema+"}";
			}
			else
			{
				if(edge_Schema.compareTo("")>0)
				{
					schema="\"dataSchema\":{"+edge_Schema+"}";
				}
			}
		}
		return schema;	
	}
	// data is an array of data's names ()
	//this function create a string like this:>>"nodes": [ { "name": "label", "type": "string" }]<<
	String nodeSchema(String[] data)
	{
		String nodes="";
		if(data.length>0)
		{
			_node_Schema=data;
			nodes="\"nodes\":[";
			for(int i=0; i<data.length;i++)
			{
				nodes+="{\"name\":\""+data[i]+"\",\"type\":\"string\"}";
				if((i+1)<data.length)
				{
					nodes+=",";
				}
			}
			nodes+="]";
		}
		return nodes;
	}
	// data is an array of data's names ()
	//this function create a string like this:>>"edges": [ { "name": "weight", "type": "string" }]<<
	String edgeSchema(String[] data)
	{
		String edges="";
		if(data.length>0)
		{
			_edge_Schema=data;
			edges="\"edges\":[";
			for(int i=0; i<data.length;i++)
			{
				edges+="{\"name\":\""+data[i]+"\",\"type\":\"string\"}";
				if((i+1)<data.length)
				{
					edges+=",";
				}
			}
			edges+="]";
		}
		return edges;
	}
//========================================================visual_style=================================================================	
/*
  var visual_style = {
                    global: {
                        backgroundColor: "#ABCFD6"
                    },
                    nodes: {
                        shape: "OCTAGON",
                        borderWidth: 3,
                        borderColor: "#ffffff",
                        size: {
                            defaultValue: 25,
                            continuousMapper: { attrName: "weight", minValue: 25, maxValue: 75 }
                        },
                        color: {
                            discreteMapper: {
                                attrName: "id",
                                entries: [
                                    { attrValue: 1, value: "#0B94B1" },
                                    { attrValue: 2, value: "#9A0B0B" },
                                    { attrValue: 3, value: "#dddd00" }
                                ]
                            }
                        },
                        labelHorizontalAnchor: "center"
                    },
                    edges: {
                        width: 3,
                        color: "#0B94B1"
                    }
                };*/
	public String networkOption(String[] node_Schema, String[] edge_Schema, String[][] node_Data, String[][] edge_Data,ArrayList<ArrayList<String>> genesColor)
	{
		String returnStr="";
		returnStr+="{\"network\":\""+toCytoScape(node_Schema, edge_Schema, node_Data, edge_Data)+"\",\"visualStyle\":\""+nodeStyle(genesColor)+"\"}";
		return returnStr;
	}
	public String networkStyle(ArrayList<ArrayList<String>> geneColor)
	{
		String retunStr="";
		retunStr+="{"+nodeStyle(geneColor)+","+edgeStyle()+"}";
		return retunStr;
		
	}
	String nodeStyle(ArrayList<ArrayList<String>> genesColor)
	{
		String returnStr="\"nodes\":{";
		//add  border width, border color
		returnStr+="\"borderWidth\":"+"\"1\",";
		returnStr+="\"borderColor\":"+"\"#e5e5e5\",";
		//add nodeSize
		returnStr+="\"size\":"+"\"25\",";
		returnStr+="\"labelYOffset\":"+"\"-25\",";
		returnStr+="\"opacity\":"+"\"0.99\",";
		 
		//add color based on gene module
		returnStr+="\"color\":{\"discreteMapper\":{\"attrName\":\"id\",\"entries\":[";
		for(int i=0;i<genesColor.size();i++)
		{
			returnStr+="{\"attrValue\":\""+genesColor.get(i).get(0)+"\",\"value\":\""+genesColor.get(i).get(1)+"\"}";
			if(i<genesColor.size()-1)
			{
				returnStr+=",";
			}
		}
		returnStr+="]}}";
		//close the node style
		returnStr+="}";
		
		return returnStr;
	}
	String edgeStyle()
	{
		/*{
                            defaultValue: 25,
                            continuousMapper: { attrName: "weight", minValue: 25, maxValue: 75 }
                        }*/
		String returnStr="\"edges\":{";
		//add   width, color
		returnStr+="\"width\":"+"{\"defaultValue\":2,\"continuousMapper\":{\"attrName\":\"weight\",\"minValue\":\"2\",\"maxValue\":\"300\",\"minAttrValue\":\"0.08\",\"maxAttrValue\":\"1.0\"}},";
		returnStr+="\"color\":"+"\"#c1c1c1\"";
		//close edge style
		returnStr+="}";
		return returnStr;
	}
	
}

