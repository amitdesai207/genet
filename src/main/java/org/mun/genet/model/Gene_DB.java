package org.mun.genet.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
//import net.sf.json.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.mun.genet.configurer.Configuration;
import org.mun.genet.util.DatabaseConnection;
import org.mun.genet.vo.GeneInfoVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Gene_DB {
	private static final Logger logger = LoggerFactory.getLogger(Gene_DB.class);
	private Connection dbConnection;
	private Configuration configuration;

	public Gene_DB(String organism, Configuration configuration) {
		dbConnection = new DatabaseConnection().getDatabaseConnection(organism);
		this.configuration = configuration;
	}

	// collect all the genes start with the input value (for user query
	// expansion)
	public ArrayList<GeneInfoVo> genesList(String id, String value) {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			// create the sql statement
			// check the category of gene of interest
			// to be sure that the value is safe
			value = safetyChecker(value);
			// create the value for the select command;
			String sqlSelectValue = "";
			String COL_id = "";
			if (id.compareTo("Symbol") == 0) {
				COL_id = "symbol, description";
				sqlSelectValue = "symbol like '" + value + "%'";
			} else {
				if (id.compareTo("Systematic ID") == 0) {
					COL_id = "systematic_id, description";
					sqlSelectValue = "systematic_id like '" + value + "%'";
				} else {
					if (id.compareTo("Entrez ID") == 0) {
						COL_id = "entrez_id1, description";
						sqlSelectValue = "entrez_id1 like '" + value + "%'";
					} else {
						if (id.compareTo("Description") == 0) {
							COL_id = "symbol, description";
							sqlSelectValue = "description like '%" + value + "%'";
						}
					}
				}
			}
			stmt = dbConnection.prepareStatement("select " + COL_id + " from gene_info where " + sqlSelectValue);
			rs = stmt.executeQuery();
			// check whether the query is executed
			// if so, save the result in rs
			// try-catch is to be sure that there is a result!!
			try {

				ArrayList<ArrayList<String>> basicResult = resultSet_to_Array(rs);
				ArrayList<GeneInfoVo> geneInfos = new ArrayList<>();
				if (basicResult.size() != 0) {
					for (ArrayList<String> genes : basicResult) {
						geneInfos.add(new GeneInfoVo(genes.get(0), genes.get(1)));
					}
					return geneInfos;
				}
				return new ArrayList<GeneInfoVo>();

			} catch (Exception exc) {
				return null;
			}

		} catch (SQLException ex) {
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block
			// in reverse-order of their creation are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		return null;
	}

	// --------------------------------------------------------------------------------------
	// geneInfoCollector returns the data in the format of
	public String[] geneInfoCollector(String id, String value) {
		String[] finalResults = { "N/A", "N/A", "N/A", "N/A", "N/A", "N/A,N/A,N/A,N/A,N/A,N/A", "N/A", "N/A" };

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"SELECT gene_info.symbol, gene_info.entrez_id1, gene_info.systematic_id, gene_info.description, gene_info.type, module, GROUP_CONCAT(distinct(pfam_id) SEPARATOR ', ') AS pfam, GROUP_CONCAT(distinct(pathway_id) SEPARATOR ', ') AS kegg, GROUP_CONCAT(distinct(protein_complexes.complex) SEPARATOR ', ') AS pc, GROUP_CONCAT(distinct(trans.tu) SEPARATOR ', ') AS tu, GROUP_CONCAT(distinct(metacyc_pathway.pathway) SEPARATOR ', ') AS pathway FROM gene_info left outer join module_has_genes on module_has_genes.symbol= gene_info.symbol left outer join pfam_annotation on pfam_annotation.symbol= gene_info.symbol left outer join kegg_annotation on kegg_annotation.symbol= gene_info.symbol left outer join metacyc_pc_annotation on metacyc_pc_annotation.symbol= gene_info.symbol left outer join protein_complexes on protein_complexes.id = metacyc_pc_annotation.complex left outer join metacyc_tu_annotation on metacyc_tu_annotation.symbol= gene_info.symbol left outer join tu trans on trans.id = metacyc_tu_annotation.tu left outer join metacyc_pathway_annotation on metacyc_pathway_annotation.symbol= gene_info.symbol left outer join metacyc_pathway on metacyc_pathway.id = metacyc_pathway_annotation.pathway WHERE gene_info.symbol=? or gene_info.systematic_id=? or gene_info.entrez_id1=? GROUP BY symbol;");
			// check the category of gene of interest
			// to be sure that the value is safe
			value = safetyChecker(value);
			stmt.setString(1, value);
			stmt.setString(2, value);
			stmt.setString(3, value);
			rs = stmt.executeQuery();
			// create the value for the select command;
			// try-catch is to be sure that there is a result!!
			try {
				// since user search for a specific gene, the size of
				// the results is 1
				ArrayList<ArrayList<String>> result = resultSet_to_Array(rs);
				if (result.size() != 0) {
					ArrayList<String> basicResult = new ArrayList<>();
					ArrayList<String> basicResults = result.get(0);
					for (int i = 0; i < basicResults.size(); i++) {
						if (i == 3)
							basicResult.add(basicResults.get(i).replace(",", "&#44;"));
						else
							basicResult.add(basicResults.get(i));
					}
					// store the systematic_ID in order to create the ncbi
					// url link
					finalResults[0] = basicResult.get(6);
					finalResults[1] = basicResult.get(7);
					finalResults[2] = basicResult.get(9);
					finalResults[3] = basicResult.get(8);
					finalResults[4] = basicResult.get(10);
					finalResults[5] = basicResult.subList(0, 5).toString().substring(1,
							basicResult.subList(0, 5).toString().length() - 1) + ", ";
					if(!basicResult.get(1).equalsIgnoreCase("NA"))
						finalResults[5] += configuration.getConfigurationByName("geneInformationLink") + basicResult.get(1);
					finalResults[6] = basicResult.get(0);
					finalResults[7] = basicResult.get(5);
				}
				return finalResults;
			} catch (Exception exc) {
				return finalResults;
			}

		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block
			// in reverse-order of their creation are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		return finalResults;
	}

	public String[][] getConditions() {
		ArrayList<ArrayList<String>> conditionsAndRanks = new ArrayList<ArrayList<String>>();

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"select `condition`, number_replicates, strain, growth_phase, medium, description from condition_information");

			// ---------------------------get the gene's
			// module-----------------------------------
			rs = stmt.executeQuery();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				conditionsAndRanks = resultSet_to_Array(rs);
			} catch (Exception exc) {
				logger.error(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		String[][] returnData = null;
		if (conditionsAndRanks.size() != 0) {
			returnData = new String[conditionsAndRanks.size()][conditionsAndRanks.get(0).size()];
			for (int i = 0; i < conditionsAndRanks.size(); i++) {
				for (int j = 0; j < conditionsAndRanks.get(i).size(); j++) {
					returnData[i][j] = conditionsAndRanks.get(i).get(j);
				}
			}
		}
		return returnData;
	}

	// collect the gene expression info based on the given symbol name
	public String[][] geneExperssionInfo(String symbol) {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		// try-catch is to be sure that the sql statement is executed, and there
		// is a result!!
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement("select `condition`, log2_ratio from gene_expression where symbol=?");
			stmt.setString(1, symbol);
			rs = stmt.executeQuery();

			ArrayList<ArrayList<String>> data = resultSet_to_Array(rs);
			String[][] result = null;
			if (data.size() != 0) {
				result = new String[data.size()][2];
				for (int i = 0; i < data.size(); i++) {
					result[i][0] = data.get(i).get(0);
					result[i][1] = data.get(i).get(1);
				}
			}
			return result;

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block
			// in reverse-order of their creation are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		return null;
	}

	public String[][][] geneExperssionInfo(Set<String> symbols) {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			StringBuffer query = new StringBuffer(
					"select `condition`, log2_ratio, symbol from gene_expression where symbol in (");
			for (String symbol : symbols) {
				query.append("'" + symbol + "'");
				query.append(",");
			}
			query.deleteCharAt(query.lastIndexOf(","));
			query.append(")");
			stmt = dbConnection.prepareStatement(query.toString());
			rs = stmt.executeQuery();

			Map<String, ArrayList<ArrayList<String>>> geneExpressionMap = new HashMap<>();
			ArrayList<ArrayList<String>> data = resultSet_to_Array(rs);
			for (int i = 0; i < data.size(); i++) {
				if (geneExpressionMap.containsKey(data.get(i).get(2))) {
					ArrayList<String> geneInfo = new ArrayList<>();
					geneInfo.add(data.get(i).get(0));
					geneInfo.add(data.get(i).get(1));
					geneExpressionMap.get(data.get(i).get(2)).add(geneInfo);
				} else {
					ArrayList<ArrayList<String>> geneList = new ArrayList<>();
					ArrayList<String> geneInfo = new ArrayList<>();
					geneInfo.add(data.get(i).get(0));
					geneInfo.add(data.get(i).get(1));
					geneList.add(geneInfo);
					geneExpressionMap.put(data.get(i).get(2), geneList);
				}
			}
			String[][][] geneExpression = new String[symbols.size()][][];
			int i = 0;
			for (Entry<String, ArrayList<ArrayList<String>>> geneExp : geneExpressionMap.entrySet()) {
				String[][] expression = new String[geneExp.getValue().size()][2];
				for (int j = 0; j < geneExp.getValue().size(); j++) {
					expression[j][0] = geneExp.getValue().get(j).get(0);
					expression[j][1] = geneExp.getValue().get(j).get(1);
				}
				geneExpression[i] = expression;
				i++;
			}
			return geneExpression;

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block
			// in reverse-order of their creation are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		return null;
	}

	// =================================================================================================================================

	public int getTotalNoOfGenesInModule(String module) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		int noOfGenesInModule = 0;
		try {
			// create the sql statement
			stmt = dbConnection.prepareStatement("select count(symbol) from module_has_genes where module=?");
			stmt.setString(1, module);
			rs = stmt.executeQuery();
			// ---------------------------get the gene's
			// module-----------------------------------
			ArrayList<ArrayList<String>> data = resultSet_to_Array(rs);
			if (data.size() != 0) {
				noOfGenesInModule = Integer.parseInt(data.get(0).get(0));
			}
			// check whether the gene has a module

		} catch (Exception exc) {
			logger.error(exc.toString());
			return 0;
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		return noOfGenesInModule;
	}

	public String[][] avrageExpression_calculation(String module, int noOfGenesInModule) {

		ResultSet rs = null;
		// ---------------Get the all genes that are in the same
		// module---------------------------
		PreparedStatement stmt1 = null;
		try {
			stmt1 = dbConnection.prepareStatement(
					"select `condition`, sum(log2_ratio)/? from gene_expression where symbol in (select symbol from module_has_genes where module=?) group by `condition`;");
			stmt1.setInt(1, noOfGenesInModule);
			stmt1.setString(2, module);
			rs = stmt1.executeQuery();
			ArrayList<ArrayList<String>> data = resultSet_to_Array(rs);
			String[][] avrages = null;
			if (data.size() != 0) {
				avrages = new String[data.size()][2];
				for (int i = 0; i < data.size(); i++) {
					avrages[i][0] = data.get(i).get(0);
					avrages[i][1] = data.get(i).get(1);
				}
			}
			return avrages;
		} catch (Exception exc) {
			logger.error(exc.toString());
			return null;
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt1 != null) {
				try {
					stmt1.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt1 = null;
			}

		}
	}

	// genesSymbol is the list of all the 3-tuple <symbol1, symbol2, weight>
	public Map<String, String> nodeColors(Set<String> symbols) {
		Map<String, String> nodeColors = new HashMap<>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			StringBuffer query = new StringBuffer(
					"select m2.symbol, ch.hexcode from module_has_genes m2 left outer join color_hex_codes ch on m2.module = ch.color where m2.symbol in (");
			for (String symbol : symbols) {
				query.append("'" + symbol + "',");
			}
			query.deleteCharAt(query.lastIndexOf(","));
			query.append(")");
			stmt = dbConnection.prepareStatement(query.toString());
			rs = stmt.executeQuery();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				ArrayList<ArrayList<String>> nodeColorsArray = resultSet_to_Array(rs);
				for (ArrayList<String> nodesColor : nodeColorsArray) {
					nodeColors.put(nodesColor.get(0), nodesColor.get(1));
				}
			} catch (Exception exc) {
				logger.error(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}

		return nodeColors;
	}

	// ===================================================================================================================================
	public String[][] networkDataForModuleGenes(Set<String> nodes) {
		ArrayList<String[]> connectedGenes = getConnectionsBetweenGenes(nodes.toArray(new String[nodes.size()]));
		String[][] returnData = new String[connectedGenes.size()][connectedGenes.get(0).length];
		for (int i = 0; i < connectedGenes.size(); i++) {
			for (int j = 0; j < connectedGenes.get(0).length; j++) {
				returnData[i][j] = connectedGenes.get(i)[j];
			}
		}
		return returnData;
	}

	public ArrayList<String[]> getConnectionsBetweenGenes(String[] symbols) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		// try-catch is to be sure that the sql statement is executed, and there
		// is a result!!
		ArrayList<String[]> connectedGenes = new ArrayList<String[]>();
		try {
			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"select symbol_1, symbol_2, weight, correlation from gene_coexpression_interaction where ((symbol_1=? and symbol_2=? ) or (symbol_1=? and symbol_2=?)) and weight!=0");
			for (int i = 0; i < symbols.length; i++) {
				for (int j = i + 1; j < symbols.length; j++) {
					stmt.setString(1, symbols[i]);
					stmt.setString(2, symbols[j]);
					stmt.setString(3, symbols[j]);
					stmt.setString(4, symbols[i]);
					rs = stmt.executeQuery();
					ArrayList<ArrayList<String>> result = resultSet_to_Array(rs);
					if (result.size() > 0) {
						connectedGenes.add(new String[] { result.get(0).get(0), result.get(0).get(1),
								result.get(0).get(2), result.get(0).get(3) });
					}
				}
			}
			return connectedGenes;
			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block
			// in reverse-order of their creation are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		return null;
	}

	// ===================================================================================================================================
	public String[][] networkData(String symbol, int limit) {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		// try-catch is to be sure that the sql statement is executed, and there
		// is a result!!
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"select symbol_1, symbol_2, weight, correlation from gene_coexpression_interaction where symbol_1=? and symbol_2!=? and weight!=0 order by weight desc limit ?");
			stmt.setString(1, symbol);
			stmt.setString(2, symbol);
			stmt.setInt(3, limit);

			rs = stmt.executeQuery();
			ArrayList<ArrayList<String>> data = resultSet_to_Array(rs);
			String[][] result_arr = null;
			if (data.size() != 0) {
				result_arr = new String[data.size()][data.get(0).size()];
				for (int i = 0; i < data.size(); i++) {
					for (int j = 0; j < data.get(0).size(); j++) {
						result_arr[i][j] = data.get(i).get(j);
					}
				}
			}
			return result_arr;
			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block
			// in reverse-order of their creation are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		return null;
	}

	public ArrayList<ArrayList<String>> readConnectedNodeData(Set<String> symbols, String parentNode) {

		PreparedStatement stmt = null;
		ResultSet rs = null;
		// try-catch is to be sure that the sql statement is executed, and there
		// is a result!!
		try {
			// create the sql statement
			StringBuffer query = new StringBuffer(
					"select symbol_1, symbol_2, weight, correlation from gene_coexpression_interaction where symbol_1 in (");
			for (String symbol : symbols) {
				if (symbol != parentNode) {
					query.append("'" + symbol + "'");
					query.append(",");
				}
			}
			query.deleteCharAt(query.lastIndexOf(","));
			query.append(") and symbol_2!=? and weight!=0 order by weight desc limit ?");
			stmt = dbConnection.prepareStatement(query.toString());
			stmt.setString(1, parentNode);
			stmt.setInt(2, 20);
			rs = stmt.executeQuery();
			ArrayList<ArrayList<String>> data = resultSet_to_Array(rs);
			// convert ArraList to array;
			return data;
			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block
			// in reverse-order of their creation are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		return null;
	}

	private ArrayList<ArrayList<String>> resultSet_to_Array(ResultSet resultSet) throws SQLException {
		ArrayList<ArrayList<String>> table = new ArrayList<ArrayList<String>>();
		if (resultSet == null)
			return table;
		int columnCount = resultSet.getMetaData().getColumnCount();

		if (resultSet.getType() == ResultSet.TYPE_FORWARD_ONLY) {
			table = new ArrayList<ArrayList<String>>();
		} else {
			resultSet.last();
			table = new ArrayList<ArrayList<String>>(resultSet.getRow());
			resultSet.beforeFirst();
		}
		for (ArrayList<String> row; resultSet.next(); table.add(row)) {
			row = new ArrayList<String>(columnCount);
			for (int c = 1; c <= columnCount; ++c) {
				if (resultSet.getString(c) != null)
					row.add(resultSet.getString(c).intern());
				else
					row.add("");
			}
		}
		return table;
	}

	String safetyChecker(String value) {
		value = value.replaceAll(";", "ddddd");
		value = value.replaceAll("\"", "ddddd");
		value = value.replaceAll("'", "ddddd");
		value = value.replaceAll(";", "ddddd");
		value = value.replaceAll("delete", "");
		value = value.replaceAll("drop", "");
		value = value.replaceAll("insert", "");
		value = value.replaceAll("select", "");
		value = value.replaceAll("DELETE", "");
		value = value.replaceAll("DROP", "");
		value = value.replaceAll("INSERT", "");
		value = value.replaceAll("SELECT", "");
		return value;
	}

}
