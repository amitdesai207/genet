package org.mun.genet.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
//import net.sf.json.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Set;

import org.mun.genet.configurer.Configuration;
import org.mun.genet.util.DatabaseConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public class Module_DB {
	private static final Logger logger = LoggerFactory.getLogger(Module_DB.class);
	private Connection dbConnection;
	private Configuration configuration;

	public Module_DB(String organism, Configuration configuration) {
		dbConnection = new DatabaseConnection().getDatabaseConnection(organism);
		this.configuration = configuration;
	}

	// ------------------------------------------------------------------------------------------------------------
	public String[][] getModuleList_plus_data() {
		ArrayList<ArrayList<String>> modulesList = new ArrayList<ArrayList<String>>();

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"select m.module, m.hub_symbol, (select count(*) from module_has_genes where module=m.module), GROUP_CONCAT(m2.significance SEPARATOR ',')  from module_info m left outer join module_significance m2 on m.module=m2.module group by m.module;");

			// ---------------------------get the
			// modules-----------------------------------
			rs = stmt.executeQuery();
			// if so, save the result in rs
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				modulesList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				logger.error(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		String[][] returnData = null;
		if (modulesList.size() != 0) {
			returnData = new String[modulesList.size()][modulesList.get(0).size()];
			for (int i = 0; i < modulesList.size(); i++) {
				for (int j = 0; j < modulesList.get(i).size(); j++) {
					returnData[i][j] = modulesList.get(i).get(j);
				}
			}
		}
		return returnData;
	}

	// -------------------------------------------------------------------------------
	public String moduleConditionsValues(String moduleName) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<ArrayList<String>> values = new ArrayList<ArrayList<String>>();
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement("select significance from module_significance where module=?");
			stmt.setString(1, moduleName);
			// ---------------------------get the
			// modules-----------------------------------
			rs = stmt.executeQuery();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				values = resultSet_to_Array(rs);
			} catch (Exception exc) {
				logger.error(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}

		ArrayList<String> significance = new ArrayList<>();
		for (ArrayList<String> value : values) {
			significance.add(value.get(0));
		}
		return StringUtils.arrayToCommaDelimitedString(significance.toArray());
	}

	// -------------------------------------------------------------------------------
	// ORDER BY rank DESC
	public String[] getOrderedConsitions() {
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<ArrayList<String>> value = new ArrayList<ArrayList<String>>();
		try {

			// create the sql statement
			stmt = dbConnection.createStatement();

			// ---------------------------get the
			// modules-----------------------------------
			rs = stmt.executeQuery("select `condition` from condition_information");
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				value = resultSet_to_Array(rs);
			} catch (Exception exc) {
				logger.error(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}

		String[] returnData = null;
		if (value.size() != 0) {
			returnData = new String[value.size()];
			for (int i = 0; i < value.size(); i++) {
				returnData[i] = value.get(i).get(0);

			}
		}
		return returnData;
	}

	// ----------------------------------------------------------------------------
	public String[] getHubgeneAndColorCode(String module) {
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<ArrayList<String>> geneAndColor = new ArrayList<ArrayList<String>>();
		try {

			// create the sql statement
			stmt = dbConnection.createStatement();

			// ---------------------------get the
			// modules-----------------------------------
			rs = stmt.executeQuery(
					"select hub_symbol, hexcode from module_info m left outer join color_hex_codes ch on m.module = ch.color where module='"
							+ module + "'");
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				geneAndColor = resultSet_to_Array(rs);
			} catch (Exception exc) {
				logger.error(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		if (geneAndColor.size() != 0) {
			String colorCode = null;
			if (geneAndColor.get(0).get(1) == null || geneAndColor.get(0).get(1).trim().isEmpty())
				colorCode = "#000000";
			else
				colorCode = geneAndColor.get(0).get(1);
			return new String[] { geneAndColor.get(0).get(0), colorCode };
		}
		return null;
	}

	// ----------------------------------------------------------------------------
	// collect the genes and their description which are in input module
	public String[][] getGenes_Description(String module, int limit) {
		return getModuleGenes(module, limit);
	}

	// ----------------------------------------------------------------------------

	// ----------------------------------------------------------------------------
	// collect the input's (gene) description
	public ArrayList<ArrayList<String>> getGeneInfoInModule(String module) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<ArrayList<String>> genes = new ArrayList<ArrayList<String>>();
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"SELECT gene_info.entrez_id1, gene_info.systematic_id, gene_info.symbol, gene_info.description, GROUP_CONCAT(distinct(pfam_id) SEPARATOR ', ') AS pfam, GROUP_CONCAT(distinct(pathway_id) SEPARATOR ', ') AS kegg, GROUP_CONCAT(distinct(protein_complexes.complex) SEPARATOR ', ') AS pc, GROUP_CONCAT(distinct(trans.tu) SEPARATOR ', ') AS tu, GROUP_CONCAT(distinct(metacyc_pathway.pathway) SEPARATOR ', ') AS pathway FROM gene_info left outer join module_has_genes on module_has_genes.symbol= gene_info.symbol left outer join pfam_annotation on pfam_annotation.symbol= gene_info.symbol left outer join kegg_annotation on kegg_annotation.symbol= gene_info.symbol left outer join metacyc_pc_annotation on metacyc_pc_annotation.symbol= gene_info.symbol left outer join protein_complexes on protein_complexes.id = metacyc_pc_annotation.complex left outer join metacyc_tu_annotation on metacyc_tu_annotation.symbol= gene_info.symbol left outer join tu trans on trans.id = metacyc_tu_annotation.tu left outer join metacyc_pathway_annotation on metacyc_pathway_annotation.symbol= gene_info.symbol left outer join metacyc_pathway on metacyc_pathway.id = metacyc_pathway_annotation.pathway where module=? GROUP BY gene_info.symbol order by gene_info.entrez_id1;");
			stmt.setString(1, module);
			rs = stmt.executeQuery();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				genes = resultSet_to_Array(rs);
			} catch (Exception exc) {
				logger.error(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}

		return genes;
	}

	private void releaseResources(ResultSet rs, PreparedStatement stmt) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException sqlEx) {
			} // ignore
			rs = null;
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException sqlEx) {
			} // ignore
			stmt = null;
		}

	}

	public ArrayList<String[]> getKeggUrls(String module) {
		ArrayList<String[]> moduleDetails = new ArrayList<>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			ArrayList<ArrayList<String>> value = new ArrayList<ArrayList<String>>();
			// get kegg details
			String keggLinkServer = configuration.getConfigurationByName("keggLinkServer");
			StringBuffer query = new StringBuffer(
					"select kegg.pathway_id, kegg_desc.description, GROUP_CONCAT(distinct(kegg_anno.symbol) SEPARATOR ', ') as genes, concat(concat('"
							+ keggLinkServer
							+ "/kegg-bin/mark_pathway_www?@', concat(kegg.pathway_id, '/default%3dyellow/')),GROUP_CONCAT(gene_info.systematic_id SEPARATOR '%09red,white/') ) as url from module_enrichment_kegg kegg left outer join kegg_description kegg_desc on kegg_desc.pathway_id= kegg.pathway_id left outer join kegg_annotation kegg_anno on kegg_anno.pathway_id= kegg.pathway_id left outer join gene_info gene_info on gene_info.symbol=kegg_anno.symbol where module=? and kegg_anno.symbol in (select g1.symbol from gene_info g1 left outer join module_has_genes m on g1.symbol=m.symbol where m.module=? order by g1.graph_degree desc) group by kegg.pathway_id");
			stmt = dbConnection.prepareStatement(query.toString());
			stmt.setString(1, module);
			stmt.setString(2, module);
			rs = stmt.executeQuery();
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				value = resultSet_to_Array(rs);
				for (int i = 0; i < value.size(); i++) {
					String[] pathWayWithDescription = new String[value.get(i).size()];
					for (int j = 0; j < value.get(i).size(); j++)
						pathWayWithDescription[j] = value.get(i).get(j);
					moduleDetails.add(pathWayWithDescription);
				}
			} catch (Exception exc) {
				logger.error(exc.toString());
			} finally {
				releaseResources(rs, stmt);
			}
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			releaseResources(rs, stmt);
		}
		return moduleDetails;
	}

	public ArrayList<String[]> getModuleEnrichemnt(String module, Set<String> symbols) {
		ArrayList<String[]> moduleDetails = new ArrayList<>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		// get pfam details
		try {
			ArrayList<ArrayList<String>> value = new ArrayList<ArrayList<String>>();
			String pfamLinkServer = configuration.getConfigurationByName("pfamLinkServer");
			StringBuffer query = new StringBuffer(
					"select pfam.pfam_id, pfam_desc.name, GROUP_CONCAT(distinct(pfam_anno.symbol) SEPARATOR ', ') as genes, concat('"
							+ pfamLinkServer
							+ "/family?acc=', pfam.pfam_id) as url from module_enrichment_pfam pfam left outer join pfam_description pfam_desc on pfam_desc.pfam_id= pfam.pfam_id left outer join pfam_annotation pfam_anno on pfam_anno.pfam_id= pfam.pfam_id where module=? and pfam_anno.symbol in (select g1.symbol from gene_info g1 left outer join module_has_genes m on g1.symbol=m.symbol where m.module=? order by g1.graph_degree desc) group by pfam.pfam_id");
			stmt = dbConnection.prepareStatement(query.toString());
			stmt.setString(1, module);
			stmt.setString(2, module);
			rs = stmt.executeQuery();
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				value = resultSet_to_Array(rs);
				for (int i = 0; i < value.size(); i++) {
					String[] pathWayWithDescription = new String[value.get(i).size()];
					for (int j = 0; j < value.get(i).size(); j++)
						pathWayWithDescription[j] = value.get(i).get(j);
					moduleDetails.add(pathWayWithDescription);
				}
			} catch (Exception exc) {
				logger.error(exc.toString());
			} finally {
				releaseResources(rs, stmt);
			}

			// get kegg details
			String keggPathwayLink = configuration.getConfigurationByName("keggPathwayLink");
			query = new StringBuffer(
					"select kegg.pathway_id, kegg_desc.description, GROUP_CONCAT(distinct(kegg_anno.symbol) SEPARATOR ', ') as genes, concat('"
							+ keggPathwayLink
							+ "', concat(kegg.pathway_id, '/default%3dyellow/')) as url from module_enrichment_kegg kegg "
							+ "left outer join kegg_description kegg_desc on kegg_desc.pathway_id= kegg.pathway_id left outer join kegg_annotation kegg_anno "
							+ "on kegg_anno.pathway_id= kegg.pathway_id left outer join gene_info gene_info on gene_info.symbol=kegg_anno.symbol where module=? "
							+ "and kegg_anno.symbol in (select g1.symbol from gene_info g1 left outer join module_has_genes m on g1.symbol=m.symbol where m.module=? "
							+ "order by g1.graph_degree desc) group by kegg.pathway_id");
			stmt = dbConnection.prepareStatement(query.toString());
			stmt.setString(1, module);
			stmt.setString(2, module);
			rs = stmt.executeQuery();
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				value = resultSet_to_Array(rs);
				for (int i = 0; i < value.size(); i++) {
					String[] pathWayWithDescription = new String[value.get(i).size()];
					for (int j = 0; j < value.get(i).size(); j++)
						pathWayWithDescription[j] = value.get(i).get(j);
					moduleDetails.add(pathWayWithDescription);
				}
			} catch (Exception exc) {
				logger.error(exc.toString());
			} finally {
				releaseResources(rs, stmt);
			}

			// get metacyc pathway details
			query = new StringBuffer("select pathway.pathway, 'MetaCyc', GROUP_CONCAT(distinct(pfam_anno.symbol) SEPARATOR ', ') as genes, '' as url from module_enrichment_metacyc module_pathway  left outer join metacyc_pathway_annotation pfam_anno on pfam_anno.pathway= module_pathway.pathway left outer join metacyc_pathway pathway on pathway.id = module_pathway.pathway where module=? and pfam_anno.symbol in (select g1.symbol from gene_info g1 left outer join module_has_genes m on g1.symbol=m.symbol where m.module=? order by g1.graph_degree desc) group by module_pathway.pathway");
			stmt = dbConnection.prepareStatement(query.toString());
			stmt.setString(1, module);
			stmt.setString(2, module);
			rs = stmt.executeQuery();
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				value = resultSet_to_Array(rs);
				for (int i = 0; i < value.size(); i++) {
					String[] pathWayWithDescription = new String[value.get(i).size()];
					for (int j = 0; j < value.get(i).size(); j++)
						pathWayWithDescription[j] = value.get(i).get(j);
					moduleDetails.add(pathWayWithDescription);
				}
			} catch (Exception exc) {
				logger.error(exc.toString());
			} finally {
				releaseResources(rs, stmt);
			}

			// get transit unit details
			query = new StringBuffer(
					"select trans.tu, 'Transcriptional Unit (TU)', GROUP_CONCAT(distinct(tu_anno.symbol) SEPARATOR ', ') as genes, '' as url from module_enrichment_tu tu left outer join metacyc_tu_annotation tu_anno on tu_anno.tu= tu.tu left outer join tu trans on trans.id = tu.tu where module=? and tu_anno.symbol in (select g1.symbol from gene_info g1 left outer join module_has_genes m on g1.symbol=m.symbol where m.module=? order by g1.graph_degree desc) group by trans.tu");
			stmt = dbConnection.prepareStatement(query.toString());
			stmt.setString(1, module);
			stmt.setString(2, module);
			rs = stmt.executeQuery();
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				value = resultSet_to_Array(rs);
				for (int i = 0; i < value.size(); i++) {
					String[] pathWayWithDescription = new String[value.get(i).size()];
					for (int j = 0; j < value.get(i).size(); j++)
						pathWayWithDescription[j] = value.get(i).get(j);
					moduleDetails.add(pathWayWithDescription);
				}
			} catch (Exception exc) {
				logger.error(exc.toString());
			} finally {
				releaseResources(rs, stmt);
			}

			// get protein complex details
			query = new StringBuffer(
					"select complex.complex, 'Protein Complex (PC)', GROUP_CONCAT(distinct(pc_anno.symbol) SEPARATOR ', ') as genes, '' as url from module_enrichment_pc pc left outer join metacyc_pc_annotation pc_anno on pc_anno.complex= pc.complex left outer join protein_complexes complex on complex.id = pc.complex where module=? and pc_anno.symbol in (select g1.symbol from gene_info g1 left outer join module_has_genes m on g1.symbol=m.symbol where m.module=? order by g1.graph_degree desc) group by complex.complex;");
			stmt = dbConnection.prepareStatement(query.toString());
			stmt.setString(1, module);
			stmt.setString(2, module);
			rs = stmt.executeQuery();
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				value = resultSet_to_Array(rs);
				for (int i = 0; i < value.size(); i++) {
					String[] pathWayWithDescription = new String[value.get(i).size()];
					for (int j = 0; j < value.get(i).size(); j++)
						pathWayWithDescription[j] = value.get(i).get(j);
					moduleDetails.add(pathWayWithDescription);
				}
			} catch (Exception exc) {
				logger.error(exc.toString());
			} finally {
				releaseResources(rs, stmt);
			}
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			releaseResources(rs, stmt);
		}
		return moduleDetails;
	}

	// ----------------------------------------------------------------------------
	// get genes of the input module
	public String[][] getModuleGenes(String module, int limit) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<ArrayList<String>> genes = new ArrayList<ArrayList<String>>();
		try {
			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"select g1.symbol, g1.description from gene_info g1 left outer join module_has_genes m on g1.symbol=m.symbol where m.module=? order by g1.graph_degree desc limit ?");
			stmt.setString(1, module);
			stmt.setInt(2, limit);
			// ---------------------------get the
			// modules-----------------------------------
			rs = stmt.executeQuery();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				genes = resultSet_to_Array(rs);
			} catch (Exception exc) {
				logger.error(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}

		String[][] returnData = null;
		if (genes.size() != 0) {
			returnData = new String[genes.size()][genes.get(0).size()];
			for (int i = 0; i < genes.size(); i++) {
				for (int j = 0; j < genes.get(0).size(); j++)
					returnData[i][j] = genes.get(i).get(j);
			}
		}
		return returnData;
	}

	// ---------------------------------------------------------------------------
	private ArrayList<ArrayList<String>> resultSet_to_Array(ResultSet resultSet) throws SQLException {
		ArrayList<ArrayList<String>> table = new ArrayList<ArrayList<String>>();
		if (resultSet == null)
			return table;

		int columnCount = resultSet.getMetaData().getColumnCount();
		if (resultSet.getType() == ResultSet.TYPE_FORWARD_ONLY) {
			table = new ArrayList<ArrayList<String>>();
		} else {
			resultSet.last();
			table = new ArrayList<ArrayList<String>>(resultSet.getRow());
			resultSet.beforeFirst();
		}
		for (ArrayList<String> row; resultSet.next(); table.add(row)) {
			row = new ArrayList<String>(columnCount);
			for (int c = 1; c <= columnCount; ++c) {
				if (resultSet.getString(c) != null)
					row.add(resultSet.getString(c).intern());
				else
					row.add("");
			}
		}
		return table;
	}

	public String[][] getConditionInfo() {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<ArrayList<String>> conditionsAndRanks = new ArrayList<ArrayList<String>>();
		try {
			stmt = dbConnection.prepareStatement(
					"select `condition`, number_replicates, strain, growth_phase, medium, description from condition_information");
			rs = stmt.executeQuery();
			try {
				// convert the data to 2d array
				conditionsAndRanks = resultSet_to_Array(rs);
			} catch (Exception exc) {
				logger.error(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			logger.error("SQLException: " + ex.getMessage());
			logger.error("SQLState: " + ex.getSQLState());
			logger.error("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		String[][] returnData = null;
		if (conditionsAndRanks.size() != 0) {
			returnData = new String[conditionsAndRanks.size()][conditionsAndRanks.get(0).size()];
			for (int i = 0; i < conditionsAndRanks.size(); i++) {
				for (int j = 0; j < conditionsAndRanks.get(i).size(); j++) {
					returnData[i][j] = conditionsAndRanks.get(i).get(j);
				}
			}
		}
		return returnData;
	}

}
