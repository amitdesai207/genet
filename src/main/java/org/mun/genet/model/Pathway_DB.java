package org.mun.genet.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.mun.genet.util.DatabaseConnection;

public class Pathway_DB {
	private Connection dbConnection;

	public Pathway_DB(String organism) {
		dbConnection = new DatabaseConnection().getDatabaseConnection(organism);
	}

	// ===========================================================================================================================
	// ============================================================================================
	// ============================================================================================
	// =============== Section 1 - Finding and getting the pathway's information
	// ==================
	// ============================================================================================
	public String[][] getPfamPathways(String value, boolean selectAll) {
		ArrayList<ArrayList<String>> pathwayList = new ArrayList<ArrayList<String>>();

		Statement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.createStatement();

			// ---------------------------get the
			// modules-----------------------------------
			if (selectAll)
				rs = stmt.executeQuery("select pfam_id, name from pfam_description");
			else
				rs = stmt.executeQuery("select pfam_id, name from pfam_description where pfam_id like '%" + value
						+ "%' or name like '%" + value + "%'");
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				pathwayList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array
		String[][] returnData;
		try {
			returnData = new String[pathwayList.size()][pathwayList.get(0).size()];
			for (int i = 0; i < pathwayList.size(); i++) {
				for (int j = 0; j < pathwayList.get(i).size(); j++) {
					if (pathwayList.get(i).get(j) == null)
						returnData[i][j] = "";
					else
						returnData[i][j] = pathwayList.get(i).get(j);
				}
			}
		} catch (IndexOutOfBoundsException ee) {
			return null;
		}
		return returnData;
	}

	public String[][] getKEGGPathways(String value, boolean selectAll) {
		ArrayList<ArrayList<String>> pathwayList = new ArrayList<ArrayList<String>>();
		Statement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.createStatement();

			// ---------------------------get the
			// modules-----------------------------------
			if (selectAll)
				rs = stmt.executeQuery("select pathway_id, description from kegg_description");
			else
				rs = stmt.executeQuery("select pathway_id, description from kegg_description where pathway_id like '%"
						+ value + "%' or description like '%" + value + "%'");
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				pathwayList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array
		// //System.out.println(">>>>pathwayListKEGG:"+pathwayList);
		String[][] returnData;
		if (pathwayList.size() == 0) {
			return null;
		}
		returnData = new String[pathwayList.size()][pathwayList.get(0).size()];
		for (int i = 0; i < pathwayList.size(); i++) {
			for (int j = 0; j < pathwayList.get(i).size(); j++) {
				if (pathwayList.get(i).get(j) == null)
					returnData[i][j] = "";
				else
					returnData[i][j] = pathwayList.get(i).get(j);
			}
		}

		return returnData;
	}

	// ===========================================================================================================================
	public String[][] getTUPathways(String value, boolean selectAll) {
		ArrayList<ArrayList<String>> pathwayList = new ArrayList<ArrayList<String>>();
		Statement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.createStatement();

			// ---------------------------get the
			// modules-----------------------------------
			if (selectAll)
				rs = stmt.executeQuery("select tu from tu");
			else
				rs = stmt.executeQuery("select tu from tu where tu like '%" + value + "%'");
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				pathwayList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array
		String[][] returnData;
		try {
			returnData = new String[pathwayList.size()][2];
			for (int i = 0; i < pathwayList.size(); i++) {

				returnData[i][0] = pathwayList.get(i).get(0);
				returnData[i][1] = "";

			}
		} catch (IndexOutOfBoundsException ee) {
			return null;
		}
		return returnData;
	}

	// ===========================================================================================================================
	public String[][] getPCPathways(String value, boolean selectAll) {
		ArrayList<ArrayList<String>> pathwayList = new ArrayList<ArrayList<String>>();
		Statement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.createStatement();

			// ---------------------------get the
			// modules-----------------------------------
			if (selectAll)
				rs = stmt.executeQuery("select complex from protein_complexes");
			else
				rs = stmt.executeQuery("select complex from protein_complexes where complex like '%" + value + "%'");
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				pathwayList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array
		String[][] returnData;
		try {
			returnData = new String[pathwayList.size()][2];
			for (int i = 0; i < pathwayList.size(); i++) {

				returnData[i][0] = pathwayList.get(i).get(0);
				returnData[i][1] = "";

			}
		} catch (IndexOutOfBoundsException ee) {
			return null;
		}
		return returnData;
	}

	// ===========================================================================================================================
	public String[][] getMetaCycPathways(String value, boolean selectAll) {
		ArrayList<ArrayList<String>> pathwayList = new ArrayList<ArrayList<String>>();
		Statement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.createStatement();

			// ---------------------------get the
			// modules-----------------------------------
			if (selectAll)
				rs = stmt.executeQuery("select pathway from metacyc_pathway");
			else
				rs = stmt.executeQuery("select pathway from metacyc_pathway where pathway like '%" + value + "%'");
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				pathwayList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array
		String[][] returnData;
		try {
			returnData = new String[pathwayList.size()][2];
			for (int i = 0; i < pathwayList.size(); i++) {

				returnData[i][0] = pathwayList.get(i).get(0);
				returnData[i][1] = "";

			}
		} catch (IndexOutOfBoundsException ee) {
			return null;
		}
		return returnData;
	}

	// ====================================================================================================================
	// ============================================================================================
	// ============================================================================================
	// ======== Section 2 - getting the module that are enriched with the given
	// pathway ===========
	// ============================================================================================
	public String[][] modulesWithTUID(String tuID) {
		ArrayList<ArrayList<String>> moduleList = new ArrayList<ArrayList<String>>();

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"select module, fdr_pvalue from module_enrichment_tu left outer join tu on tu.id = module_enrichment_tu.tu where tu.tu = ?");
			stmt.setString(1, tuID);
			rs = stmt.executeQuery();
			try {
				// convert the data to 2d array
				moduleList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array

		if (moduleList.size() == 0) {
			return null;
		}
		String[][] returnData = new String[moduleList.size()][moduleList.get(0).size()];
		for (int i = 0; i < moduleList.size(); i++) {
			returnData[i][0] = moduleList.get(i).get(0);
			double x = Double.parseDouble(moduleList.get(i).get(1));
			returnData[i][1] = Double.toString(x);

		}
		return returnData;
	}

	// ===========================================================================================================================
	public String[][] modulesWithPCID(String pcID) {
		ArrayList<ArrayList<String>> moduleList = new ArrayList<ArrayList<String>>();

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"select module, fdr_pvalue from module_enrichment_pc left outer join protein_complexes on protein_complexes.id = module_enrichment_pc.complex where protein_complexes.complex = ?");
			stmt.setString(1, pcID);
			rs = stmt.executeQuery();
			try {
				// convert the data to 2d array
				moduleList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array

		if (moduleList.size() == 0) {
			return null;
		}
		String[][] returnData = new String[moduleList.size()][moduleList.get(0).size()];
		for (int i = 0; i < moduleList.size(); i++) {
			returnData[i][0] = moduleList.get(i).get(0);
			double x = Double.parseDouble(moduleList.get(i).get(1));
			returnData[i][1] = Double.toString(x);

		}
		return returnData;
	}

	// ===========================================================================================================================
	public String[][] modulesWithMetaCycID(String metaID) {
		ArrayList<ArrayList<String>> moduleList = new ArrayList<ArrayList<String>>();

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"select metacyc.module, metacyc.fdr_pvalue from module_enrichment_metacyc metacyc left outer join metacyc_pathway pathway on metacyc.pathway = pathway.id where pathway.pathway = ?");
			stmt.setString(1, metaID);

			rs = stmt.executeQuery();
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				moduleList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array

		if (moduleList.size() == 0) {
			return null;
		}
		String[][] returnData = new String[moduleList.size()][moduleList.get(0).size()];
		for (int i = 0; i < moduleList.size(); i++) {
			returnData[i][0] = moduleList.get(i).get(0);
			double x = Double.parseDouble(moduleList.get(i).get(1));
			returnData[i][1] = Double.toString(x);

		}
		return returnData;
	}

	// ===========================================================================================================================
	public String[][] modulesWithKEGGID(String keggID) {
		ArrayList<ArrayList<String>> moduleList = new ArrayList<ArrayList<String>>();

		Statement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.createStatement();

			// ---------------------------get the
			// modules-----------------------------------
			rs = stmt.executeQuery(
					"select module, fdr_pvalue from module_enrichment_kegg where pathway_id= '" + keggID + "'");
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				moduleList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array
		if (moduleList.size() == 0) {
			return null;
		}

		String[][] returnData = new String[moduleList.size()][moduleList.get(0).size()];
		for (int i = 0; i < moduleList.size(); i++) {
			returnData[i][0] = moduleList.get(i).get(0);
			double x = Double.parseDouble(moduleList.get(i).get(1));
			returnData[i][1] = Double.toString(x);
		}
		return returnData;
	}

	// ===========================================================================================================================
	public String[][] modulesWithPfamID(String pfamID) {
		ArrayList<ArrayList<String>> moduleList = new ArrayList<ArrayList<String>>();

		Statement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.createStatement();

			// ---------------------------get the
			// modules-----------------------------------
			rs = stmt.executeQuery(
					"select module, fdr_pvalue from module_enrichment_pfam where pfam_id= '" + pfamID + "'");
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				moduleList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array
		// System.out.println("moduleList.size()!!!!:"+moduleList.size());
		if (moduleList.size() == 0) {
			return null;
		}

		String[][] returnData = new String[moduleList.size()][moduleList.get(0).size()];
		for (int i = 0; i < moduleList.size(); i++) {
			returnData[i][0] = moduleList.get(i).get(0);
			double x = Double.parseDouble(moduleList.get(i).get(1));
			returnData[i][1] = Double.toString(x);

		}
		return returnData;
	}

	// ============================================================================================
	// ============================================================================================
	// ========= Section 3 - getting the number of genes in same pathway and
	// module ===============
	// ============================================================================================
	/*
	 * we need to get the number of genes that have the same module and pathway.
	 * From design perspective, this function should be in Gene_DB. But when an
	 * instance of the gene_DB is created, a new connection to the database is
	 * constructed; therefore, to avoid that, this function is defined in
	 * pathway_DB to reduce the required connection to the database
	 */
	public String geneNOwithSameModule_PfamPathway(String module, String pfamID) {

		ArrayList<ArrayList<String>> pathwayList = new ArrayList<ArrayList<String>>();

		Statement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.createStatement();

			// ---------------------------get the
			// modules-----------------------------------
			rs = stmt.executeQuery(
					"select count(pfam_annotation.symbol), count(module_has_genes.symbol) from module_has_genes left join pfam_annotation on module_has_genes.symbol=pfam_annotation.symbol and pfam_annotation.pfam_id='"
							+ pfamID + "' where module_has_genes.module='" + module + "'");
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				pathwayList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		String returnData;
		try {
			returnData = pathwayList.get(0).get(0);
			returnData += "/" + pathwayList.get(0).get(1);
		} catch (IndexOutOfBoundsException ee) {
			return "";
		}
		return returnData;
	}

	// ================================================================================
	public String geneNOwithSameModule_KEGGPathway(String module, String keggID) {

		ArrayList<ArrayList<String>> pathwayList = new ArrayList<ArrayList<String>>();

		Statement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.createStatement();

			// ---------------------------get the
			// modules-----------------------------------
			rs = stmt.executeQuery(
					"select count(kegg_annotation.symbol), count(module_has_genes.symbol) from module_has_genes left join kegg_annotation on module_has_genes.symbol=kegg_annotation.symbol and kegg_annotation.pathway_id='"
							+ keggID + "' where module_has_genes.module='" + module + "'");
			// if so, save the result in rs
			rs = stmt.getResultSet();
			// try-catch is to be sure that there is a result!!
			try {
				// convert the data to 2d array
				pathwayList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array
		String returnData;
		try {
			returnData = pathwayList.get(0).get(0);
			returnData += "/" + pathwayList.get(0).get(1);
		} catch (IndexOutOfBoundsException ee) {
			return "";
		}
		return returnData;
	}

	// ================================================================================
	// ================================================================================
	public String geneNOwithSameModule_MetaCycPathway(String module, String metaID) {

		ArrayList<ArrayList<String>> pathwayList = new ArrayList<ArrayList<String>>();

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"select count(metacyc_pathway_annotation.symbol), count(module_has_genes.symbol) from module_has_genes left join metacyc_pathway_annotation on module_has_genes.symbol=metacyc_pathway_annotation.symbol and metacyc_pathway_annotation.pathway = (select id from metacyc_pathway where pathway = ?) where module_has_genes.module = ?");
			stmt.setString(1, metaID);
			stmt.setString(2, module);
			rs = stmt.executeQuery();
			try {
				// convert the data to 2d array
				pathwayList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array
		String returnData;
		try {
			returnData = pathwayList.get(0).get(0);
			returnData += "/" + pathwayList.get(0).get(1);
		} catch (IndexOutOfBoundsException ee) {
			return "";
		}
		return returnData;
	}

	// ================================================================================
	// ================================================================================
	public String geneNOwithSameModule_PCPathway(String module, String pcID) {

		ArrayList<ArrayList<String>> pathwayList = new ArrayList<ArrayList<String>>();

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"select count(metacyc_pc_annotation.symbol), count(module_has_genes.symbol) from module_has_genes left join metacyc_pc_annotation on module_has_genes.symbol=metacyc_pc_annotation.symbol and metacyc_pc_annotation.complex = (select id from protein_complexes where complex = ?) where module_has_genes.module = ?");
			stmt.setString(1, pcID);
			stmt.setString(2, module);
			rs = stmt.executeQuery();
			try {
				// convert the data to 2d array
				pathwayList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array
		String returnData;
		try {
			returnData = pathwayList.get(0).get(0);
			returnData += "/" + pathwayList.get(0).get(1);
		} catch (IndexOutOfBoundsException ee) {
			return "";
		}
		return returnData;
	}

	// ================================================================================
	// ================================================================================
	public String geneNOwithSameModule_TUPathway(String module, String tuID) {

		ArrayList<ArrayList<String>> pathwayList = new ArrayList<ArrayList<String>>();

		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			// create the sql statement
			stmt = dbConnection.prepareStatement(
					"select count(metacyc_tu_annotation.symbol), count(module_has_genes.symbol) from module_has_genes left join metacyc_tu_annotation on module_has_genes.symbol=metacyc_tu_annotation.symbol and metacyc_tu_annotation.tu = (select id from tu where tu = ?) where module_has_genes.module=?");
			stmt.setString(1, tuID);
			stmt.setString(2, module);
			rs = stmt.executeQuery();
			try {
				// convert the data to 2d array
				pathwayList = resultSet_to_Array(rs);
			} catch (Exception exc) {
				// System.out.println(exc.toString());
				return null;
			}

			// check whether the query is executed
		} catch (SQLException ex) {
			// handle any errors
			// System.out.println("SQLException: " + ex.getMessage());
			// System.out.println("SQLState: " + ex.getSQLState());
			// System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			// resources in a finally{} block are released
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
				} // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
				} // ignore
				stmt = null;
			}

		}
		// convert arrayList to Array
		String returnData;
		try {
			returnData = pathwayList.get(0).get(0);
			returnData += "/" + pathwayList.get(0).get(1);
		} catch (IndexOutOfBoundsException ee) {
			return "";
		}
		return returnData;
	}

	// ================================================================================
	// ================================================================================
	private ArrayList<ArrayList<String>> resultSet_to_Array(ResultSet resultSet) throws SQLException {
		ArrayList<ArrayList<String>> table = new ArrayList<ArrayList<String>>();
		if (resultSet == null)
			return table;
		int columnCount = resultSet.getMetaData().getColumnCount();

		if (resultSet.getType() == ResultSet.TYPE_FORWARD_ONLY) {
			// //System.out.println("if forward");
			table = new ArrayList<ArrayList<String>>();
		} else {
			// //System.out.println("else e forward");
			resultSet.last();
			table = new ArrayList<ArrayList<String>>(resultSet.getRow());
			resultSet.beforeFirst();
		}
		// int count=0;
		for (ArrayList<String> row; resultSet.next(); table.add(row)) {
			row = new ArrayList<String>(columnCount);
			// count++;
			for (int c = 1; c <= columnCount; ++c)
				row.add(resultSet.getString(c).intern());
		}
		// //System.out.println("count is "+count);
		return table;
	}
}
