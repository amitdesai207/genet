package org.mun.genet.model;

import org.springframework.web.multipart.MultipartFile;

public class UploadModel {

	private String email = null;
	private MultipartFile geneExpressionFile = null;
	private String clientIP = null;
	private String operatingSystem = null;
	private String browser = null;
	private boolean sendEmail = true;
	private String taxonomyId = null;
	private String pubmedId = null;
	private String correlation = null;
	private String networktype = null;
	private int threshold = 0;
	private MultipartFile geneTable = null;
	private MultipartFile conditionTable = null;
	private String keggId = null;
	private MultipartFile tuAnnotationFile = null;
	private MultipartFile pcAnnotationFile = null;
	private MultipartFile pfamAnnotationFile = null;
	private MultipartFile proteinSeqFile = null;
	private MultipartFile pathwayAnnotationFile = null;
	private boolean consent = false;
	private String organismName = null;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public boolean isSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	public MultipartFile getGeneExpressionFile() {
		return geneExpressionFile;
	}

	public void setGeneExpressionFile(MultipartFile geneExpressionFile) {
		this.geneExpressionFile = geneExpressionFile;
	}

	public String getTaxonomyId() {
		return taxonomyId;
	}

	public void setTaxonomyId(String taxonomyId) {
		this.taxonomyId = taxonomyId;
	}

	public String getPubmedId() {
		return pubmedId;
	}

	public void setPubmedId(String pubmedId) {
		this.pubmedId = pubmedId;
	}

	public String getCorrelation() {
		return correlation;
	}

	public void setCorrelation(String correlation) {
		this.correlation = correlation;
	}

	public String getNetworktype() {
		return networktype;
	}

	public void setNetworktype(String networktype) {
		this.networktype = networktype;
	}

	public int getThreshold() {
		return threshold;
	}

	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}

	public MultipartFile getGeneTable() {
		return geneTable;
	}

	public void setGeneTable(MultipartFile geneTable) {
		this.geneTable = geneTable;
	}

	public MultipartFile getConditionTable() {
		return conditionTable;
	}

	public void setConditionTable(MultipartFile conditionTable) {
		this.conditionTable = conditionTable;
	}

	public String getKeggId() {
		return keggId;
	}

	public void setKeggId(String keggId) {
		this.keggId = keggId;
	}

	public MultipartFile getTuAnnotationFile() {
		return tuAnnotationFile;
	}

	public void setTuAnnotationFile(MultipartFile tuAnnotationFile) {
		this.tuAnnotationFile = tuAnnotationFile;
	}

	public MultipartFile getPcAnnotationFile() {
		return pcAnnotationFile;
	}

	public void setPcAnnotationFile(MultipartFile pcAnnotationFile) {
		this.pcAnnotationFile = pcAnnotationFile;
	}

	public MultipartFile getPfamAnnotationFile() {
		return pfamAnnotationFile;
	}

	public void setPfamAnnotationFile(MultipartFile pfamAnnotationFile) {
		this.pfamAnnotationFile = pfamAnnotationFile;
	}

	public MultipartFile getProteinSeqFile() {
		return proteinSeqFile;
	}

	public void setProteinSeqFile(MultipartFile proteinSeqFile) {
		this.proteinSeqFile = proteinSeqFile;
	}

	public MultipartFile getPathwayAnnotationFile() {
		return pathwayAnnotationFile;
	}

	public void setPathwayAnnotationFile(MultipartFile pathwayAnnotationFile) {
		this.pathwayAnnotationFile = pathwayAnnotationFile;
	}

	public boolean isConsent() {
		return consent;
	}

	public void setConsent(boolean consent) {
		this.consent = consent;
	}

	public String getOrganismName() {
		return organismName;
	}

	public void setOrganismName(String organismName) {
		this.organismName = organismName;
	}

}