package org.mun.genet.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Utility {

	private static final Random randomGenerator = new Random();
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

	public static String generateRandomSequence(Long id) {

		String sequenceId = "";

		for (int counter = 0; counter < 3; counter++) {
			int randomNo = randomGenerator.nextInt(62);
			if (randomNo < 26)
				sequenceId = sequenceId + String.valueOf((char) (randomNo + 'a'));
			else if (randomNo > 26 && randomNo < 36)
				sequenceId = sequenceId + String.valueOf(randomNo - 26);
			else
				sequenceId = sequenceId + String.valueOf((char) ((randomNo - 36) + 'A'));
		}

		return sequenceId + id;
	}

	public static Long generateRandomNumberSequence(int id) {
		String sequenceId = "" + id;
		for (int counter = 0; counter < 3; counter++) {
			int randomNo = randomGenerator.nextInt(10);
			sequenceId = sequenceId + randomNo;
		}
		return Long.parseLong(sequenceId);
	}

	public static Date getDateFromString(String stringDate) {

		Date date = null;
		if (stringDate != null) {
			try {
				date = dateFormat.parse(stringDate);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return date;
	}

}
