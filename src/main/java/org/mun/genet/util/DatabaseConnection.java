package org.mun.genet.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DatabaseConnection {

	private Connection dbConnection = null;
	private String url = null;
	private String username = null;
	private String password = null;

	public Connection getDatabaseConnection(String organism) {
		fetchDatabaseDetails(organism);
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			dbConnection = DriverManager.getConnection(url + "?user=" + username + "&password=" + password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbConnection;
	}

	private void fetchDatabaseDetails(String organism) {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL resource = classLoader.getResource("db.properties");
			input = new FileInputStream(resource.getFile());
			// load a properties file
			prop.load(input);
			url = prop.getProperty("db.url").replace("gene_users",
					organism.trim().toLowerCase().replaceAll("[^A-Za-z0-9 ]", "").replace(" ", "_"));
			username = prop.getProperty("db.username");
			password = prop.getProperty("db.password");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
