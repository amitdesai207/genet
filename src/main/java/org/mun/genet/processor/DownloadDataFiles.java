package org.mun.genet.processor;

public enum DownloadDataFiles {

	ORGANISM("ORGANISM"), CONDITION_INFORMATION("CONDITION_INFORMATION"), MODULE_SIGNIFICANCE("MODULE_SIGNIFICANCE"), GENE_PFAM_DESC(
			"GENE_PFAM_DESCRIPTION"), GENE_PFAM("GENE_PFAM"), GENE_KEGG_DESC("GENE_KEGG_DESCRIPTION"), GENE_KEGG("GENE_KEGG"), GENE_METACYC(
			"GENE_METACYC"), GENE_PROTEIN("GENE_PROTEIN"), GENE_TU("GENE_TRANS_UNIT"), MODULE_ENRICHMENT("MODULE_ENRICHMENT"), GENE_INFO(
			"GENE_INFO"), GENE_EXPRESSION("GENE_EXPRESSION"), GENE_COEXPRESSION_INTERACTION("GENE_COEXPRESSION_INTERACTION");

	private String type;

	private DownloadDataFiles(String type) {
		this.type = type;
	}

	public static DownloadDataFiles getDownloadDataFiles(String input) {
		for (DownloadDataFiles myType : DownloadDataFiles.values()) {
			if (myType.type.equalsIgnoreCase(input)) {
				return myType;
			}
		}
		return null;
	}

	public boolean isEqual(Object type) {
		if (type == null) {
			return false;
		}
		if (!(type instanceof String)) {
			return false;
		}
		if (this.type.equalsIgnoreCase((String) type)) {
			return true;
		}

		return false;
	}

	public String toString() {
		return type;
	}

}
