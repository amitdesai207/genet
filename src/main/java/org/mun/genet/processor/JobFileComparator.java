package org.mun.genet.processor;

import java.io.File;
import java.util.Comparator;

public class JobFileComparator implements Comparator<File> {

	@Override
	public int compare(File file1, File file2) {
		if (file1 == null && file2 == null)
			return 0;
		else if (file1 == null || ImportDataFiles.getImportDataFiles(file1.getName().replace(".csv", "").toUpperCase()) == null)
			return 1;
		else if (file2 == null || ImportDataFiles.getImportDataFiles(file2.getName().replace(".csv", "").toUpperCase()) == null)
			return -1;
		else
			return ImportDataFiles.getImportDataFiles(file1.getName().replace(".csv", "").toUpperCase()).compareTo(
					ImportDataFiles.getImportDataFiles(file2.getName().replace(".csv", "").toUpperCase()));
	}
}
