package org.mun.genet.processor;

public enum ImportDataFiles {

	ORGANISM("ORGANISM"), CONDITION_INFORMATION("CONDITION_INFORMATION"), MODULE_SIGNIFICANCE(
			"MODULE_SIGNIFICANCE"), GENE_INFO("GENE_INFO"), GENE_EXPRESSION("GENE_EXPRESSION"), GENE_PFAM(
					"GENE_PFAM"), GENE_KEGG("GENE_KEGG"), CORRELATION_MATRIX("CORRELATION_MATRIX"), WEIGHTED_MATRIX(
							"WEIGHTED_MATRIX"), GENE_PFAM_DESC("GENE_PFAM_DESC"), GENE_KEGG_DESC(
									"GENE_KEGG_DESC"), GENE_METACYC("GENE_METACYC"), GENE_PROTEIN(
											"GENE_PROTEIN"), GENE_TU("GENE_TU"), MODULE_ENRICHMENT("MODULE_ENRICHMENT");

	private String type;

	private ImportDataFiles(String type) {
		this.type = type;
	}

	public static ImportDataFiles getImportDataFiles(String input) {
		for (ImportDataFiles myType : ImportDataFiles.values()) {
			if (myType.type.equalsIgnoreCase(input)) {
				return myType;
			}
		}
		return null;
	}

	public boolean isEqual(Object type) {
		if (type == null) {
			return false;
		}
		if (!(type instanceof String)) {
			return false;
		}
		if (this.type.equalsIgnoreCase((String) type)) {
			return true;
		}

		return false;
	}

	public String toString() {
		return type;
	}

}
