package org.mun.genet.processor.Adapters;

import org.mun.genet.processor.ParseException;
import org.mun.genet.vo.ImportContext;
import org.mun.genet.vo.NotificationVo;
import org.mun.genet.vo.SdkExceptionVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.CannotCreateTransactionException;

public abstract class BaseAdapter {

	protected static final Logger logger = LoggerFactory.getLogger(BaseAdapter.class);
	protected String cvsSplitBy = "\t";
	protected static String CSV_FILE_EXTENSION = ".csv";
	protected static String CYTO_FILE_EXTENSION = ".sif";

	protected void saveException(ParseException ex, ImportContext context) {
		try {
			SdkExceptionVo sdkVo = new SdkExceptionVo();
			sdkVo.setEntity(ex.getEntityName());
			sdkVo.setCauseMessage(ex.getErrorCause());
			sdkVo.setErrDetail(ex.getMessage());
			sdkVo.setJobId(context.getJob().getId());
			sdkVo.setCustomMessage("Unable to parse " + ex.getEntityName() + " at line no: " + ex.getLineNo());
			sdkVo.setLineNo(ex.getLineNo());
			context.getSdkExceptionService().createSdkException(sdkVo);
		} catch (Exception e) {
			logger.error("Exception while logging exception.", e);
			if (e instanceof CannotCreateTransactionException) {
				NotificationVo mail = context.getEmailService()
						.createDownloadLogMail(context.getConfiguration().getConfigurationByName("mail.mailccid"));
				try {
					context.getEmailUtility().sendMail(mail, null);
				} catch (Exception ex1) {
					logger.debug("Error while sending email: " + ex1.getMessage());
				}
			}
		}
	}
}
