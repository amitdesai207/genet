package org.mun.genet.processor.Adapters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.jobreceiver.EmptyException;
import org.mun.genet.model.JobStatistics;
import org.mun.genet.processor.ImportDataFiles;
import org.mun.genet.processor.ParseException;
import org.mun.genet.vo.ImportContext;

public class ParseModule extends BaseAdapter implements CSVAdapter {

	private JobStatistics jobStat = null;

	public void parseModule(File file, ImportContext context, String entityName) {
		DataSource dataSource = context.getDataSource();
		BufferedReader reader = null;
		String line = "";
		jobStat = new JobStatistics();
		int recordReceived = 0;
		int recordSuccessful = 0;
		int rowCounter = 1;
		Connection dbConnection = null;
		try {
			dbConnection = dataSource.getConnection();
			reader = new BufferedReader(new FileReader(file + File.separator + entityName + CSV_FILE_EXTENSION));
			String selectTuInfo = "SELECT module FROM module_info WHERE module = ?";
			PreparedStatement statementToSelectTU = dbConnection.prepareStatement(selectTuInfo);
			String insertTuInfo = "INSERT INTO module_info (module, hub_symbol) VALUES (?,?)";
			PreparedStatement statementToInsertTU = dbConnection.prepareStatement(insertTuInfo);
			String insertTableSQL = "INSERT INTO module_significance (module, `condition`, significance, condition_information_condition, module_info_module) VALUES (?,?,?,?,?)";
			PreparedStatement preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			List<String> columnNames = new ArrayList<String>();
			while ((line = reader.readLine()) != null) {
				String[] pfamDesc = line.trim().replace("\"", "").split(cvsSplitBy);
				for (int i = 0; i < pfamDesc.length; i++) {
					pfamDesc[i] = pfamDesc[i].replace("\"", "");
				}
				if (rowCounter == 1) {
					for (String column : pfamDesc) {
						if(!column.trim().isEmpty())
							columnNames.add(column);
					}
				} else {
					recordReceived++;
					statementToSelectTU.setString(1, pfamDesc[0]);
					if (!statementToSelectTU.executeQuery().next()) {
						statementToInsertTU.setString(1, pfamDesc[0]);
						statementToInsertTU.setString(2, pfamDesc[1]);
						try {
							statementToInsertTU.executeUpdate();
						} catch (Exception e) {
							saveException(new ParseException(e, entityName, rowCounter), context);
						}
					}
					if (columnNames.size() != pfamDesc.length-2) {
						throw new ParseException("No of columns in row not equal to no of column headers",
								entityName, rowCounter);
					}
					for (int i = 0; i < columnNames.size(); i++) {
						preparedStatement.setString(1, pfamDesc[0]);
						preparedStatement.setString(2, columnNames.get(i));
						preparedStatement.setString(3, pfamDesc[i + 2]);
						preparedStatement.setString(4, columnNames.get(i));
						preparedStatement.setString(5, pfamDesc[0]);
						try {
							preparedStatement.executeUpdate();
						} catch (Exception e) {
							saveException(
									new ParseException(e, "Cannot find condition information: " + columnNames.get(i)
											+ " in condition_information.csv", entityName, rowCounter), context);
							throw new EmptyException(e, entityName, rowCounter);
						}
					}
					logger.info("Upload complete. Line no: " + rowCounter + " for entity: " + entityName);
					recordSuccessful++;
				}
				rowCounter++;
			}
			statementToInsertTU.close();
			statementToSelectTU.close();
			preparedStatement.close();
		} catch (Exception e) {
			throw new AdapterException(e, entityName, rowCounter);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Error: " + e);
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
			jobStat.setRecordsReceivedCount(recordReceived);
			jobStat.setRecordsSuccessfulCount(recordSuccessful);
			context.getStatistics().put(ImportDataFiles.getImportDataFiles(entityName), jobStat);
		}
	}

	@Override
	public void process(File file, ImportContext context, String entityName) {
		parseModule(file, context, entityName);
	}

}
