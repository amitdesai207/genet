package org.mun.genet.processor.Adapters;

import java.io.File;

import org.mun.genet.vo.ImportContext;

public interface CSVAdapter {

	public void process(File file, ImportContext context, String entityName);

}
