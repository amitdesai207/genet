package org.mun.genet.processor.Adapters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.jobreceiver.EmptyException;
import org.mun.genet.model.JobStatistics;
import org.mun.genet.processor.ImportDataFiles;
import org.mun.genet.processor.ParseException;
import org.mun.genet.vo.ImportContext;

public class ParseModuleEnrichment extends BaseAdapter implements CSVAdapter {

	private JobStatistics jobStat = null;

	public void parsePFAMDescAndInsertRecord(File file, ImportContext context, String entityName) {
		DataSource dataSource = context.getDataSource();
		BufferedReader reader = null;
		String line = "";
		jobStat = new JobStatistics();
		int recordReceived = 0;
		int recordSuccessful = 0;
		int rowCounter = 1;
		Connection dbConnection = null;
		try {
			dbConnection = dataSource.getConnection();
			reader = new BufferedReader(new FileReader(file + File.separator + entityName + CSV_FILE_EXTENSION));
			String getPathwayId = "SELECT id FROM metacyc_pathway WHERE pathway = ?";
			PreparedStatement selectPathwayId = dbConnection.prepareStatement(getPathwayId);
			String getProteinId = "SELECT id FROM protein_complexes WHERE complex = ?";
			PreparedStatement selectProteinId = dbConnection.prepareStatement(getProteinId);
			String getTUId = "SELECT id FROM tu WHERE tu = ?";
			PreparedStatement selectTUId = dbConnection.prepareStatement(getTUId);
			String insertPfamInfo = "INSERT INTO module_enrichment_pfam (module, pfam_id, pfam_description_pfam_id, module_info_module, fdr_pvalue, pvalue) VALUES (?,?,?,?,?,?)";
			String insertKeggInfo = "INSERT INTO module_enrichment_kegg (module, pathway_id, fdr_pvalue, pvalue, kegg_description_pathway_id, module_info_module) VALUES (?,?,?,?,?,?)";
			String insertPathwayInfo = "INSERT INTO module_enrichment_metacyc (module, pathway, fdr_pvalue, p_value, module_info_module) VALUES (?,?,?,?,?)";
			String insertProteinInfo = "INSERT INTO module_enrichment_pc (module, complex, fdr_pvalue, pvalue, module_info_module) VALUES (?,?,?,?,?)";
			String insertTuInfo = "INSERT INTO module_enrichment_tu (module, tu, fdr_pvalue, pvalue, module_info_module) VALUES (?,?,?,?,?)";
			PreparedStatement statementToInsertPfam = dbConnection.prepareStatement(insertPfamInfo);
			PreparedStatement statementToInsertKegg = dbConnection.prepareStatement(insertKeggInfo);
			PreparedStatement statementToInsertPathway = dbConnection.prepareStatement(insertPathwayInfo);
			PreparedStatement statementToInsertProtein = dbConnection.prepareStatement(insertProteinInfo);
			PreparedStatement statementToInsertTU = dbConnection.prepareStatement(insertTuInfo);
			ResultSet rs = null;
			while ((line = reader.readLine()) != null) {
				if (rowCounter == 1) {
					rowCounter++;
					continue;
				}
				recordReceived++;
				String[] pfamDesc = line.trim().replace("\"", "").split(cvsSplitBy);
				for (int i = 0; i < pfamDesc.length; i++) {
					pfamDesc[i] = pfamDesc[i].replace("\"", "");
				}
				try {
					switch (pfamDesc[2].toLowerCase()) {
					case "pfam":
						statementToInsertPfam.setString(1, pfamDesc[0]);
						statementToInsertPfam.setString(2, pfamDesc[1]);
						statementToInsertPfam.setString(3, pfamDesc[1]);
						statementToInsertPfam.setString(4, pfamDesc[0]);
						if (pfamDesc[8].equalsIgnoreCase("na"))
							statementToInsertPfam.setString(5, null);
						else
							statementToInsertPfam.setFloat(5, Float.parseFloat(pfamDesc[8]));
						if (pfamDesc[7].equalsIgnoreCase("na"))
							statementToInsertPfam.setString(6, null);
						else
							statementToInsertPfam.setFloat(6, Float.parseFloat(pfamDesc[7]));
						try {
							statementToInsertPfam.executeUpdate();
						} catch (Exception e) {
							saveException(new ParseException(e,
									"Pfam id " + pfamDesc[1] + "is not present in gene_pfam_desc.csv", entityName,
									rowCounter), context);
						}
						break;
					case "pathwayskegg":
						statementToInsertKegg.setString(1, pfamDesc[0]);
						statementToInsertKegg.setString(2, pfamDesc[1]);
						if (pfamDesc[8].equalsIgnoreCase("na"))
							statementToInsertKegg.setString(3, null);
						else
							statementToInsertKegg.setFloat(3, Float.parseFloat(pfamDesc[8]));
						if (pfamDesc[7].equalsIgnoreCase("na"))
							statementToInsertKegg.setString(4, null);
						else
							statementToInsertKegg.setFloat(4, Float.parseFloat(pfamDesc[7]));
						statementToInsertKegg.setString(5, pfamDesc[1]);
						statementToInsertKegg.setString(6, pfamDesc[0]);
						try {
							statementToInsertKegg.executeUpdate();
						} catch (Exception e) {
							saveException(new ParseException(e,
									"Pathway id " + pfamDesc[1] + "is not present in gene_kegg_desc.csv", entityName,
									rowCounter), context);
						}
						break;
					case "pathwaysmetacyc":
						selectPathwayId.setString(1, pfamDesc[1]);
						rs = selectPathwayId.executeQuery();
						if (rs.next()) {
							int id = rs.getInt(1);
							statementToInsertPathway.setString(1, pfamDesc[0]);
							statementToInsertPathway.setInt(2, id);
							if (pfamDesc[8].equalsIgnoreCase("na"))
								statementToInsertPathway.setString(3, null);
							else
								statementToInsertPathway.setFloat(3, Float.parseFloat(pfamDesc[8]));
							if (pfamDesc[7].equalsIgnoreCase("na"))
								statementToInsertPathway.setString(4, null);
							else
								statementToInsertPathway.setFloat(4, Float.parseFloat(pfamDesc[7]));
							statementToInsertPathway.setString(5, pfamDesc[0]);
							try {
								statementToInsertPathway.executeUpdate();
							} catch (Exception e) {
								saveException(new ParseException(e,
										"Pathway " + pfamDesc[1] + "is not present in gene_metacyc.csv", entityName,
										rowCounter), context);
							}
						}
						break;
					case "proteincomplex":
						selectProteinId.setString(1, pfamDesc[1]);
						rs = selectProteinId.executeQuery();
						if (rs.next()) {
							int id = rs.getInt(1);
							statementToInsertProtein.setString(1, pfamDesc[0]);
							statementToInsertProtein.setInt(2, id);
							if (pfamDesc[8].equalsIgnoreCase("na"))
								statementToInsertProtein.setString(3, null);
							else
								statementToInsertProtein.setFloat(3, Float.parseFloat(pfamDesc[8]));
							if (pfamDesc[7].equalsIgnoreCase("na"))
								statementToInsertProtein.setString(4, null);
							else
								statementToInsertProtein.setFloat(4, Float.parseFloat(pfamDesc[7]));
							statementToInsertProtein.setString(5, pfamDesc[0]);
							try {
								statementToInsertProtein.executeUpdate();
							} catch (Exception e) {
								saveException(new ParseException(e,
										"Protein complex " + pfamDesc[1] + "is not present in gene_protein.csv",
										entityName, rowCounter), context);
							}
						}
						break;
					case "tu":
						selectTUId.setString(1, pfamDesc[1]);
						rs = selectTUId.executeQuery();
						if (rs.next()) {
							int id = rs.getInt(1);
							statementToInsertTU.setString(1, pfamDesc[0]);
							statementToInsertTU.setInt(2, id);
							if (pfamDesc[8].equalsIgnoreCase("na"))
								statementToInsertTU.setString(3, null);
							else
								statementToInsertTU.setFloat(3, Float.parseFloat(pfamDesc[8]));
							if (pfamDesc[7].equalsIgnoreCase("na"))
								statementToInsertTU.setString(4, null);
							else
								statementToInsertTU.setFloat(4, Float.parseFloat(pfamDesc[7]));
							statementToInsertTU.setString(5, pfamDesc[0]);
							try {
								statementToInsertTU.executeUpdate();
							} catch (Exception e) {
								saveException(new ParseException(e,
										"Transition Unit " + pfamDesc[1] + "is not present in gene_tu.csv", entityName,
										rowCounter), context);
							}
						}
						break;
					}
					logger.info("Upload complete. Line no: " + rowCounter + " for entity: " + entityName);
					recordSuccessful++;
				} catch (Exception e) {
					saveException(new ParseException(e, entityName, rowCounter), context);
					throw new EmptyException(e, entityName, rowCounter);
				}
				rowCounter++;
			}
			statementToInsertKegg.close();
			statementToInsertPathway.close();
			statementToInsertPfam.close();
			statementToInsertProtein.close();
			statementToInsertTU.close();
		} catch (Exception e) {
			throw new AdapterException(e, entityName, rowCounter);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Error: " + e);
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
			jobStat.setRecordsReceivedCount(recordReceived);
			jobStat.setRecordsSuccessfulCount(recordSuccessful);
			context.getStatistics().put(ImportDataFiles.getImportDataFiles(entityName), jobStat);
		}
	}

	@Override
	public void process(File file, ImportContext context, String entityName) {
		parsePFAMDescAndInsertRecord(file, context, entityName);
	}

}
