package org.mun.genet.processor.Adapters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.jobreceiver.EmptyException;
import org.mun.genet.model.JobStatistics;
import org.mun.genet.processor.ImportDataFiles;
import org.mun.genet.processor.ParseException;
import org.mun.genet.vo.ImportContext;

public class ParseGeneExpression extends BaseAdapter implements CSVAdapter {

	private JobStatistics jobStat = null;

	public void parsePFAMDescAndInsertRecord(File file, ImportContext context, String entityName) {
		DataSource dataSource = context.getDataSource();
		BufferedReader reader = null;
		String line = "";
		jobStat = new JobStatistics();
		int recordReceived = 0;
		int recordSuccessful = 0;
		int rowCounter = 1;
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			reader = new BufferedReader(new FileReader(file + File.separator + entityName + CSV_FILE_EXTENSION));
			String selectTuInfo = "SELECT entrez_id FROM gene_info WHERE symbol = ?";
			PreparedStatement statementToSelectTU = connection.prepareStatement(selectTuInfo);
			String insertTuInfo = "INSERT INTO gene_expression (symbol, `condition`, log2_ratio, gene_info_entrez_id) VALUES (?,?,?,?)";
			PreparedStatement statementToInsertTU = connection.prepareStatement(insertTuInfo);
			List<String> conditions = new ArrayList<String>();
			while ((line = reader.readLine()) != null) {
				String[] pfamDesc = line.trim().replaceAll("\"", "").split(cvsSplitBy);
				if (rowCounter == 1) {
					for (String column : pfamDesc) {
						if (!column.trim().isEmpty())
							conditions.add(column);
					}
				} else if (pfamDesc.length != 0) {
					statementToSelectTU.setString(1, pfamDesc[0]);
					ResultSet resultSet = statementToSelectTU.executeQuery();
					Long entrez_id = null;
					while (resultSet.next()) {
						entrez_id = resultSet.getLong(1);
					}
					if (entrez_id != null) {
						if (!(conditions.size() == pfamDesc.length - 1 || conditions.size() == pfamDesc.length)) {
							throw new ParseException("No of columns in row not equal to no of column headers",
									entityName, rowCounter);
						}
						int conditionCounter = 0;
						if (conditions.size() == pfamDesc.length)
							conditionCounter++;
						for (int i = 0; i < conditions.size(); i++, conditionCounter++) {
							statementToInsertTU.setString(1, pfamDesc[0]);
							statementToInsertTU.setString(2, conditions.get(conditionCounter));
							if (pfamDesc.length > i + 1)
								if (pfamDesc[i + 1].equalsIgnoreCase("na"))
									statementToInsertTU.setString(3, null);
								else
									statementToInsertTU.setFloat(3, Float.parseFloat(pfamDesc[i + 1]));
							else
								break;
							statementToInsertTU.setLong(4, entrez_id);
							recordReceived++;
							try {
								statementToInsertTU.executeUpdate();
								recordSuccessful++;
							} catch (SQLException ex) {
								saveException(new ParseException(ex,
										"Condition " + conditions.get(conditionCounter)
												+ "not present in condition_information.csv",
										entityName, rowCounter), context);
								throw new EmptyException(ex, entityName, rowCounter);
							}
						}
					} else
						saveException(new ParseException("Symbol " + pfamDesc[0] + "not present in gene_info",
								entityName, rowCounter), context);
					logger.info("Upload complete. Line no: " + rowCounter + " for entity: " + entityName);
				}
				rowCounter++;
			}
			statementToInsertTU.close();
			statementToSelectTU.close();
		} catch (Exception e) {
			throw new AdapterException(e, entityName, rowCounter);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Error: " + e);
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
			jobStat.setRecordsReceivedCount(recordReceived);
			jobStat.setRecordsSuccessfulCount(recordSuccessful);
			context.getStatistics().put(ImportDataFiles.getImportDataFiles(entityName), jobStat);
		}
	}

	@Override
	public void process(File file, ImportContext context, String entityName) {
		parsePFAMDescAndInsertRecord(file, context, entityName);
	}

}
