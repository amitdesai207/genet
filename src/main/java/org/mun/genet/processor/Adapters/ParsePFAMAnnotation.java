package org.mun.genet.processor.Adapters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.jobreceiver.EmptyException;
import org.mun.genet.model.JobStatistics;
import org.mun.genet.processor.ImportDataFiles;
import org.mun.genet.processor.ParseException;
import org.mun.genet.vo.ImportContext;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class ParsePFAMAnnotation extends BaseAdapter implements CSVAdapter {

	private JobStatistics jobStat = null;

	public void parsePFAMDescAndInsertRecord(File file, ImportContext context, String entityName) {
		DataSource dataSource = context.getDataSource();
		BufferedReader reader = null;
		String line = "";
		jobStat = new JobStatistics();
		int recordReceived = 0;
		int recordSuccessful = 0;
		int rowCounter = 1;
		Connection dbConnection = null;
		try {
			dbConnection = dataSource.getConnection();
			reader = new BufferedReader(new FileReader(file + File.separator + entityName + CSV_FILE_EXTENSION));
			String selectSystematicSQL = "SELECT symbol from gene_info where systematic_id = ?";
			String selectEntrezSQL = "SELECT symbol from gene_info where entrez_id1 = ?";
			String insertTableSQL = "INSERT INTO pfam_annotation (symbol, pfam_id, pfam_description_pfam_id) VALUES (?,?,?)";
			PreparedStatement preparedStatementSysId = dbConnection.prepareStatement(selectSystematicSQL);
			PreparedStatement preparedStatementEntId = dbConnection.prepareStatement(selectEntrezSQL);
			PreparedStatement preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			while ((line = reader.readLine()) != null) {
				if (rowCounter == 1) {
					rowCounter++;
					continue;
				}
				recordReceived++;
				String[] pfamDesc = line.trim().replace("\"", "").split(cvsSplitBy);
				String symbol = pfamDesc[0];
				preparedStatementSysId.setString(1, symbol);
				ResultSet sysIdResult = preparedStatementSysId.executeQuery();
				if (sysIdResult.next()) {
					symbol = sysIdResult.getString(1);
				} else {
					preparedStatementEntId.setString(1, symbol);
					ResultSet entIdResult = preparedStatementSysId.executeQuery();
					if (entIdResult.next()) {
						symbol = entIdResult.getString(1);
					}
				}
				preparedStatement.setString(1, symbol);
				preparedStatement.setString(2, pfamDesc[1]);
				preparedStatement.setString(3, pfamDesc[1]);
				try {
					preparedStatement.executeUpdate();
					logger.info("Upload complete. Line no: " + rowCounter + " for entity: " + entityName);
					recordSuccessful++;
				} catch (Exception e) {
					if (!(e instanceof MySQLIntegrityConstraintViolationException)) {
						saveException(new ParseException(e, "pfam id: " + pfamDesc[1] + " not present in gene_pfam_desc.csv", entityName, rowCounter), context);
						throw new EmptyException(e, entityName, rowCounter);
					}
				}
				rowCounter++;
			}
			preparedStatement.close();
		} catch (Exception e) {
			throw new AdapterException(e, entityName, rowCounter);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Error: " + e);
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
			jobStat.setRecordsReceivedCount(recordReceived);
			jobStat.setRecordsSuccessfulCount(recordSuccessful);
			context.getStatistics().put(ImportDataFiles.getImportDataFiles(entityName), jobStat);
		}
	}

	@Override
	public void process(File file, ImportContext context, String entityName) {
		parsePFAMDescAndInsertRecord(file, context, entityName);
	}

}
