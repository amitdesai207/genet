package org.mun.genet.processor.Adapters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.jobreceiver.EmptyException;
import org.mun.genet.model.JobStatistics;
import org.mun.genet.processor.ImportDataFiles;
import org.mun.genet.processor.ParseException;
import org.mun.genet.vo.ImportContext;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class ParseTransUnit extends BaseAdapter implements CSVAdapter {

	private JobStatistics jobStat = null;

	public void parsePFAMDescAndInsertRecord(File file, ImportContext context, String entityName) {
		DataSource dataSource = context.getDataSource();
		BufferedReader reader = null;
		String line = "";
		jobStat = new JobStatistics();
		int recordReceived = 0;
		int recordSuccessful = 0;
		int rowCounter = 1;
		Connection dbConnection = null;
		try {
			dbConnection = dataSource.getConnection();
			reader = new BufferedReader(new FileReader(file + File.separator + entityName + CSV_FILE_EXTENSION));
			String selectInsertId = "SELECT LAST_INSERT_ID()";
			PreparedStatement statementToSelectInsertId = dbConnection.prepareStatement(selectInsertId);
			String selectTuInfo = "SELECT id FROM tu WHERE tu = ?";
			PreparedStatement statementToSelectTU = dbConnection.prepareStatement(selectTuInfo);
			String insertTuInfo = "INSERT INTO tu (tu) VALUES (?)";
			PreparedStatement statementToInsertTU = dbConnection.prepareStatement(insertTuInfo);
			String insertTableSQL = "INSERT INTO metacyc_tu_annotation (symbol, tu) VALUES (?,?)";
			PreparedStatement preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			while ((line = reader.readLine()) != null) {
				if (rowCounter == 1) {
					rowCounter++;
					continue;
				}
				if (line.startsWith("#"))
					continue;
				String[] pfamDesc = line.trim().replace("\"", "").split(cvsSplitBy);
				for (int i = 0; i < pfamDesc.length; i++) {
					pfamDesc[i] = pfamDesc[i].replace("\"", "");
				}
				recordReceived++;
				try {
					int id = 0;
					statementToSelectTU.setString(1, pfamDesc[1]);
					ResultSet result = statementToSelectTU.executeQuery();
					if (!result.next()) {
						statementToInsertTU.setString(1, pfamDesc[1]);
						statementToInsertTU.executeUpdate();
						ResultSet result1 = statementToSelectInsertId.executeQuery();
						if (result1.next())
							id = result1.getInt(1);
					} else {
						id = result.getInt(1);
					}
					if (id != 0) {
						preparedStatement.setString(1, pfamDesc[0]);
						preparedStatement.setInt(2, id);
						preparedStatement.executeUpdate();
					}
					logger.info("Upload complete. Line no: " + rowCounter + " for entity: " + entityName);
					recordSuccessful++;
				} catch (Exception e) {
					if (!(e instanceof MySQLIntegrityConstraintViolationException)) {
						saveException(new ParseException(e, entityName, rowCounter), context);
						throw new EmptyException(e, entityName, rowCounter);
					}
				}
				rowCounter++;
			}
			statementToInsertTU.close();
			statementToSelectTU.close();
			preparedStatement.close();
		} catch (Exception e) {
			throw new AdapterException(e, entityName, rowCounter);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Error: " + e);
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
			jobStat.setRecordsReceivedCount(recordReceived);
			jobStat.setRecordsSuccessfulCount(recordSuccessful);
			context.getStatistics().put(ImportDataFiles.getImportDataFiles(entityName), jobStat);
		}
	}

	@Override
	public void process(File file, ImportContext context, String entityName) {
		parsePFAMDescAndInsertRecord(file, context, entityName);
	}

}
