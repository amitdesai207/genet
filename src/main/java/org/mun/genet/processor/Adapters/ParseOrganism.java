package org.mun.genet.processor.Adapters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.mun.genet.configurer.ImportJobStatusType;
import org.mun.genet.daos.JobCleanupService;
import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.vo.ImportContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ParseOrganism extends BaseAdapter {

	@Autowired
	private DataSource dataSource;
	@Autowired
	private JobCleanupService jobCleanupService;
	private static final Logger logger = LoggerFactory.getLogger(ParseOrganism.class);

	public DataSource process(File file, ImportContext context) throws AdapterException {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			String databaseName = null;

			databaseName = context.getJob().getOrganism();
			PreparedStatement preparedStatement = connection.prepareStatement("Select * from Organism where name=?");
			preparedStatement.setString(1, databaseName);
			context.setOrganism(databaseName);
			ResultSet results = preparedStatement.executeQuery();
			if (results.next() != false) {
				if (context.getJob().isOverrideExistingDatabase()) {
					jobCleanupService.cleanupDatabase(databaseName);
				} else {
					throw new AdapterException(
							new Exception(
									"Organism data already present in our database. Connot complete with the upload"),
							"Organism", 1, ImportJobStatusType.FAILED_DUPLICATE_ORGANISM);
				}
			}
			databaseName = databaseName.trim().toLowerCase().replaceAll("[^A-Za-z0-9 ]", "").replace(" ", "_");
			String createDatabase = "create database " + databaseName;
			preparedStatement = connection
					.prepareStatement("insert into Organism (name, pubmedIds, approved, version, file_id) values('"
							+ context.getJob().getOrganism() + "','" + context.getJob().getPubmedId() + "', false, 1, "
							+ context.getJob().getDataPath() + ")");
			preparedStatement.execute();
			preparedStatement = connection.prepareStatement(createDatabase);
			preparedStatement.execute();

			BasicDataSource newDataSource = new BasicDataSource();
			newDataSource.setDriverClassName(((BasicDataSource) dataSource).getDriverClassName());
			newDataSource.setUsername(((BasicDataSource) dataSource).getUsername());
			newDataSource.setPassword(((BasicDataSource) dataSource).getPassword());
			newDataSource.setUrl(((BasicDataSource) dataSource).getUrl().replace("gene_users", databaseName));
			createTablesInNewDatabase(newDataSource);
			connection.close();
			return newDataSource;
		} catch (Exception ex) {
			try {
				connection.close();
			} catch (SQLException e) {
			}
			if (ex instanceof AdapterException) {
				throw (AdapterException) ex;
			}
			logger.error("Error while uploading organism" + ex);
			throw new AdapterException(ex, "Organism", 1);
		}

	};

	private void createTablesInNewDatabase(DataSource newDataSource) throws Exception {
		List<String> datascripts = fetchDatabaseCreationScripts();
		Connection connection = null;
		try {
			connection = newDataSource.getConnection();
			for (String dataTableScript : datascripts) {
				PreparedStatement preparedStatement = connection.prepareStatement(dataTableScript);
				preparedStatement.execute();
			}
		} catch (Exception ex) {
			throw new AdapterException(ex, "Organism", 1);
		} finally {
			if (connection != null)
				connection.close();
		}

	}

	private List<String> fetchDatabaseCreationScripts() throws Exception {
		InputStream input = null;
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL resource = classLoader.getResource("DbScripts.sql");
			BufferedReader reader = new BufferedReader(new FileReader(resource.getFile()));
			String line = null;
			List<String> dataScripts = new ArrayList<>();
			while ((line = reader.readLine()) != null) {
				if (!line.trim().isEmpty())
					dataScripts.add(line.trim());
			}
			reader.close();
			return dataScripts;
		} catch (IOException ex) {
			logger.error("Unable to fetch database creation scripts" + ex);
			throw new AdapterException(new Exception("Unable to fetch database creation scripts"), "Organism", 1);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					logger.error("Unable to fetch database creation scripts" + e);
				}
			}
		}

	}

}
