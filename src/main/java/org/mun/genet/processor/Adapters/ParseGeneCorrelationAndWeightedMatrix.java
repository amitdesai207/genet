package org.mun.genet.processor.Adapters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.jobreceiver.EmptyException;
import org.mun.genet.model.JobStatistics;
import org.mun.genet.processor.ImportDataFiles;
import org.mun.genet.processor.ParseException;
import org.mun.genet.vo.ImportContext;

public class ParseGeneCorrelationAndWeightedMatrix extends BaseAdapter implements CSVAdapter {

	@SuppressWarnings("resource")
	public void parsePFAMDescAndInsertRecord(final File folder, final ImportContext context, String entityName) {
		DataSource dataSource = context.getDataSource();
		BufferedReader reader = null;
		BufferedReader readerWt = null;
		String line = "";
		String lineWt = "";
		JobStatistics jobStat = new JobStatistics();
		int recordReceived = 0;
		int recordSuccessful = 0;
		int rowCounter = 1;
		Connection connection = null;
		PrintWriter fileWriter = null;
		PrintWriter fileWriterWt = null;
		try {
			connection = dataSource.getConnection();
			File file = new File(folder + File.separator + entityName + CSV_FILE_EXTENSION);
			fileWriter = new PrintWriter(file.getAbsolutePath().replace(CSV_FILE_EXTENSION, CYTO_FILE_EXTENSION));
			reader = new BufferedReader(new FileReader(file));
			String wtFileName = file.getAbsolutePath().replace(entityName, ImportDataFiles.WEIGHTED_MATRIX.toString());
			fileWriterWt = new PrintWriter(wtFileName.replace(CSV_FILE_EXTENSION, CYTO_FILE_EXTENSION));
			readerWt = new BufferedReader(new FileReader(wtFileName));

			String selectTuInfo = "SELECT entrez_id FROM gene_info WHERE symbol = ?";
			PreparedStatement statementToSelectTU = connection.prepareStatement(selectTuInfo);
			String insertTuInfo = "INSERT INTO gene_coexpression_interaction (symbol_1, symbol_2, correlation, weight, gene_info_entrez_id) VALUES (?,?,?,?,?)";
			PreparedStatement statementToInsertTU = connection.prepareStatement(insertTuInfo);
			String updateGeneInfo = "UPDATE gene_info SET graph_degree=? where symbol=?";
			PreparedStatement statementToUpdateGene = connection.prepareStatement(updateGeneInfo);
			List<String> symbols = new ArrayList<String>();
			int batchGeneCounter = 0;
			while ((line = reader.readLine()) != null && (lineWt = readerWt.readLine()) != null) {
				String[] pfamDesc = line.trim().replace("\"", "").split(cvsSplitBy);
				String[] pfamDescWt = lineWt.trim().replace("\"", "").split(cvsSplitBy);
				for (int i = 1; i < pfamDesc.length; i++) {
					pfamDesc[i] = pfamDesc[i].replace("\"", "");
				}
				for (int i = 1; i < pfamDescWt.length; i++) {
					pfamDescWt[i] = pfamDescWt[i].replace("\"", "");
				}
				if (rowCounter == 1) {
					for (String column : pfamDesc) {
						if (!column.trim().isEmpty())
							symbols.add(column);
					}
				} else if (pfamDesc.length > 0 && pfamDescWt.length > 0) {
					statementToSelectTU.setString(1, pfamDesc[0]);
					ResultSet resultSet = statementToSelectTU.executeQuery();
					Long entrez_id = null;
					while (resultSet.next()) {
						entrez_id = resultSet.getLong(1);
					}
					if (entrez_id != null) {
						if (symbols.size() != pfamDesc.length - 1 && symbols.size() != pfamDescWt.length - 1) {
							throw new ParseException("No of columns in row not equal to no of column headers",
									entityName, rowCounter);
						}
						connection.setAutoCommit(false);
						int batchCounter = 0;
						int graph_degree = 0;
						for (int i = 0; i < symbols.size(); i++) {
							statementToInsertTU.setString(1, symbols.get(i));
							statementToInsertTU.setString(2, pfamDesc[0]);
							if (pfamDesc[i + 1].equalsIgnoreCase("na"))
								statementToInsertTU.setString(3, null);
							else
								statementToInsertTU.setFloat(3, Float.parseFloat(pfamDesc[i + 1]));
							if (pfamDescWt[i + 1].equalsIgnoreCase("na")) {
								statementToInsertTU.setString(4, null);
								pfamDescWt[i + 1] = "0";
							} else
								statementToInsertTU.setFloat(4, Float.parseFloat(pfamDescWt[i + 1]));
							if (Double.parseDouble(pfamDescWt[i + 1]) != 0)
								graph_degree++;
							statementToInsertTU.setLong(5, entrez_id);
							statementToInsertTU.addBatch();
							fileWriter.print(symbols.get(i) + " ");
							fileWriter.print(pfamDesc[i + 1] + " ");
							fileWriter.print(pfamDesc[0]);
							fileWriter.println();
							fileWriterWt.print(symbols.get(i) + " ");
							fileWriterWt.print(pfamDescWt[i + 1] + " ");
							fileWriterWt.print(pfamDescWt[0]);
							fileWriterWt.println();
							recordReceived++;
							batchCounter++;
							if (batchCounter % 100 == 0 || i + 1 == symbols.size()) {
								try {
									statementToInsertTU.executeBatch();
									connection.commit();
									recordSuccessful = recordSuccessful + batchCounter;
								} catch (SQLException ex) {
									saveException(new ParseException(ex,
											"Symbol " + symbols.get(i) + " is missing in gene_info.csv", entityName,
											rowCounter), context);
									connection.rollback();
									throw new EmptyException(ex, entityName, rowCounter);
								}
								batchCounter = 0;
							}
						}
						statementToUpdateGene.setInt(1, graph_degree);
						statementToUpdateGene.setString(2, pfamDesc[0]);
						statementToUpdateGene.addBatch();
						batchGeneCounter++;
						if (batchGeneCounter % 100 == 0) {
							try {
								statementToUpdateGene.executeBatch();
								connection.commit();
							} catch (SQLException ex) {
								saveException(
										new ParseException(ex, "Symbol " + pfamDesc[0] + " is missing in gene_info.csv",
												entityName, rowCounter),
										context);
								connection.rollback();
								throw new AdapterException(ex, entityName, rowCounter);
							}
							batchGeneCounter = 0;
						}
					} else
						saveException(new ParseException("Symbol " + pfamDesc[0] + " is missing in gene_info",
								entityName, rowCounter), context);
					logger.info("Upload complete. Line no: " + rowCounter + " for entity: " + entityName + " & entity: "
							+ ImportDataFiles.WEIGHTED_MATRIX);
				}
				rowCounter++;
			}
			if (batchGeneCounter != 0) {
				try {
					statementToUpdateGene.executeBatch();
					connection.commit();
				} catch (SQLException ex) {
					saveException(
							new ParseException(ex, "Symbols are missing in gene_info.csv", entityName, rowCounter),
							context);
					connection.rollback();
				}
			}
			fileWriter.close();
			fileWriterWt.close();
			statementToSelectTU.close();
			statementToInsertTU.close();
		} catch (Exception e) {
			throw new AdapterException(e, entityName, rowCounter);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Error: " + e);
				}
			}
			if (readerWt != null) {
				try {
					readerWt.close();
				} catch (IOException e) {
					logger.error("Error: " + e);
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
			if (fileWriter != null) {
				try {
					fileWriter.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
			if (fileWriterWt != null) {
				try {
					fileWriterWt.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
			jobStat.setRecordsReceivedCount(recordReceived);
			jobStat.setRecordsSuccessfulCount(recordSuccessful);
			if (context.getStatistics().containsKey(ImportDataFiles.getImportDataFiles(entityName))) {
				JobStatistics prevJobStat = context.getStatistics().get(ImportDataFiles.getImportDataFiles(entityName));
				prevJobStat.setRecordsReceivedCount(
						jobStat.getRecordsReceivedCount() + prevJobStat.getRecordsReceivedCount());
				prevJobStat.setRecordsSuccessfulCount(
						jobStat.getRecordsSuccessfulCount() + prevJobStat.getRecordsSuccessfulCount());
				context.getStatistics().put(ImportDataFiles.WEIGHTED_MATRIX, prevJobStat);
			} else {
				context.getStatistics().put(ImportDataFiles.getImportDataFiles(entityName), jobStat);
				context.getStatistics().put(ImportDataFiles.WEIGHTED_MATRIX, jobStat);
			}
		}
	}

	@Override
	public void process(final File file, final ImportContext context, String entityName) {
		parsePFAMDescAndInsertRecord(file, context, entityName);
	}

}
