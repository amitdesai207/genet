package org.mun.genet.processor.Adapters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.sql.DataSource;

import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.jobreceiver.EmptyException;
import org.mun.genet.model.JobStatistics;
import org.mun.genet.processor.ImportDataFiles;
import org.mun.genet.processor.ParseException;
import org.mun.genet.vo.ImportContext;

public class ParseConditionInformation extends BaseAdapter implements CSVAdapter {

	private JobStatistics jobStat = null;

	public void parseConditionInformationInsertRecord(File file, ImportContext context, String entityName) {
		DataSource dataSource = context.getDataSource();
		BufferedReader reader = null;
		String line = "";
		jobStat = new JobStatistics();
		int recordReceived = 0;
		int recordSuccessful = 0;
		int rowCounter = 1;
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			reader = new BufferedReader(new FileReader(file + File.separator + entityName + CSV_FILE_EXTENSION));
			String insertTableSQL = "INSERT INTO condition_information (`condition`, number_replicates, strain, growth_phase, medium, description) VALUES (?,?,?,?,?,?)";
			PreparedStatement preparedStatement = connection.prepareStatement(insertTableSQL);
			while ((line = reader.readLine()) != null) {
				if (rowCounter == 1) {
					rowCounter++;
					continue;
				}
				recordReceived++;
				String[] conditionData = line.trim().replace("\"", "").split(cvsSplitBy);
				preparedStatement.setString(1, conditionData[0].trim());
				preparedStatement.setInt(2, Integer.parseInt(conditionData[1]));
				preparedStatement.setString(3, conditionData[2]);
				preparedStatement.setString(4, conditionData[3]);
				preparedStatement.setString(5, conditionData[4]);
				preparedStatement.setString(6, conditionData[5]);
				try {
					preparedStatement.executeUpdate();
					logger.info("Upload complete. Line no: " + rowCounter + " for entity: " + entityName);
					recordSuccessful++;
				} catch (Exception e) {
					saveException(new ParseException(e, entityName, rowCounter), context);
					throw new EmptyException(e, entityName, rowCounter);
				}
				rowCounter++;
			}
			preparedStatement.close();
		} catch (Exception e) {
			throw new AdapterException(e, entityName, rowCounter);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Error: " + e);
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
			jobStat.setRecordsReceivedCount(recordReceived);
			jobStat.setRecordsSuccessfulCount(recordSuccessful);
			context.getStatistics().put(ImportDataFiles.getImportDataFiles(entityName), jobStat);
		}
	}

	@Override
	public void process(File file, ImportContext context, String entityName) {
		parseConditionInformationInsertRecord(file, context, entityName);
	}

}
