package org.mun.genet.processor.Adapters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.sql.DataSource;

import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.jobreceiver.EmptyException;
import org.mun.genet.model.JobStatistics;
import org.mun.genet.processor.ImportDataFiles;
import org.mun.genet.processor.ParseException;
import org.mun.genet.vo.ImportContext;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class ParseKEGGAnnotation extends BaseAdapter implements CSVAdapter {

	private JobStatistics jobStat = null;

	public void parsePFAMDescAndInsertRecord(File file, ImportContext context, String entityName) {
		DataSource dataSource = context.getDataSource();
		BufferedReader reader = null;
		String line = "";
		jobStat = new JobStatistics();
		int recordReceived = 0;
		int recordSuccessful = 0;
		int rowCounter = 1;
		Connection dbConnection = null;
		try {
			dbConnection = dataSource.getConnection();
			reader = new BufferedReader(new FileReader(file + File.separator + "GENE_KEGG_v2" + CSV_FILE_EXTENSION));
			String insertTableSQL = "INSERT INTO kegg_annotation (symbol, pathway_id, kegg_description_pathway_id) VALUES (?,?,?)";
			PreparedStatement preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			while ((line = reader.readLine()) != null) {
				if (rowCounter == 1) {
					rowCounter++;
					continue;
				}
				recordReceived++;
				String[] pfamDesc = line.trim().replace("\"", "").split(cvsSplitBy);
				preparedStatement.setString(1, pfamDesc[0]);
				preparedStatement.setString(2, pfamDesc[1]);
				preparedStatement.setString(3, pfamDesc[1]);
				try {
					preparedStatement.executeUpdate();
					logger.info("Upload complete. Line no: " + rowCounter + " for entity: " + entityName);
					recordSuccessful++;
				} catch (Exception e) {
					if (!(e instanceof MySQLIntegrityConstraintViolationException)) {
						saveException(new ParseException(e, "Pathway Id: " + pfamDesc[1] + " is not present in gene_kegg_desc.csv", entityName,
								rowCounter), context);
						throw new EmptyException(e, entityName, rowCounter);
					}
				}
				rowCounter++;
			}
			preparedStatement.close();
		} catch (Exception e) {
			throw new AdapterException(e, entityName, rowCounter);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Error: " + e);
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
			jobStat.setRecordsReceivedCount(recordReceived);
			jobStat.setRecordsSuccessfulCount(recordSuccessful);
			context.getStatistics().put(ImportDataFiles.getImportDataFiles(entityName), jobStat);
		}
	}

	@Override
	public void process(File file, ImportContext context, String entityName) {
		parsePFAMDescAndInsertRecord(file, context, entityName);
	}

}
