package org.mun.genet.processor.Adapters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.sql.DataSource;

import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.jobreceiver.EmptyException;
import org.mun.genet.model.JobStatistics;
import org.mun.genet.processor.ImportDataFiles;
import org.mun.genet.processor.ParseException;
import org.mun.genet.util.Utility;
import org.mun.genet.vo.ImportContext;

public class ParseGeneInfo extends BaseAdapter implements CSVAdapter {

	private JobStatistics jobStat = null;

	public void parsePFAMDescAndInsertRecord(File file, ImportContext context, String entityName) {
		DataSource dataSource = context.getDataSource();
		BufferedReader reader = null;
		String line = "";
		jobStat = new JobStatistics();
		int recordReceived = 0;
		int recordSuccessful = 0;
		int rowCounter = 1;
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			reader = new BufferedReader(new FileReader(file + File.separator + entityName + CSV_FILE_EXTENSION));
			String selectModuleInfo = "SELECT module FROM module_info WHERE module = ?";
			String insertModuleInfo = "INSERT INTO module_info (module) VALUES (?)";
			String insertModuleGenes = "INSERT INTO module_has_genes (module, id, symbol, module_info_module, gene_info_entrez_id) VALUES (?,?,?,?,?)";
			String insertGeneInfo = "INSERT INTO gene_info (entrez_id, entrez_id1, systematic_id, symbol, description, type) VALUES (?,?,?,?,?,?)";
			PreparedStatement statementToSelectModuleInfo = connection.prepareStatement(selectModuleInfo);
			PreparedStatement statementToInsertModuleInfo = connection.prepareStatement(insertModuleInfo);
			PreparedStatement statementToInsertModule = connection.prepareStatement(insertModuleGenes);
			PreparedStatement statementToInsertGene = connection.prepareStatement(insertGeneInfo);
			while ((line = reader.readLine()) != null) {
				if (rowCounter == 1) {
					rowCounter++;
					continue;
				}
				String[] pfamDesc = line.trim().replace("\"", "").split(cvsSplitBy);
				for (int i = 0; i < pfamDesc.length; i++) {
					pfamDesc[i] = pfamDesc[i].replace("\"", "");
				}
				recordReceived++;
				try {
					Long geneId = Utility.generateRandomNumberSequence(rowCounter);
					statementToInsertGene.setLong(1, geneId);
					statementToInsertGene.setString(2, pfamDesc[0]);
					statementToInsertGene.setString(3, pfamDesc[2]);
					statementToInsertGene.setString(4, pfamDesc[1]);
					statementToInsertGene.setString(5, pfamDesc[3]);
					statementToInsertGene.setString(6, pfamDesc[4]);
					statementToInsertGene.executeUpdate();
					if (pfamDesc[5] != null && !pfamDesc[5].trim().isEmpty() && !pfamDesc[5].equals("NA")) {
						try {
							statementToSelectModuleInfo.setString(1, pfamDesc[5]);
							if (!statementToSelectModuleInfo.executeQuery().next()) {
								statementToInsertModuleInfo.setString(1, pfamDesc[5]);
								statementToInsertModuleInfo.executeUpdate();
							}

							statementToInsertModule.setString(1, pfamDesc[5]);
							statementToInsertModule.setLong(2, Utility.generateRandomNumberSequence(rowCounter));
							statementToInsertModule.setString(3, pfamDesc[1]);
							statementToInsertModule.setString(4, pfamDesc[5]);
							statementToInsertModule.setLong(5, geneId);
							statementToInsertModule.executeUpdate();
						} catch (Exception e) {
							saveException(new ParseException(e, "Module " + pfamDesc[5]
									+ " not present in GENE_INFO.csv", entityName, rowCounter), context);
						}
					}
					logger.info("Upload complete. Line no: " + rowCounter + " for entity: " + entityName);
					recordSuccessful++;
				} catch (Exception e) {
					saveException(new ParseException(e, entityName, rowCounter), context);
					throw new EmptyException(e, entityName, rowCounter);
				}
				rowCounter++;
			}
			statementToInsertGene.close();
			statementToInsertModule.close();
		} catch (Exception e) {
			throw new AdapterException(e, entityName, rowCounter);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("Error: " + e);
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
			jobStat.setRecordsReceivedCount(recordReceived);
			jobStat.setRecordsSuccessfulCount(recordSuccessful);
			context.getStatistics().put(ImportDataFiles.getImportDataFiles(entityName), jobStat);
		}
	}

	@Override
	public void process(File file, ImportContext context, String entityName) {
		parsePFAMDescAndInsertRecord(file, context, entityName);
	}

}
