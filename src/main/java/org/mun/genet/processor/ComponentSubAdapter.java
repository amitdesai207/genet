package org.mun.genet.processor;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.mun.genet.daos.JobCleanupService;
import org.mun.genet.daos.JobService;
import org.mun.genet.daos.SdkExceptionService;
import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.processor.Adapters.CSVAdapter;
import org.mun.genet.vo.ImportContext;
import org.mun.genet.vo.SdkExceptionVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ComponentSubAdapter implements ApplicationContextAware {

	private static final Logger logger = LoggerFactory.getLogger(ComponentSubAdapter.class);
	@Autowired
	private SdkExceptionService sdkExceptionService;
	@Autowired
	private JobService jobService;
	@Autowired
	private JobCleanupService jobCleanupService;
	private ApplicationContext applicationContext;

	public void adaptComponent(final File componentFile, final ImportContext context,
			final Map<String, CSVAdapter> group, final Map<String, CSVAdapter> subGrp) throws Exception {
		for (ImportDataFiles entityName : ImportDataFiles.values()) {
			CSVAdapter adapterToImport = group.get(entityName.toString());
			if (adapterToImport != null && context.getFilesUploaded().contains(entityName)) {
				if (jobService.isJobActive(context.getJobId())) {
					try {
						adapterToImport.process(componentFile, context, entityName.toString());
					} catch (AdapterException ex) {
						saveAdapterException(ex, entityName.toString(), context);
					}
				} else {
					jobCleanupService.cleanupDatabase(context.getOrganism());
					throw new AdapterException("Job Interrupted by user");
				}
			}
		}

		if (subGrp != null) {
			final Map<String, CSVAdapter> subGrp2 = new ConcurrentHashMap<String, CSVAdapter>();
			subGrp2.put(ImportDataFiles.CORRELATION_MATRIX.toString(),
					subGrp.remove(ImportDataFiles.CORRELATION_MATRIX.toString()));
			Thread group1Thread = null;
			Thread group2Thread = null;
			try {
				group1Thread = new Thread(
						context.getJobId().toString() + "_" + context.getUserEmail() + "_group1_subgroup1") {
					public void run() {
						ComponentSubGroupAdapter subAdapter = applicationContext
								.getBean(ComponentSubGroupAdapter.class);
						try {
							subAdapter.adaptComponent(componentFile, context, subGrp);
						} catch (Exception ex) {
							throw new AdapterException(ex);
						}
					}
				};
				group1Thread.start();

				group2Thread = new Thread(
						context.getJobId().toString() + "_" + context.getUserEmail() + "_group1_subgroup2") {
					public void run() {
						ComponentSubGroupAdapter subAdapter = applicationContext
								.getBean(ComponentSubGroupAdapter.class);
						try {
							subAdapter.adaptComponent(componentFile, context, subGrp2);
						} catch (Exception ex) {
							throw new AdapterException(ex);
						}
					}
				};
				group2Thread.start();
				group1Thread.join();
				group2Thread.join();
			} catch (Exception ex) {
				group1Thread.interrupt();
				group2Thread.interrupt();
				AdapterException exAdapt = new AdapterException(ex);
				saveAdapterException(exAdapt, "Thread Interuppted", context);
				throw exAdapt;
			}
		}
	}

	private void saveAdapterException(AdapterException ex, String entityName, ImportContext context) {
		try {
			SdkExceptionVo sdkVo = new SdkExceptionVo();
			sdkVo.setEntity(entityName);
			sdkVo.setCauseMessage(ex.getMessage());
			sdkVo.setErrDetail(ex.getMessage());
			sdkVo.setJobId(context.getJob().getId());
			if (ex.getLineNo() != 0) {
				sdkVo.setCustomMessage("Unable to parse " + entityName + "at lineNo" + ex.getLineNo());
				sdkVo.setLineNo(ex.getLineNo());
			}
			sdkExceptionService.createSdkException(sdkVo);
		} catch (Exception e) {
			logger.error("Exception while logging exception.", e);
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
