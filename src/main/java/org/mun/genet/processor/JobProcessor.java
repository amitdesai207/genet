package org.mun.genet.processor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.FileUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.mun.genet.configurer.Configuration;
import org.mun.genet.configurer.ImportJobStatusType;
import org.mun.genet.configurer.NotificationType;
import org.mun.genet.daos.FileStoreService;
import org.mun.genet.daos.JobCleanupService;
import org.mun.genet.daos.JobResultService;
import org.mun.genet.daos.JobService;
import org.mun.genet.daos.OrganismService;
import org.mun.genet.daos.SdkExceptionService;
import org.mun.genet.encryptors.GZipCompressor;
import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.jobreceiver.CorrelationType;
import org.mun.genet.jobreceiver.NetworkType;
import org.mun.genet.mail.EmailService;
import org.mun.genet.mail.SendEmailUtility;
import org.mun.genet.model.DownloadDataModel;
import org.mun.genet.model.JobStatistics;
import org.mun.genet.processor.Adapters.ParseOrganism;
import org.mun.genet.vo.ImportContext;
import org.mun.genet.vo.JobResultVo;
import org.mun.genet.vo.JobVo;
import org.mun.genet.vo.SdkExceptionVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * JobProcessor processes an import job. As per given job definition it reads
 * input file and by using ComponentImportManager imports components to QBO.
 */
@Component
public class JobProcessor {
	private static final Logger logger = LoggerFactory.getLogger(JobProcessor.class);
	private static String CSV_FILE_EXTENSION = ".csv";
	private static String KEGG_ID_SEPERATOR = ":";
	private static String CSV_FIELD_SEPERATOR = "\t";
	private static String CYTO_FILE_EXTENSION = ".sif";

	@Autowired
	private EmailService emailService;
	@Autowired
	private JobService jobService;
	@Autowired
	private JobResultService jobResultService;
	@Autowired
	private Configuration configuration;
	@Autowired
	private FileStoreService fileStoreService;
	@Autowired
	private SdkExceptionService sdkExceptionService;
	@Autowired
	private ComponentAdapter componentAdapter;
	@Autowired
	private OrganismService organismService;
	@Autowired
	private JobCleanupService jobCleanupService;
	@Autowired
	private ParseOrganism parseOrganism;
	@Autowired
	private SendEmailUtility emailUtility;
	@Autowired
	private GZipCompressor Gcompress;

	public void process(final ImportContext context) {
		FileInputStream inputFileStream = null;
		File inputFolder = null;
		Map<ImportDataFiles, JobStatistics> statistics = new HashMap<ImportDataFiles, JobStatistics>();

		try {
			logger.info("Import job processing started for job id: " + context.getJobId());
			jobService.markJobAsActive(context.getJob().getId(), new Date());
			inputFolder = new File(fileStoreService.getStoredFile(context.getDataPath()));

			JobStatistics jobStat = new JobStatistics();
			jobStat.setRecordsReceivedCount(1);
			context.setDataSource(parseOrganism.process(inputFolder, context));
			context.setSdkExceptionService(sdkExceptionService);
			context.setConfiguration(configuration);
			context.setEmailService(emailService);
			context.setEmailUtility(emailUtility);
			jobStat.setRecordsSuccessfulCount(1);
			context.getStatistics().put(ImportDataFiles.ORGANISM, jobStat);

			if (Boolean.parseBoolean(configuration.getConfigurationByName("skipRGenerator")))
				generateRequiredFilesForImport(context, inputFolder);
			context.setStatistics(statistics);
			File[] csvFiles = inputFolder.listFiles();
			List<ImportDataFiles> cvsFileNames = new ArrayList<>();
			for (File csvFile : csvFiles) {
				String nodeName = csvFile.getName().replace(CSV_FILE_EXTENSION, "");
				cvsFileNames.add(ImportDataFiles.getImportDataFiles(nodeName));
			}
			context.setFilesUploaded(cvsFileNames);

			componentAdapter.adaptComponent(inputFolder, context);

			if (sdkExceptionService.findAllSdkExceptionsForJob(context.getJob().getDisplayId()).size() == 0) {
				DownloadDataModel dataModel = new DownloadDataModel();
				for (File csvFile : csvFiles) {
					String nodeName = csvFile.getName().replace(CSV_FILE_EXTENSION, "").replace(CYTO_FILE_EXTENSION, "");
					if (nodeName.equals(configuration.getConfigurationByName("proteinSequenceFastaFileName")))
						dataModel.setProtein_sequence(true);
					else {
						ImportDataFiles importFile = ImportDataFiles.getImportDataFiles(nodeName);
						if (importFile != null)
							dataModel.setImportDataFileInModel(importFile.toString());
					}
				}
				String fileName = "Genet_" + context.getOrganism().replace(" ", "_") + "_ALL";
				String fileBase = configuration.getConfigurationByName("file.storage.path");
				File downloadFolder = new File(fileBase + File.separator + "download" + File.separator);
				FileUtils.forceMkdir(downloadFolder);
				File downloadFile = new File(downloadFolder.getAbsolutePath() + File.separator + fileName + ".tar.gz");
				List<String> fileList = dataModel.getImportDataFilePathsInModel(inputFolder.getAbsolutePath(),
						configuration.getConfigurationByName("proteinSequenceFastaFileName"));
				Gcompress.compressMultipleFiles(fileList, downloadFile.getAbsolutePath());
				
				jobService.markJobAsComplete(context.getJob().getId(), null, new Date(),
						ImportJobStatusType.SUCCESSFUL);
				organismService
						.markDownloadReady(organismService.getOrganismId(context.getJob().getOrganism()).toString());
				logger.info("Job processing done for the job : " + context.getJob().getId());
				emailService.saveNotificationMail(context.getJob(), NotificationType.JOB_COMPLETE);
			} else {
				jobService.markJobAsComplete(context.getJob().getId(), null, new Date(), ImportJobStatusType.FAILED);
				logger.info("Job processing done with errors for the job : " + context.getJob().getId());
				emailService.saveNotificationMail(context.getJob(), NotificationType.JOB_ERROR);
				fileStoreService.setCompeletedDate(context.getDataPath());
				jobCleanupService.cleanupDatabase(context.getOrganism());
			}
		} catch (Exception ee) {
			boolean cleanDatabase = true;
			if (ee instanceof AdapterException) {
				AdapterException adapterException = (AdapterException) ee;
				if (adapterException.getFailedStatus() != null) {
					jobService.markJobAsComplete(context.getJob().getId(), null, new Date(),
							adapterException.getFailedStatus());
					if (adapterException.getFailedStatus() == ImportJobStatusType.FAILED_DUPLICATE_ORGANISM)
						cleanDatabase = false;
				}
			} else
				jobService.markJobAsComplete(context.getJob().getId(), null, new Date(), ImportJobStatusType.FAILED);
			fileStoreService.setCompeletedDate(context.getDataPath());
			logger.warn("Error proccessing job.", ee);
			SdkExceptionVo sdkVo = new SdkExceptionVo();
			sdkVo.setCauseMessage(ee.getMessage());
			sdkVo.setJobId(context.getJob().getId());
			sdkVo.setCustomMessage("Error while processing Job");
			sdkVo.setErrDetail(ee.getMessage());
			sdkExceptionService.createSdkException(sdkVo);
			emailService.saveNotificationMail(context.getJob(), NotificationType.JOB_ERROR);
			if (cleanDatabase)
				jobCleanupService.cleanupDatabase(context.getOrganism());
		} finally {
			safeClose(inputFileStream);
			try {
				saveJobStatistics(statistics, context);
			} catch (Exception e) {
				logger.warn("Unable to close streams.");
			}
		}
	}

	private void generateRequiredFilesForImport(ImportContext context, File userFolder) throws Exception {
		try {
			JobVo job = context.getJob();
			// Generate KEGG files
			if (job.getKeggId() != null) {
				generateKeggFile(job.getKeggId(), userFolder);
				generateKeggDescriptionFile(job.getKeggId(), userFolder);
			}

			File proteinSequenceFile = new File(
					userFolder + File.separator + configuration.getConfigurationByName("proteinSequenceFastaFileName"));
			if (proteinSequenceFile.exists()) {
				generatePfamAnnotaionFile(proteinSequenceFile, userFolder);
			}

			StringBuilder commandLineArgs = new StringBuilder();
			commandLineArgs.append(userFolder.getAbsolutePath() + " ");
			commandLineArgs.append(job.getThreshold() + " ");
			commandLineArgs.append(CorrelationType.getCorrelationType(job.getCorrelation()).name() + " ");
			commandLineArgs.append(NetworkType.getNetworkType(job.getNetworkType()).name().toLowerCase() + " ");
			if (job.getKeggId() != null)
				commandLineArgs.append("TRUE ");
			else
				commandLineArgs.append("FALSE ");
			File[] csvFiles = userFolder.listFiles();
			List<ImportDataFiles> cvsFileNames = new ArrayList<>();
			for (File csvFile : csvFiles) {
				String nodeName = csvFile.getName().replace(CSV_FILE_EXTENSION, "");
				cvsFileNames.add(ImportDataFiles.getImportDataFiles(nodeName));
			}
			if (cvsFileNames.contains(ImportDataFiles.GENE_TU))
				commandLineArgs.append("TRUE ");
			else
				commandLineArgs.append("FALSE ");
			if (cvsFileNames.contains(ImportDataFiles.GENE_PROTEIN))
				commandLineArgs.append("TRUE ");
			else
				commandLineArgs.append("FALSE ");
			if (cvsFileNames.contains(ImportDataFiles.GENE_PFAM))
				commandLineArgs.append("TRUE ");
			else
				commandLineArgs.append("FALSE ");
			if (cvsFileNames.contains(ImportDataFiles.GENE_METACYC))
				commandLineArgs.append("TRUE ");
			else
				commandLineArgs.append("FALSE ");

			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL resource = classLoader.getResource("GenetFileGenerator.R");

			// Make a system call to RScript
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec("Rscript --no-restore --no-site-file --no-init-file "
					+ Paths.get(resource.toURI()) + " " + commandLineArgs.toString());
			process.waitFor();
			if (process.exitValue() != 0) {
				if (process.getErrorStream() != null) {
					BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
					String line = null;
					StringBuilder error = new StringBuilder();
					while ((line = errorReader.readLine()) != null) {
						error.append(line);
					}
					errorReader.close();
					throw new AdapterException(
							new Exception("Problem occured while processing files in Rscript. " + error.toString()),
							"Data Generation", 0, ImportJobStatusType.FAILED);
				} else {
					throw new AdapterException(new Exception(
							"Problem occured while processing files in Rscript. The process stopped with error code "
									+ process.exitValue()),
							"Data Generation", 0, ImportJobStatusType.FAILED);
				}
			}
		} catch (Exception ex) {
			throw new AdapterException(ex, "Data Generation", 0, ImportJobStatusType.FAILED);
		}
	}

	private void generatePfamAnnotaionFile(File proteinSequenceFile, File userFolder) throws Exception {
		String line = null;
		String eValue = configuration.getConfigurationByName("pfamGenerationEValue");
		ConcurrentHashMap<String, List<String[]>> pfamJobLinks = new ConcurrentHashMap<String, List<String[]>>();

		BufferedWriter keggWriter = new BufferedWriter(new FileWriter(
				userFolder.getAbsolutePath() + File.separator + ImportDataFiles.GENE_PFAM + CSV_FILE_EXTENSION));
		keggWriter.newLine();

		BufferedReader proteinSeqReader = new BufferedReader(new FileReader(proteinSequenceFile));
		List<String[]> proteinSeq = null;
		String geneSymbol = null;
		while ((line = proteinSeqReader.readLine()) != null) {
			String data = line.trim();
			if (data.startsWith(">")) {
				if (proteinSeq != null) {
					pfamJobLinks.put(geneSymbol, proteinSeq);
				}
				proteinSeq = new ArrayList<>();
				geneSymbol = data.replace(">", "");
			} else {
				String pfamJobLink = submitSequenceJobToPfamServer(data, eValue);
				if (pfamJobLink != null) {
					proteinSeq.add(new String[] { pfamJobLink, data });
					while (!pfamJobLinks.isEmpty()) {
						for (Entry<String, List<String[]>> keyValue : pfamJobLinks.entrySet()) {
							if (keyValue.getValue().isEmpty()) {
								pfamJobLinks.remove(keyValue.getKey());
							} else {
								List<String[]> pfamJobs = keyValue.getValue();
								Iterator<String[]> pfamJobsIterator = pfamJobs.iterator();
								while (pfamJobsIterator.hasNext()) {
									ArrayList<String> pfamIds = fetchPfamsFromJobLink(keyValue.getKey(),
											pfamJobsIterator.next());
									if (pfamIds != null) {
										for (String pfam : pfamIds) {
											keggWriter.write(keyValue.getKey() + CSV_FIELD_SEPERATOR + pfam);
											keggWriter.newLine();
										}
										pfamJobsIterator.remove();
									}
								}
							}
						}
					}
				}
			}
		}
		proteinSeqReader.close();
		keggWriter.close();
	}

	private ArrayList<String> fetchPfamsFromJobLink(String geneSymbol, String[] pfamJob) throws Exception {
		StringBuffer xmlResponse = new StringBuffer();
		ArrayList<String> pfams = new ArrayList<>();
		try {
			URL url = new URL(pfamJob[0]);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");

			int responseCode = urlConnection.getResponseCode();
			if (responseCode == 202)
				return null;
			else if (responseCode == 500) {
				// Resubmit the job
				String eValue = configuration.getConfigurationByName("pfamGenerationEValue");
				String pfamJobLink = submitSequenceJobToPfamServer(pfamJob[1], eValue);
				if (pfamJobLink != null)
					pfamJob[0] = pfamJobLink;
				return null;
			}

			String line = null;
			BufferedReader responseReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			while ((line = responseReader.readLine()) != null) {
				xmlResponse.append(line);
			}
			responseReader.close();
			urlConnection.disconnect();

			SAXBuilder saxBuilder = new SAXBuilder();
			Document doc = saxBuilder.build(new StringReader(xmlResponse.toString()));
			List<Element> matches = doc.getRootElement().getChildren().get(0).getChildren();
			if (!matches.isEmpty()) {
				Element DocSum = matches.get(0).getChildren().get(0).getChildren().get(0);
				for (Element match : DocSum.getChildren()) {
					String attribute = match.getAttributeValue("accession");
					if (attribute != null) {
						String[] pfamValue = attribute.split("\\.");
						if (pfamValue.length > 0)
							pfams.add(pfamValue[0]);
					}
				}
			}
		} catch (Exception ex) {
			logger.error("Error while fetching pfam: " + ex + "\n" + xmlResponse + "\n. Server will retry");
			// Resubmit the job
			String eValue = configuration.getConfigurationByName("pfamGenerationEValue");
			String pfamJobLink = submitSequenceJobToPfamServer(pfamJob[1], eValue);
			if (pfamJobLink != null)
				pfamJob[0] = pfamJobLink;
			return null;
		}
		return pfams;
	}

	private String submitSequenceJobToPfamServer(String sequence, String eValue) throws Exception {
		int retry = 0;
		String returnValue = null;
		while (retry != 3) {
			try {
				URL url = new URL(configuration.getConfigurationByName("pfamSequenceSearchLink"));
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setRequestMethod("POST");
				urlConnection.setRequestProperty("User-Agent",
						"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
				String urlParameters = "evalue=" + eValue + "&seq=" + sequence;
				urlConnection.setDoOutput(true);
				DataOutputStream parameterWriter = new DataOutputStream(urlConnection.getOutputStream());
				parameterWriter.writeBytes(urlParameters);
				parameterWriter.flush();
				parameterWriter.close();

				String line = null;
				StringBuffer xmlResponse = new StringBuffer();
				BufferedReader responseReader = new BufferedReader(
						new InputStreamReader(urlConnection.getInputStream()));
				while ((line = responseReader.readLine()) != null) {
					xmlResponse.append(line);
				}
				responseReader.close();
				urlConnection.disconnect();

				SAXBuilder saxBuilder = new SAXBuilder();
				Document doc = saxBuilder.build(new StringReader(xmlResponse.toString()));
				Element DocSum = doc.getRootElement().getChildren().get(0).getChildren().get(1);
				if (DocSum.getName().equalsIgnoreCase("result_url")) {
					returnValue = DocSum.getValue();
					break;
				}
			} catch (Exception ex) {
				retry++;
				logger.error(
						"Error while creating pfam search job for sequence: " + sequence + " . The command will retry");
			}
		}
		return returnValue;
	}

	private void generateKeggDescriptionFile(String keggId, File userFolder) throws Exception {
		String keggIdPathSeperator = configuration.getConfigurationByName("keggIdPathSeperator");
		URL url = new URL(configuration.getConfigurationByName("keggDesciption") + keggId);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("GET");
		urlConnection.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");

		BufferedWriter keggWriter = new BufferedWriter(new FileWriter(
				userFolder.getAbsolutePath() + File.separator + ImportDataFiles.GENE_KEGG_DESC + CSV_FILE_EXTENSION));
		BufferedReader responseReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		String line = null;
		keggWriter.newLine();
		while ((line = responseReader.readLine()) != null) {
			line = line.trim();
			int tabIndex = line.indexOf("	");
			String keggIdValue = line.substring(0, tabIndex);
			String keggDesc = line.substring(tabIndex + 1);
			keggWriter.write(keggIdValue.replace(keggIdPathSeperator, "") + CSV_FIELD_SEPERATOR + keggDesc.trim());
			keggWriter.newLine();
		}
		responseReader.close();
		keggWriter.close();
	}

	private void generateKeggFile(String keggId, File userFolder) throws Exception {
		String keggIdPathSeperator = configuration.getConfigurationByName("keggIdPathSeperator");
		URL url = new URL(configuration.getConfigurationByName("keggId") + keggId);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("GET");
		urlConnection.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");

		BufferedWriter keggWriter = new BufferedWriter(new FileWriter(
				userFolder.getAbsolutePath() + File.separator + ImportDataFiles.GENE_KEGG + CSV_FILE_EXTENSION));
		BufferedReader responseReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		String line = null;
		keggWriter.newLine();
		while ((line = responseReader.readLine()) != null) {
			String[] tokens = line.trim().split("\\s");
			keggWriter.write(tokens[0].replace(keggId + KEGG_ID_SEPERATOR, "") + CSV_FIELD_SEPERATOR
					+ tokens[1].replace(keggIdPathSeperator, ""));
			keggWriter.newLine();
		}
		responseReader.close();
		keggWriter.close();
	}

	private void saveJobStatistics(Map<ImportDataFiles, JobStatistics> statistics, ImportContext context) {

		logger.info("############################ JOB RESULT SUMMARY #############################");
		logger.info("############################ JOB NO: " + String.format("%-10d", context.getJobId())
				+ " #############################");
		for (Map.Entry<ImportDataFiles, JobStatistics> entry : statistics.entrySet()) {
			JobResultVo jobResult = new JobResultVo();
			jobResult.setEntity(entry.getKey().toString());
			jobResult.setRecordsReceived(entry.getValue().getRecordsReceivedCount());
			jobResult.setRecordsSuccessful(entry.getValue().getRecordsSuccessfulCount());
			jobResult.setRecordsFailed(jobResult.getRecordsReceived() - jobResult.getRecordsSuccessful());
			jobResult.setJobId(context.getJobId());
			jobResultService.createJobResult(jobResult);
			logger.info("### Entity: " + String.format("%-17s", jobResult.getEntity()) + " , Success: "
					+ String.format("%-4d", jobResult.getRecordsSuccessful()) + " , Failed: "
					+ String.format("%-4d", jobResult.getRecordsFailed()) + " ###");
		}
		logger.info("############################ JOB RESULT SUMMARY #############################");
	}

	private void safeClose(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (IOException e) {
				logger.warn("Unable to close " + closeable);
			}
		}
	}

}
