package org.mun.genet.processor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.mun.genet.vo.ImportContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalculateGraphDegree {

	protected static final Logger logger = LoggerFactory.getLogger(CalculateGraphDegree.class);

	public void processGraph(ImportContext context) throws Exception {
		DataSource dataSource = context.getDataSource();
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			String selectAllGeneSymbols = "SELECT distinct(symbol) from gene_info";
			PreparedStatement statementToselectAllGeneSymbols = connection.prepareStatement(selectAllGeneSymbols);
			String graphDegreeStatement = "select count(*) from gene_coexpression_interaction where symbol_1=? and weight!=0";
			PreparedStatement statementToGetGraphDegree = connection.prepareStatement(graphDegreeStatement);
			String updateGeneInfo = "update gene_info set graph_degree=? where symbol=?";
			PreparedStatement statementToUpdateGene = connection.prepareStatement(updateGeneInfo);

			ResultSet resultSet = statementToselectAllGeneSymbols.executeQuery();
			while (resultSet.next()) {
				// Get Symbol
				String symbol = resultSet.getString(1);
				// Calculate Graph degree
				statementToGetGraphDegree.setString(1, symbol);
				ResultSet resultSet1 = statementToGetGraphDegree.executeQuery();
				int graphDegree = 0;
				while (resultSet1.next()) {
					graphDegree = resultSet1.getInt(1);
				}
				// update gene_info with graph degree
				statementToUpdateGene.setInt(1, graphDegree);
				statementToUpdateGene.execute();
			}
			statementToselectAllGeneSymbols.close();
			statementToGetGraphDegree.close();
			statementToUpdateGene.close();
		} catch (Exception e) {
			throw e;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception e) {
					logger.error("Error: " + e);
				}
			}
		}
	}
}
