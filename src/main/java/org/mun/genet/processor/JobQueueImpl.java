package org.mun.genet.processor;

import java.util.Date;
import java.util.List;

import org.mun.genet.configurer.Configuration;
import org.mun.genet.configurer.ImportJobStatusType;
import org.mun.genet.daos.JobService;
import org.mun.genet.daos.NotificationService;
import org.mun.genet.daos.SdkExceptionService;
import org.mun.genet.mail.SendEmailUtility;
import org.mun.genet.vo.ImportContext;
import org.mun.genet.vo.JobVo;
import org.mun.genet.vo.NotificationVo;
import org.mun.genet.vo.SdkExceptionVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class JobQueueImpl implements JobQueue, ApplicationContextAware {

	private static final Logger logger = LoggerFactory.getLogger(JobQueueImpl.class);
	private static final Logger loggerServerCapacity = LoggerFactory.getLogger("QuartzLogger");
	private static boolean sendingEmails = false;

	@Autowired
	private JobService jobService;
	@Autowired
	private Configuration config;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private SendEmailUtility sendEmailUtility;
	@Autowired
	private SdkExceptionService sdkExceptionService;

	private ApplicationContext applicationContext;

	public synchronized int runQueuedJobs() {

		// Check server load
		if (!Boolean.parseBoolean(config.getConfigurationByName("stopSchedulingJobs"))) {
			int activeJobsCount = jobService.findActiveJobCount();
			int maxThreads = Integer.parseInt(config.getConfigurationByName("scheduler.max_concurrent_jobs"));

			// check if server has any capacity to start new jobs
			int serverCapacity = maxThreads - activeJobsCount;
			if (serverCapacity < maxThreads)
				loggerServerCapacity.info("serverCapacity = " + serverCapacity + "/" + maxThreads);

			if (serverCapacity > 0) {

				List<JobVo> jobVos = jobService.findAllPendingJobs();
				JobVo jobVo = null;

				if (jobVos != null && jobVos.size() > 0) {
					for (int i = 0; i < serverCapacity; i++) {

						if (jobVos.size() == i)
							break;
						// if the no of pending jobs is lesser than available
						// server capacity
						try {
							jobVo = jobVos.get(i);
							final ImportContext importContext = new ImportContext(jobVo);
							Thread t = new Thread(jobVo.getId().toString() + "_" + jobVo.getUserEmail()) {
								public void run() {
									JobProcessor jp = applicationContext.getBean(JobProcessor.class);
									jp.process(importContext);
								}
							};
							t.start();
						} catch (Exception ex) {
							jobService.markJobAsComplete(jobVo.getId(), new Date(), new Date(),
									ImportJobStatusType.FAILED);
							logger.debug("Error while processing file: " + ex.getMessage());
							SdkExceptionVo sdkVo = new SdkExceptionVo();
							sdkVo.setErrDetail(ex.getMessage());
							sdkVo.setCustomMessage("Error while processing file");
							sdkVo.setJobId(jobVo.getId());
							sdkExceptionService.createSdkException(sdkVo);
						}
					}
				}

			} else {
				loggerServerCapacity.debug("Server Capacity Full");
				return 0;
			}

			// Send all queued mails
			if (!sendingEmails && Boolean.parseBoolean(config.getConfigurationByName("mail.enable")))
				sendQueuedEmail();

		}
		return 1;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	synchronized private void sendQueuedEmail() {
		sendingEmails = true;
		int maxAllowedRetry = Integer.parseInt(config.getConfigurationByName("mail.allowedRetry"));
		// Find all pending email in DB and send them
		List<NotificationVo> pendingMails = notificationService.findAllPendingNotifications();
		if (pendingMails.size() > 0)
			logger.info("Sending all Queued Emails: " + pendingMails.size());
		for (NotificationVo mail : pendingMails) {
			try {
				sendEmailUtility.sendMail(mail, null);
				notificationService.markNotificationAsSent(mail.getId());
			} catch (Exception ex) {
				logger.error("Error while sending email: " + ex.getMessage());
				notificationService.incrementRetryCountForNotification(mail.getId(), maxAllowedRetry);
				break;
			}
		}

		// Find all RETRY_NEEDED mails from DB and retry to send them
		List<NotificationVo> retryMails = notificationService.findAllRetriableNotifications();
		for (NotificationVo mail : retryMails) {
			try {
				sendEmailUtility.sendMail(mail, null);
				notificationService.markNotificationAsSent(mail.getId());
			} catch (Exception ex) {
				notificationService.incrementRetryCountForNotification(mail.getId(), maxAllowedRetry);
				if (mail.getNumberOfRetries() + 1 == maxAllowedRetry)
					logger.warn("Error while sending mail: " + ex.getMessage());
				break;
			}
		}
		sendingEmails = false;
	}

}