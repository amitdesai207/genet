package org.mun.genet.processor;

public class ParseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String entityName;
	private int lineNo;
	private String errorCause;

	public ParseException(String msg, String entityName, int lineNo) {
		super(msg);
		this.errorCause = msg;
		this.entityName = entityName;
		this.lineNo = lineNo;
	}

	public ParseException(Exception e, String entityName, int lineNo) {
		super(e);
		this.errorCause = e.getMessage();
		this.entityName = entityName;
		this.lineNo = lineNo;
	}

	public ParseException(Exception e, String errorCause, String entityName, int lineNo) {
		super(e);
		this.errorCause = errorCause;
		this.entityName = entityName;
		this.lineNo = lineNo;
	}

	public String getEntityName() {
		return entityName;
	}

	public int getLineNo() {
		return lineNo;
	}

	public String getErrorCause() {
		return errorCause;
	}

}
