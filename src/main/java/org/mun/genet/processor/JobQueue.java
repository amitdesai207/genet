package org.mun.genet.processor;

public interface JobQueue {
	public int runQueuedJobs();
}
