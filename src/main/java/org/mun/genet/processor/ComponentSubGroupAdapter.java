package org.mun.genet.processor;

import java.io.File;
import java.util.Map;

import org.mun.genet.daos.JobCleanupService;
import org.mun.genet.daos.JobService;
import org.mun.genet.daos.SdkExceptionService;
import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.processor.Adapters.CSVAdapter;
import org.mun.genet.vo.ImportContext;
import org.mun.genet.vo.SdkExceptionVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ComponentSubGroupAdapter {

	private static final Logger logger = LoggerFactory.getLogger(ComponentSubGroupAdapter.class);
	@Autowired
	private SdkExceptionService sdkExceptionService;
	@Autowired
	private JobService jobService;
	@Autowired
	private JobCleanupService jobCleanupService;

	public void adaptComponent(final File componentFile, final ImportContext context, final Map<String, CSVAdapter> group)
			throws Exception {
		for (ImportDataFiles entityName : ImportDataFiles.values()) {
			CSVAdapter adapterToImport = group.get(entityName.toString());
			if (adapterToImport != null && context.getFilesUploaded().contains(entityName)) {
				if (jobService.isJobActive(context.getJobId())) {
					try {
						adapterToImport.process(componentFile, context, entityName.toString());
					} catch (AdapterException ex) {
						saveAdapterException(ex, entityName.toString(), context);
					}
				} else {
					jobCleanupService.cleanupDatabase(context.getOrganism());
					throw new AdapterException("Job Interrupted by user");
				}
			}
		}
	}

	private void saveAdapterException(AdapterException ex, String entityName, ImportContext context) {
		try {
			SdkExceptionVo sdkVo = new SdkExceptionVo();
			sdkVo.setEntity(entityName);
			sdkVo.setCauseMessage(ex.getMessage());
			sdkVo.setErrDetail(ex.getMessage());
			sdkVo.setJobId(context.getJob().getId());
			if (ex.getLineNo() != 0) {
				sdkVo.setCustomMessage("Unable to parse " + entityName + "at lineNo" + ex.getLineNo());
				sdkVo.setLineNo(ex.getLineNo());
			}
			sdkExceptionService.createSdkException(sdkVo);
		} catch (Exception e) {
			logger.error("Exception while logging exception.", e);
		}
	}

}
