package org.mun.genet.processor;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.mun.genet.daos.JobCleanupService;
import org.mun.genet.daos.JobService;
import org.mun.genet.jobreceiver.AdapterException;
import org.mun.genet.processor.Adapters.CSVAdapter;
import org.mun.genet.processor.Adapters.ParseConditionInformation;
import org.mun.genet.processor.Adapters.ParseGeneCorrelationAndWeightedMatrix;
import org.mun.genet.processor.Adapters.ParseGeneExpression;
import org.mun.genet.processor.Adapters.ParseGeneInfo;
import org.mun.genet.processor.Adapters.ParseKEGGAnnotation;
import org.mun.genet.processor.Adapters.ParseKEGGDescription;
import org.mun.genet.processor.Adapters.ParseMetaCycPathway;
import org.mun.genet.processor.Adapters.ParseModule;
import org.mun.genet.processor.Adapters.ParseModuleEnrichment;
import org.mun.genet.processor.Adapters.ParsePFAMAnnotation;
import org.mun.genet.processor.Adapters.ParsePFAMDesc;
import org.mun.genet.processor.Adapters.ParseProteinComplex;
import org.mun.genet.processor.Adapters.ParseTransUnit;
import org.mun.genet.vo.ImportContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ComponentAdapter implements ApplicationContextAware {

	@Autowired
	private JobService jobService;
	@Autowired
	private JobCleanupService jobCleanupService;

	private ApplicationContext applicationContext;

	public void adaptComponent(final File componentFile, final ImportContext context)
			throws AdapterException, InterruptedException, Exception {

		final Map<String, CSVAdapter> adaptersGrp1 = new ConcurrentHashMap<String, CSVAdapter>();
		final Map<String, CSVAdapter> adaptersGrp2 = new ConcurrentHashMap<String, CSVAdapter>();
		final Map<String, CSVAdapter> adaptersGrp3 = new ConcurrentHashMap<String, CSVAdapter>();

		adaptersGrp1.put(ImportDataFiles.values()[1].toString(), new ParseConditionInformation());
		adaptersGrp1.put(ImportDataFiles.values()[2].toString(), new ParseModule());
		adaptersGrp1.put(ImportDataFiles.values()[3].toString(), new ParseGeneInfo());

		adaptersGrp2.put(ImportDataFiles.values()[4].toString(), new ParseGeneExpression());
		adaptersGrp2.put(ImportDataFiles.values()[5].toString(), new ParsePFAMAnnotation());
		adaptersGrp2.put(ImportDataFiles.values()[6].toString(), new ParseKEGGAnnotation());
		adaptersGrp2.put(ImportDataFiles.values()[7].toString(), new ParseGeneCorrelationAndWeightedMatrix());

		adaptersGrp3.put(ImportDataFiles.values()[9].toString(), new ParsePFAMDesc());
		adaptersGrp3.put(ImportDataFiles.values()[10].toString(), new ParseKEGGDescription());
		adaptersGrp3.put(ImportDataFiles.values()[11].toString(), new ParseMetaCycPathway());
		adaptersGrp3.put(ImportDataFiles.values()[12].toString(), new ParseProteinComplex());
		adaptersGrp3.put(ImportDataFiles.values()[13].toString(), new ParseTransUnit());
		adaptersGrp3.put(ImportDataFiles.values()[14].toString(), new ParseModuleEnrichment());

		if (jobService.isJobActive(context.getJobId())) {
			Thread group1Thread = null;
			Thread group2Thread = null;
			try {
				group1Thread = new Thread(context.getJobId().toString() + "_" + context.getUserEmail() + "_group1") {
					public void run() {
						ComponentSubAdapter subAdapter = applicationContext.getBean(ComponentSubAdapter.class);
						try {
							subAdapter.adaptComponent(componentFile, context, adaptersGrp1, adaptersGrp2);
						} catch (Exception ex) {
							throw new AdapterException(ex);
						}
					}
				};
				group1Thread.start();

				group2Thread = new Thread(context.getJobId().toString() + "_" + context.getUserEmail() + "_group2") {
					public void run() {
						ComponentSubAdapter subAdapter = applicationContext.getBean(ComponentSubAdapter.class);
						try {
							subAdapter.adaptComponent(componentFile, context, adaptersGrp3, null);
						} catch (Exception ex) {
							throw new AdapterException(ex);
						}
					}
				};
				group2Thread.start();

				group1Thread.join();
				group2Thread.join();
			} catch (Exception ex) {
				group1Thread.interrupt();
				group2Thread.interrupt();
				throw new AdapterException(ex);
			}
		} else {
			jobCleanupService.cleanupDatabase(context.getOrganism());
			throw new AdapterException("Job Interrupted by user");
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

}
