package org.mun.genet.commandvalidators;

import java.util.regex.Pattern;

import org.mun.genet.model.SignupModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class SignupValidator implements Validator {

	Logger logger = LoggerFactory.getLogger(SignupValidator.class);

	private static final Pattern ALPHA_NUMERIC_PATTERN = Pattern.compile("[a-zA-Z0-9_]+$");
	private static final Pattern ALPHA_NUMERIC_PASSWORD_PATTERN = Pattern.compile("[a-zA-Z0-9_$!@#%&^*-]+$");
	private static final Pattern ALPHA_PATTERN = Pattern.compile("[a-zA-Z\\s\t-]+$");
	private static final Pattern EMAIL_PATTERN = Pattern
			.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

	@Override
	public boolean supports(Class<?> clazz) {
		return SignupModel.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		SignupModel signupModel = (SignupModel) target;

		// Validate Email
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", null, "Email is required");
		if (isNotNullAndEmpty(signupModel.getEmail())) {
			if (!isPatternMatched(EMAIL_PATTERN, signupModel.getEmail())) {
				errors.rejectValue("email", null, "Invalid Email Address format");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", null, "UserName is required");
		if (isNotNullAndEmpty(signupModel.getUserName())) {
			if (!isPatternMatched(EMAIL_PATTERN, signupModel.getUserName())
					&& !isPatternMatched(ALPHA_NUMERIC_PATTERN, signupModel.getUserName())) {
				errors.rejectValue("userName", null, "Username can be your email address OR should be Alpha Numeric");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", null, "First Name is required");
		if (isNotNullAndEmpty(signupModel.getFirstName())) {
			if (!isPatternMatched(ALPHA_PATTERN, signupModel.getFirstName())) {
				errors.rejectValue("firstName", null, "First Name should contain alphabets only");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", null, "Last Name is required");
		if (isNotNullAndEmpty(signupModel.getLastName())) {
			if (!isPatternMatched(ALPHA_PATTERN, signupModel.getLastName())) {
				errors.rejectValue("lastName", null, "Last Name should contain alphabets only");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", null, "Password is required");
		if (isNotNullAndEmpty(signupModel.getPassword())) {
			if (signupModel.getPassword().length() < 8) {
				errors.rejectValue("password", null, "Password should be minimum size of 8");
			}
			if (!isPatternMatched(ALPHA_NUMERIC_PASSWORD_PATTERN, signupModel.getPassword())) {
				errors.rejectValue("password", null,
						"Password can contain an alphabet, number or special character(_$!@#%&^*-)");
			}
		}

	}

	private boolean isNotNullAndEmpty(String input) {
		if (input == null || input.isEmpty())
			return false;
		return true;
	}

	private boolean isPatternMatched(Pattern pattern, String input) {
		return pattern.matcher(input).matches();
	}

}
