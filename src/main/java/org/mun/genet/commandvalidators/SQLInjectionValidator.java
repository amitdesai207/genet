package org.mun.genet.commandvalidators;

import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class SQLInjectionValidator implements Validator {
	private static final Pattern ALPHA_NUMERIC_PATTERN = Pattern.compile("[a-zA-Z0-9-_\\s\t.@]+$");

	@Override
	public boolean supports(Class<?> clazz) {
		return String.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		String value = (String) target;
		if (isNotNullAndNotEmpty(value)) {
			if (!isPatternMatched(ALPHA_NUMERIC_PATTERN, value)) {
				throw new RuntimeException(
						"Value should be alpha numeric. And it can contain '-', '_', '@'");
			}
		} else {
			throw new RuntimeException("Value must not not be empty");
		}

	}

	private boolean isNotNullAndNotEmpty(String value) {
		if (value == null || !StringUtils.hasText(value.toString()))
			return false;
		return true;
	}

	private boolean isPatternMatched(Pattern pattern, String input) {
		return pattern.matcher(input).matches();
	}

}
