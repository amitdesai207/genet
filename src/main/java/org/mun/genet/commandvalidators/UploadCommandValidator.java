package org.mun.genet.commandvalidators;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.mun.genet.configurer.Configuration;
import org.mun.genet.model.UploadModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UploadCommandValidator implements Validator {

	Logger logger = LoggerFactory.getLogger(UploadCommandValidator.class);
	private static final Pattern CSV_FILE_EXTENSION_PATTERN = Pattern.compile("([^.]+(\\.(?i)(csv)|(txt)|(tsv))$)");
	private static final Pattern FASTA_FILE_EXTENSION_PATTERN = Pattern.compile("([^.]+(\\.(?i)(fasta)|(txt)|(fa))$)");
	private static final Pattern NUMERIC_PATTERN = Pattern.compile("\\d+");

	private List<String> organismKeggIds = new ArrayList<>();

	@Autowired
	private Configuration dbConfiguration;

	public UploadCommandValidator() {
		try {
			Properties cachedProperties = new Properties();
			InputStream input = null;
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL resource = classLoader.getResource("app.properties");
			input = new FileInputStream(resource.getFile());
			cachedProperties.load(input);
			input.close();

			URL url = new URL((String) cachedProperties.get("organismKeggIds"));
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection
					.setRequestProperty("User-Agent",
							"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");

			BufferedReader responseReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			while ((line = responseReader.readLine()) != null) {
				organismKeggIds.add(line.trim().split("\\s")[1]);
			}
			responseReader.close();
			urlConnection.disconnect();
		} catch (Exception ex) {
		}
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return UploadModel.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		UploadModel uploadModel = (UploadModel) target;

		if (uploadModel.getTaxonomyId() == null || uploadModel.getTaxonomyId().isEmpty()) {
			errors.rejectValue("taxonomyId", null, "Taxonomy Id is required and cannot be empty");
		} else if (!isPatternMatched(NUMERIC_PATTERN, uploadModel.getTaxonomyId())) {
			errors.rejectValue("taxonomyId", null, "Only numbers are allowed for Taxonomy Id");
		} else {
			String organismName = isTaxonomyIdValid(uploadModel.getTaxonomyId());
			if (organismName == null)
				errors.rejectValue("taxonomyId", null,
						"A valid Taxonomy Id is required. Please refer the NCBI database for correct Id");
			else
				uploadModel.setOrganismName(organismName);
		}

		if (uploadModel.getPubmedId() == null || uploadModel.getPubmedId().isEmpty()) {
			errors.rejectValue("pubmedId", null, "PubMed Id is required and cannot be empty");
		} else if (!isPatternMatched(NUMERIC_PATTERN, uploadModel.getPubmedId())) {
			errors.rejectValue("pubmedId", null, "Only numbers are allowed for PubMed Id");
		} else if (!isPubMedIdValid(uploadModel.getPubmedId())) {
			errors.rejectValue("pubmedId", null,
					"A valid PubMed Id is required. Please refer the NCBI database for correct Id");
		}

		// Validate File
		if (uploadModel.getGeneExpressionFile() == null || uploadModel.getGeneExpressionFile().getSize() == 0) {
			errors.rejectValue("geneExpressionFile", null, "Please select Gene Expression Matrix file to upload");
		} else if (!isPatternMatched(CSV_FILE_EXTENSION_PATTERN, uploadModel.getGeneExpressionFile()
				.getOriginalFilename())) {
			errors.rejectValue("geneExpressionFile", null, "Only csv files are accepted for Gene Expression Matrix");
		}

		if (uploadModel.getCorrelation() == null || uploadModel.getCorrelation().isEmpty()) {
			errors.rejectValue("correlation", null, "Valid Correlation value must be selected");
		}

		if (uploadModel.getNetworktype() == null || uploadModel.getNetworktype().isEmpty()) {
			errors.rejectValue("networktype", null, "Valid Network Type must be selected");
		}

		if (!(1 <= uploadModel.getThreshold() && uploadModel.getThreshold() < 30)) {
			errors.rejectValue("threshold", null, "Threshold value shound be between 1  and 29");
		}

		if (uploadModel.getGeneTable() == null || uploadModel.getGeneTable().getSize() == 0) {
			errors.rejectValue("geneTable", null, "Please select Gene Information Table file to upload");
		} else if (!isPatternMatched(CSV_FILE_EXTENSION_PATTERN, uploadModel.getGeneTable().getOriginalFilename())) {
			errors.rejectValue("geneTable", null, "Only csv files are accepted for Gene Information Table");
		}

		if (uploadModel.getConditionTable() == null || uploadModel.getConditionTable().getSize() == 0) {
			errors.rejectValue("conditionTable", null, "Please select Condition Information Table file to upload");
		} else if (!isPatternMatched(CSV_FILE_EXTENSION_PATTERN, uploadModel.getConditionTable().getOriginalFilename())) {
			errors.rejectValue("conditionTable", null, "Only csv files are accepted for Condition Information Table");
		}

		boolean keggId = false;
		boolean tuAnnotation = false;
		boolean pcAnnotation = false;
		boolean pfamAnnotation = false;
		boolean proteinSeq = false;
		boolean pathwayAnnotation = false;

		if (!uploadModel.getKeggId().isEmpty() && organismKeggIds.contains(uploadModel.getKeggId().trim())) {
			keggId = true;
		}

		if (uploadModel.getTuAnnotationFile() != null
				&& uploadModel.getTuAnnotationFile().getSize() != 0
				&& isPatternMatched(CSV_FILE_EXTENSION_PATTERN, uploadModel.getTuAnnotationFile().getOriginalFilename())) {
			tuAnnotation = true;
		}

		if (uploadModel.getPcAnnotationFile() != null
				&& uploadModel.getPcAnnotationFile().getSize() != 0
				&& isPatternMatched(CSV_FILE_EXTENSION_PATTERN, uploadModel.getPcAnnotationFile().getOriginalFilename())) {
			tuAnnotation = true;
		}

		if (uploadModel.getPfamAnnotationFile() != null
				&& uploadModel.getPfamAnnotationFile().getSize() != 0
				&& isPatternMatched(CSV_FILE_EXTENSION_PATTERN, uploadModel.getPfamAnnotationFile()
						.getOriginalFilename())) {
			tuAnnotation = true;
		}

		if (uploadModel.getProteinSeqFile() != null && uploadModel.getProteinSeqFile().getSize() != 0
				&& isPatternMatched(FASTA_FILE_EXTENSION_PATTERN, uploadModel.getProteinSeqFile().getOriginalFilename())) {
			tuAnnotation = true;
		}

		if (uploadModel.getPathwayAnnotationFile() != null
				&& uploadModel.getPathwayAnnotationFile().getSize() != 0
				&& isPatternMatched(CSV_FILE_EXTENSION_PATTERN, uploadModel.getPathwayAnnotationFile()
						.getOriginalFilename())) {
			tuAnnotation = true;
		}

		if (!(keggId || tuAnnotation || pcAnnotation || (pfamAnnotation || proteinSeq) || pathwayAnnotation)) {
			if (uploadModel.getKeggId() == null || uploadModel.getKeggId().isEmpty()) {
				errors.rejectValue("keggId", null, "KEGG Id is required and cannot be empty");
			} else if (!organismKeggIds.contains(uploadModel.getKeggId().trim())) {
				errors.rejectValue("keggId", null,
						"A valid KEGG Id is required. Please refer the NCBI database for correct Id");
			}

			if (uploadModel.getTuAnnotationFile() == null || uploadModel.getTuAnnotationFile().getSize() == 0) {
				errors.rejectValue("tuAnnotationFile", null,
						"Please select Transcriptional Unit annotation file to upload");
			} else if (!isPatternMatched(CSV_FILE_EXTENSION_PATTERN, uploadModel.getTuAnnotationFile()
					.getOriginalFilename())) {
				errors.rejectValue("tuAnnotationFile", null,
						"Only .csv/.txt/.tsv files are accepted for Transcriptional Unit annotation");
			}

			if (uploadModel.getPcAnnotationFile() == null || uploadModel.getPcAnnotationFile().getSize() == 0) {
				errors.rejectValue("pcAnnotationFile", null, "Please select Protein Complex Annotation file to upload");
			} else if (!isPatternMatched(CSV_FILE_EXTENSION_PATTERN, uploadModel.getPcAnnotationFile()
					.getOriginalFilename())) {
				errors.rejectValue("pcAnnotationFile", null,
						"Only .csv/.txt/.tsv files are accepted for Protein Complex Annotation");
			}

			if (uploadModel.getPfamAnnotationFile() == null || uploadModel.getPfamAnnotationFile().getSize() == 0) {
				errors.rejectValue("pfamAnnotationFile", null, "Please select Pfam Annotation file to upload");
			} else if (!isPatternMatched(CSV_FILE_EXTENSION_PATTERN, uploadModel.getPfamAnnotationFile()
					.getOriginalFilename())) {
				errors.rejectValue("pfamAnnotationFile", null, "Only .csv/.txt/.tsv files are accepted for Pfam Annotation");
			}

			if (uploadModel.getProteinSeqFile() == null || uploadModel.getProteinSeqFile().getSize() == 0) {
				errors.rejectValue("proteinSeqFile", null, "Please select Protein Sequence file to upload");
			} else if (!isPatternMatched(FASTA_FILE_EXTENSION_PATTERN, uploadModel.getProteinSeqFile()
					.getOriginalFilename())) {
				errors.rejectValue("proteinSeqFile", null, "Only .fasta/.fa/.txt files are accepted for Protein Sequence");
			}

			if (uploadModel.getPathwayAnnotationFile() == null || uploadModel.getPathwayAnnotationFile().getSize() == 0) {
				errors.rejectValue("pathwayAnnotationFile", null, "Please select Pathway Annotation file to upload");
			} else if (!isPatternMatched(CSV_FILE_EXTENSION_PATTERN, uploadModel.getPathwayAnnotationFile()
					.getOriginalFilename())) {
				errors.rejectValue("pathwayAnnotationFile", null, "Only .csv/.txt/.tsv files are accepted for Pathway Annotation");
			}
		}

	}

	private String isTaxonomyIdValid(String taxonomyId) {
		try {
			URL url = new URL(dbConfiguration.getConfigurationByName("taxonomyId") + taxonomyId.trim());
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection
					.setRequestProperty("User-Agent",
							"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");

			BufferedReader responseReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			StringBuffer xmlResponse = new StringBuffer();
			String line = null;
			while ((line = responseReader.readLine()) != null) {
				xmlResponse.append(line.trim());
			}
			responseReader.close();
			urlConnection.disconnect();
			
			SAXBuilder saxBuilder = new SAXBuilder();
			Document doc = saxBuilder.build(new StringReader(xmlResponse.toString()));
			Element DocSum = doc.getRootElement().getChildren().get(0);
			if (!DocSum.getName().equalsIgnoreCase("ERROR")) {
				List<Element> items = DocSum.getChildren("Item");
				for (Element element : items) {
					if (element.getAttribute("Name").getValue().equalsIgnoreCase("ScientificName")) {
						return element.getValue();
					}
				}
			}
			return null;
		} catch (Exception ex) {
			return null;
		}
	}

	private boolean isPubMedIdValid(String pubMedId) {
		try {
			URL url = new URL(dbConfiguration.getConfigurationByName("pubMedId") + pubMedId.trim());
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection
					.setRequestProperty("User-Agent",
							"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");

			BufferedReader responseReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			StringBuffer xmlResponse = new StringBuffer();
			String line = null;
			while ((line = responseReader.readLine()) != null) {
				xmlResponse.append(line.trim());
			}
			responseReader.close();
			urlConnection.disconnect();

			SAXBuilder saxBuilder = new SAXBuilder();
			Document doc = saxBuilder.build(new StringReader(xmlResponse.toString()));
			Element DocSum = doc.getRootElement().getChildren().get(0);
			if (!DocSum.getName().equalsIgnoreCase("ERROR")) {
				return true;
			}
			return false;
		} catch (Exception ex) {
			return false;
		}
	}

	private boolean isPatternMatched(Pattern pattern, String input) {
		return pattern.matcher(input).matches();
	}

}
