package org.mun.genet.commandvalidators;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class EmailValidator implements Validator {

	private static final Pattern EMAIL_VALIDATOR_PATTERN = Pattern
			.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
	Logger logger = LoggerFactory.getLogger(EmailValidator.class);

	@Override
	public boolean supports(Class<?> arg0) {
		return String.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object obj, Errors errors) {

		String emailAddr = (String) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailAddress", null, "Email address is required");
		if (isNotNullAndEmpty(emailAddr)) {
			if (!isPatternMatched(EMAIL_VALIDATOR_PATTERN, emailAddr)) {
				errors.rejectValue("emailAddress", null, "Invalid Email Address format");
			}
		}

	}

	private boolean isNotNullAndEmpty(String input) {
		if (input == null || input.isEmpty())
			return false;
		return true;
	}

	private boolean isPatternMatched(Pattern pattern, String input) {
		return pattern.matcher(input).matches();
	}

}
