package org.mun.genet.auth;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mun.genet.daos.entities.Role.UserRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationSuccessHandler.class);

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		request.getSession().invalidate();
		request.getSession(true);
		Set<String> obtainedRoles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
		String userRole = null;
		if (obtainedRoles.contains(UserRoles.Web_Tool_Admin.getUserRole()))
			userRole = UserRoles.Web_Tool_Admin.getUserRole();
		else
			userRole = (String) obtainedRoles.toArray()[0];
		try {
			String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			SessionCache.processLogin(username, request.getSession(), userRole);
		} catch (Exception e) {
			logger.warn("error processing login " + e);
			// Some time java.lang.IllegalStateException: invalidate exception
			// thrown by above method so checking role set or not before
			// redirect to home
			if (request.getSession().getAttribute("ROLE") == null) {
				request.getSession().setAttribute("ROLE", userRole);
			}
		}
		if ((boolean) SecurityContextHolder.getContext().getAuthentication().getCredentials()) {
			request.getSession().setAttribute("recoveryMode", true);
			response.sendRedirect("reset");
		} else
			response.sendRedirect("jobHomePage");
	}
}
