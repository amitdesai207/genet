package org.mun.genet.auth;

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import org.mun.genet.daos.entities.User;
import org.mun.genet.daos.repositories.UserRepository;
import org.mun.genet.encryptors.SymmetricAesEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;

public class DatabaseBasedAuthentication implements AuthenticationManager, ApplicationContextAware {

	private static final Logger logger = LoggerFactory.getLogger(DatabaseBasedAuthentication.class);

	private ApplicationContext applicationContext;

	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		String QDBusername = auth.getName();
		String QDBpassword = (String) auth.getCredentials();

		UserRepository userRepository = (UserRepository) applicationContext.getBean("userRepository");
		User user = userRepository.findByUserName(QDBusername);
		if (user == null) {
			logger.error("Error authenticating. Username not found");
			throw new BadCredentialsException("You have entered invalid Username or Password");
		}
		String password = new SymmetricAesEncryptor().decrypt(user.getPassword(),
				getDatabaseEncryptKey("DBEntryEncrptKey"));
		if (!password.equals(QDBpassword)) {
			logger.error("Error authenticating. Password does not match");
			throw new BadCredentialsException("You have entered invalid Username or Password");
		}
		
		if (!user.isActive()) {
			logger.error("Error authenticating. User has not verified email");
			throw new BadCredentialsException("Your email has not been verified yet. Please verify your email to login");
		}

		Collection<GrantedAuthority> athorities = new ArrayList<GrantedAuthority>();
		athorities.add(new GrantedAuthorityImpl(user.getRole()));

		if (athorities.size() == 0) {
			logger.error("Error authenticating with database. login.insufficientCredentials");
			throw new InsufficientAuthenticationException("You do not have sufficient privilege to access the site");
		}

		return new UsernamePasswordAuthenticationToken(auth.getName(), user.isReset(), athorities);
	}

	private String getDatabaseEncryptKey(String string) {
		Properties cachedProperties = new Properties();
		InputStream input = null;
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL resource = classLoader.getResource("app.properties");
			input = new FileInputStream(resource.getFile());
			// load a properties file
			cachedProperties.load(input);
			return cachedProperties.getProperty(string);
		} catch (IOException ex) {
			logger.error("Unable to load properties file: ", ex);
		} finally {
			safeClose(input);
		}
		logger.info("Configuration initialized");
		return null;
	}

	private void safeClose(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (IOException e) {
				logger.warn("Unable to close " + closeable);
			}
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
