package org.mun.genet.auth;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This cache keeps track of user sessions. It allows only one session per user.
 */
public class SessionCache {
	private static Map<String, HttpSession> sessions = new ConcurrentHashMap<String, HttpSession>();
	private static final Logger logger = LoggerFactory.getLogger(SessionCache.class);

	static synchronized void processLogin(String user, HttpSession newSession, String role) {
		if (sessions.containsKey(user)) {
			try {
				sessions.put(user, newSession).invalidate();
			} catch (Exception e) {
				logger.error("Session already invalidated");
			}
			// if already login
		} else {
			// First time login
			sessions.put(user, newSession);
		}
		newSession.setAttribute("ROLE", role);
	}

	public static synchronized void processLogout(String user) {
		sessions.remove(user);
	}
}
