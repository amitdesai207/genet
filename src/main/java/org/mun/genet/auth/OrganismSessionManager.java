package org.mun.genet.auth;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

@Component
public class OrganismSessionManager {

	private Map<HttpSession, String> organismSessions = new ConcurrentHashMap<HttpSession, String>();

	public String getSessionOrganism(HttpSession session, String organism) {
		if (organismSessions.containsKey(session) && (organism == null || organism.equals("------Please Select------"))) {
			organism = organismSessions.get(session);
		} else if (organism != null) {
			organismSessions.put(session, organism);
		}
		return organism;
	}
}
