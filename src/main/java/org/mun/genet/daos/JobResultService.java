package org.mun.genet.daos;

import java.util.List;

import org.mun.genet.vo.JobResultVo;

public interface JobResultService {

	public void createJobResult(JobResultVo jobResultVo);

	public List<JobResultVo> findAllResultsForJob(String jobId);
}
