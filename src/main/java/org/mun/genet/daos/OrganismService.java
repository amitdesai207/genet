package org.mun.genet.daos;

import java.util.ArrayList;
import java.util.List;

import org.mun.genet.vo.OrganismVo;

public interface OrganismService {

	public List<String> getOrganismList(String role);

	public List<OrganismVo> getOrganisms();

	public List<ArrayList<String>> getOrganismToApprove();

	public String approveOrganism(String organismId);

	public void rejectOrganism(String organismId);

	public void markDownloadReady(String organismId);

	public Long getOrganismId(String organismName);
	
	public String getFileStorePath(String organismName);
}
