package org.mun.genet.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JobCleanupServiceImpl implements JobCleanupService {

	private static final Logger logger = LoggerFactory.getLogger(JobCleanupServiceImpl.class);

	@Autowired
	private DataSource dataSource;

	@Override
	public void cleanupDatabase(String organismName) {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			String databaseName = organismName.trim().toLowerCase().replace(" ", "_");
			PreparedStatement preparedStatement = connection.prepareStatement("drop database " + databaseName);
			preparedStatement.execute();
			preparedStatement = connection.prepareStatement("delete from Organism where name = ?");
			preparedStatement.setString(1, organismName);
			preparedStatement.execute();
			connection.close();
		} catch (Exception ex) {
			logger.error("Cannot cleanup database" + ex);
		}
	}

}