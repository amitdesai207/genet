package org.mun.genet.daos;

import java.util.Date;
import java.util.List;

import org.mun.genet.configurer.ImportJobStatusType;
import org.mun.genet.vo.JobVo;

public interface JobService {

	public List<JobVo> findAllPendingJobs();

	public void markJobAsComplete(Long jobId, Date jobStartTime, Date jobEndTime, ImportJobStatusType status);

	public void markJobAsActive(Long jobId, Date jobStartTime);

	public JobVo addJob(JobVo jobVo);

	public int findActiveJobCount();

	public List<JobVo> findAllJobsForUser(String userEmail, String role);

	public List<JobVo> findAll();

	public JobVo findByDisplayId(String id, String role);

	public List<JobVo> findAllRunningJobs();

	public void markJobAsInturrupted(Long jobId);
	
	public void markJobAsPending(String jobId);
	
	public void markJobAsFailed(String jobId);

	public void updateFileStoreId(Long jobId, Long fileStoreId);
	
	public boolean isJobActive(Long jobId);

	public void stopJobByUser(String jobId);
	
	public void approveOverrideJob(String jobId);
	
}
