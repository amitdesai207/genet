package org.mun.genet.daos;

import org.mun.genet.vo.UserVo;

public interface UserService {

	public void addUser(UserVo userVo);

	public boolean isUsernamePresent(UserVo userVo);

	public UserVo isUsernameAuthentic(UserVo userVo);

	public boolean isUserEmailPresent(UserVo userVo);
	
	public void resetPasswordForUserWithResetRequest();

	public UserVo resetPassword(UserVo userVo);

	public void resetUserPassword(UserVo userVo);
	
	public boolean activateUser(String key);
}
