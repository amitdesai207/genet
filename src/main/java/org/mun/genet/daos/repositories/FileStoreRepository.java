package org.mun.genet.daos.repositories;

import java.util.Date;
import java.util.List;

import org.mun.genet.daos.entities.FileStore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FileStoreRepository extends JpaRepository<FileStore, Long> {

	public FileStore findByDisplayId(String displayId);

	@Modifying
	@Query("delete from FileStore file where file.dateCompleted < :startDate")
	public void deleteOutdatedJobFiles(@Param("startDate") Date startDate);

	@Query("SELECT fileStore FROM FileStore fileStore WHERE fileStore.dateCompleted < :startDate")
	public List<FileStore> selectOutdatedJobFiles(@Param("startDate") Date startDate);	
}
