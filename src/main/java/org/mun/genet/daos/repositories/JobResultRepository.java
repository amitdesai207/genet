package org.mun.genet.daos.repositories;

import java.util.Date;
import java.util.List;

import org.mun.genet.daos.entities.Job;
import org.mun.genet.daos.entities.JobResult;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobResultRepository extends JpaRepository<JobResult, Long> {

	public List<JobResult> findByJob(Job job);

	public List<JobResult> findByDateCreated(Date date);

	public List<JobResult> findByJobAndDateCreated(Job job, Date date);

	public List<JobResult> findByDateCreatedBetween(Date startDate, Date endDate);

	public List<JobResult> findByJobAndDateCreatedBetween(Job job, Date startDate, Date endDate);

}
