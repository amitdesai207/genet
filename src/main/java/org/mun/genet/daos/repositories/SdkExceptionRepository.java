package org.mun.genet.daos.repositories;

import java.util.Date;
import java.util.List;

import org.mun.genet.daos.entities.Job;
import org.mun.genet.daos.entities.SdkException;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SdkExceptionRepository extends JpaRepository<SdkException, Long> {

	public List<SdkException> findByJob(Job job);

	public List<SdkException> findByDateCreated(Date date);

	public List<SdkException> findByDateCreatedBetween(Date startDate, Date endDate);

	public List<SdkException> findByJobAndDateCreated(Job job, Date date);

	public List<SdkException> findByJobAndDateCreatedBetween(Job job, Date startDate, Date endDate);

	public SdkException findByJobAndEntity(Job job, String entity);
}
