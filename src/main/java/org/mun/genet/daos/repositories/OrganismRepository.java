package org.mun.genet.daos.repositories;

import java.util.List;

import org.mun.genet.daos.entities.Organism;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrganismRepository extends JpaRepository<Organism, Long> {

	Organism findByName(String name);

	List<Organism> findByApproved(boolean status);
}
