package org.mun.genet.daos.repositories;

import java.util.List;

import org.mun.genet.daos.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

	List<User> findAllByUserEmail(String userEmailCanonical);

	User findByUserEmail(String userEmailCanonical);

	User findByUserName(String userEmailCanonical);

	User findByUserNameAndPassword(String username, String password);
	
	List<User> findAllByReset(boolean reset);
	
	User findByActivation(String key);
}
