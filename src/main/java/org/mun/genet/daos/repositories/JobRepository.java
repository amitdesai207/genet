package org.mun.genet.daos.repositories;

import java.util.Date;
import java.util.List;

import org.mun.genet.daos.entities.Job;
import org.mun.genet.daos.entities.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobRepository extends JpaRepository<Job, Long> {

	List<Job> findByStatus(String status);

	List<Job> findByUser(User user);

	List<Job> findByUserAndStatus(User user, String status);

	List<Job> findByDateCreated(Date date);

	List<Job> findByDateCreatedBetween(Date startDate, Date endDate);

	List<Job> findByStatusAndDateCreated(String status, Date date);

	List<Job> findByStatusAndDateCreatedBetween(String status, Date startDate, Date endDate);

	Job findByDisplayId(String displayId);

	Job findByDisplayIdAndUser(String displayId, User user);

	List<Job> findByDisplayId(String displayId, Sort sort);

	Job findById(Long id);

}
