package org.mun.genet.daos.repositories;

import java.util.Date;
import java.util.List;

import org.mun.genet.daos.entities.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification, Long> {

	public List<Notification> findByReceiver(String receiver);

	public List<Notification> findByReceiverAndLastUpdated(String receiver, Date date);

	public List<Notification> findByReceiverAndLastUpdatedBetween(String receiver, Date startDate,
			Date endDate);

	public List<Notification> findByStatus(String status);
}
