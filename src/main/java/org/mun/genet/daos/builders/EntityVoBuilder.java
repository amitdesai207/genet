package org.mun.genet.daos.builders;

import org.mun.genet.daos.entities.FileStore;
import org.mun.genet.daos.entities.Job;
import org.mun.genet.daos.entities.JobResult;
import org.mun.genet.daos.entities.Notification;
import org.mun.genet.daos.entities.Organism;
import org.mun.genet.daos.entities.SdkException;
import org.mun.genet.daos.entities.User;
import org.mun.genet.vo.FileStoreVo;
import org.mun.genet.vo.JobResultVo;
import org.mun.genet.vo.JobVo;
import org.mun.genet.vo.NotificationVo;
import org.mun.genet.vo.OrganismVo;
import org.mun.genet.vo.SdkExceptionVo;
import org.mun.genet.vo.UserVo;

public interface EntityVoBuilder {

	public JobVo buildJobVoFromEntity(Job job);

	public JobResultVo buildJobResultVoFromEntity(JobResult jobResult);

	public NotificationVo buildNotificationVoFromEntity(Notification notification);

	public SdkExceptionVo buildSdkExceptionVoFromEntity(SdkException sdkException);

	public Job updateJobEntityFromVo(JobVo jobVo, Job job);

	public Job buildJobEntityFromVo(JobVo jobVo);

	public JobResult buildJobResultEntityFromVo(JobResultVo jobResultVo);

	public Notification buildNotificationEntityFromVo(NotificationVo notificationVo);

	public SdkException buildSdkExceptionEntityFromVo(SdkExceptionVo sdkExceptionVo);

	public UserVo buildUserVoFromEntity(User user);

	public User buildUserEntityFromVo(UserVo userVo);

	public OrganismVo buildOrganism(Organism organism);

	public FileStoreVo buildFileStoreVoFromEntity(FileStore fileStore);

}
