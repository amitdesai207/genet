package org.mun.genet.daos.builders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mun.genet.configurer.Configuration;
import org.mun.genet.daos.entities.FileStore;
import org.mun.genet.daos.entities.Job;
import org.mun.genet.daos.entities.JobResult;
import org.mun.genet.daos.entities.Notification;
import org.mun.genet.daos.entities.Organism;
import org.mun.genet.daos.entities.SdkException;
import org.mun.genet.daos.entities.User;
import org.mun.genet.vo.FileStoreVo;
import org.mun.genet.vo.JobResultVo;
import org.mun.genet.vo.JobVo;
import org.mun.genet.vo.NotificationVo;
import org.mun.genet.vo.OrganismVo;
import org.mun.genet.vo.SdkExceptionVo;
import org.mun.genet.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EntityVoBuilderImpl implements EntityVoBuilder {

	@Autowired
	private Configuration configuration;

	@Override
	public JobVo buildJobVoFromEntity(Job job) {

		JobVo jobVo = new JobVo();
		jobVo.setId(job.getId());
		jobVo.setDisplayId(job.getDisplayId());
		jobVo.setDataPath(job.getDataPath());
		jobVo.setDateCreated(job.getDateCreated());
		jobVo.setJobEndTime(job.getJobEndTime());
		jobVo.setJobStartTime(job.getJobStartTime());
		jobVo.setLastUpdated(job.getLastUpdated());
		jobVo.setStatus(job.getStatus());
		jobVo.setBrowser(job.getBrowser());
		jobVo.setOperatingSystem(job.getOperatingSystem());
		jobVo.setClientIP(job.getClientIP());
		jobVo.setFileSize(job.getFileSize());
		jobVo.setUserEmail(job.getUser().getUserEmail());
		jobVo.setOverrideExistingDatabase(job.getApprovedOverride());
		jobVo.setCorrelation(job.getCorrelation());
		jobVo.setKeggId(job.getKeggId());
		jobVo.setNetworkType(job.getNetworkType());
		jobVo.setPubmedId(job.getPubmedId());
		jobVo.setTaxonomyId(job.getTaxonomyId());
		jobVo.setThreshold(job.getThreshold());
		jobVo.setOrganism(job.getOrganism());
		return jobVo;
	}

	@Override
	public JobResultVo buildJobResultVoFromEntity(JobResult jobResult) {
		JobResultVo jobResultVo = new JobResultVo();
		jobResultVo.setId(jobResult.getId());
		jobResultVo.setEntity(jobResult.getEntity());
		jobResultVo.setRecordsReceived(jobResult.getRecordsReceived());
		jobResultVo.setRecordsFailed(jobResult.getRecordsFailed());
		jobResultVo.setRecordsSuccessful(jobResult.getRecordsSuccessful());
		jobResultVo.setJobId(jobResult.getJob().getId());
		jobResultVo.setDateCreated(jobResult.getDateCreated());
		jobResultVo.setLastUpdated(jobResult.getLastUpdated());
		return jobResultVo;
	}

	@Override
	public NotificationVo buildNotificationVoFromEntity(Notification notification) {

		NotificationVo notificationVo = new NotificationVo();
		notificationVo.setCopiedReceiver(notification.getCopiedReceiver());
		notificationVo.setMessageToSend(notification.getMessageToSend());
		notificationVo.setId(notification.getId());
		notificationVo.setNumberOfRetries(notification.getNumberOfRetries());
		notificationVo.setReceiver(notification.getReceiver());
		notificationVo.setStatus(notification.getStatus());
		notificationVo.setSender(notification.getSender());
		notificationVo.setDateCreated(notification.getDateCreated());
		notificationVo.setLastUpdated(notification.getLastUpdated());
		notificationVo.setSubject(notification.getSubject());
		return notificationVo;
	}

	@Override
	public SdkExceptionVo buildSdkExceptionVoFromEntity(SdkException sdkException) {
		SdkExceptionVo sdkVo = new SdkExceptionVo();
		sdkVo.setCauseMessage(sdkException.getCauseMessage());
		sdkVo.setCustomMessage(sdkException.getCustomMessage());
		sdkVo.setDateCreated(sdkException.getDateCreated());
		sdkVo.setEntity(sdkException.getEntity());
		sdkVo.setErrDetail(sdkException.getErrDetail());
		sdkVo.setId(sdkException.getId());
		sdkVo.setJobId(sdkException.getJob().getId());
		sdkVo.setLastUpdated(sdkException.getLastUpdated());
		sdkVo.setLineNo(sdkException.getLineNo());
		return sdkVo;
	}

	@Override
	public Job buildJobEntityFromVo(JobVo jobVo) {
		return updateJobEntityFromVo(jobVo, null);
	}

	@Override
	public Job updateJobEntityFromVo(JobVo jobVo, Job job) {
		Date now = new Date();
		if (job == null)
			job = new Job();
		job.setDisplayId(jobVo.getDisplayId());
		job.setDataPath(jobVo.getDataPath());
		job.setDateCreated(now);
		job.setJobEndTime(jobVo.getJobEndTime());
		job.setJobStartTime(jobVo.getJobStartTime());
		job.setLastUpdated(now);
		job.setStatus(jobVo.getStatus());
		job.setClientIP(jobVo.getClientIP());
		job.setBrowser(jobVo.getBrowser());
		job.setOperatingSystem(jobVo.getOperatingSystem());
		job.setFileSize(jobVo.getFileSize());
		job.setApprovedOverride(jobVo.isOverrideExistingDatabase());
		job.setCorrelation(jobVo.getCorrelation());
		job.setKeggId(jobVo.getKeggId());
		job.setNetworkType(jobVo.getNetworkType());
		job.setPubmedId(jobVo.getPubmedId());
		job.setTaxonomyId(jobVo.getTaxonomyId());
		job.setThreshold(jobVo.getThreshold());
		job.setOrganism(jobVo.getOrganism());
		return job;
	}

	@Override
	public JobResult buildJobResultEntityFromVo(JobResultVo jobResultVo) {
		Date now = new Date();
		JobResult jobResult = new JobResult();
		jobResult.setDateCreated(now);
		jobResult.setEntity(jobResultVo.getEntity());
		jobResult.setLastUpdated(now);
		jobResult.setRecordsReceived(jobResultVo.getRecordsReceived());
		jobResult.setRecordsFailed(jobResultVo.getRecordsFailed());
		jobResult.setRecordsSuccessful(jobResultVo.getRecordsSuccessful());
		return jobResult;
	}

	@Override
	public Notification buildNotificationEntityFromVo(NotificationVo notificationVo) {
		Date now = new Date();
		Notification notification = new Notification();
		notification.setCopiedReceiver(notificationVo.getCopiedReceiver());
		notification.setDateCreated(now);
		notification.setLastUpdated(now);
		notification.setMessageToSend(notificationVo.getMessageToSend());
		notification.setNumberOfRetries(notificationVo.getNumberOfRetries());
		notification.setReceiver(notificationVo.getReceiver());
		notification.setStatus(notificationVo.getStatus());
		notification.setSubject(notificationVo.getSubject());
		notification.setSender(notificationVo.getSender());
		return notification;
	}

	@Override
	public SdkException buildSdkExceptionEntityFromVo(SdkExceptionVo sdkExceptionVo) {
		Date now = new Date();
		SdkException sdkException = new SdkException();
		if (sdkExceptionVo.getCauseMessage().length() > 250)
			sdkException.setCauseMessage(sdkExceptionVo.getCauseMessage().substring(0, 250));
		else
			sdkException.setCauseMessage(sdkExceptionVo.getCauseMessage());
		sdkException.setCustomMessage(sdkExceptionVo.getCustomMessage());
		sdkException.setDateCreated(now);
		sdkException.setEntity(sdkExceptionVo.getEntity());
		sdkException.setErrDetail(sdkExceptionVo.getErrDetail());
		sdkException.setLastUpdated(now);
		sdkException.setLineNo(sdkExceptionVo.getLineNo());
		return sdkException;
	}

	@Override
	public UserVo buildUserVoFromEntity(User user) {
		UserVo userVo = new UserVo();
		userVo.setDateCreated(user.getDateCreated());
		userVo.setId(user.getId());
		userVo.setLastUpdated(user.getLastUpdated());
		userVo.setUserName(user.getUserName());
		userVo.setUserEmail(user.getUserEmail());
		userVo.setInstitute(user.getInstitute());
		userVo.setPassword(user.getPassword());
		userVo.setRole(user.getRole());
		userVo.setFirstName(user.getFirstName());
		userVo.setLastName(user.getLastName());
		userVo.setConsent(user.isConsent());
		userVo.setReset(user.isReset());
		userVo.setActive(user.isActive());
		userVo.setActivation(user.getActivation());
		return userVo;
	}

	@Override
	public User buildUserEntityFromVo(UserVo userVo) {
		Date now = new Date();
		User user = new User();
		user.setDateCreated(now);
		user.setLastUpdated(now);
		user.setUserEmail(userVo.getUserEmail());
		user.setUserName(userVo.getUserName());
		user.setInstitute(userVo.getInstitute());
		user.setPassword(userVo.getPassword());
		user.setRole(userVo.getRole());
		user.setFirstName(userVo.getFirstName());
		user.setLastName(userVo.getLastName());
		user.setConsent(userVo.isConsent());
		user.setReset(userVo.isReset());
		user.setActive(userVo.isActive());
		user.setActivation(userVo.getActivation());
		return user;
	}

	@Override
	public OrganismVo buildOrganism(Organism organism) {
		String pubmedIds = organism.getPubmedIds();
		List<String> pubmeds = new ArrayList<>();
		OrganismVo organismVo = new OrganismVo();
		organismVo.setName(organism.getName());
		organismVo.setFilePathId(organism.getFilePathId());
		if (pubmedIds != null && !pubmedIds.isEmpty()) {
			for (String pubmedId : pubmedIds.split(",")) {
				pubmeds.add(configuration.getConfigurationByName("pubmedArticleLink") + pubmedId);
			}
			organismVo.setPubmedIds(pubmeds);
		}
		return organismVo;
	}

	@Override
	public FileStoreVo buildFileStoreVoFromEntity(FileStore fileStore) {
		FileStoreVo fileStoreVo = new FileStoreVo();
		fileStoreVo.setId(fileStore.getId());
		fileStoreVo.setDisplayId(fileStore.getDisplayId());
		fileStoreVo.setFilePath(fileStore.getFilePath());
		return fileStoreVo;
	}
}
