package org.mun.genet.daos;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.mun.genet.configurer.Configuration;
import org.mun.genet.daos.builders.EntityVoBuilder;
import org.mun.genet.daos.entities.FileStore;
import org.mun.genet.daos.repositories.FileStoreRepository;
import org.mun.genet.util.Utility;
import org.mun.genet.vo.FileStoreVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

@Repository
@Transactional(readOnly = true)
public class FileStoreServiceImpl implements FileStoreService {

	private static final Logger logger = LoggerFactory.getLogger(FileStoreServiceImpl.class);

	@Autowired
	private FileStoreRepository fileRepository;
	@Autowired
	private Configuration configuration;
	@Autowired
	private EntityVoBuilder builder;

	@Override
	@Transactional
	public FileStoreVo storeFile(MultipartFile file, String username) throws IOException {

		Date now = new Date();
		FileStore fileStoreObject = new FileStore();
		fileStoreObject.setDateCreated(now);

		String fileBase = configuration.getConfigurationByName("file.storage.path");

		File userFolder = new File(fileBase + File.separator + username + File.separator
				+ new SimpleDateFormat("MM-dd-yyyy-hh-mm").format(now));
		FileUtils.forceMkdir(userFolder);
		if (userFolder.exists()) {
			fileStoreObject.setFilePath(userFolder.getAbsolutePath());
			fileStoreObject = fileRepository.save(fileStoreObject);
			FileStoreVo fileStoreVo = builder.buildFileStoreVoFromEntity(fileStoreObject);
			return fileStoreVo;
		} else
			return null;
	}

	@Override
	@Transactional
	public void setCompeletedDate(Long fileStoreId) {

		Date now = new Date();
		FileStore fileStoreObject = fileRepository.findOne(fileStoreId);
		if (fileStoreObject != null) {
			fileStoreObject.setDateCompleted(now);
		}
	}

	@Override
	@Transactional
	public void setDisplayId(Long fileStoreId) {

		FileStore fileStoreObject = fileRepository.findOne(fileStoreId);
		if (fileStoreObject != null) {
			fileStoreObject.setDisplayId(Utility.generateRandomSequence(fileStoreObject.getId()));
		}
	}

	@Override
	public String getStoredFile(Long fileStoreId) throws Exception {

		FileStore fileStoreObject = fileRepository.findOne(fileStoreId);
		if (fileStoreObject != null) {
			return fileStoreObject.getFilePath();
		}

		throw new Exception("File is not available. FileStoreId: " + fileStoreId);
	}

	@Override
	public InputStream getFileStream(String fileStoreId) throws Exception {
		FileStore file = fileRepository.findByDisplayId(fileStoreId);
		if (file != null)
			return new FileInputStream(new File(file.getFilePath()));
		throw new Exception("File has been deleted");
	}

	@Override
	@Transactional
	public void pruneAllFileStoreObjects() {

		long deleteAfterTime = Long.parseLong(configuration.getConfigurationByName("storage.cleanup.file_after"));
		Date pruneDate = new Date(System.currentTimeMillis() - deleteAfterTime * 1000);

		List<FileStore> fileStores = fileRepository.selectOutdatedJobFiles(pruneDate);
		for (FileStore fileStore : fileStores) {
			try {
				File file = new File(fileStore.getFilePath());
				FileUtils.cleanDirectory(file);
				FileUtils.forceDelete(file);
			} catch (IOException ex) {
				logger.error("File at path " + fileStore.getFilePath() + "connot be deleted");
			}
			fileStore.setDateCompleted(null);
		}
		logger.info("Pruning files has been completed successfully");
	}

	private void safeClose(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (IOException e) {
				logger.warn("Unable to close " + closeable);
			}
		}
	}

	@Override
	public void storeFileInFileRespository(MultipartFile file, String folder, String fileName) throws Exception {
		FileOutputStream outputStream = new FileOutputStream(new File(folder + File.separator + fileName));
		FileCopyUtils.copy(file.getInputStream(), outputStream);
		safeClose(file.getInputStream());
		safeClose(outputStream);
	}

	@Override
	public void storeExistingFileInFileRespository(FileInputStream fileStream, String folder, String fileName)
			throws Exception {
		FileOutputStream outputStream = new FileOutputStream(new File(folder + File.separator + fileName));
		FileCopyUtils.copy(fileStream, outputStream);
		safeClose(fileStream);
		safeClose(outputStream);
	}

}
