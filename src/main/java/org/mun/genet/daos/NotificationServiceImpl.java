package org.mun.genet.daos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mun.genet.configurer.NotificationStatusType;
import org.mun.genet.daos.builders.EntityVoBuilder;
import org.mun.genet.daos.entities.Notification;
import org.mun.genet.daos.repositories.NotificationRepository;
import org.mun.genet.vo.NotificationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	private NotificationRepository notificationRepository;
	@Autowired
	private EntityVoBuilder builder;

	@Override
	@Transactional
	public void createNotification(NotificationVo notificationVo) {
		Notification notification = builder.buildNotificationEntityFromVo(notificationVo);
		notificationRepository.save(notification);
	}

	@Override
	@Transactional
	public void incrementRetryCountForNotification(Long notificationId, int allowedRetries) {
		if (notificationId == null) {
			throw new IllegalArgumentException(
					"incrementRetryCountForNotification: Null notificationId");
		}
		Notification notification = notificationRepository.findOne(notificationId);
		if (notification != null) {
			int numberOfRetries = notification.getNumberOfRetries();
			notification.setLastUpdated(new Date());
			if (notification.getNumberOfRetries() < allowedRetries) {
				numberOfRetries++;
				notification.setNumberOfRetries(numberOfRetries);
				notification.setStatus(NotificationStatusType.RETRY_NEEDED.toString());
			} else {
				notification.setStatus(NotificationStatusType.FAILED.toString());
			}
			notificationRepository.save(notification);
		} else {
			throw new IllegalArgumentException(
					"incrementRetryCountForNotification: Invalid notificationId");
		}
	}

	@Override
	@Transactional
	public void markNotificationAsFailed(Long notificationId) {
		if (notificationId == null) {
			throw new IllegalArgumentException("markNotificationAsFailed: Null notificationId");
		}
		Notification notification = notificationRepository.findOne(notificationId);
		if (notification != null) {
			notification.setLastUpdated(new Date());
			notification.setStatus(NotificationStatusType.FAILED.toString());
			notificationRepository.save(notification);
		} else {
			throw new IllegalArgumentException("markNotificationAsFailed: Invalid notificationId");
		}

	}

	@Override
	@Transactional
	public void markNotificationAsSent(Long notificationId) {
		if (notificationId == null) {
			throw new IllegalArgumentException("markNotificationAsSent: Null notificationId");
		}
		Notification notification = notificationRepository.findOne(notificationId);
		if (notification != null) {
			notification.setLastUpdated(new Date());
			notification.setStatus(NotificationStatusType.SENT.toString());
			notificationRepository.save(notification);
		} else {
			throw new IllegalArgumentException("markNotificationAsSent: Invalid notificationId");
		}

	}

	@Override
	public List<NotificationVo> findAllPendingNotifications() {
		List<Notification> entities = notificationRepository
				.findByStatus(NotificationStatusType.PENDING.toString());
		List<NotificationVo> result = new ArrayList<NotificationVo>();

		for (Notification entity : entities) {
			result.add(builder.buildNotificationVoFromEntity(entity));
		}
		return result;
	}

	@Override
	public List<NotificationVo> findAllFailedNotifications() {
		List<Notification> entities = notificationRepository
				.findByStatus(NotificationStatusType.FAILED.toString());
		List<NotificationVo> result = new ArrayList<NotificationVo>();

		for (Notification entity : entities) {
			result.add(builder.buildNotificationVoFromEntity(entity));
		}
		return result;
	}

	@Override
	public List<NotificationVo> findAllRetriableNotifications() {
		List<Notification> entities = notificationRepository
				.findByStatus(NotificationStatusType.RETRY_NEEDED.toString());
		List<NotificationVo> result = new ArrayList<NotificationVo>();

		for (Notification entity : entities) {
			result.add(builder.buildNotificationVoFromEntity(entity));
		}
		return result;
	}

}
