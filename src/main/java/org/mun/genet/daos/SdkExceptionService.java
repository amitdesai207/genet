package org.mun.genet.daos;

import java.util.List;

import org.mun.genet.vo.SdkExceptionVo;

public interface SdkExceptionService {

	public void createSdkException(SdkExceptionVo sdkExceptionVo);

	public List<SdkExceptionVo> findAllSdkExceptionsForJob(String jobId);

}
