package org.mun.genet.daos;

import java.util.ArrayList;
import java.util.List;

import org.mun.genet.daos.builders.EntityVoBuilder;
import org.mun.genet.daos.entities.Job;
import org.mun.genet.daos.entities.JobResult;
import org.mun.genet.daos.repositories.JobRepository;
import org.mun.genet.daos.repositories.JobResultRepository;
import org.mun.genet.vo.JobResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class JobResultServiceImpl implements JobResultService {

	@Autowired
	private JobResultRepository jobResultRepository;
	@Autowired
	private JobRepository jobRepository;
	@Autowired
	private EntityVoBuilder builder;

	@Override
	@Transactional
	public void createJobResult(JobResultVo jobResultVo) {

		Job job = jobRepository.findOne(jobResultVo.getJobId());
		JobResult jobResult = builder.buildJobResultEntityFromVo(jobResultVo);
		jobResult.setJob(job);

		jobResultRepository.save(jobResult);
	}

	@Override
	public List<JobResultVo> findAllResultsForJob(String jobId) {
		List<JobResultVo> jobResultVos = new ArrayList<JobResultVo>();
		Job job = jobRepository.findByDisplayId(jobId);
		if (job != null) {
			List<JobResult> jobResults = jobResultRepository.findByJob(job);
			for (JobResult jobResult : jobResults)
				jobResultVos.add(builder.buildJobResultVoFromEntity(jobResult));
		}
		return jobResultVos;
	}

}
