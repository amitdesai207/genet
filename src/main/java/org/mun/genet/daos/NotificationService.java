package org.mun.genet.daos;

import java.util.List;

import org.mun.genet.vo.NotificationVo;

public interface NotificationService {

	public void createNotification(NotificationVo notificationVo);

	public void incrementRetryCountForNotification(Long notificationId, int allowedRetries);

	public void markNotificationAsFailed(Long notificationId);

	public void markNotificationAsSent(Long notificationId);

	public List<NotificationVo> findAllPendingNotifications();

	public List<NotificationVo> findAllFailedNotifications();

	public List<NotificationVo> findAllRetriableNotifications();
}
