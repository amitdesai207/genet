package org.mun.genet.daos;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.mun.genet.vo.FileStoreVo;
import org.springframework.web.multipart.MultipartFile;

public interface FileStoreService {

	public FileStoreVo storeFile(MultipartFile file, String username) throws IOException;

	public void setCompeletedDate(Long fileStoreId);

	public String getStoredFile(Long fileStoreId) throws Exception;

	public void pruneAllFileStoreObjects();

	public InputStream getFileStream(String fileStoreId) throws Exception;

	public void setDisplayId(Long fileStoreId);

	public void storeFileInFileRespository(MultipartFile file, String folder, String fileName) throws Exception;

	public void storeExistingFileInFileRespository(FileInputStream fileStream, String folder, String fileName)
			throws Exception;

}
