package org.mun.genet.daos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mun.genet.configurer.ImportJobStatusType;
import org.mun.genet.daos.builders.EntityVoBuilder;
import org.mun.genet.daos.entities.Job;
import org.mun.genet.daos.entities.Role;
import org.mun.genet.daos.entities.User;
import org.mun.genet.daos.repositories.JobRepository;
import org.mun.genet.daos.repositories.UserRepository;
import org.mun.genet.jobreceiver.UploadException;
import org.mun.genet.util.Utility;
import org.mun.genet.vo.JobVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class JobServiceImpl implements JobService {

	@Autowired
	private JobRepository jobRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EntityVoBuilder builder;

	@Override
	public List<JobVo> findAllPendingJobs() {

		List<JobVo> jobVos = new ArrayList<JobVo>();
		List<Job> jobs = jobRepository.findByStatus(ImportJobStatusType.PENDING.toString());
		for (Job job : jobs) {
			JobVo jobVo = builder.buildJobVoFromEntity(job);
			jobVos.add(jobVo);
		}
		return jobVos;
	}

	@Override
	@Transactional
	public void updateFileStoreId(Long jobId, Long fileStoreId) {
		Job job = jobRepository.findOne(jobId);
		job.setDataPath(fileStoreId);
		job.setStatus(ImportJobStatusType.PENDING.toString());
	}

	@Override
	@Transactional
	public void markJobAsComplete(Long jobId, Date jobStartTime, Date jobEndTime, ImportJobStatusType status) {
		Job job = jobRepository.findOne(jobId);
		job.setStatus(status.toString());
		if (jobStartTime != null)
			job.setJobStartTime(jobStartTime);
		job.setJobEndTime(jobEndTime);
	}

	@Override
	@Transactional
	public void markJobAsActive(Long jobId, Date jobStartTime) {
		Job job = jobRepository.findOne(jobId);
		job.setStatus(ImportJobStatusType.RUNNING.toString());
		job.setJobStartTime(jobStartTime);
	}

	@Override
	@Transactional
	public JobVo addJob(JobVo jobVo) throws UploadException {
		Job job;
		User user = userRepository.findByUserEmail(jobVo.getUserEmail().toLowerCase());
		job = builder.buildJobEntityFromVo(jobVo);
		job.setUser(user);
		job = jobRepository.save(job);
		job.setDisplayId(Utility.generateRandomSequence(job.getId()));
		return builder.buildJobVoFromEntity(job);
	}

	@Override
	public int findActiveJobCount() {
		List<Job> runningJobs = jobRepository.findByStatus(ImportJobStatusType.RUNNING.toString());
		if (runningJobs != null)
			return runningJobs.size();
		else
			return 0;
	}

	@Override
	public List<JobVo> findAll() {
		Sort latestFirst = new Sort(Direction.DESC, "id");
		List<Job> allJobs = jobRepository.findAll(latestFirst);
		List<JobVo> vos = new ArrayList<JobVo>();
		for (Job job : allJobs) {
			JobVo jobVo = builder.buildJobVoFromEntity(job);
			vos.add(jobVo);
		}
		return vos;
	}

	@Override
	public JobVo findByDisplayId(String id, String role) {
		Job job = null;
		if (role != null && (role.equals(Role.UserRoles.Web_Tool_Admin.name()))) {
			job = jobRepository.findByDisplayId(id);
		} else {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String userName = authentication.getName();
			User user = userRepository.findByUserName(userName);
			job = jobRepository.findByDisplayIdAndUser(id, user);
		}
		if (job != null) {
			JobVo jobVo = builder.buildJobVoFromEntity(job);
			return jobVo;
		} else
			return null;
	}

	@Override
	public List<JobVo> findAllJobsForUser(String userEmail, String role) {
		List<JobVo> jobVos = new ArrayList<JobVo>();
		User user = userRepository.findByUserEmail(userEmail.toLowerCase());
		if (user != null) {
			List<Job> jobs = null;
			if (role != null && (role.equals(Role.UserRoles.Web_Tool_Admin.name()))) {
				jobs = jobRepository.findByUser(user);
			} else {
				jobs = jobRepository.findByUser(user);
			}
			for (Job job : jobs) {
				JobVo jobVo = builder.buildJobVoFromEntity(job);
				jobVos.add(jobVo);
			}
		}
		return jobVos;
	}

	@Override
	public List<JobVo> findAllRunningJobs() {
		List<JobVo> jobVos = new ArrayList<JobVo>();
		List<Job> jobs = jobRepository.findByStatus(ImportJobStatusType.RUNNING.toString());
		for (Job job : jobs) {
			jobVos.add(builder.buildJobVoFromEntity(job));
		}
		return jobVos;
	}

	@Override
	@Transactional
	public void markJobAsInturrupted(Long jobId) {
		Job job = jobRepository.findOne(jobId);
		job.setStatus(ImportJobStatusType.INTERRUPTED.toString());
	}

	@Override
	@Transactional
	public void markJobAsPending(String jobId) {
		Job job = jobRepository.findByDisplayId(jobId);
		job.setStatus(ImportJobStatusType.PENDING.toString());
	}

	@Override
	@Transactional
	public void markJobAsFailed(String jobId) {
		Job job = jobRepository.findByDisplayId(jobId);
		job.setStatus(ImportJobStatusType.FAILED.toString());
	}

	@Override
	@Transactional
	public void stopJobByUser(String jobId) {
		Job job = jobRepository.findByDisplayId(jobId);
		job.setStatus(ImportJobStatusType.INTERRUPTED.toString());
	}

	@Override
	@Transactional
	public void approveOverrideJob(String jobId) {
		Job job = jobRepository.findByDisplayId(jobId);
		job.setApprovedOverride(true);
	}

	@Override
	public boolean isJobActive(Long jobId) {
		Job job = jobRepository.findOne(jobId);
		if (ImportJobStatusType.INTERRUPTED.isEqual(job.getStatus()))
			return false;
		else if (ImportJobStatusType.RUNNING.isEqual(job.getStatus()))
			return true;
		return false;
	}
}
