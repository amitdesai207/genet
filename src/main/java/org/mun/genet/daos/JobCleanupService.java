package org.mun.genet.daos;

public interface JobCleanupService {

	public void cleanupDatabase(String organismName);
}
