package org.mun.genet.daos;

import java.util.List;
import java.util.Random;

import org.mun.genet.configurer.Configuration;
import org.mun.genet.daos.builders.EntityVoBuilder;
import org.mun.genet.daos.entities.User;
import org.mun.genet.daos.repositories.UserRepository;
import org.mun.genet.encryptors.SymmetricAesEncryptor;
import org.mun.genet.util.Utility;
import org.mun.genet.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EntityVoBuilder builder;
	@Autowired
	SymmetricAesEncryptor aesEncryptor;
	@Autowired
	Configuration configuration;

	@Override
	@Transactional
	public void addUser(UserVo userVo) {
		User user = builder.buildUserEntityFromVo(userVo);
		userRepository.save(user);
		userVo.setId(user.getId().longValue());
	}

	@Override
	public boolean isUsernamePresent(UserVo userVo) {
		User user = userRepository.findByUserName(userVo.getUserName());
		if (user == null)
			return false;
		return true;
	}

	@Override
	public boolean isUserEmailPresent(UserVo userVo) {
		User user = userRepository.findByUserEmail(userVo.getUserEmail());
		if (user == null)
			return false;
		return true;
	}

	@Override
	public UserVo isUsernameAuthentic(UserVo userVo) {
		User user = userRepository.findByUserName(userVo.getUserName());
		if (user == null)
			return null;
		return builder.buildUserVoFromEntity(user);
	}
	
	@Override
	@Transactional
	public boolean activateUser(String key) {
		User user = userRepository.findByActivation(key);
		if (user == null)
			return false;
		user.setActive(true);
		return true;
	}

	@Override
	@Transactional
	public void resetPasswordForUserWithResetRequest() {
		List<User> users = userRepository.findAllByReset(true);
		for (User user : users)
			user.setPassword(aesEncryptor.encrypt(Utility.generateRandomSequence(new Random(1927).nextLong()),
					configuration.getConfigurationByName("DBEntryEncrptKey")));
	}

	@Override
	@Transactional
	public UserVo resetPassword(UserVo userVo) {
		User user = userRepository.findByUserName(userVo.getUserName());
		user.setReset(true);
		user.setPassword(aesEncryptor.encrypt(Utility.generateRandomSequence(new Random(1927).nextLong()),
				configuration.getConfigurationByName("DBEntryEncrptKey")));
		return builder.buildUserVoFromEntity(user);
	}

	@Override
	@Transactional
	public void resetUserPassword(UserVo userVo) {
		User user = userRepository.findByUserName(userVo.getUserName());
		user.setPassword(aesEncryptor.encrypt(userVo.getPassword(),
				configuration.getConfigurationByName("DBEntryEncrptKey")));
		user.setReset(false);
	}

}
