package org.mun.genet.daos;

import java.util.ArrayList;
import java.util.List;

import org.mun.genet.daos.builders.EntityVoBuilder;
import org.mun.genet.daos.entities.Job;
import org.mun.genet.daos.entities.SdkException;
import org.mun.genet.daos.repositories.JobRepository;
import org.mun.genet.daos.repositories.SdkExceptionRepository;
import org.mun.genet.vo.SdkExceptionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class SdkExceptionServiceImpl implements SdkExceptionService {

	@Autowired
	private SdkExceptionRepository sdkExceptionRepository;
	@Autowired
	private JobRepository jobRepository;
	@Autowired
	private EntityVoBuilder builder;

	@Override
	@Transactional
	public void createSdkException(SdkExceptionVo sdkExceptionVo) {
		if (sdkExceptionVo == null) {
			throw new IllegalArgumentException("createSdkException: Null sdkExceptionVo");
		}
		SdkException sdkException = builder.buildSdkExceptionEntityFromVo(sdkExceptionVo);
		Job job = jobRepository.findOne(sdkExceptionVo.getJobId());
		sdkException.setJob(job);
		sdkExceptionRepository.save(sdkException);

	}

	@Override
	public List<SdkExceptionVo> findAllSdkExceptionsForJob(String jobId) {
		Job job = jobRepository.findByDisplayId(jobId);
		if (job != null) {
			List<SdkException> exception = sdkExceptionRepository.findByJob(job);
			List<SdkExceptionVo> vos = new ArrayList<SdkExceptionVo>();
			for (SdkException e : exception) {
				vos.add(builder.buildSdkExceptionVoFromEntity(e));
			}
			return vos;
		} else
			return null;
	}

}
