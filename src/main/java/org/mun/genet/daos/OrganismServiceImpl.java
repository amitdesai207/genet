package org.mun.genet.daos;

import java.util.ArrayList;
import java.util.List;

import org.mun.genet.daos.builders.EntityVoBuilder;
import org.mun.genet.daos.entities.Organism;
import org.mun.genet.daos.repositories.FileStoreRepository;
import org.mun.genet.daos.repositories.OrganismRepository;
import org.mun.genet.vo.OrganismVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class OrganismServiceImpl implements OrganismService {

	@Autowired
	private OrganismRepository organismRepository;
	@Autowired
	private EntityVoBuilder builder;
	@Autowired
	private JobCleanupService jobCleanupService;
	@Autowired
	private FileStoreRepository fileStoreRepository;

	@Override
	public List<String> getOrganismList(String role) {
		List<String> organisms = new ArrayList<String>();
		List<Organism> organismNames = null;
		if (role != null)
			organismNames = organismRepository.findAll();
		else
			organismNames = organismRepository.findByApproved(true);
		for (Organism organism : organismNames) {
			organisms.add(organism.getName());
		}
		return organisms;
	}

	@Override
	public List<ArrayList<String>> getOrganismToApprove() {
		List<ArrayList<String>> organisms = new ArrayList<ArrayList<String>>();
		List<Organism> organismNames = organismRepository.findByApproved(false);
		ArrayList<String> row = new ArrayList<>();
		for (Organism organism : organismNames) {
			row.add(organism.getName());
			row.add(organism.getId().toString());
			organisms.add(row);
		}
		return organisms;
	}

	@Override
	@Transactional
	public String approveOrganism(String id) {
		Organism organism = organismRepository.findOne(Long.parseLong(id));
		organism.setApproved(true);
		return organism.getName();
	}

	@Override
	@Transactional
	public void markDownloadReady(String id) {
		Organism organism = organismRepository.findOne(Long.parseLong(id));
		organism.setDownloadReady(true);
	}

	@Override
	public void rejectOrganism(String id) {
		Organism organism = organismRepository.findOne(Long.parseLong(id));
		jobCleanupService.cleanupDatabase(organism.getName());
	}

	@Override
	public List<OrganismVo> getOrganisms() {
		List<OrganismVo> organismVos = new ArrayList<OrganismVo>();
		List<Organism> organisms = organismRepository.findByApproved(true);
		for (Organism organism : organisms) {
			organismVos.add(builder.buildOrganism(organism));
		}
		return organismVos;
	}

	@Override
	public Long getOrganismId(String organismName) {
		Organism organism = organismRepository.findByName(organismName);
		return organism.getId();
	}

	@Override
	public String getFileStorePath(String organismName) {
		Organism organism = organismRepository.findByName(organismName);
		Long filePathId = organism.getFilePathId();
		return fileStoreRepository.findOne(filePathId).getFilePath();
	}

}
