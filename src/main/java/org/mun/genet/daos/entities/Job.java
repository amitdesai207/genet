package org.mun.genet.daos.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * The persistent class for the job database table.
 * 
 */
@Entity
public class Job implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "display_id")
	private String displayId;

	@Column(name = "taxonomy_id")
	private String taxonomyId;

	@Column(name = "pubmed_id")
	private String pubmedId;

	private int correlation;

	@Column(name = "network_type")
	private int networkType;

	private int threshold;

	@Column(name = "kegg_id")
	private String keggId;

	private String organism;

	@Column(name = "file_id")
	private Long filePathId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created")
	private Date dateCreated;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_updated")
	private Date lastUpdated;

	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "job_start_time")
	private Date jobStartTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "job_end_time")
	private Date jobEndTime;

	@Column(name = "client_ip")
	private String clientIP;

	@Column(name = "operating_system")
	private String operatingSystem;

	private String browser;

	@Column(name = "file_size")
	private Long fileSize;

	@Version
	private Integer version;

	@Column(name = "approved_override")
	private Boolean approvedOverride;

	// bi-directional many-to-one association to User
	@ManyToOne
	private User user;

	// bi-directional many-to-one association to JobResult
	@OneToMany(mappedBy = "job")
	private Set<JobResult> jobResults;

	// bi-directional many-to-one association to SdkException
	@OneToMany(mappedBy = "job")
	private Set<SdkException> sdkExceptions;

	public Job() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDataPath() {
		return this.filePathId;
	}

	public void setDataPath(Long dataPath) {
		this.filePathId = dataPath;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getVersion() {
		return this.version;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<JobResult> getJobResults() {
		return this.jobResults;
	}

	public void setJobResults(Set<JobResult> jobResults) {
		this.jobResults = jobResults;
	}

	public Set<SdkException> getSdkExceptions() {
		return this.sdkExceptions;
	}

	public void setSdkExceptions(Set<SdkException> sdkExceptions) {
		this.sdkExceptions = sdkExceptions;
	}

	public Date getJobStartTime() {
		return jobStartTime;
	}

	public void setJobStartTime(Date jobStartTime) {
		this.jobStartTime = jobStartTime;
	}

	public Date getJobEndTime() {
		return jobEndTime;
	}

	public void setJobEndTime(Date jobEndTime) {
		this.jobEndTime = jobEndTime;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getDisplayId() {
		return displayId;
	}

	public void setDisplayId(String displayId) {
		this.displayId = displayId;
	}

	public Boolean getApprovedOverride() {
		return approvedOverride;
	}

	public void setApprovedOverride(Boolean approvedOverride) {
		this.approvedOverride = approvedOverride;
	}

	public String getTaxonomyId() {
		return taxonomyId;
	}

	public void setTaxonomyId(String taxonomyId) {
		this.taxonomyId = taxonomyId;
	}

	public String getPubmedId() {
		return pubmedId;
	}

	public void setPubmedId(String pubmedId) {
		this.pubmedId = pubmedId;
	}

	public int getThreshold() {
		return threshold;
	}

	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}

	public String getKeggId() {
		return keggId;
	}

	public void setKeggId(String keggId) {
		this.keggId = keggId;
	}

	public Long getFilePathId() {
		return filePathId;
	}

	public void setFilePathId(Long filePathId) {
		this.filePathId = filePathId;
	}

	public int getCorrelation() {
		return correlation;
	}

	public void setCorrelation(int correlation) {
		this.correlation = correlation;
	}

	public int getNetworkType() {
		return networkType;
	}

	public void setNetworkType(int networkType) {
		this.networkType = networkType;
	}

	public String getOrganism() {
		return organism;
	}

	public void setOrganism(String organism) {
		this.organism = organism;
	}

}