package org.mun.genet.daos.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
public class Organism implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;
	
	private String pubmedIds;
	
	private Boolean approved;
	
	@Column(name = "download_ready")
	private Boolean downloadReady;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created")
	private Date dateCreated;

	@Column(name = "file_id")
	private Long filePathId;
	
	@Version
	private Integer version;

	public Organism() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getPubmedIds() {
		return pubmedIds;
	}

	public void setPubmedIds(String pubmedIds) {
		this.pubmedIds = pubmedIds;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}

	public Boolean getDownloadReady() {
		return downloadReady;
	}

	public void setDownloadReady(Boolean downloadReady) {
		this.downloadReady = downloadReady;
	}

	public Long getFilePathId() {
		return filePathId;
	}

	public void setFilePathId(Long filePathId) {
		this.filePathId = filePathId;
	}

}