package org.mun.genet.daos.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * The persistent class for the job_result database table.
 * 
 */
@Entity
@Table(name = "job_result")
public class JobResult implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String entity;

	@Column(name = "records_failed")
	private int recordsFailed;

	@Column(name = "records_received")
	private int recordsReceived;

	@Column(name = "records_successful")
	private int recordsSuccessful;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created")
	private Date dateCreated;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_updated")
	private Date lastUpdated;

	@Version
	private Integer version;

	// bi-directional many-to-one association to Job
	@ManyToOne
	private Job job;

	public JobResult() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntity() {
		return this.entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public int getRecordsFailed() {
		return this.recordsFailed;
	}

	public void setRecordsFailed(int recordsFailed) {
		this.recordsFailed = recordsFailed;
	}

	public int getRecordsSuccessful() {
		return this.recordsSuccessful;
	}

	public void setRecordsSuccessful(int recordsSuccessful) {
		this.recordsSuccessful = recordsSuccessful;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Integer getVersion() {
		return this.version;
	}

	public Job getJob() {
		return this.job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public int getRecordsReceived() {
		return recordsReceived;
	}

	public void setRecordsReceived(int recordsReceived) {
		this.recordsReceived = recordsReceived;
	}

}