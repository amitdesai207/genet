package org.mun.genet.daos.entities;

public class Role {

	public static enum UserRoles {
		Web_Tool_Admin("Web_Tool_Admin"), 
		OTHER("OTHER");

		private String userRole;

		private UserRoles(String r) {
			userRole = r;
		}

		public String getUserRole() {
			return userRole;
		}

	}

}
