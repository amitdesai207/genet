package org.mun.genet.encryptors;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.io.FileUtils;
import org.mun.genet.jobreceiver.UploadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TarCompressor {

	private static final Logger logger = LoggerFactory.getLogger(TarCompressor.class);

	public String unCompress(String compressedFile, String unCompressedFile) throws UploadException {
		// initialize local variables
		int bytesRead = 0;
		byte[] buffer = null;
		OutputStream outputFileStream = null;
		FileInputStream fileInputStream = null;
		TarArchiveInputStream compressedFileStream = null;
		try {
			// Open the input compressed file
			fileInputStream = new FileInputStream(compressedFile);
			compressedFileStream = new TarArchiveInputStream(fileInputStream);
			String unzippedFolder = null;
			ArchiveEntry entry;
			while ((entry = compressedFileStream.getNextEntry()) != null) {
				if (!entry.isDirectory()) {
					// Create uncompressed file stream
					File unzippedFile = new File(unCompressedFile + File.separator + entry.getName());
					unzippedFolder = unzippedFile.getParent();
					FileUtils.forceMkdir(unzippedFile.getParentFile());
					outputFileStream = new FileOutputStream(unzippedFile);
					// Transfer bytes from GZIP input stream to output stream
					buffer = new byte[2048];
					while ((bytesRead = compressedFileStream.read(buffer)) > 0) {
						outputFileStream.write(buffer, 0, bytesRead);
					}
					outputFileStream.flush();
					outputFileStream.close();
				}
			}

			// close all streams
			compressedFileStream.close();
			return unzippedFolder;
		} catch (Exception ex) {
			throw new UploadException(ex);
		} finally {
			buffer = null;
			safeClose(compressedFileStream);
			safeClose(fileInputStream);
			safeClose(outputFileStream);
		}
	}

	private void safeClose(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (IOException e) {
				logger.warn("Unable to close " + closeable);
			}
		}
	}
}
