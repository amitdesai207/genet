package org.mun.genet.encryptors;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SymmetricAesEncryptor implements Encryptor {

	private static final Logger logger = LoggerFactory.getLogger(SymmetricAesEncryptor.class);

	@Override
	public String encrypt(String input, String aeskey) throws IllegalArgumentException {
		if (input == null || input.isEmpty()) {
			throw new IllegalArgumentException();
		}
		try {
			Key key = getKeyForEncryption(aeskey);
			Cipher desCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			desCipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] cleartext = input.getBytes();
			byte[] ciphertext = desCipher.doFinal(cleartext);
			cleartext = null;
			return getEncodedStringFromBytes(ciphertext);
		} catch (Exception e) {
			logger.warn("encrypt: Exception caught", e);
		}
		return null;
	}

	@Override
	public String decrypt(String input, String aeskey) throws IllegalArgumentException {
		if (input == null || input.isEmpty()) {
			throw new IllegalArgumentException();
		}
		try {
			Key key = getKeyForEncryption(aeskey);
			Cipher desCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			byte[] ciphertext = getBytesFromEncodedString(input);
			desCipher.init(Cipher.DECRYPT_MODE, key);
			byte[] cleartext = desCipher.doFinal(ciphertext);
			ciphertext = null;
			return new String(cleartext);
		} catch (Exception e) {
			logger.warn("decrypt: Exception caught", e);
		}
		return null;
	}

	private Key getKeyForEncryption(String aeskey) {
		try {
			byte[] bytes = getBytesFromEncodedString(aeskey);
			SecretKeySpec key = new SecretKeySpec(bytes, "AES");
			bytes = null;
			return key;
		} catch (Exception e) {
			logger.warn("getKeyForEncryption: Exception caught", e);
		}
		return null;
	}

	private String getEncodedStringFromBytes(byte[] bytes) {
		return DatatypeConverter.printHexBinary(bytes);
	}

	private byte[] getBytesFromEncodedString(String str) {
		return DatatypeConverter.parseHexBinary(str);
	}

}
