package org.mun.genet.encryptors;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.io.IOUtils;
import org.mun.genet.jobreceiver.UploadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GZipCompressor {

	private static final Logger logger = LoggerFactory.getLogger(GZipCompressor.class);

	public void compress(String inputFile, String compressedFile) throws UploadException {
		// initialize local variables
		int bytesRead = 0;
		byte[] buffer = null;
		FileInputStream inputFileStream = null;
		FileOutputStream fileOutStream = null;
		GZIPOutputStream compressedFileStream = null;
		try {
			// Open the input file
			inputFileStream = new FileInputStream(new File(inputFile));

			// Create the GZIP output stream for output file
			fileOutStream = new FileOutputStream(compressedFile);
			compressedFileStream = new GZIPOutputStream(fileOutStream);

			// Transfer bytes from the input file to the GZIP output stream
			buffer = new byte[2048];
			while ((bytesRead = inputFileStream.read(buffer)) > 0) {
				compressedFileStream.write(buffer, 0, bytesRead);
			}
			compressedFileStream.flush();

			// close all streams
			inputFileStream.close();
			compressedFileStream.close();
			fileOutStream.close();

		} catch (Exception ex) {
			logger.error("Download logs failed: " + ex);
			throw new UploadException(ex);
		} finally {
			buffer = null;
			safeClose(inputFileStream);
			safeClose(fileOutStream);
			safeClose(compressedFileStream);
		}
	}

	public void unCompress(String compressedFile, String unCompressedFile) throws UploadException {
		// initialize local variables
		int bytesRead = 0;
		byte[] buffer = null;
		OutputStream outputFileStream = null;
		FileInputStream fileInputStream = null;
		GZIPInputStream compressedFileStream = null;
		try {
			// Open the input compressed file
			fileInputStream = new FileInputStream(compressedFile);
			compressedFileStream = new GZIPInputStream(fileInputStream);

			// Create uncompressed file stream
			outputFileStream = new FileOutputStream(unCompressedFile);

			// Transfer bytes from GZIP input stream to output stream
			buffer = new byte[2048];
			while ((bytesRead = compressedFileStream.read(buffer)) > 0) {
				outputFileStream.write(buffer, 0, bytesRead);
			}
			outputFileStream.flush();

			// close all streams
			outputFileStream.close();
			compressedFileStream.close();
		} catch (Exception ex) {
			throw new UploadException(ex);
		} finally {
			buffer = null;
			safeClose(compressedFileStream);
			safeClose(fileInputStream);
			safeClose(outputFileStream);
		}
	}

	public void compressMultipleFiles(List<String> inputFiles, String compressedFile) throws Exception {
		FileOutputStream fileOutput = null;
		BufferedOutputStream bufferedOutput = null;
		GzipCompressorOutputStream gzipOutput = null;
		TarArchiveOutputStream tarOutput = null;

		try {
			fileOutput = new FileOutputStream(new File(compressedFile));
			bufferedOutput = new BufferedOutputStream(fileOutput);
			gzipOutput = new GzipCompressorOutputStream(bufferedOutput);
			tarOutput = new TarArchiveOutputStream(gzipOutput);
			tarOutput.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);
			
			for (String inputFile : inputFiles) {
				File file = new File(inputFile);
				String entryName = file.getName();
				TarArchiveEntry tarEntry = new TarArchiveEntry(file, entryName);
				tarOutput.putArchiveEntry(tarEntry);
				IOUtils.copy(new FileInputStream(file), tarOutput);
				tarOutput.closeArchiveEntry();
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			tarOutput.finish();
			safeClose(tarOutput);
			safeClose(gzipOutput);
			safeClose(bufferedOutput);
			safeClose(fileOutput);
		}
	}

	private void safeClose(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (IOException e) {
				logger.warn("Unable to close " + closeable);
			}
		}
	}
}
