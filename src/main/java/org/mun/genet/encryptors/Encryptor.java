package org.mun.genet.encryptors;

public interface Encryptor {

	String encrypt(String input, String key) throws IllegalArgumentException;

	String decrypt(String input, String key) throws IllegalArgumentException;
}
