package org.mun.genet.encryptors;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.mun.genet.jobreceiver.UploadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ZipCompressor {

	private static final Logger logger = LoggerFactory.getLogger(ZipCompressor.class);

	public void unCompress(String compressedFile, String unCompressedFile) throws UploadException {
		byte[] buffer = new byte[1024];
		try {
			File folder = new File(unCompressedFile);
			if (!folder.exists()) {
				folder.mkdir();
			}
			ZipInputStream zis = new ZipInputStream(new FileInputStream(compressedFile));
			ZipEntry ze = zis.getNextEntry();
			while (ze != null) {
				String fileName = ze.getName();
				File newFile = new File(unCompressedFile + File.separator + fileName);
				new File(newFile.getParent()).mkdirs();
				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				safeClose(fos);
				ze = zis.getNextEntry();
			}
			zis.closeEntry();
			safeClose(zis);
		} catch (IOException ex) {
			throw new UploadException(ex);
		}
	}

	private void safeClose(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (IOException e) {
				logger.warn("Unable to close " + closeable);
			}
		}
	}
}
