package org.mun.genet.initialize;

import org.springframework.context.ApplicationContext;

public interface Initializable {
	public void initialize(ApplicationContext context);
}
