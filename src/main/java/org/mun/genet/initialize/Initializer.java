package org.mun.genet.initialize;

import org.mun.genet.configurer.ConfigCache;
import org.mun.genet.configurer.DBIntigrityChecker;
import org.mun.genet.configurer.EmailConfigurer;
import org.mun.genet.configurer.SchedulerConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
class Initializer implements ApplicationListener<ContextRefreshedEvent> {
	@Autowired
	private ConfigCache dbConfigCache;
	@Autowired
	private EmailConfigurer emailConfigurer;
	@Autowired
	private SchedulerConfigurer schedulerConfigurer;
	@Autowired
	private DBIntigrityChecker dbIntigrityChecker;

	public void onApplicationEvent(ContextRefreshedEvent event) {
		// Note: the order of the initializations is important, don't change it
		// without understanding consequences.
		init(dbConfigCache, event.getApplicationContext());
		init(emailConfigurer, event.getApplicationContext());
		init(dbIntigrityChecker, event.getApplicationContext());
		init(schedulerConfigurer, event.getApplicationContext());

	}

	private void init(Initializable initializable, ApplicationContext context) {
		initializable.initialize(context);
	}
}
