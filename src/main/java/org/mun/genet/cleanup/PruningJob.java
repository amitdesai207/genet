package org.mun.genet.cleanup;

import org.mun.genet.daos.FileStoreService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
public class PruningJob extends QuartzJobBean {

	private FileStoreService fileStoreService;

	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		fileStoreService.pruneAllFileStoreObjects();
	}

	public void setRemover(FileStoreService fileStoreService) {
		this.fileStoreService = fileStoreService;
	}

}