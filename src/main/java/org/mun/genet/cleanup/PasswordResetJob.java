package org.mun.genet.cleanup;

import org.mun.genet.daos.UserService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
public class PasswordResetJob extends QuartzJobBean {

	private UserService userService;

	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		userService.resetPasswordForUserWithResetRequest();
	}

	public void setResetter(UserService userService) {
		this.userService = userService;
	}

}